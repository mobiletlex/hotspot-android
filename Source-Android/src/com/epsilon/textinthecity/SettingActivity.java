package com.epsilon.textinthecity;

import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingActivity extends BaseActivity implements OnClickListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_setting), false);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SETTING);
		
		Tracker t = PoemApplication.getInstance().getTracker(
                TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
        	.setCategory(getString(R.string.google_analytics_event_category_tab_open))
        	.setAction(getString(R.string.google_analytics_event_action_open))
        	.setLabel(getString(R.string.google_analytics_event_open_tab_setting)).build());
		
		setupWidthCenter();
		
		setTypeFaceControls();
		
		initEventClick();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		
		int countEventUnread = Common.countEventsUnread(this);
		if(countEventUnread > 0){
			((TextView) findViewById(R.id.tvCountEvents)).setText(countEventUnread+"");
			((TextView) findViewById(R.id.tvCountEvents)).setVisibility(View.VISIBLE);
		}
		else {
			((TextView) findViewById(R.id.tvCountEvents)).setText("");
			((TextView) findViewById(R.id.tvCountEvents)).setVisibility(View.GONE);
		}
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f));
        ((LinearLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	/**
	 * Set typeFace for text
	 */
	private void setTypeFaceControls() {
		
		((TextView) findViewById(R.id.tvAbout)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvTerm)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvPrivacy)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvCredit)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvFont)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvEvents)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvTutorial)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvCompetition)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
	}
	
	/**
	 * Init event click controls
	 */
	private void initEventClick() {
		((TextView) findViewById(R.id.tvAbout)).setOnClickListener(this);
		((TextView) findViewById(R.id.tvTerm)).setOnClickListener(this);
		((TextView) findViewById(R.id.tvPrivacy)).setOnClickListener(this);
		((TextView) findViewById(R.id.tvCredit)).setOnClickListener(this);
		((TextView) findViewById(R.id.tvFont)).setOnClickListener(this);
		((RelativeLayout) findViewById(R.id.rlEvents)).setOnClickListener(this);
		((TextView) findViewById(R.id.tvTutorial)).setOnClickListener(this);
		((TextView) findViewById(R.id.tvCompetition)).setOnClickListener(this);
		
		if(Common.checkLogin(this)){

			((Button) findViewById(R.id.btnLogout)).setVisibility(View.VISIBLE);
			((Button) findViewById(R.id.btnLogout)).setOnClickListener(this);
		}
	}

	@Override
	public void onClick(View v) {
		Intent it;
		switch (v.getId()) {
		case R.id.tvAbout:
			it = new Intent(SettingActivity.this, AboutActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			break;
		case R.id.tvTerm:
			Common.openBrowser(this, Constant.URL_SETTING_TERM_CONDITION);		
			break;
		case R.id.tvPrivacy:
			Common.openBrowser(this, Constant.URL_SETTING_PRIVACY);
			break;
		case R.id.tvCredit:
			it = new Intent(SettingActivity.this, CreditsActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			break;
		case R.id.tvFont:
			it = new Intent(SettingActivity.this, SettingFontActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			break;
		case R.id.rlEvents:
			it = new Intent(SettingActivity.this, EventsActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			break;
		case R.id.tvTutorial:
			it = new Intent(SettingActivity.this, TutorialActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			it.putExtra(TutorialActivity.KEY_FROM_SETTING, true);
			startActivity(it);
			overridePendingTransition(0,0);
			break;
		case R.id.tvCompetition:
			Common.openBrowser(this, Constant.URL_SETTING_COMPETITION);
//			it = new Intent(SettingActivity.this, CompetitionActivity.class);
//			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//			startActivity(it);
//			overridePendingTransition(0,0);
			break;
		case R.id.btnLogout:
			
			new AlertDialog.Builder(SettingActivity.this)
//			.setTitle(getString(R.string.warning))
			.setMessage(getString(R.string.msg_confirm_logout))
			.setPositiveButton(getString(R.string.logout),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,int whichButton) {
						Common.clearLogin(SettingActivity.this);
						((Button) findViewById(R.id.btnLogout)).setVisibility(View.INVISIBLE);
					}
				})
			.setNegativeButton(getString(R.string.cancel), 
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				})
			.create()
			.show();
						
			break;

		default:
			break;
		}
	}

}

