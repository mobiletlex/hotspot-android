package com.epsilon.textinthecity;

import com.epsilon.textinthecity.common.Constant;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Html;
import android.view.Display;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CompetitionActivity extends BaseActivity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_competition);
		

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_competition), true);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SETTING);
			
		setTypeFaceControls();
		
		setupWidthCenter();
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	
	private void setTypeFaceControls() {
		
		((TextView) findViewById(R.id.tvCompetition)).setTypeface(PoemApplication.getInstance().typeFaceUfontsComFutura_book);
		
		((TextView) findViewById(R.id.tvCompetition)).setText(Html.fromHtml(getString(R.string.competition_text)));
	}
	
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}
	
}
