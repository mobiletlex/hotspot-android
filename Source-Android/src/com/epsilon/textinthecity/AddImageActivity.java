package com.epsilon.textinthecity;

import java.io.File;
import java.io.IOException;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;

public class AddImageActivity extends BaseActivity implements OnClickListener {
	
	public static final String KEY_PHOTO = "KEY_PHOTO";
	public static final String KEY_CAPTION = "KEY_CAPTION";
	
	private static final int CODE_PICK_CROP = 102;
	private static final int CODE_PICK_IMAGE = 101;
	
	private String mUriAvatar = "";
	
	private Button btnCancel;
	private Button btnDone;
	private TextView tvImageCaption;
	private EditText edtCaption;
	private Button btnAddChange;
	private Button btnRemove;
	private ImageView imgPhoto;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_image);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_add_image), true);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SUBMIT);
		
		initControls();
				
		setTypeFaceControls();
		
		setupWidthCenter();
		
		initData();
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	@Override
	protected void onDestroy() {
		
		super.onDestroy();
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		Log.e("CongVC", "======resultCode:" + resultCode + "====data==" + data);
		
		if(requestCode == CODE_PICK_IMAGE){
			if (resultCode == RESULT_OK && data != null) {

				Options opts = new Options();
				opts.inSampleSize = 3;

//				File tempFile = getTempFile(1);
//				String filePath = tempFile.getAbsolutePath();
//				Log.e("CongVC", "======filePath:" + filePath);
//				
//				mUriAvatar = filePath;
//
//				Bitmap bm = BitmapFactory.decodeFile(filePath, opts);
				
				Point point = new Point(0, 0);
		        Display display = getWindowManager().getDefaultDisplay();
		        display.getSize(point);
		        
//				Common.scaleAndSetImageBitmap(imgPhoto, bm, point.x, (int)(point.x*0.8f));
				
				
				Uri mImageCaptureUri = data.getData();
				Log.e("CongVC", "====mImageCaptureUri==" + mImageCaptureUri);
				
				if(null != mImageCaptureUri){
					performCrop(mImageCaptureUri);
					return;
				}
	            
			}
		}
		else if (requestCode == CODE_PICK_CROP) {
	        if (resultCode == RESULT_OK && data != null) {
	        	Point point = new Point(0, 0);
		        Display display = getWindowManager().getDefaultDisplay();
		        display.getSize(point);
		        
//		        
//	            // get the returned data
//	            Bundle extras = data.getExtras();
//	            // get the cropped bitmap
//	            Bitmap selectedBitmap = extras.getParcelable("data");
//
////	            imgView.setImageBitmap(selectedBitmap);
//	            Common.scaleAndSetImageBitmap(imgPhoto, selectedBitmap, point.x, (int)(point.x*0.8f));
		        
		        Options opts = new Options();
				opts.inSampleSize = 3;
		        
		        Uri mImageCaptureUri = data.getData();
				
	            String path = null;
	            if(null != mImageCaptureUri){
	            	path = getRealPathFromURI(mImageCaptureUri); //from Gallery
	            }
	 
	            if (path == null && null != mImageCaptureUri){
	            	path = mImageCaptureUri.getPath(); //from File Manager
	            }
	             
	            if(path == null){
					File tempFile = getTempFile(1);
					String filePath = tempFile.getAbsolutePath();
					
					path = filePath;
	            }
	 
	            if (path != null){
	            	mUriAvatar = path;
	                Bitmap bitmap  = BitmapFactory.decodeFile(path, opts);
	                Common.scaleAndSetImageBitmap(imgPhoto, bitmap, point.x, (int)(point.x*0.8f));
	            }
	        }
	    }
	}
	
	private void performCrop(Uri picUri) {
	    try {
	    	
	    	Uri uri = getTempUri(1);

	        Intent cropIntent = new Intent("com.android.camera.action.CROP");
	        // indicate image type and Uri
	        cropIntent.setDataAndType(picUri, "image/*");
	        // set crop properties
	        cropIntent.putExtra("crop", "true");
	        // indicate aspect of desired crop
	        cropIntent.putExtra("aspectX", Constant.CONST_CROP_IMAGE_SIZE);
	        cropIntent.putExtra("aspectY", Constant.CONST_CROP_IMAGE_SIZE);
	        cropIntent.putExtra("outputX", Constant.CONST_CROP_IMAGE_SIZE);
	        cropIntent.putExtra("outputY", Constant.CONST_CROP_IMAGE_SIZE);
	        
	        cropIntent.putExtra("scale", true);
	        cropIntent.putExtra("scaleUpIfNeeded", true);
	        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
	        cropIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
	        
	        // retrieve data on return
//	        cropIntent.putExtra("return-data", true);
	        // start the activity - we handle returning in onActivityResult
	        startActivityForResult(cropIntent, CODE_PICK_CROP);
	    }
	    // respond to users whose devices do not support the crop action
	    catch (ActivityNotFoundException anfe) {
	        // display an error message
	        String errorMessage = "Whoops - your device doesn't support the crop action!";
	        Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
	        toast.show();
	        
	        Options opts = new Options();
			opts.inSampleSize = 3;

			Point point = new Point(0, 0);
	        Display display = getWindowManager().getDefaultDisplay();
	        display.getSize(point);
	        
	        try {
	        	String path = null;
	            if(null != picUri){
	            	path = getRealPathFromURI(picUri); //from Gallery
	            }
	 
	            if (path == null && null != picUri){
	            	path = picUri.getPath(); //from File Manager
	            }
	             
	            if(path == null){
					File tempFile = getTempFile(1);
					String filePath = tempFile.getAbsolutePath();
					
					path = filePath;
	            }
	 
	            if (path != null){
	            	mUriAvatar = path;
	                Bitmap bitmap  = BitmapFactory.decodeFile(path, opts);
	                Common.scaleAndSetImageBitmap(imgPhoto, bitmap, point.x, (int)(point.x*0.8f));
	            }
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
	    
	   
	}
	
	@SuppressWarnings("deprecation")
	public String getRealPathFromURI(Uri contentUri) {
        try {
        	String [] proj      = {MediaStore.Images.Media.DATA};
            Cursor cursor       = managedQuery( contentUri, proj, null, null,null);
     
            if (cursor == null) return null;
     
            int column_index    = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
     
            cursor.moveToFirst();
     
            return cursor.getString(column_index);
		} catch (Exception e) {
			return null;
		}
    }
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		btnCancel = (Button) findViewById(R.id.btnScreenBack);
		btnDone = (Button) findViewById(R.id.btnEdit);
		btnAddChange = (Button) findViewById(R.id.btnAddChange);
		btnRemove = (Button) findViewById(R.id.btnRemove);
		
		tvImageCaption = (TextView) findViewById(R.id.tvImageCaption);
		edtCaption = (EditText) findViewById(R.id.edtCaption);
		imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
		
		btnCancel.setText(getString(R.string.cancel));
		btnDone.setText(getString(R.string.my_entries_done));
		btnDone.setTextColor(getResources().getColor(R.color.black));
		
		btnCancel.setBackgroundColor(Color.TRANSPARENT);
		btnDone.setBackgroundColor(Color.TRANSPARENT);
		btnCancel.setPadding(getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_left_right), 
				getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_top_bottom), 
				getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_left_right), 
				getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_top_bottom));
		btnDone.setPadding(getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_left_right), 
				getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_top_bottom), 
				getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_left_right), 
				getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_top_bottom));
		
		btnCancel.setOnClickListener(this);
		btnDone.setOnClickListener(this);
				
		btnCancel.setVisibility(View.VISIBLE);
		btnDone.setVisibility(View.VISIBLE);
		
		btnDone.setPadding(0, 0, 0, 0);
		btnCancel.setPadding(0, 0, 0, 0);
		
		btnAddChange.setOnClickListener(this);
		btnRemove.setOnClickListener(this);
		
		try {
			InputMethodManager imm = (InputMethodManager)getSystemService(
				      Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtCaption.getWindowToken(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void initData() {
		try {
			mUriAvatar = getIntent().getStringExtra(KEY_PHOTO);
			String caption = getIntent().getStringExtra(KEY_CAPTION);
			
			if(null != caption && !caption.isEmpty()){
				edtCaption.setText(caption);
			}
			
			Point point = new Point(0, 0);
	        Display display = getWindowManager().getDefaultDisplay();
	        display.getSize(point);
			if(null != mUriAvatar && !mUriAvatar.isEmpty()){
				if(mUriAvatar.startsWith("http") || mUriAvatar.startsWith("https")){
					
					Common.loadImageView(imgPhoto, mUriAvatar, point.x, (int)(point.x*0.8f), this);
				}
				else {
					Options opts = new Options();
					opts.inSampleSize = 3;
					Bitmap bm = BitmapFactory.decodeFile(mUriAvatar, opts);
					Common.scaleAndSetImageBitmap(imgPhoto, bm, point.x, (int)(point.x*0.8f));
//					imgPhoto.setImageBitmap(bm);
				}
			}
			else {
				imgPhoto.setBackgroundResource(R.drawable.no_image);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Set type face for controls
	 */
	private void setTypeFaceControls() {
		btnCancel.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		btnDone.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		btnAddChange.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		btnRemove.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		edtCaption.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		tvImageCaption.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
        
        LayoutParams lp1 = new LayoutParams(point.x, (int)(point.x*0.8f));
        lp1.addRule(RelativeLayout.BELOW, R.id.top_layout);
		imgPhoto.setLayoutParams(lp1);
         
	}
	
	
	@Override
	public void onClick(View v) {
		Intent it;
		switch (v.getId()) {
		case R.id.btnScreenBack:
			onBackPressed();
			break;
		case R.id.btnEdit:
			it = new Intent();
			it.putExtra(KEY_PHOTO, mUriAvatar);
			it.putExtra(KEY_CAPTION, edtCaption.getText().toString().trim());
			setResult(RESULT_OK, it);
			finish();
			break;
		case R.id.btnAddChange:
			openGallery(1);
			break;
		case R.id.btnRemove:
			mUriAvatar = "";
			imgPhoto.setBackgroundResource(R.drawable.no_image);
			break;
		
		default:
			break;
		}
	}
	
	/**
	 * Open gallery choose image
	 * 
	 * @param image
	 * @param type
	 */
	private void openGallery(int type) {
		
		Uri uri = getTempUri(type);
		
		if(null != uri){
		
//			mCurrentImageView = image;
			Intent intent = new Intent();
			intent.setType("image/*");
//			intent.putExtra("crop", "true");
//			intent.putExtra("aspectX", Constant.CONST_CROP_IMAGE_SIZE);
//			intent.putExtra("aspectY", Constant.CONST_CROP_IMAGE_SIZE);
//			intent.putExtra("outputX", Constant.CONST_CROP_IMAGE_SIZE);
//			intent.putExtra("outputY", Constant.CONST_CROP_IMAGE_SIZE);
//			intent.putExtra("scale", true);
//			intent.putExtra("scaleUpIfNeeded", true);
//	
//			intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//			intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(intent, "Select Picture"),
					CODE_PICK_IMAGE);
		}

	}
	
	/**
	 * Get uri temp
	 * 
	 * @param type
	 * @return
	 */
	private Uri getTempUri(int type) {
		File file = getTempFile(type);
		if(null == file){
			return null;
		}
		return Uri.fromFile(file);
	}

	/**
	 * Get file image temp
	 * 
	 * @param type
	 * @return
	 */
	private File getTempFile(int type) {
		if (isSDCARDMounted()) {

			File f = new File(Environment.getExternalStorageDirectory(),
					"temp_" + type + ".jpg");
			try {
				f.createNewFile();
			} catch (IOException e) {

			}
			return f;
		} else {
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_no_sdcard)).show();;
			return null;
		}
	}

	/**
	 * Check SDCard is mounted
	 * 
	 * @return
	 */
	private boolean isSDCARDMounted() {
		String status = Environment.getExternalStorageState();
		if (status.equals(Environment.MEDIA_MOUNTED))
			return true;
		return false;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		try {
			setResult(RESULT_CANCELED);
			finish();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
