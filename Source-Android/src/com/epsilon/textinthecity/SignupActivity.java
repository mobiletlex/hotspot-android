package com.epsilon.textinthecity;

import com.android.volley.VolleyError;
import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.api.RegisterUserApi;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.inter_face.RegisterUserLisener;
import com.epsilon.textinthecity.object.User;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;

import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SignupActivity extends BaseActivity implements OnClickListener {
	
	private EditText edtFirstName;
	private EditText edtLastName;
	private EditText edtPenName;
	private EditText edtEmail;
	private EditText edtPasword;
	private EditText edtPaswordConfirm;
	private EditText edtContactNumber;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_signup), true);
		
		addTabMenuLayout(R.id.bottom_layout, 
				getIntent().getIntExtra(LoginActivity.TAB_KEY, Constant.TAB_MENU_INDEX_SUBMIT));
		
		initControls();
		
		setTypeFaceControls();
		
		setupWidthCenter();
		
		inputFilter();
		
		PoemHelper.getInstance().register(this);
	}
	
	@Override
	protected void onDestroy() {
		PoemHelper.getInstance().unregister(this);
		super.onDestroy();
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		edtFirstName = (EditText) findViewById(R.id.edtFirstName);
		edtLastName = (EditText) findViewById(R.id.edtLastName);
		edtPenName = (EditText) findViewById(R.id.edtPenName);
		edtEmail = (EditText) findViewById(R.id.edtEmail);
		edtPasword = (EditText) findViewById(R.id.edtPass);
		edtPaswordConfirm = (EditText) findViewById(R.id.edtPassConfirm);
		edtContactNumber = (EditText) findViewById(R.id.edtContactNumber);
		
		final ImageView imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		
		((Button) findViewById(R.id.btnSignup)).setOnClickListener(this);
	}
	
	private void setTypeFaceControls() {
		
		((TextView) findViewById(R.id.tvFirstName)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvLastName)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvPenName)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvEmail)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvPass)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvPassConfirm)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvContactNumber)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.edtFirstName)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.edtLastName)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.edtPenName)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.edtEmail)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.edtPass)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.edtPassConfirm)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.edtContactNumber)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((Button) findViewById(R.id.btnSignup)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	/**
	 * Input filter controls
	 */
	private void inputFilter() {
		InputFilter[] iFilter = new InputFilter[1];
		// Filter input email
		iFilter[0] = new InputFilter.LengthFilter(Constant.LENGTH_INPUT_EMAIL);
		edtEmail.setFilters(iFilter);
		
		// Filter input password, first name, last name, pen name
		iFilter = new InputFilter[1];
		iFilter[0] = new InputFilter.LengthFilter(Constant.LENGTH_INPUT_USER);
		edtFirstName.setFilters(iFilter);
		edtLastName.setFilters(iFilter);
		edtPenName.setFilters(iFilter);
		edtPasword.setFilters(iFilter);
		edtPaswordConfirm.setFilters(iFilter);
	}
	
	private RegisterUserLisener mRegisterListener = new RegisterUserLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_signup_fail)).show();
			
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			
			createDialogMessage(getString(R.string.msg_error_title), 
					errorMsg).show();
			
		}
		
		@Override
		public void requestCompleted(User user) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			
			if(null != user){
				
				Tracker t = PoemApplication.getInstance().getTracker(
		                TrackerName.APP_TRACKER);
		        t.send(new HitBuilders.EventBuilder()
		        	.setCategory(getString(R.string.google_analytics_event_category_account))
		        	.setAction(getString(R.string.google_analytics_event_action_register))
		        	.setLabel(user.getPen_name()).build());
		    	
	    		Common.setLogin(SignupActivity.this, user.getEmail(), 
					    			user.getPassword(), 
					    			user.getAccess_token(),
					    			user.getPen_name());
	    		
		    	Toast.makeText(SignupActivity.this, getString(R.string.msg_signup_successful), Toast.LENGTH_LONG).show();
				setResult(RESULT_OK);
				finish();
		    	
		    }
			
		}
	};
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {
		
		findViewById(R.id.lnLoading).setVisibility(View.GONE);
		// Signup error
	    if(null != event.getResult() && !event.getResult().isEmpty()
	    		&& event.getResult().equals(Constant.REGISTER_ERROR_DEF)){
	    	
	    		createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_signup_fail)).show();
	    }
	    // Signup successful
	    else if(null != event.getUser()){
	    		User user = event.getUser();
	    		Common.setLogin(this, user.getEmail(), 
					    			user.getPassword(), 
					    			user.getAccess_token(),
					    			user.getPen_name());
	    		
		    	Toast.makeText(this, getString(R.string.msg_signup_successful), Toast.LENGTH_LONG).show();
				setResult(RESULT_OK);
				finish();
	    }
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnSignup:
			if(! isNetworkAvailable()){
				createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_no_network)).show();
			}
			else if(checkValidateInput()){
				processSignup();
			}
			break;

		default:
			break;
		}
	}
	
	/**
	 * Check input validate
	 * 
	 * @return
	 */
	private boolean checkValidateInput() {
		
		// Check input empty
		if(edtFirstName.getText().toString().trim().isEmpty()){
			
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_fill_firstname_empty))
					.show();
			edtFirstName.requestFocus();
			
			return false;
			
		}
		if(edtLastName.getText().toString().trim().isEmpty()){
			
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_fill_lastname_empty))
					.show();
			edtLastName.requestFocus();
			
			return false;
			
		}
		if(edtPenName.getText().toString().trim().isEmpty()){
			
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_fill_penname_empty))
					.show();
			edtPenName.requestFocus();
			
			return false;
			
		}
		if(edtEmail.getText().toString().trim().isEmpty()){
			
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_fill_email_empty))
					.show();
			edtEmail.requestFocus();
			
			return false;
			
		}
		if(edtPasword.getText().toString().isEmpty()){
			
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_fill_signup_password_empty))
					.show();
			edtPasword.requestFocus();
			
			return false;
			
		}
				
		if(!checkEmailIsValid(edtEmail.getText().toString().trim())){
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_email_invalid))
				.show();
			edtEmail.requestFocus();
			return false;
		}
		
		if(edtPasword.getText().toString().contains(" ") || 
				edtPasword.getText().toString().length() < 6 ||
				edtPasword.getText().toString().length() > 16){
			
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_fill_signup_password_empty))
				.show();
			edtPasword.requestFocus();
			return false;
		}
		
		if(!edtPaswordConfirm.getText().toString().equals(edtPasword.getText().toString())){
			
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_fill_password_confirm_not_match))
					.show();
			edtPaswordConfirm.requestFocus();
			
			return false;
			
		}
		
		return true;
	}
	
	/**
	 * Process signup user
	 */
	private void processSignup() {
		
		User user = new User();
		user.setFirst_name(edtFirstName.getText().toString().trim());
		user.setLast_name(edtLastName.getText().toString().trim());
		user.setPen_name(edtPenName.getText().toString().trim());
		user.setEmail(edtEmail.getText().toString().trim());
		user.setPassword(edtPasword.getText().toString());
		user.setPassword_confirmation(edtPaswordConfirm.getText().toString());
		user.setPhone(edtContactNumber.getText().toString());
		
//		Api.postRegisterUser(this, user);
		new RegisterUserApi().registerUser(this, user, mRegisterListener);
		findViewById(R.id.lnLoading).setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		try {
			setResult(RESULT_CANCELED);
			finish();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
