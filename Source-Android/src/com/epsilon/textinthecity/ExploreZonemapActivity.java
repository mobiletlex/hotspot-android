package com.epsilon.textinthecity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.epsilon.textinthecity.adapter.ListPoemByLocationAdapter;
import com.epsilon.textinthecity.adapter.ListPoemZoneAdapter;
import com.epsilon.textinthecity.api.LocationByZoneApi;
import com.epsilon.textinthecity.api.PoemByLocationApi;
import com.epsilon.textinthecity.api.PoemByZoneApi;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.inter_face.LocationByZoneLisener;
import com.epsilon.textinthecity.inter_face.PoemByLocationLisener;
import com.epsilon.textinthecity.inter_face.PoemByZoneLisener;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.object.LocationMap;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.object.PoemPagging;
import com.epsilon.textinthecity.object.Zone;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;

public class ExploreZonemapActivity extends BaseActivity implements OnClickListener {
	
//	private LinearLayout lnListItem;
	private LoadMoreListView lvPoems;
	private ImageView imgLoading;
	
	private GoogleMap mMap;
	private ListPoemZoneAdapter mAdapter;
	private List<Poem> mListPoems;
	
	private Zone mZone;
	
	private int mCurrentPage = 0;
	private int mLastPage = 0;
	
	@SuppressLint("UseSparseArrays")
	private HashMap<Integer, String> hashRefixLocation = new HashMap<Integer, String>();
	private HashMap<String, Location> hashLocationMarker = new HashMap<String, Location>();
	
	List<LocationMap> listLocationsMap;
	
	public PopupWindow mPopupWindowLocationGoogle;
	private View mViewPopupLocationGoogle;
	private TextView tvPopupLocationGoogleTitle;
	private LoadMoreListView lvLocationGoogle;
	private Button btnPopupLocationGoogleClose;
	private ListPoemByLocationAdapter mAdapterPoemByLocation;
	private LinearLayout lnPopupLocationGoogleMain;
	private LinearLayout lnPopupLocationGoogleLoading;
	private TextView tvPopupLocationGoogleNoPoems;
	
	DisplayMetrics displayMetrics = new DisplayMetrics();
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_explore_zonemap);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_explore), true);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_EXPLORE);
		
//		findViewById(R.id.btnEdit).setVisibility(View.VISIBLE);
//		findViewById(R.id.btnEdit).setBackgroundResource(R.drawable.icon_location);
//		findViewById(R.id.btnEdit).setOnClickListener(this);
		
		mZone = (Zone)getIntent().getSerializableExtra(ExploreActivity.KEY_ZONE);
		
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		setupMapZone();
		
		initControls();
		
		initPopup();
		
//		PoemHelper.getInstance().register(this);
//		
		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		try {
			int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
			if (status == ConnectionResult.SUCCESS) {
				initMap();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(isNetworkAvailable()){
			showListLocationOnMap();
			
			loadListPoems();
		}
		else {
			findViewById(R.id.imgRefresh).setVisibility(View.VISIBLE);
			findViewById(R.id.imgRefresh).setOnClickListener(this);
			imgLoading.setVisibility(View.GONE);
			
		}
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
					
		try {
			int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
			if (status != ConnectionResult.SUCCESS) {
				checkGooglePlayServicesAvailable();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override 
	protected void onDestroy() {
//		PoemHelper.getInstance().unregister(this);
	    super.onDestroy();
	  }
	
	private void checkGooglePlayServicesAvailable() {
	    int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (status != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
	            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, 0);
	            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
	                @Override
	                public void onCancel(DialogInterface dialogInterface) {
	                    ExploreZonemapActivity.this.finish();
	                }
	            });
	            dialog.show();
	        } else {
	            Toast.makeText(this, "This device is not supported google play service.", Toast.LENGTH_LONG).show();
	            finish();
	        }
	    }
	    else {
	    	mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
	    	initMap();
//			if(null == listLocationsMap){
//				showListLocationOnMap();
//			}
//			else {
//				processShowLocationOnMap();
//			}
	    }
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		((TextView) findViewById(R.id.tvZoneName)).setText(mZone.getName());
		((TextView) findViewById(R.id.tvZoneName)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
		
		((TextView) findViewById(R.id.tvZonePoems)).setText(
				getString(R.string.explore_map_zone_poems_replace, mZone.getName()));
		((TextView) findViewById(R.id.tvZonePoems)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
//		((TextView) findViewById(R.id.tvZonePoems)).setPadding(getPaddingScreen(), 0, 0, 0);
		
//		lnListItem = (LinearLayout) findViewById(R.id.lnListItem);
		lvPoems = (LoadMoreListView) findViewById(R.id.listPoems);
		
		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
	}
	
	/**
	 * Init view for popup
	 */
	private void initPopup() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 mViewPopupLocationGoogle = inflater.inflate(R.layout.popup_poem_by_location, null, false);
		 
		 tvPopupLocationGoogleTitle = (TextView) mViewPopupLocationGoogle.findViewById(R.id.tvLocationGoogleTitle);
		 lvLocationGoogle = (LoadMoreListView) mViewPopupLocationGoogle.findViewById(R.id.listLocationsGoogle);
		 btnPopupLocationGoogleClose = (Button) mViewPopupLocationGoogle.findViewById(R.id.btnLocationGoogleClose);
		 lnPopupLocationGoogleMain = (LinearLayout) mViewPopupLocationGoogle.findViewById(R.id.lnPopMain);
		 lnPopupLocationGoogleLoading = (LinearLayout) mViewPopupLocationGoogle.findViewById(R.id.lnLoading);
		 
		 tvPopupLocationGoogleTitle.setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		 btnPopupLocationGoogleClose.setOnClickListener(this);
		 
		 final ImageView popupImgLoading = (ImageView)mViewPopupLocationGoogle.findViewById(R.id.imgLoading);
		 popupImgLoading.post(new Runnable() {
				    @Override
				    public void run() {
				        try {
				        	AnimationDrawable frameAnimation =
					            (AnimationDrawable) popupImgLoading.getBackground();
					        frameAnimation.start();
						} catch (Exception e) {
							e.printStackTrace();
						}
				    }
				}
			);
		 
		 tvPopupLocationGoogleNoPoems = new TextView(this);
		 tvPopupLocationGoogleNoPoems.setGravity(Gravity.CENTER_HORIZONTAL);
		 tvPopupLocationGoogleNoPoems.setTypeface(PoemApplication.getInstance().typeFaceFuturaLightOblique);
		 tvPopupLocationGoogleNoPoems.setText(getString(R.string.no_poem));
	}
	
	/**
	 * Show popup
	 * @param view
	 */
	private void showPopupImage(View view) {
		mPopupWindowLocationGoogle = new PopupWindow(this);
		mPopupWindowLocationGoogle.setTouchable(true);
		mPopupWindowLocationGoogle.setFocusable(true);
		mPopupWindowLocationGoogle.setOutsideTouchable(true);
		mPopupWindowLocationGoogle.setTouchInterceptor(new OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	mPopupWindowLocationGoogle.dismiss();
	                return true;
	            }
	            return false;
	        }
	    });
		mPopupWindowLocationGoogle.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowLocationGoogle.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowLocationGoogle.setOutsideTouchable(false);
		mPopupWindowLocationGoogle.setContentView(mViewPopupLocationGoogle);
		mPopupWindowLocationGoogle.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_gray_alpha));
	    
		mPopupWindowLocationGoogle.showAsDropDown(view, 0, (int)(- findViewById(R.id.top_layout).getHeight()));
	}
	
	/**
	 * Show list location marker on Map
	 */
	private void showListLocationOnMap() {
//		new ListLocationByZoneAsyncTask(mZone.getId(), this).execute();
		new LocationByZoneApi().getAllLocationsByZone(this, mZone.getId(), mLocationByZoneListener);
	}
	
	
	/**
	 * Set up width and height map image
	 */
	private void setupMapZone() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((LinearLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													paddingCenterLayout/2);
        
        // Set width and height for map layout
        LayoutParams lpMapLayout = 
        		new LayoutParams((int)(point.x*0.9f), (int)(point.x*0.9f*0.841f));
        ((RelativeLayout) findViewById(R.id.map_layout)).setLayoutParams(lpMapLayout);
        
        // Set width and height for map
//        android.widget.RelativeLayout.LayoutParams lp = 
//        		new android.widget.RelativeLayout.LayoutParams((int)(point.x*0.9f), 
//        														(int)(point.x*0.9f*0.841f));
//        ((MapView) findViewById(R.id.mapview)).setLayoutParams(lp);
         
	}
	
	/**
	 * Load list poems from server
	 */
	private void loadListPoems() {
	
//		new ListPoemsMapAsyncTask(mCurrentPage, Constant.PER_PAGE_POEM_DEF, mZone.getId(), this).execute();
		
		new PoemByZoneApi().getAllPoemsByZone(this, mCurrentPage, 
											Constant.PER_PAGE_POEM_DEF, 
											mZone.getId(), mPoemByZoneListener);
		
	}
	
	private void initMap() {
		try {
			LatLng positionMoveCamera = new LatLng(mZone.getLat(), 
					mZone.getLon());
			// Move the camera instantly to hamburg with a zoom of 15.
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionMoveCamera, 12));
			
			// Zoom in, animating the camera.
			mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
			
			mMap.setMyLocationEnabled(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {
	    
		// Result for get list poems
		if(null != event.getListPoemsPagging()){
	    		    
		    processShowListPoems(event.getListPoemsPagging());
		}
		// Result for list location by zone
		else if(null != event.getListLocationsMap()){
			
			listLocationsMap = event.getListLocationsMap();
						
			processShowLocationOnMap();
		}
	}
	
	private LocationByZoneLisener mLocationByZoneListener = new LocationByZoneLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleted(List<LocationMap> listLocation) {
			listLocationsMap = listLocation;
			
			processShowLocationOnMap();
		}
	};
	
	private PoemByZoneLisener mPoemByZoneListener = new PoemByZoneLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
		}
		
		@Override
		public void requestCompleted(PoemPagging poemPagging) {
			if(null != poemPagging){
				processShowListPoems(poemPagging);
			}
			else {
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			}
		}
	};
	
	private PoemByLocationLisener mPoemByLocationListener = new PoemByLocationLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			lnPopupLocationGoogleLoading.setVisibility(View.GONE);
			lnPopupLocationGoogleMain.setVisibility(View.VISIBLE);
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			lnPopupLocationGoogleLoading.setVisibility(View.GONE);
			lnPopupLocationGoogleMain.setVisibility(View.VISIBLE);
		}
		
		@Override
		public void requestCompleted(List<Poem> listPoem) {
			if(null == listPoem && listPoem.size() <= 0){
				
				listPoem = new ArrayList<Poem>();
			}
			mAdapterPoemByLocation = new ListPoemByLocationAdapter(ExploreZonemapActivity.this, listPoem);
			lvLocationGoogle.setAdapter(mAdapterPoemByLocation);
			if(listPoem.size() <= 0){
				lvLocationGoogle.addHeaderView(tvPopupLocationGoogleNoPoems);
			}
			lnPopupLocationGoogleLoading.setVisibility(View.GONE);
			lnPopupLocationGoogleMain.setVisibility(View.VISIBLE);
		}
	};
	
	
	/**
	 * Process show list poems ti list view
	 */
	private void processShowListPoems(PoemPagging listPoemsPagging) {
		
		((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		
		mCurrentPage = listPoemsPagging.getCurrent_page();
		mLastPage = listPoemsPagging.getLast_page();
		
		List<Poem> listPoemsTemp = listPoemsPagging.getData();
		
		if(null != listPoemsTemp && listPoemsTemp.size() > 0){
	    	if(null == mListPoems){
	    		mListPoems = listPoemsTemp;
	    		mAdapter = new ListPoemZoneAdapter(this, mListPoems, hashRefixLocation);
	    		lvPoems.setAdapter(mAdapter);
	    		lvPoems.setOnLoadMoreListener(new OnLoadMoreListener() {
	    			public void onLoadMore() {
	    				if(mCurrentPage < mLastPage){
//	    					new ListPoemsMapAsyncTask(mCurrentPage+1, Constant.PER_PAGE_POEM_DEF, mZone.getId(), 
//	    							ExploreZonemapActivity.this).execute();
	    					
	    					new PoemByZoneApi().getAllPoemsByZone(ExploreZonemapActivity.this, 
	    														mCurrentPage+1, 
																Constant.PER_PAGE_POEM_DEF, 
																mZone.getId(), 
																mPoemByZoneListener);
	    				}
	    				else {
	    					// Call onLoadMoreComplete when the LoadMore task, has finished
	    					lvPoems.onLoadMoreComplete();
	    				}
	    			}
	    		});
	    	}
	    	else {
	    		for (int i = 0; i < listPoemsTemp.size(); i++){
	    			mListPoems.add(listPoemsTemp.get(i));
//					mAdapter.addItem(listPoemsTemp.get(i));
				}
	    		mAdapter.notifyDataSetChanged();

				// Call onLoadMoreComplete when the LoadMore task, has finished
				lvPoems.onLoadMoreComplete();

	    	}
	    }
	}
	
	/**
	 * process show listlocation by Zone from serrver
	 * 
	 * @param listLocationsMap
	 */
	@SuppressLint("InflateParams")
	private void processShowLocationOnMap() {
		
		if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) != ConnectionResult.SUCCESS){
			return;
		}

		mMap.clear(); 
			
		if(null != listLocationsMap && listLocationsMap.size() > 0 ){
			
			int color = getResources().getColor(R.color.white);
			for (int i = 0; i < listLocationsMap.size(); i++) {
				
				LocationMap location = listLocationsMap.get(i);
												
				int background = R.drawable.marker_lock;
				if(location.isUnlock_required()){
					background = R.drawable.marker_unlock_green;
				}
				
				View marker = ((LayoutInflater) 
						getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
				TextView numTxt = (TextView) marker.findViewById(R.id.num_txt);
				numTxt.setBackgroundResource(background);
				numTxt.setTextColor(color);
				numTxt.setText(mZone.getRefix()+""+(i+1));
				
				LatLng position = new LatLng(location.getLat(), location.getLon());
				mMap.addMarker(new MarkerOptions().position(position)
				        .title(location.getName())
				        .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(marker))));
								
				hashRefixLocation.put(location.getId(), mZone.getRefix()+""+(i+1));
				hashLocationMarker.put(location.getName()+position.toString(), location.convertToLocation());
				
			}
		}
		
		mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
//            	new UnlockDialog(ExploreZonemapActivity.this, 
//						android.R.style.Theme_Translucent_NoTitleBar, 
//						hashLocationMarker.get(marker.getTitle()+marker.getPosition().toString()))
//				.show();

            	
//            	List<Poem> listPoemByLocation = new ArrayList<Poem>();
//            	if(null != mListPoems && mListPoems.size() > 0){
//            		listPoemByLocation.add(mListPoems.get(mListPoems.size()-1));
//            		if(mListPoems.size() >= 2){
//            			listPoemByLocation.add(mListPoems.get(mListPoems.size()-2));
//            		}
//    			}
//        		mAdapterPoemByLocation = new ListPoemByLocationAdapter(ExploreZonemapActivity.this, listPoemByLocation);
//				lvLocationGoogle.setAdapter(mAdapterPoemByLocation);
            	
            	
            	new PoemByLocationApi().getAllPoemsByLocation(ExploreZonemapActivity.this, 
			            			hashLocationMarker.get(marker.getTitle()+marker.getPosition().toString()).getId(), 
			            			mPoemByLocationListener);
            	
				tvPopupLocationGoogleTitle.setText(marker.getTitle());
				lnPopupLocationGoogleMain.setVisibility(View.GONE);
				lnPopupLocationGoogleLoading.setVisibility(View.VISIBLE);
				
				lvLocationGoogle.removeHeaderView(tvPopupLocationGoogleNoPoems);
				showPopupImage(findViewById(R.id.top_layout));

            }
        });
		
		mMap.setInfoWindowAdapter(new InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker marker) {              
                return null;
            }           

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker marker) {

                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);

                // Getting reference to the TextView to set title
                TextView note = (TextView) v.findViewById(R.id.note);
                ((TextView) v.findViewById(R.id.note)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
                note.setText(marker.getTitle() );

                // Returning the view containing InfoWindow contents
                return v;

            }

        });
		
		if(null != mAdapter){
			mAdapter.notifyDataSetChanged();
		}
	}
	
	
	// Convert a view to bitmap
	private Bitmap createDrawableFromView(View view) {
		view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.buildDrawingCache();
		Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
 
		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);
 
		return bitmap;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnEdit:
			try {
				if(null != PoemApplication.getInstance().lastKnownLocaton){
					int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
				    if (status == ConnectionResult.SUCCESS) {
				    	
						LatLng positionMoveCamera = new LatLng(
								PoemApplication.getInstance().lastKnownLocaton.getLatitude(), 
								PoemApplication.getInstance().lastKnownLocaton.getLongitude());
						// Move the camera instantly to hamburg with a zoom of 15.
						mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionMoveCamera, 10));
						
						// Zoom in, animating the camera.
						mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
												
				    }
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case R.id.imgRefresh:
			if(isNetworkAvailable()){
				showListLocationOnMap();
				
				loadListPoems();
				imgLoading.setVisibility(View.VISIBLE);
				findViewById(R.id.imgRefresh).setVisibility(View.GONE);
			}
			else {
				Toast.makeText(this, getString(R.string.msg_error_no_network), 
						Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.btnLocationGoogleClose:
			mPopupWindowLocationGoogle.dismiss();
			break;

		default:
			break;
		}
	}
	
}
