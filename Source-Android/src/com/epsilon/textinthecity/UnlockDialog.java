package com.epsilon.textinthecity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.epsilon.textinthecity.common.CleanUpUtil;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.object.Photo;
import com.viewpagerindicator.CirclePageIndicator;

public class UnlockDialog extends Dialog {

	private Context mContext;
	private Location mLocation;
	
	
	public UnlockDialog(Context context, int theme, Location location) {
		super(context, theme);
		mContext = context;
		mLocation = location;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.unlock_location);
		
		
		Point point = new Point(0, 0);
        Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
        display.getSize(point);
   
        LayoutParams lp = new LayoutParams((int)(point.x*0.9f), (int)(point.x*0.9f*1.48f));
        lp.addRule(RelativeLayout.ABOVE, R.id.vBottom);
        lp.addRule(RelativeLayout.BELOW, R.id.vTop);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        lp.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        ((LinearLayout) findViewById(R.id.unlock_layout)).setLayoutParams(lp);
        
        LayoutParams lpPager = new LayoutParams((int)(point.x*0.9f), (int)(point.x*0.9f*0.63f));
        ((ViewPager) findViewById(R.id.viewpager)).setLayoutParams(lpPager);
        
        android.widget.LinearLayout.LayoutParams lpRlPager = 
        		new android.widget.LinearLayout.LayoutParams((int)(point.x*0.9f), (int)(point.x*0.9f*0.63f));
        ((RelativeLayout) findViewById(R.id.rlViewPager)).setLayoutParams(lpRlPager);
        
        int paddingCenterLayout = (point.x - (int)(point.x*0.95f))/2;
        ((LinearLayout) findViewById(R.id.lnFunFactContent)).setPadding(paddingCenterLayout, 
													0, 
													paddingCenterLayout, 
													paddingCenterLayout/2);
        ((TextView) findViewById(R.id.tvUnlockFunFatc)).setPadding(paddingCenterLayout, 
													paddingCenterLayout/2, 
													paddingCenterLayout, 
													0);
        
        if(null != mLocation.getPhotos() && mLocation.getPhotos().length > 0){
        	final GalleryPagerAdapter adapterGallery = new GalleryPagerAdapter(mContext, mLocation.getPhotos());
        	 ((ViewPager) findViewById(R.id.viewpager)).setAdapter(adapterGallery);
        	 ((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(0, true);
        	 
        	 CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
 			mIndicator.setViewPager((ViewPager) findViewById(R.id.viewpager));
 			
 			final Button btnNext = (Button) findViewById(R.id.btnNext);
	        final Button btnPrev = (Button) findViewById(R.id.btnPrev);
	        if(adapterGallery.getCount() > 1){
				btnNext.setVisibility(View.VISIBLE);
			}
	        mIndicator.setOnPageChangeListener(
	        		new ViewPager.OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int arg0) {
					if(adapterGallery.getCount() <= 1){
						btnPrev.setVisibility(View.GONE);
						btnNext.setVisibility(View.GONE);
					}
					else if(arg0 == 0){
						btnPrev.setVisibility(View.GONE);
						btnNext.setVisibility(View.VISIBLE);
					}
					else if(arg0 >= adapterGallery.getCount()-1) {
						btnPrev.setVisibility(View.VISIBLE);
						btnNext.setVisibility(View.GONE);
					}
					else {
						btnPrev.setVisibility(View.VISIBLE);
						btnNext.setVisibility(View.VISIBLE);
					}
				}
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}
				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
			});
	        
	        btnNext.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(
							((ViewPager) findViewById(R.id.viewpager)).getCurrentItem()+1, true);
				}
			});
	        btnPrev.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(
							((ViewPager) findViewById(R.id.viewpager)).getCurrentItem()-1, true);
				}
			});
        	
        }
        
        String funFact = "";
        String locationName = "";
        if(null != mLocation){
        	funFact = mLocation.getFun_fact();
        	locationName = mLocation.getName();
        }
        ((TextView) findViewById(R.id.tvFunFactContent)).setText(funFact);
        ((TextView) findViewById(R.id.tvUnlockFunFatc)).setText(locationName);
        
        ((Button) findViewById(R.id.btnUnlockClose)).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				UnlockDialog.this.dismiss();
			}
		});
        
        ((TextView) findViewById(R.id.tvFunFactContent)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaOblique);
        ((TextView) findViewById(R.id.tvUnlockFunFatc)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
        ((TextView) findViewById(R.id.tvUnlockLocation)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
        
	}
		
	private class GalleryPagerAdapter extends android.support.v4.view.PagerAdapter {
		private Photo[] urls;
		private Context context;
//		private ImageLoader imageLoader;
//		private ImageListener imageListener;
		public GalleryPagerAdapter(Context context, Photo[] urls) {
			this.urls = urls;
			this.context = context;
//			imageLoader = ((PoemApplication)context.getApplicationContext()).getImageLoader();
		}

		@SuppressLint("InflateParams")
		public Object instantiateItem(View collection, final int position) {
			LayoutInflater inflater = (LayoutInflater) collection.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.gallery_page, null);
			ImageView imageView =  (ImageView) view.findViewById(R.id.gallery_image);
//			imageListener = ImageLoader.getImageListener(imageView, R.drawable.no_image, R.drawable.no_image);
//			imageView.setScaleType(ScaleType.FIT_XY);
			if(null != urls && urls.length > 0){
//				imageLoader.get(urls[position].getLink(), imageListener);
				
				Point point = new Point(0, 0);
		        Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
		        display.getSize(point);
		        
				Common.loadImageView(imageView, urls[position].getLink(), 
						(int)(point.x*0.9f), (int)(point.x*0.9f*1.48f), mContext);
			}
			
			((ViewPager) collection).addView(view, 0);
			return view;
		}

		@Override
		public int getCount() {
			return urls == null ? 1 : urls.length;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == ((View) arg1);
		}
		
		@Override
		public void destroyItem(View collection, int position, Object o) {
		    View view = (View) o;
		    ((ViewPager) collection).removeView(view);
		    CleanUpUtil.cleanupView(view);
		    view = null;
		}
	}

}
