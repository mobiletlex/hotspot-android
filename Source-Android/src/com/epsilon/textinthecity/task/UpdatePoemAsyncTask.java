package com.epsilon.textinthecity.task;

import java.util.List;

import org.apache.http.NameValuePair;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class UpdatePoemAsyncTask extends AsyncTask<Void, Void, Poem> {
	
	private List<NameValuePair> nameValuePairs;
	private Context context;
	private int id;
	private boolean isFlagEnglish = false;
	
	public UpdatePoemAsyncTask(Context context,
			List<NameValuePair> nameValuePairs, int id,
			boolean isFlagEnglish) {
		this.context = context;
		this.nameValuePairs = nameValuePairs;
		this.id = id;
		this.isFlagEnglish = isFlagEnglish;
	}

	@Override 
	protected Poem doInBackground(Void... params) {
	    
//	    Api.postJsonDataWithImage(Constant.API_POEM_CREATE, nameValuePairs, context);
		return Api.updatePoemTest(context, nameValuePairs, id);
//	    return null;
	  }
	 
	  @Override 
	  protected void onPostExecute(Poem result) {
		  if(null == result){
			  if(isFlagEnglish){
				  PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_UPDATE_ENG_ERROR_DEF));
			  }
			  else {
				  PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_UPDATE_ERROR_DEF));
			  }
		  }
		  else {
			  PoemHelper.getInstance().post(new AsyncTaskResultEvent(result));
		  }
	  }
}
