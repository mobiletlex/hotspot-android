package com.epsilon.textinthecity.task;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class ListPoemsSubmissionAsyncTask extends AsyncTask<Void, Void, List<Poem>> {
	
	private Context context;
	
	public ListPoemsSubmissionAsyncTask(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	@Override 
	protected List<Poem> doInBackground(Void... params) {
	    
//	    return Constant.CreateListPoemsTest();
		 return Api.getPoemSubmission(context);
	  }
	 
	  @Override 
	  protected void onPostExecute(List<Poem> result) {
		  if(null != result){
			  ArrayList<Poem> arrResult = new ArrayList<Poem>();
			  arrResult.addAll(result);
			  PoemHelper.getInstance().post(new AsyncTaskResultEvent(arrResult, 0, true));
		  }
		  else {
			  PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_SUBMISSION_ERROR_DEF));
		  }
	    
	  }
}
