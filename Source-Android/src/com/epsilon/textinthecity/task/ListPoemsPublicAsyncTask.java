package com.epsilon.textinthecity.task;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.Constant.SortField;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.PoemPagging;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class ListPoemsPublicAsyncTask extends AsyncTask<Void, Void, PoemPagging> {
	
	private Context context;
	private int page;
	private int perPage;
	private SortField sortField;
	private int filter;
	
	public ListPoemsPublicAsyncTask(Context context, int page, int perPage, SortField sortField, int filter) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.perPage = perPage;
		this.page = page;
		this.sortField = sortField;
		this.filter = filter;
	}

	@Override 
	protected PoemPagging doInBackground(Void... params) {
	    
//	    return Constant.CreateListPoemsTest();
		 return Api.getPoemPublic(context, page, perPage, sortField, filter);
	  }
	 
	  @Override 
	  protected void onPostExecute(PoemPagging result) {
		  if(null != result){
			  AsyncTaskResultEvent asyncResult = new AsyncTaskResultEvent();
			  asyncResult.setListPoemsPublic(result);
			  PoemHelper.getInstance().post(asyncResult);
		  }
		  else {
			  PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_PUBLIC_ERROR_DEF));
		  }
	    
	  }
}
