package com.epsilon.textinthecity.task;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class ListPoemsAsyncTask extends AsyncTask<Void, Void, List<Poem>> {
	
	Context context;
	
	public ListPoemsAsyncTask(Context context) {
		this.context = context;
	}

	@Override 
	protected List<Poem> doInBackground(Void... params) {
	    
//	    return Constant.CreateListPoemsTest();
		 return Api.getRandomPoems(context);
	  }
	 
	  @Override 
	  protected void onPostExecute(List<Poem> result) {
	    PoemHelper.getInstance().post(new AsyncTaskResultEvent(result));
	  }
}
