package com.epsilon.textinthecity.task.taskevent;

import java.util.ArrayList;
import java.util.List;

import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.object.LocationMap;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.object.PoemPagging;
import com.epsilon.textinthecity.object.User;

public class AsyncTaskResultEvent {

	private String result;
	private List<Poem> listPoems;
	private List<Location> listLocations;
	private PoemPagging listPoemsPagging;
	private List<LocationMap> listLocationsMap;
	private LocationMap locationMap;
	private Poem poem;
	private User user;
	private ArrayList<Poem> listPoemsDraft;
	private ArrayList<Poem> listPoemsSubmission;
	private PoemPagging listPoemsFeatured;
	private PoemPagging listPoemsPublic;
	private PoemPagging listPoemSearch;
	private Poem poemDetail;
	
	private Poem poemLike;
	private Location locationById;
	
	
	public AsyncTaskResultEvent() {
	  }
	 
	  public AsyncTaskResultEvent(String result) {
	    this.result = result;
	  }
	  
	  public AsyncTaskResultEvent(List<Poem> listPoems) {
	    this.listPoems = listPoems;
	  }
	  
	  public AsyncTaskResultEvent(ArrayList<Poem> listPoemsDraft, int draft, int poem) {
	    this.listPoemsDraft = listPoemsDraft;
	  }
	  
	  public AsyncTaskResultEvent(ArrayList<Poem> listPoemsSubmission, int draft, boolean poem) {
		    this.listPoemsSubmission = listPoemsSubmission;
		  }
	  
	  public AsyncTaskResultEvent(List<Location> listLocations, boolean isAll) {
	    this.listLocations = listLocations;
	  }
	  
	  public AsyncTaskResultEvent(List<LocationMap> listLocationsMap, int map) {
		    this.listLocationsMap = listLocationsMap;
		  }
	  
	  public AsyncTaskResultEvent(PoemPagging listPoemsPagging) {
		    this.listPoemsPagging = listPoemsPagging;
	  }
	  
	  public AsyncTaskResultEvent(LocationMap locationMap) {
		    this.locationMap = locationMap;
	  }
	  
	  public AsyncTaskResultEvent(Poem poem) {
		    this.poem = poem;
	  }
	  
	  public AsyncTaskResultEvent(User user) {
		    this.user = user;
	  }
	 
	  public String getResult() {
	    return result;
	  }

	public List<Poem> getListPoems() {
		return listPoems;
	}

	public List<Location> getListLocations() {
		return listLocations;
	}
	
	public List<LocationMap> getListLocationsMap() {
		return listLocationsMap;
	}
	
	public PoemPagging getListPoemsPagging() {
		return listPoemsPagging;
	}

	public LocationMap getLocationMap() {
		return locationMap;
	}

	public Poem getPoem() {
		return poem;
	}

	public User getUser() {
		return user;
	}

	public ArrayList<Poem> getListPoemsDraft() {
		return listPoemsDraft;
	}

	public ArrayList<Poem> getListPoemsSubmission() {
		return listPoemsSubmission;
	}

	public PoemPagging getListPoemsFeatured() {
		return listPoemsFeatured;
	}

	public void setListPoemsFeatured(PoemPagging listPoemsFeatured) {
		this.listPoemsFeatured = listPoemsFeatured;
	}

	public PoemPagging getListPoemsPublic() {
		return listPoemsPublic;
	}

	public void setListPoemsPublic(PoemPagging listPoemsPublic) {
		this.listPoemsPublic = listPoemsPublic;
	}

	public Poem getPoemLike() {
		return poemLike;
	}

	public void setPoemLike(Poem poemLike) {
		this.poemLike = poemLike;
	}
	
	public PoemPagging getListPoemSearch() {
		return listPoemSearch;
	}

	public void setListPoemSearch(PoemPagging listPoemSearch) {
		this.listPoemSearch = listPoemSearch;
	}

	public Poem getPoemDetail() {
		return poemDetail;
	}

	public void setPoemDetail(Poem poemDetail) {
		this.poemDetail = poemDetail;
	}

	public Location getLocationById() {
		return locationById;
	}

	public void setLocationById(Location locationById) {
		this.locationById = locationById;
	}
	  
}
