package com.epsilon.textinthecity.task;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.PoemPagging;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class ListPoemsMapAsyncTask extends AsyncTask<Void, Void, PoemPagging> {
	
	private int page;
	private int perPage;
	private int zoneId;
	Context context;
	public ListPoemsMapAsyncTask(int page, int perPage, int zoneId, Context context) {
		this.page = page;
		this.perPage = perPage;
		this.zoneId = zoneId;
		this.context = context;
	}

	@Override 
	protected PoemPagging doInBackground(Void... params) {
		
		if (isCancelled()) {
			return null;
		}

//		// Simulates a background task
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//		}
//	    
//	    return Constant.CreateListPoemsTest();
		
		return Api.getPoemsByZoneId(page, perPage, zoneId, context);
	  }
	 
	  @Override 
	  protected void onPostExecute(PoemPagging result) {
		 
	    PoemHelper.getInstance().post(new AsyncTaskResultEvent(result));
	  }
}
