package com.epsilon.textinthecity.task;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.PoemPagging;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class ListPoemsSearchAsyncTask extends AsyncTask<Void, Void, PoemPagging> {
	
	private Context context;
	private int page;
	private int perPage;
	private String searchText;
	
	public ListPoemsSearchAsyncTask(Context context, int page, int perPage, String searchText) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.perPage = perPage;
		this.page = page;
		this.searchText = searchText;
	}

	@Override 
	protected PoemPagging doInBackground(Void... params) {
	    
//	    return Constant.CreateListPoemsTest();
		 return Api.getPoemSearch(context, page, perPage, searchText);
	  }
	 
	  @Override 
	  protected void onPostExecute(PoemPagging result) {
		  if(null != result){
			  AsyncTaskResultEvent asyncResult = new AsyncTaskResultEvent();
			  asyncResult.setListPoemSearch(result);
			  PoemHelper.getInstance().post(asyncResult);
		  }
		  else {
			  PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_SEARCH_ERROR_DEF));
		  }
	    
	  }
}
