package com.epsilon.textinthecity.task;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.LocationMap;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class ListLocationNearByAsyncTask extends AsyncTask<Void, Void, List<LocationMap>> {
	
	private double lat;
	private double lon;
	Context context;
	public ListLocationNearByAsyncTask(double lat, double lon, Context context) {
		this.lat = lat;
		this.lon = lon;
		this.context = context;
	}

	@Override 
	protected List<LocationMap> doInBackground(Void... params) {
	    
//	    return Constant.CreateListAllLocationsTest();
		return Api.getLocationsNearBy(lat, lon, context);
	  }
	 
	  @Override 
	  protected void onPostExecute(List<LocationMap> result) {
		  
	    PoemHelper.getInstance().post(new AsyncTaskResultEvent(result, 1));
	  }
}
