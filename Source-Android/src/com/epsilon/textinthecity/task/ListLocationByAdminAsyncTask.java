package com.epsilon.textinthecity.task;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class ListLocationByAdminAsyncTask extends AsyncTask<Void, Void, List<Location>> {
	
	Context context;
	
	public ListLocationByAdminAsyncTask(Context context) {
		this.context = context;
	}

	@Override 
	protected List<Location> doInBackground(Void... params) {
	    
//	    return Constant.CreateListAllLocationsTest();
	    return Api.getAllLocationsByAdmin(context);
	  }
	 
	  @Override 
	  protected void onPostExecute(List<Location> result) {
	    PoemHelper.getInstance().post(new AsyncTaskResultEvent(result, true));
	  }
}
