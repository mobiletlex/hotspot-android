package com.epsilon.textinthecity.task;

import java.util.List;

import org.apache.http.NameValuePair;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class PostPoemAsyncTask extends AsyncTask<Void, Void, Poem> {
	
	private List<NameValuePair> nameValuePairs;
	private Context context;
	private boolean isFlagEnglish = false;
	
	public PostPoemAsyncTask(Context context,
			List<NameValuePair> nameValuePairs, boolean isFlagEnglish) {
		this.context = context;
		this.nameValuePairs = nameValuePairs;
		this.isFlagEnglish = isFlagEnglish;
	}

	@Override 
	protected Poem doInBackground(Void... params) {
	    
//	    Api.postJsonDataWithImage(Constant.API_POEM_CREATE, nameValuePairs, context);
		return Api.postPoemTest(context, nameValuePairs);
//	    return null;
	  }
	 
	  @Override 
	  protected void onPostExecute(Poem result) {
		  if(null == result){
			  if(isFlagEnglish){
				  PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ENG_ERROR_DEF));
			  }
			  else {
				  PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ERROR_DEF));
			  }
			  
		  }
		  else {
			  PoemHelper.getInstance().post(new AsyncTaskResultEvent(result));
		  }
	  }
}
