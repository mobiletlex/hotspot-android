package com.epsilon.textinthecity.task;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.LocationMap;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class ListLocationByZoneAsyncTask extends AsyncTask<Void, Void, List<LocationMap>> {
	
	Context context;
	private int zoneId;
	public ListLocationByZoneAsyncTask(int zoneId, Context context) {
		this.zoneId = zoneId;
		this.context = context;
	}

	@Override 
	protected List<LocationMap> doInBackground(Void... params) {
	    
//	    return Constant.CreateListAllLocationsTest();
		return Api.getLocationsByZone(zoneId, context);
	  }
	 
	  @Override 
	  protected void onPostExecute(List<LocationMap> result) {

	    PoemHelper.getInstance().post(new AsyncTaskResultEvent(result, 1));
	  }
}
