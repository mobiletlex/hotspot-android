package com.epsilon.textinthecity.task;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;

public class PostLocationAsyncTask extends AsyncTask<Void, Void, List<Location>> {
	
	Context context;
	String locationname; 
	String address;
	int zoneId;
	String funFact;
	double lat;
	double lon;
	
	public PostLocationAsyncTask(Context context,
									String locationname, 
									String address,
									int zoneId,
									String funFact,
									double lat,
									double lon) {
		this.context = context;
		this.locationname = locationname;
		this.address = address;
		this.zoneId = zoneId;
		this.funFact = funFact;
		this.lat = lat;
		this.lon = lon;
	}

	@Override 
	protected List<Location> doInBackground(Void... params) {
	    
//	    return Constant.CreateListAllLocationsTest();
	    Api.postLocation1(context, locationname, address, zoneId, funFact, lat, lon);
	    return null;
	  }
	 
	  @Override 
	  protected void onPostExecute(List<Location> result) {
	    PoemHelper.getInstance().post(new AsyncTaskResultEvent(result, true));
	  }
}
