package com.epsilon.textinthecity.volley_interface;

import com.android.volley.VolleyError;

public interface JsonResponseListener {

	public void requestStarted();
    public void requestCompleted(String json);
    public void requestEndedWithError(VolleyError error);
}
