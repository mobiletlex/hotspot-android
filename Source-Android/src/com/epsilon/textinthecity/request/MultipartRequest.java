//package com.epsilon.textinthecity.request;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.IOException;
//import java.util.List;
//
//import org.apache.http.HttpEntity;
//import org.apache.http.NameValuePair;
//import org.apache.http.entity.mime.MultipartEntityBuilder;
//import org.apache.http.entity.mime.content.FileBody;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.NetworkResponse;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.VolleyLog;
//
///**
// * Created by albert on 14-3-21.
// */
//public class MultipartRequest extends Request<String> {
//    public static final String KEY_PICTURE = "photo";
//    public static final String KEY_PICTURE_NAME = "filename";
//    public static final String KEY_ROUTE_ID = "route_id";
//
//    private HttpEntity mHttpEntity;
//
//    List<NameValuePair> nameValuePairs;
//    private String mRouteId;
//    private Response.Listener mListener;
//    
//    public MultipartRequest(String url, List<NameValuePair> nameValuePairs,
//	            Response.Listener<String> listener,
//	            Response.ErrorListener errorListener) {
//		super(Method.POST, url, errorListener);
//		
//		mListener = listener;
//		this.nameValuePairs = nameValuePairs;
//		mHttpEntity = buildMultipartEntity();
//	}
//
//    public MultipartRequest(String url, String filePath, String routeId,
//                            Response.Listener<String> listener,
//                            Response.ErrorListener errorListener) {
//        super(Method.POST, url, errorListener);
//
//        mRouteId = routeId;
//        mListener = listener;
//        mHttpEntity = buildMultipartEntity(filePath);
//    }
//
//    public MultipartRequest(String url, File file, String routeId,
//                            Response.Listener<String> listener,
//                            Response.ErrorListener errorListener) {
//        super(Method.POST, url, errorListener);
//
//        mRouteId = routeId;
//        mListener = listener;
//        mHttpEntity = buildMultipartEntity(file);
//    }
//
//    private HttpEntity buildMultipartEntity(String filePath) {
//        File file = new File(filePath);
//        return buildMultipartEntity(file);
//    }
//    
//    private HttpEntity buildMultipartEntity() {
//    	for (int index = 0; index < nameValuePairs.size(); index++) {
//			String name = nameValuePairs.get(index).getName();
//			String value = nameValuePairs.get(index).getValue();
//			if(name.contains("photo")){
//				File file = new File(value);
//			    return buildMultipartEntity(file);
//			}
//    	}
//    	
//        return null;
//    }
//
//    private HttpEntity buildMultipartEntity(File file) {
//        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//        String fileName = file.getName();
//        FileBody fileBody = new FileBody(file,"binary/octet-stream");
//        
//        
//        builder.addPart(KEY_PICTURE, fileBody);
////        builder.addBinaryBody(KEY_PICTURE, file);
//        for (int index = 0; index < nameValuePairs.size(); index++) {
//			String name = nameValuePairs.get(index).getName();
//			String value = nameValuePairs.get(index).getValue();
//			
//			builder.addTextBody(name, value);
//        }
//        return builder.build();
//    }
//
//    @Override
//    public String getBodyContentType() {
//        return mHttpEntity.getContentType().getValue();
//    }
//
//    @Override
//    public byte[] getBody() throws AuthFailureError {
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        try {
//            mHttpEntity.writeTo(bos);
//        } catch (IOException e) {
//            VolleyLog.e("IOException writing to ByteArrayOutputStream");
//        }
//        return bos.toByteArray();
//    }
//
//    @Override
//    protected Response<String> parseNetworkResponse(NetworkResponse response) {
//        return Response.success("Uploaded", getCacheEntry());
//    }
//
//    @Override
//    protected void deliverResponse(String response) {
//        mListener.onResponse(response);
//    }
//}