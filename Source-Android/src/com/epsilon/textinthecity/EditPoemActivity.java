package com.epsilon.textinthecity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.api.Api;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.Link;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.object.PhotoPoem;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.task.UpdatePoemAsyncTask;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputFilter;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class EditPoemActivity extends BaseActivity implements OnClickListener {
	
	public static final String KEY_POEM = "KEY_URL_PHOTO";
		
	private static final int LOGIN_CODE = 100;
	private static final int CODE_PICK_IMAGE = 101;
	private static final int LOCATION_CODE = 102;
	
	public static final int FLAG_NEXT = 1;
	public static final int FLAG_SAVE = 2;
	public static final int FLAG_SUBMIT = 3;
	
	private int mLocationId = 0;
	
//	private PopupWindow mPopupWindow;
//	private View mViewPopup;
//	private PopupWindow mPopupWindowLocation;
//	private View mViewPopupLocation;
	
//	private TextView tvPopupCaption;
//	private EditText edtPopupCaption;
//	private Button btnPopupLoadPhoto;
//	private ImageView imgSample;
	
	private ImageView imgPhoto;
	private Button btnClose;
	private TextView tvCaption;
	private EditText edtCaption;
	private RelativeLayout rlImageLayout;
	
//	private TextView tvPopupLocation;
//	private TextView tvPopupLocationName;
//	private TextView tvPopupLocationAddress;
//	private Button btnPopupLocation;
	private TextView tvLocationName;
	
	
	private String mUriAvatar = "";
	
	private ImageView imgLoading;
	
	private Poem mPoem;
	
	private int mFlagPostSattus = FLAG_SAVE;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_poem);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_edit), false);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_MY_ENTRIES);
		
		mPoem = (Poem)getIntent().getSerializableExtra(ExploreActivity.KEY_POEM_OBJECT);
		
		if(null != savedInstanceState){
			mPoem = (Poem)savedInstanceState.getSerializable(ExploreActivity.KEY_POEM_OBJECT);
		}
		
		initControls();
				
		setTypeFaceControls();
		
		setupWidthCenter();

		PoemHelper.getInstance().register(this);
		
		initData(savedInstanceState);
		
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		
		if(null != savedInstanceState){
			mPoem = (Poem)savedInstanceState.getSerializable(ExploreActivity.KEY_POEM_OBJECT);
			initData(savedInstanceState);
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PoemHelper.getInstance().register(this);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			PoemHelper.getInstance().unregister(this);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	protected void onDestroy() {
		try {
			PoemHelper.getInstance().unregister(this);
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onDestroy();
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		
		EditText edtTitle = (EditText) findViewById(R.id.edtTitle);
		EditText edtContent = (EditText) findViewById(R.id.edtContent);
		
		savedInstanceState.putSerializable(ExploreActivity.KEY_POEM_OBJECT, mPoem);
		savedInstanceState.putString(SubmitActivity.KEY_TITLE, edtTitle.getText().toString());
		savedInstanceState.putString(SubmitActivity.KEY_CONTENT, edtContent.getText().toString());
		savedInstanceState.putInt(SubmitActivity.KEY_LOCATION_ID, mLocationId);
		savedInstanceState.putString(SubmitActivity.KEY_LOCATION_NAME, tvLocationName.getText().toString());
		if(!mUriAvatar.isEmpty()){
			savedInstanceState.putString(SubmitActivity.KEY_CAPTION, edtCaption.getText().toString());
			savedInstanceState.putString(SubmitActivity.KEY_URL_PHOTO, mUriAvatar);
		}
		
		super.onSaveInstanceState(savedInstanceState);
	}
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {

		if(null != event.getResult() && event.getResult().equals(Constant.LOCATION_GET_BY_ID_ERROR_DEF)){
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		}
		else if(null != event.getLocationById()){
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			tvLocationName.setText(event.getLocationById().getName());
		}
		// Result for list location by zone
		else if(null != event.getResult() && event.getResult().equals(Constant.POEM_UPDATE_ERROR_DEF)){
						
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_poem_update_fail)).show();
		}
		// add new poem success
		else if(null != event.getPoem()){
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			Intent it = new Intent(EditPoemActivity.this, MyEntriesActivity.class);
			if(mFlagPostSattus == FLAG_SAVE){
				it.putExtra(ShareActivity.KEY_SHARE_SCREEN, false);
				Toast.makeText(this, getString(R.string.msg_poem_save_draft_successful), Toast.LENGTH_LONG).show();
			}
			else{
				if(mPoem.getStatus_id() == Constant.POEM_STATUS_DRAFT){
					Tracker t = PoemApplication.getInstance().getTracker(
			                TrackerName.APP_TRACKER);
			        t.send(new HitBuilders.EventBuilder()
			        	.setCategory(getString(R.string.google_analytics_event_category_poem))
			        	.setAction(getString(R.string.google_analytics_event_action_submit))
			        	.setLabel(
			        			SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME) + ", " +
			        			event.getPoem().getId()+"")
			        	.build());
				}
				
				it.putExtra(ShareActivity.KEY_SHARE_SCREEN, true);
				Toast.makeText(this, getString(R.string.msg_poem_update_successful), Toast.LENGTH_LONG).show();
			}
			it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			finish();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == LOGIN_CODE && resultCode == RESULT_CANCELED) {
			finish();
		}
		else if(requestCode == CODE_PICK_IMAGE){
			if (resultCode == RESULT_OK && data != null) {
				
				mUriAvatar = data.getStringExtra(AddImageActivity.KEY_PHOTO);
				String caption = data.getStringExtra(AddImageActivity.KEY_CAPTION);
								
				edtCaption.setText(caption);
				
				if(mUriAvatar.isEmpty()){
					imgPhoto.setBackgroundResource(R.drawable.no_image);
					rlImageLayout.setVisibility(View.GONE);
				}
				else {
					Options opts = new Options();
					opts.inSampleSize = 4;

					File tempFile = getTempFile(1);
					String filePath = tempFile.getAbsolutePath();
					
//					mUriAvatar = filePath;

					Bitmap bm = BitmapFactory.decodeFile(filePath, opts);
					imgPhoto.setImageBitmap(bm);
					
					rlImageLayout.setVisibility(View.VISIBLE);
				}

//				Options opts = new Options();
//				opts.inSampleSize = 4;
//
//				File tempFile = getTempFile(1);
//				String filePath = tempFile.getAbsolutePath();
//				
//				mUriAvatar = filePath;
//
//				Bitmap bm = BitmapFactory.decodeFile(filePath, opts);
////				imgSample.setImageBitmap(bm);
//				imgPhoto.setImageBitmap(bm);
//				
//				rlImageLayout.setVisibility(View.VISIBLE);
//
////				btnPopupLoadPhoto.setText(getString(R.string.sumit_popup_delete_photo));
			}
		}
		else if(requestCode == LOCATION_CODE && resultCode == RESULT_OK){
			if(null != data) {
				mLocationId = data.getIntExtra(LocationsActivity.KEY_LOCATION_ID, 0);
				
				tvLocationName.setText(data.getStringExtra(LocationsActivity.KEY_LOCATION_NAME));
//				tvPopupLocationName.setText(data.getStringExtra(LocationsActivity.KEY_LOCATION_NAME));
//				tvPopupLocationAddress.setText(data.getStringExtra(LocationsActivity.KEY_LOCATION_ADDRESS));
			}
		}
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		

		((Button) findViewById(R.id.btnNext)).setOnClickListener(this);
		((Button) findViewById(R.id.btnSave)).setOnClickListener(this);
		((Button) findViewById(R.id.btnSubmit)).setOnClickListener(this);
		((Button) findViewById(R.id.btnGetImage)).setOnClickListener(this);
		((Button) findViewById(R.id.btnMarkerLocation)).setOnClickListener(this);
		
		tvCaption = (TextView) findViewById(R.id.tvCaption);
		 edtCaption = (EditText) findViewById(R.id.edtCaption);
		 btnClose = (Button) findViewById(R.id.btnClose);
		 imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
		 rlImageLayout = (RelativeLayout) findViewById(R.id.rlImagePhoto);
		 
		 btnClose.setOnClickListener(this);
		 edtCaption.setFocusable(false);
			edtCaption.setEnabled(false);
		 
		 tvLocationName = (TextView) findViewById(R.id.tvLocation);
		 
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		InputFilter[] iFilter = new InputFilter[1];
		// Filter input title
		iFilter[0] = new InputFilter.LengthFilter(Constant.LENGTH_INPUT_POEM_TITLE);
		((EditText) findViewById(R.id.edtTitle)).setFilters(iFilter);
	}
	
	private void initData(Bundle savedInstanceState) {
		String languageName = Constant.HASH_LANGUAGES.get(mPoem.getLanguage_id()).getName();
		String title = mPoem.getTitle();
		String content = mPoem.getContent();
		if(null != savedInstanceState){
			title = savedInstanceState.getString(SubmitActivity.KEY_TITLE, "");
			content = savedInstanceState.getString(SubmitActivity.KEY_CONTENT, "");
		}
		
		((TextView) findViewById(R.id.tvLanguageEng)).setText(languageName);
		((EditText) findViewById(R.id.edtTitle)).setText(title);
		((EditText) findViewById(R.id.edtContent)).setText(content);
		
		// Load category
		String category = getString(R.string.sumit_category_public);
		if(mPoem.getCategory_id() == Constant.CATEGORY_ID_SCHOOL){
			category = getString(R.string.sumit_category_school);
		}
		((TextView) findViewById(R.id.tvCategoryPublic)).setText(category);
		
		// Check button display/hide
		if(mPoem.getLanguage_id() != Constant.LANGUAGE_ID_ENG) {
			((Button) findViewById(R.id.btnNext)).setVisibility(View.VISIBLE);
			((Button) findViewById(R.id.btnSave)).setVisibility(View.GONE);
			((Button) findViewById(R.id.btnSubmit)).setVisibility(View.GONE);
		}
		else {
			((Button) findViewById(R.id.btnNext)).setVisibility(View.GONE);
			
			((Button) findViewById(R.id.btnSave)).setVisibility(View.VISIBLE);
			((Button) findViewById(R.id.btnSubmit)).setVisibility(View.VISIBLE);
		}
		
		if(mPoem.getStatus_id() != Constant.POEM_STATUS_DRAFT){
			((Button) findViewById(R.id.btnSave)).setVisibility(View.GONE);
		}
		
		
		if(null != savedInstanceState){
			mLocationId = savedInstanceState.getInt(SubmitActivity.KEY_LOCATION_ID, 0);
			tvLocationName.setText(savedInstanceState.getString(SubmitActivity.KEY_LOCATION_NAME, ""));
		}
		else {
			mLocationId = mPoem.getLocation_id();
			Location location = Common.getLocationById(mPoem.getLocation_id(), this);
			if(null != location){
				
				tvLocationName.setText(location.getName());
//				tvPopupLocationName.setText(location.getName());
//				tvPopupLocationAddress.setText(location.getAddress());
			}
			else {
				Api.getLocationById(new ArrayList<NameValuePair>(), this, mLocationId);
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			}
		}
		
		if(null != savedInstanceState){
			mUriAvatar = savedInstanceState.getString(SubmitActivity.KEY_URL_PHOTO, "");
			edtCaption.setText(savedInstanceState.getString(SubmitActivity.KEY_CAPTION, ""));
		}
		else {
			if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0){
				mUriAvatar = mPoem.getPhotos()[0].getLinks().getOrg();
				edtCaption.setText(mPoem.getPhotos()[0].getCaption());
			}
		}
		
		if( !mUriAvatar.isEmpty()){
			
			if(! mUriAvatar.startsWith("http") && ! mUriAvatar.startsWith("https")){
				Options opts = new Options();
				opts.inSampleSize = 4;

				File tempFile = getTempFile(1);
				String filePath = tempFile.getAbsolutePath();
				
				Bitmap bm = BitmapFactory.decodeFile(filePath, opts);
				imgPhoto.setImageBitmap(bm);
			}
			else {
				Point point = new Point(0, 0);
		        Display display = getWindowManager().getDefaultDisplay();
		        display.getSize(point);
		        
				Common.loadImageView(imgPhoto, mUriAvatar, point.x/5, point.x/5, this);
			}
			
			rlImageLayout.setVisibility(View.VISIBLE);
		}
		else {
			rlImageLayout.setVisibility(View.GONE);
		}
		
	}
	
	/**
	 * Set type face for controls
	 */
	private void setTypeFaceControls() {
		((TextView) findViewById(R.id.tvStar1)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvStar2)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvStar3)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvLanguage)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvCategory)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvLanguageEng)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		
		((EditText) findViewById(R.id.edtTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvCategoryPublic)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvCategorySchool)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		
		((EditText) findViewById(R.id.edtContent)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((Button) findViewById(R.id.btnNext)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((Button) findViewById(R.id.btnSubmit)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((Button) findViewById(R.id.btnSave)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		tvCaption.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 edtCaption.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvLocationName.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
        
        LayoutParams lp1 = new LayoutParams(point.x/5, point.x/5);
		imgPhoto.setLayoutParams(lp1);
         
	}
	
//	
//	/**
//	 * Show popup
//	 * @param view
//	 */
//	private void showPopupImage(View view) {
//		mPopupWindow = new PopupWindow(this);
//	    mPopupWindow.setTouchable(true);
//	    mPopupWindow.setFocusable(true);
//	    mPopupWindow.setOutsideTouchable(true);
//	    mPopupWindow.setTouchInterceptor(new OnTouchListener() {
//	        public boolean onTouch(View v, MotionEvent event) {
//	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
//	            	mPopupWindow.dismiss();
//	                return true;
//	            }
//	            return false;
//	        }
//	    });
//	    mPopupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
//	    mPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//	    mPopupWindow.setOutsideTouchable(false);
//	    mPopupWindow.setContentView(mViewPopup);
//	    
//	    mPopupWindow.showAsDropDown(view, 0, 0);
//	}
//	
//	/**
//	 * Show popup
//	 * @param view
//	 */
//	private void showPopupLocation(View view) {
//		mPopupWindowLocation = new PopupWindow(this);
//		mPopupWindowLocation.setTouchable(true);
//		mPopupWindowLocation.setFocusable(true);
//		mPopupWindowLocation.setOutsideTouchable(true);
//		mPopupWindowLocation.setTouchInterceptor(new OnTouchListener() {
//	        public boolean onTouch(View v, MotionEvent event) {
//	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
//	            	mPopupWindowLocation.dismiss();
//	                return true;
//	            }
//	            return false;
//	        }
//	    });
//		mPopupWindowLocation.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
//		mPopupWindowLocation.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//		mPopupWindowLocation.setOutsideTouchable(false);
//		mPopupWindowLocation.setContentView(mViewPopupLocation);
//	    
//		mPopupWindowLocation.showAsDropDown(view, 0, 0);
//	}
	
	
	@Override
	public void onClick(View v) {
		Intent it;
		switch (v.getId()) {
		case R.id.btnNext:
			processUploadPoem(FLAG_NEXT);
			break;
		case R.id.btnGetImage:
//			openGallery(1);
			it = new Intent(EditPoemActivity.this, AddImageActivity.class);
			it.putExtra(AddImageActivity.KEY_PHOTO, mUriAvatar);
			it.putExtra(AddImageActivity.KEY_CAPTION, edtCaption.getText().toString());
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivityForResult(it, CODE_PICK_IMAGE);
			overridePendingTransition(0,0);
			break;
		case R.id.btnClose:
			imgPhoto.setImageResource(R.drawable.no_image);
			mUriAvatar = "";
			rlImageLayout.setVisibility(View.GONE);
			break;
		case R.id.btnMarkerLocation:
			it = new Intent(EditPoemActivity.this, LocationsActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivityForResult(it, LOCATION_CODE);
			overridePendingTransition(0,0);
			break;
		case R.id.btnSave:
			mFlagPostSattus = FLAG_SAVE;
			processUploadPoem(FLAG_SAVE);
			break;
		case R.id.btnSubmit:
			mFlagPostSattus = FLAG_SUBMIT;
			processUploadPoem(FLAG_SUBMIT);
			break;

		default:
			break;
		}
	}
	
	/**
	 * Process upload poem into server
	 * 
	 * @param isSubmit
	 */
	private void processUploadPoem(int flag) {
		
		EditText edtTitle = (EditText) findViewById(R.id.edtTitle);
		EditText edtContent = (EditText) findViewById(R.id.edtContent);
		
		if(edtTitle.getText().toString().trim().isEmpty()){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_fill_title_empty)).show();
			edtTitle.requestFocus();
			return;
		}
		
		if(edtContent.getText().toString().trim().isEmpty()){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_fill_content_empty)).show();
			edtContent.requestFocus();
			return;
		}
				
		if(mLocationId == 0){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_location_must_be_choose)).show();
			return;
		}
		
//		if(mUriAvatar.isEmpty() || edtPopupCaption.getText().toString().trim().isEmpty()){
//			createDialogMessage(getString(R.string.msg_error_title), 
//					getString(R.string.msg_error_must_be_choose_image)).show();
//			return;
//		}
		
		mPoem.setContent(edtContent.getText().toString().trim());
		mPoem.setTitle(edtTitle.getText().toString().trim());
		
		if(null == mPoem.getPhotos() || mPoem.getPhotos().length <= 0){
			PhotoPoem[] photo = new PhotoPoem[1];
			photo[0] = new PhotoPoem();
			mPoem.setPhotos(photo);
		}
		
//		if(!mUriAvatar.isEmpty()){
			Link linkObj = new Link();
			linkObj.setThumbnail(mUriAvatar);
			mPoem.getPhotos()[0].setLinks(linkObj);
			mPoem.getPhotos()[0].setCaption(edtCaption.getText().toString().trim());
//		}
		
		mPoem.setLocation_id(mLocationId);
		
		if(flag == FLAG_NEXT){

			Intent it = new Intent(EditPoemActivity.this, EditPoemEngActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			it.putExtra(KEY_POEM, mPoem);
			startActivity(it);
			overridePendingTransition(0,0);
			return;
		}
		
		try {
//			JSONObject jsonRequest = new JSONObject();
//			jsonRequest.put("id", ""+mPoem.getId());
//			jsonRequest.put("location_id", ""+mLocationId);
//			jsonRequest.put("author", mPoem.getAuthor());
//			jsonRequest.put("status_id", ""+(flag==FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH));
//			jsonRequest.put("category_id", ""+ mPoem.getCategory_id());
//			jsonRequest.put("poem_contents[0][language_id]", mPoem.getContents().get(0).getLanguage_id()+"");
//			jsonRequest.put("poem_contents[0][title]", mPoem.getContents().get(0).getTitle());
//			jsonRequest.put("poem_contents[0][content]", mPoem.getContents().get(0).getContent());
//			if(!mUriAvatar.isEmpty()){
//				if(! mUriAvatar.startsWith("http") && ! mUriAvatar.startsWith("https")){
//					jsonRequest.put("photo", ""+ mUriAvatar);
//				}
//				jsonRequest.put("caption", ""+ mPoem.getPhotos()[0].getCaption());
//				
//			}
//			else {
//				jsonRequest.put("photo", "-1");
//				jsonRequest.put("caption", "");
//			}
//			
//			Map<String, String> mapParams = new HashMap<String, String>();
//			mapParams.put("id", ""+mPoem.getId());
//			mapParams.put("location_id", ""+mLocationId);
//			mapParams.put("author", mPoem.getAuthor());
//			mapParams.put("status_id", ""+(flag==FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH));
//			mapParams.put("category_id", ""+ mPoem.getCategory_id());
//			mapParams.put("poem_contents[0][language_id]", mPoem.getContents().get(0).getLanguage_id()+"");
//			mapParams.put("poem_contents[0][title]", mPoem.getContents().get(0).getTitle());
//			mapParams.put("poem_contents[0][content]", mPoem.getContents().get(0).getContent());
//			if(!mUriAvatar.isEmpty()){
//				if(! mUriAvatar.startsWith("http") && ! mUriAvatar.startsWith("https")){
//					mapParams.put("photo", ""+ mUriAvatar);
//				}
//				mapParams.put("caption", ""+ mPoem.getPhotos()[0].getCaption());
//			}
//			else {
//				mapParams.put("photo", "-1");
//				mapParams.put("caption", "");
//			}
//			
//			Api.putUpdatePoem(this, mapParams, jsonRequest);
//			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			
			
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("id", ""+mPoem.getId()));
			nameValuePairs.add(new BasicNameValuePair("location_id", ""+mLocationId));
			nameValuePairs.add(new BasicNameValuePair("author", mPoem.getAuthor()));
			nameValuePairs.add(new BasicNameValuePair("status_id", ""+(flag==FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH)));
			nameValuePairs.add(new BasicNameValuePair("category_id", ""+ mPoem.getCategory_id()));
			nameValuePairs.add(new BasicNameValuePair("language_id", mPoem.getLanguage_id()+""));
			nameValuePairs.add(new BasicNameValuePair("title", mPoem.getTitle()));
			nameValuePairs.add(new BasicNameValuePair("content", mPoem.getContent()));
			if(!mUriAvatar.isEmpty()){
				if(! mUriAvatar.startsWith("http") && ! mUriAvatar.startsWith("https")){
					nameValuePairs.add(new BasicNameValuePair("photo", ""+ mUriAvatar));
				}
//				if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0){
					nameValuePairs.add(new BasicNameValuePair("caption", ""+ mPoem.getPhotos()[0].getCaption()));
//				}
			}
			else {
				nameValuePairs.add(new BasicNameValuePair("photo", "-1"));
				nameValuePairs.add(new BasicNameValuePair("caption", ""));
			}
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			new UpdatePoemAsyncTask(this, nameValuePairs, mPoem.getId(), false).execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	/**
	 * Get file image temp
	 * 
	 * @param type
	 * @return
	 */
	private File getTempFile(int type) {
		if (isSDCARDMounted()) {

			File f = new File(Environment.getExternalStorageDirectory(),
					"temp_" + type + ".jpg");
			try {
				f.createNewFile();
			} catch (IOException e) {

			}
			return f;
		} else {
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_no_sdcard)).show();;
			return null;
		}
	}

	/**
	 * Check SDCard is mounted
	 * 
	 * @return
	 */
	private boolean isSDCARDMounted() {
		String status = Environment.getExternalStorageState();
		if (status.equals(Environment.MEDIA_MOUNTED))
			return true;
		return false;
	}

}
