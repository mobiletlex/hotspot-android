package com.epsilon.textinthecity;

import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.squareup.otto.Subscribe;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SubmitInputInfoActivity extends BaseActivity implements OnClickListener {
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_submit_input_info);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_submit), false);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SUBMIT);
		
		initControls();
		
		setTypeFaceControls();
		
		setupWidthCenter();
	}
	
	@Override
	protected void onDestroy() {
//		PoemHelper.getInstance().unregister(this);
		super.onDestroy();
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		
		((Button) findViewById(R.id.btnBack)).setOnClickListener(this);
		((Button) findViewById(R.id.btnNext)).setOnClickListener(this);
	}
	
	private void setTypeFaceControls() {
		((TextView) findViewById(R.id.tvStar2)).setTypeface(PoemApplication.getInstance().typeFaceFuturaMedium);
		
		((TextView) findViewById(R.id.tvSubmitEngLabel)).setTypeface(PoemApplication.getInstance().typeFaceFuturaMedium);
		((TextView) findViewById(R.id.tvTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaMedium);
		
		((TextView) findViewById(R.id.edtContent)).setTypeface(PoemApplication.getInstance().typeFaceFuturaMedium);
		
		((Button) findViewById(R.id.btnNext)).setTypeface(PoemApplication.getInstance().typeFaceFuturaMedium);
		((Button) findViewById(R.id.btnBack)).setTypeface(PoemApplication.getInstance().typeFaceFuturaMedium);
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {
	    
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnBack:
			onBackPressed();
			break;
		case R.id.btnNext:
			
			break;

		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	

}
