package com.epsilon.textinthecity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			
			PlaceholderFragment fragment = new PlaceholderFragment();
			fragmentTransaction.add(R.id.container, fragment, "1");
			
			fragmentTransaction.commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			
			((Button) rootView.findViewById(R.id.btnMove)).setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent it = new Intent(MainActivity.this, ExploreActivity.class);
					it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					it.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
					it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					startActivity(it);
					overridePendingTransition(0,0);
				}
			});
			
			return rootView;
		}
	}

}
