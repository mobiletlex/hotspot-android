package com.epsilon.textinthecity.helper;

import com.squareup.otto.Bus;

public class PoemHelper {
	 
	private static final Bus POEM_BUS = new Bus();
	 
	  public static Bus getInstance() {
	    return POEM_BUS;
	  }
}