package com.epsilon.textinthecity.helper;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;

import com.epsilon.textinthecity.PoemDetailActivity;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.adapter.ShareIntentListAdapter;
import com.epsilon.textinthecity.common.Constant;


public class ShareHelper {
	Context context;
	String subject;
	String body;
	String link;
	String image;
	String subjectMail;
	String bodyMail;
	public ShareHelper(Context context, String subject, String body, String link, String image) {
		this.context = context;
		this.subject = subject;
		this.body = body;
		this.link = link;
		this.image = image;
	}
	
	public ShareHelper(Context context, String subject, String body, 
								String link, String image, String subjectMail, String bodyMail) {
		this.context = context;
		this.subject = subject;
		this.body = body;
		this.link = link;
		this.image = image;
		this.subjectMail = subjectMail;
		this.bodyMail = bodyMail;
	}
	
	public void share() {
		Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
		sendIntent.setType("text/plain");
		List<ResolveInfo> activities = context.getPackageManager().queryIntentActivities(sendIntent, 0);
		
		Object[] arrApp = activities.toArray();
		
		boolean hasFacebook = false, hasTwitter = false, hasGooglePlus = false, hasInstagram = false;
		 for ( ResolveInfo info : activities){
			 Log.e("CongVC", "====Share===" + info.activityInfo.packageName + "======" + info.activityInfo.name);
			
//		    	if(hasFacebook && hasGooglePlus && hasTwitter && hasInstagram){
//		    		break;
//		    	}
		    	
	//	    	if(info.activityInfo.packageName.contains("facebook")){
		    	if (info.activityInfo.packageName.contains("com.facebook.katana")) {
		    		hasFacebook = true;
		    	}
	//	    	else if(info.activityInfo.packageName.contains("twitter")){
		    	else if(info.activityInfo.packageName.contains("com.twitter.android")){
		    		hasTwitter = true;
		    	}
	//	    	else if(info.activityInfo.packageName.contains("plus")){
		    	else if(info.activityInfo.packageName.contains("com.google.android.apps.plus")){
		    		hasGooglePlus = true;
		    	}
		    	else if(info.activityInfo.packageName.contains("instagram")){
		    		hasInstagram = true;
		    	}
		    }
		 
		 Object[] arrAppFull;
		
		 
		 if(hasFacebook && hasGooglePlus && hasTwitter && hasInstagram){
	 		arrAppFull = arrApp;
	 	}
		 else {
			 int number = 0;
			 number += hasFacebook?0:1;
			 number += hasTwitter?0:1;
			 number += hasGooglePlus?0:1;
			 number += hasInstagram?0:1;
			 
			 arrAppFull = new Object[arrApp.length+number];
			 
			 number = 0;
			 if(!hasFacebook){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.facebook);
				 hashApp.put("icon", R.drawable.fb);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 if(!hasTwitter){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.twitter);
				 hashApp.put("icon", R.drawable.tw);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 if(!hasGooglePlus){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.google);
				 hashApp.put("icon", R.drawable.google);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 if(!hasInstagram){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.instagram);
				 hashApp.put("icon", R.drawable.in);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 
			 for (Object object : arrApp) {
				arrAppFull[number] = object;
				number++;
			}
		 }
		
	//	arrApp.
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Share with...");
		final ShareIntentListAdapter adapter = new ShareIntentListAdapter((Activity)context, R.layout.item_share, arrAppFull);
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(adapter.getItem(which) instanceof ResolveInfo){
					ResolveInfo info = (ResolveInfo) adapter.getItem(which);
					Intent intent = new Intent(android.content.Intent.ACTION_SEND);
					intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
					intent.setType("text/plain");
					intent.putExtra(Intent.EXTRA_SUBJECT, subject);
					intent.putExtra(Intent.EXTRA_TEXT, body);
					((Activity)context).startActivity(intent);
				}
				else {
					
					HashMap<String, Integer> app = (HashMap<String, Integer>) adapter.getItem(which);
					
					String appPackageName = "";
					
					if(app.get("title") == R.string.facebook){
						appPackageName = "com.facebook.katana";
					}
					else if(app.get("title") == R.string.google){
						appPackageName = "com.google.android.apps.plus";
					}
					else if(app.get("title") == R.string.twitter){
						appPackageName = "com.twitter.android";
					}
					else if(app.get("title") == R.string.instagram){
						appPackageName = "com.instagram.android";
					}
					
					try {
						((Activity)context).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
					} catch (android.content.ActivityNotFoundException anfe) {
						((Activity)context).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
					}
				}
				
			}
			
		});
		Dialog dialog = builder.create();
		
		dialog.show();
	}
	
	public void share1(final String poemTitle, final String author) {
		Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
		sendIntent.setType("text/plain");
		List<ResolveInfo> activities = context.getPackageManager().queryIntentActivities(sendIntent, 0);
		
		Object[] arrAppFull = new Object[4];
		 int number = 0;
		
		boolean hasFacebook = false, hasTwitter = false, hasGmail = false, hasEmail = false;
		 for ( ResolveInfo info : activities){
			 
			 
		    	if(hasFacebook && hasGmail && hasTwitter && hasEmail){
		    		break;
		    	}
		    	if (info.activityInfo.packageName.contains("com.facebook.katana")) {
		    		hasFacebook = true;
		    		arrAppFull[number] = info;
		    		number++;
		    	}
		    	else if(info.activityInfo.packageName.contains("com.twitter.android")){
		    		hasTwitter = true;
		    		arrAppFull[number] = info;
		    		number++;
		    	}
		    	else if (info.activityInfo.packageName.endsWith(".gm") ||
				          info.activityInfo.name.toLowerCase().contains("gmail")){
		    		hasGmail = true;
		    		arrAppFull[number] = info;
		    		number++;
		    	}
		    	else if(info.activityInfo.packageName.contains("com.android.email")){
		    		hasEmail = true;
		    		arrAppFull[number] = info;
		    		number++;
		    	}
		    }
		 
		 
		
		 
		 
			 if(!hasFacebook){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.facebook);
				 hashApp.put("icon", R.drawable.fb);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 if(!hasTwitter){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.twitter);
				 hashApp.put("icon", R.drawable.tw);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 if(!hasGmail){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.gmail);
				 hashApp.put("icon", R.drawable.gmail);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 if(!hasEmail){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.email);
				 hashApp.put("icon", R.drawable.email);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 
//			 for (Object object : arrApp) {
//				arrAppFull[number] = object;
//				number++;
//			}
				
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Share with...");
		final ShareIntentListAdapter adapter = new ShareIntentListAdapter((Activity)context, R.layout.item_share, arrAppFull);
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(adapter.getItem(which) instanceof ResolveInfo){
					
					ResolveInfo info = (ResolveInfo) adapter.getItem(which);
					
					String strBody = "";
					
					if(info.activityInfo.packageName.contains("com.twitter.android")){
						strBody = context.getString(R.string.share_poem_twitter, 
								poemTitle, 
								author, 
								"",
								"");
//						int lengthImage = 0;
//						if(null != image && !image.isEmpty() && image.length() > 0){
//							lengthImage = 23;
//						}
//						int lengthBody = strBody.length() + lengthImage + 46;
						int lengthBody = strBody.length() + 46;
						String titleTruncate = poemTitle;
						if(lengthBody > 140){
							try {
								titleTruncate = poemTitle.substring(0, poemTitle.length() - (lengthBody - 137)) + "...";
							} catch (Exception e) {
								titleTruncate = "...";
								e.printStackTrace();
							}
						}
						strBody = context.getString(R.string.share_poem_twitter, 
								titleTruncate, 
								author, 
								Constant.URL_APPLE_STORE,
								Constant.URL_GOOGLE_PLAY_STORE);
					}
					
					if(info.activityInfo.packageName.contains("com.facebook.katana")){
						((PoemDetailActivity)context).processShareFacebook(bodyMail, subjectMail, image, link);
						return;
					}
					
					
					Intent intent = new Intent(android.content.Intent.ACTION_SEND);
					intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
					intent.setType("image/png");
					if(info.activityInfo.packageName.contains("com.twitter.android")){
						intent.putExtra(Intent.EXTRA_SUBJECT, subject.isEmpty()?link:(subject));
						intent.putExtra(Intent.EXTRA_TEXT, strBody);
					}
					else{
						intent.putExtra(Intent.EXTRA_SUBJECT, subjectMail);
						intent.putExtra(Intent.EXTRA_TEXT, bodyMail);
						if(null != image && !image.isEmpty() && image.length() > 0){
							intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(image)));
						}
					}
					
					((Activity)context).startActivity(intent);
					
					((PoemDetailActivity)context).trackShareGoogleAnalytics();;
				}
				else {
					
					HashMap<String, Integer> app = (HashMap<String, Integer>) adapter.getItem(which);
					
					String appPackageName = "";
					
					if(app.get("title") == R.string.facebook){
						appPackageName = "com.facebook.katana";
					}
					else if(app.get("title") == R.string.twitter){
						appPackageName = "com.twitter.android";
					}
					else if(app.get("title") == R.string.gmail){
						appPackageName = "com.google.android.gm";
					}
					else if(app.get("title") == R.string.email){
						appPackageName = "com.android.email";
					}
					
					try {
						((Activity)context).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
					} catch (android.content.ActivityNotFoundException anfe) {
						((Activity)context).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
					}
				}
				
			}
			
		});
		Dialog dialog = builder.create();
		
		dialog.show();
	}
	
	public void share2() {
		Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
		sendIntent.setType("text/plain");
		List<ResolveInfo> activities = context.getPackageManager().queryIntentActivities(sendIntent, 0);
		
		Object[] arrAppFull = new Object[4];
		 int number = 0;
		
		boolean hasFacebook = false, hasTwitter = false, hasGmail = false, hasEmail = false;
		 for ( ResolveInfo info : activities){
			 
			 
		    	if(hasFacebook && hasGmail && hasTwitter && hasEmail){
		    		break;
		    	}
		    	if (info.activityInfo.packageName.contains("com.facebook.katana")) {
		    		hasFacebook = true;
		    		arrAppFull[number] = info;
		    		number++;
		    	}
		    	else if(info.activityInfo.packageName.contains("com.twitter.android")){
		    		hasTwitter = true;
		    		arrAppFull[number] = info;
		    		number++;
		    	}
		    	else if (info.activityInfo.packageName.endsWith(".gm") ||
				          info.activityInfo.name.toLowerCase().contains("gmail")){
		    		hasGmail = true;
		    		arrAppFull[number] = info;
		    		number++;
		    	}
		    	else if(info.activityInfo.packageName.contains("com.android.email")){
		    		hasEmail = true;
		    		arrAppFull[number] = info;
		    		number++;
		    	}
		    }
		 
		 
		
		 
		 
			 if(!hasFacebook){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.facebook);
				 hashApp.put("icon", R.drawable.fb);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 if(!hasTwitter){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.twitter);
				 hashApp.put("icon", R.drawable.tw);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 if(!hasGmail){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.gmail);
				 hashApp.put("icon", R.drawable.gmail);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 if(!hasEmail){
				 HashMap<String, Integer> hashApp = new HashMap<String, Integer>();
				 hashApp.put("title", R.string.email);
				 hashApp.put("icon", R.drawable.email);
				 arrAppFull[number] = hashApp;
				 number++;
			 }
			 
//			 for (Object object : arrApp) {
//				arrAppFull[number] = object;
//				number++;
//			}
				
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Share with...");
		final ShareIntentListAdapter adapter = new ShareIntentListAdapter((Activity)context, R.layout.item_share, arrAppFull);
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(adapter.getItem(which) instanceof ResolveInfo){
					
					ResolveInfo info = (ResolveInfo) adapter.getItem(which);
					
					String strBody = "";
					if(body.isEmpty() || body.length() == 0){
						if(info.activityInfo.packageName.contains("com.twitter.android")){
							if((subject.length()+link.length()) > 140){
								try {
									strBody = subject.substring(0, 140-link.length()) + "\n" + link;
								} catch (Exception e) {
									strBody = link;
								}
							}
							else {
								strBody = subject + "\n" + link;
							}
						}
						else if(info.activityInfo.packageName.contains("com.facebook.katana")){
							strBody = subject + "\n" + link;
						}
						else {
							strBody = link;
						}
					}
					else {
						try {
							int lengthImage = 0;
							if(null != image && !image.isEmpty() && image.length() > 0){
								lengthImage = 23;
							}
							
							if(((body.length()+"\n".length()+link.length()+lengthImage+"\n".length()+subject.length()) <= 137) ||
									! info.activityInfo.packageName.contains("com.twitter.android")){
								if(info.activityInfo.packageName.contains("com.twitter.android") ||
										info.activityInfo.packageName.contains("com.facebook.katana")){
									strBody = subject + "\n" + body + "\n" + link;
								}
								else {
									strBody = body + "\n" + link;
								}
							}
							else {
								if(((link.length()+lengthImage+"\n".length()+subject.length()) < 137)){
									strBody = subject + "\n" + body.substring(0, (137-"\n".length())-link.length()-lengthImage-subject.length())+ "...\n" + link;
								}
								else {
									strBody = subject.substring(0, (137-"\n".length())-link.length()-lengthImage)+ "...\n" + link;
								}
							}
						} catch (Exception e) {
							strBody = subject +"\n"+link;
						}
					}
					
					if(info.activityInfo.packageName.contains("com.facebook.katana")){
						((PoemDetailActivity)context).processShareFacebook(strBody, subject, image, link);
						return;
					}
					
					
					Intent intent = new Intent(android.content.Intent.ACTION_SEND);
					intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
					intent.setType("image/png");
					if(info.activityInfo.packageName.contains("com.twitter.android")){
						intent.putExtra(Intent.EXTRA_SUBJECT, subject.isEmpty()?link:(subject));
						intent.putExtra(Intent.EXTRA_TEXT, strBody);
					}
					else{
						intent.putExtra(Intent.EXTRA_SUBJECT, subjectMail);
						intent.putExtra(Intent.EXTRA_TEXT, bodyMail);
					}
					
					if(null != image && !image.isEmpty() && image.length() > 0){
						intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(image)));
					}
					((Activity)context).startActivity(intent);
					
					((PoemDetailActivity)context).trackShareGoogleAnalytics();;
				}
				else {
					
					HashMap<String, Integer> app = (HashMap<String, Integer>) adapter.getItem(which);
					
					String appPackageName = "";
					
					if(app.get("title") == R.string.facebook){
						appPackageName = "com.facebook.katana";
					}
					else if(app.get("title") == R.string.twitter){
						appPackageName = "com.twitter.android";
					}
					else if(app.get("title") == R.string.gmail){
						appPackageName = "com.google.android.gm";
					}
					else if(app.get("title") == R.string.email){
						appPackageName = "com.android.email";
					}
					
					try {
						((Activity)context).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
					} catch (android.content.ActivityNotFoundException anfe) {
						((Activity)context).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
					}
				}
				
			}
			
		});
		Dialog dialog = builder.create();
		
		dialog.show();
	}
}