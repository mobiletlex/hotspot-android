package com.epsilon.textinthecity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.task.PostPoemAsyncTask;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputFilter;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class SubmitActivity extends BaseActivity implements OnClickListener {
	
	public static final String KEY_LANGUAGE_ID = "KEY_LANGUAGE_ID";
	public static final String KEY_CATEGORY = "KEY_CATEGORY";
	public static final String KEY_TITLE = "KEY_TITLE";
	public static final String KEY_CONTENT = "KEY_CONTENT";
	public static final String KEY_LOCATION_ID = "KEY_LOCATION_ID";
	public static final String KEY_LOCATION_NAME = "KEY_LOCATION_NAME";
	public static final String KEY_CAPTION = "KEY_CAPTION";
	public static final String KEY_URL_PHOTO = "KEY_URL_PHOTO";
	
	private static final int LOGIN_CODE = 100;
	private static final int CODE_PICK_IMAGE = 101;
	private static final int LOCATION_CODE = 102;
	
	public static final int FLAG_NEXT = 1;
	public static final int FLAG_SAVE = 2;
	public static final int FLAG_SUBMIT = 3;
	
	private int mLanguageId = Constant.LANGUAGE_ID_ENG;
	private int mCategory = Constant.CATEGORY_ID_PUBLIC;
	private int mLocationId = 0;
	

//	private PopupWindow mPopupWindow;
//	private View mViewPopup;
//	private PopupWindow mPopupWindowLocation;
//	private View mViewPopupLocation;
	
//	private TextView tvPopupCaption;
//	private EditText edtPopupCaption;
//	private Button btnPopupLoadPhoto;
//	private ImageView imgSample;
	
	private ImageView imgPhoto;
	private Button btnClose;
	private TextView tvCaption;
	private EditText edtCaption;
	private RelativeLayout rlImageLayout;
	
//	private TextView tvPopupLocation;
//	private TextView tvPopupLocationName;
//	private TextView tvPopupLocationAddress;
//	private Button btnPopupLocation;
	private TextView tvLocationName;
	
	private String mUriAvatar = "";
	
	private ImageView imgLoading;
	private int mFlagPostSattus = FLAG_SAVE;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_submit);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_submit), false);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SUBMIT);
		
		Tracker t = PoemApplication.getInstance().getTracker(
                TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
        	.setCategory(getString(R.string.google_analytics_event_category_tab_open))
        	.setAction(getString(R.string.google_analytics_event_action_open))
        	.setLabel(getString(R.string.google_analytics_event_open_tab_submit)).build());
		
		initControls();
				
		setTypeFaceControls();
		
		setupWidthCenter();
		
		rlImageLayout.setVisibility(View.GONE);
		tvLocationName.setText("");
		
		if(! Common.checkLogin(this)){
			Intent it = new Intent(SubmitActivity.this, LoginActivity.class);
			it.putExtra(LoginActivity.TAB_KEY, Constant.TAB_MENU_INDEX_SUBMIT);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivityForResult(it, LOGIN_CODE);
			overridePendingTransition(0,0);
		}
		
		if(null != savedInstanceState){
			initData(savedInstanceState);
		}
		
		PoemHelper.getInstance().register(this);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		
		if(null != savedInstanceState){
			initData(savedInstanceState);
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PoemHelper.getInstance().register(this);
				
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			PoemHelper.getInstance().unregister(this);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	protected void onDestroy() {
		try {
			PoemHelper.getInstance().unregister(this);
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onDestroy();
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		
		EditText edtTitle = (EditText) findViewById(R.id.edtTitle);
		EditText edtContent = (EditText) findViewById(R.id.edtContent);
		
		savedInstanceState.putInt(KEY_LANGUAGE_ID, mLanguageId);
		savedInstanceState.putString(KEY_TITLE, edtTitle.getText().toString());
		savedInstanceState.putInt(KEY_CATEGORY, mCategory);
		savedInstanceState.putString(KEY_CONTENT, edtContent.getText().toString());
		savedInstanceState.putInt(KEY_LOCATION_ID, mLocationId);
		savedInstanceState.putString(KEY_LOCATION_NAME, tvLocationName.getText().toString());
		if(!mUriAvatar.isEmpty()){
			savedInstanceState.putString(KEY_CAPTION, edtCaption.getText().toString());
			savedInstanceState.putString(KEY_URL_PHOTO, mUriAvatar);
		}
		
		super.onSaveInstanceState(savedInstanceState);
	}
	
	private void initData(Bundle savedInstanceState) {
		
		mLanguageId = savedInstanceState.getInt(KEY_LANGUAGE_ID, Constant.LANGUAGE_ID_ENG);
		
		initLanguage();
		
		((EditText) findViewById(R.id.edtTitle)).setText(savedInstanceState.getString(KEY_TITLE, ""));
		((EditText) findViewById(R.id.edtContent)).setText(savedInstanceState.getString(KEY_CONTENT, ""));
		
		mCategory = savedInstanceState.getInt(KEY_CATEGORY, Constant.CATEGORY_ID_PUBLIC);
		
		initCategoty();
		
		mLocationId = savedInstanceState.getInt(KEY_LOCATION_ID, 0);
		
		tvLocationName.setText(savedInstanceState.getString(KEY_LOCATION_NAME, ""));
		
		if(savedInstanceState.containsKey(KEY_URL_PHOTO)){
			
			rlImageLayout.setVisibility(View.VISIBLE);
			
			edtCaption.setText(savedInstanceState.getString(KEY_CAPTION, ""));
			
			Options opts = new Options();
			opts.inSampleSize = 4;

			File tempFile = getTempFile(1);
			String filePath = tempFile.getAbsolutePath();
			
			mUriAvatar = savedInstanceState.getString(KEY_URL_PHOTO, "");

			Bitmap bm = BitmapFactory.decodeFile(filePath, opts);
			imgPhoto.setImageBitmap(bm);
			
			
		}
		else {
			rlImageLayout.setVisibility(View.GONE);
		}
		
	}
	
	private void initLanguage() {
		if(mLanguageId == Constant.LANGUAGE_ID_ENG){
			
			processLanguageChoose(R.color.tab_meu_background_color, 
								R.color.tab_meu_background_focus_color, 
								R.color.tab_meu_background_focus_color, 
								R.color.tab_meu_background_focus_color, 
								View.VISIBLE, 
								View.VISIBLE, 
								View.GONE);
		}
		else if(mLanguageId == Constant.LANGUAGE_ID_CHINA){
		
			processLanguageChoose(R.color.tab_meu_background_focus_color,
								R.color.tab_meu_background_color, 
								R.color.tab_meu_background_focus_color, 
								R.color.tab_meu_background_focus_color, 
								View.GONE, 
								View.GONE, 
								View.VISIBLE);
		}
		else if(mLanguageId == Constant.LANGUAGE_ID_MALAY){
		
			processLanguageChoose(R.color.tab_meu_background_focus_color,
								R.color.tab_meu_background_focus_color,
								R.color.tab_meu_background_color,  
								R.color.tab_meu_background_focus_color, 
								View.GONE, 
								View.GONE, 
								View.VISIBLE);
		}
		else if(mLanguageId == Constant.LANGUAGE_ID_TAMIL){
		
			processLanguageChoose(R.color.tab_meu_background_focus_color,
								R.color.tab_meu_background_focus_color, 
								R.color.tab_meu_background_focus_color,
								R.color.tab_meu_background_color,  
								View.GONE, 
								View.GONE, 
								View.VISIBLE);
		}
	}
	
	private void initCategoty() {
		
		if(mCategory == Constant.CATEGORY_ID_PUBLIC){
		
			((TextView) findViewById(R.id.tvCategoryPublic)).setTextColor(
					getResources().getColor(R.color.tab_meu_background_color));
			((TextView) findViewById(R.id.tvCategorySchool)).setTextColor(
					getResources().getColor(R.color.tab_meu_background_focus_color));
		
		}
		else if(mCategory == Constant.CATEGORY_ID_SCHOOL){
		
			((TextView) findViewById(R.id.tvCategoryPublic)).setTextColor(
					getResources().getColor(R.color.tab_meu_background_focus_color));
			((TextView) findViewById(R.id.tvCategorySchool)).setTextColor(
					getResources().getColor(R.color.tab_meu_background_color));
		}
	}
		
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {

		// Result for list location by zone
		if(null != event.getResult() && event.getResult().equals(Constant.POEM_ADD_ERROR_DEF)){
						
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_poem_add_fail)).show();
		}
		// add new poem success
		else if(null != event.getPoem()){
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			
			Intent it;
			if(mFlagPostSattus == FLAG_SAVE){
				Toast.makeText(this, getString(R.string.msg_poem_save_draft_successful), Toast.LENGTH_LONG).show();
				it = new Intent(SubmitActivity.this, MyEntriesActivity.class);
			}
			else{
				Tracker t = PoemApplication.getInstance().getTracker(
		                TrackerName.APP_TRACKER);
		        t.send(new HitBuilders.EventBuilder()
		        	.setCategory(getString(R.string.google_analytics_event_category_poem))
		        	.setAction(getString(R.string.google_analytics_event_action_submit))
		        	.setLabel(
		        			SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME) + ", " +
		        			event.getPoem().getId()+"")
		        	.build());
				
				Toast.makeText(this, getString(R.string.msg_poem_add_successful), Toast.LENGTH_LONG).show();
				it = new Intent(SubmitActivity.this, ShareActivity.class);
				it.putExtra(ShareActivity.KEY_POEM, event.getPoem());
				it.putExtra(ShareActivity.KEY_SHARE_IMAGE, mUriAvatar);
			}
			it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			finish();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
				
		if(requestCode == LOGIN_CODE && resultCode == RESULT_CANCELED) {
			finish();
		}
		else if(requestCode == CODE_PICK_IMAGE){
			
			if (resultCode == RESULT_OK && data != null) {
				
				mUriAvatar = data.getStringExtra(AddImageActivity.KEY_PHOTO);
				String caption = data.getStringExtra(AddImageActivity.KEY_CAPTION);
								
				edtCaption.setText(caption);
				
				if(mUriAvatar.isEmpty()){
					imgPhoto.setBackgroundResource(R.drawable.no_image);
					rlImageLayout.setVisibility(View.GONE);
				}
				else {
					Options opts = new Options();
					opts.inSampleSize = 4;

//					File tempFile = getTempFile(1);
//					String filePath = tempFile.getAbsolutePath();
				
//					Bitmap bm = BitmapFactory.decodeFile(filePath, opts);
					Bitmap bm = BitmapFactory.decodeFile(mUriAvatar, opts);
					
					imgPhoto.setImageBitmap(bm);
					
					rlImageLayout.setVisibility(View.VISIBLE);
				}

//				Options opts = new Options();
//				opts.inSampleSize = 4;
//
//				File tempFile = getTempFile(1);
//				String filePath = tempFile.getAbsolutePath();
//				
//				mUriAvatar = filePath;
//
//				Bitmap bm = BitmapFactory.decodeFile(filePath, opts);
//				imgPhoto.setImageBitmap(bm);
//				
//				rlImageLayout.setVisibility(View.VISIBLE);

			}
		}
		else if(requestCode == LOCATION_CODE && resultCode == RESULT_OK){
			if(null != data) {
				mLocationId = data.getIntExtra(LocationsActivity.KEY_LOCATION_ID, 0);
				
				tvLocationName.setText(data.getStringExtra(LocationsActivity.KEY_LOCATION_NAME));
//				tvPopupLocationName.setText(data.getStringExtra(LocationsActivity.KEY_LOCATION_NAME));
//				tvPopupLocationAddress.setText(data.getStringExtra(LocationsActivity.KEY_LOCATION_ADDRESS));
			}
		}
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){

		((Button) findViewById(R.id.btnNext)).setOnClickListener(this);
		((Button) findViewById(R.id.btnSave)).setOnClickListener(this);
		((Button) findViewById(R.id.btnSubmit)).setOnClickListener(this);
		((Button) findViewById(R.id.btnGetImage)).setOnClickListener(this);
		((Button) findViewById(R.id.btnMarkerLocation)).setOnClickListener(this);
		
		((TextView) findViewById(R.id.tvLanguageEng)).setOnClickListener(mEventLanguageClick);
		((TextView) findViewById(R.id.tvLanguageChina)).setOnClickListener(mEventLanguageClick);
		((TextView) findViewById(R.id.tvLanguageMalay)).setOnClickListener(mEventLanguageClick);
		((TextView) findViewById(R.id.tvLanguageTamil)).setOnClickListener(mEventLanguageClick);
		
		((TextView) findViewById(R.id.tvCategoryPublic)).setOnClickListener(mEventCategoryClick);
		((TextView) findViewById(R.id.tvCategorySchool)).setOnClickListener(mEventCategoryClick);
		
		tvCaption = (TextView) findViewById(R.id.tvCaption);
		 edtCaption = (EditText) findViewById(R.id.edtCaption);
		 btnClose = (Button) findViewById(R.id.btnClose);
		 imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
		 rlImageLayout = (RelativeLayout) findViewById(R.id.rlImagePhoto);
		 
		 btnClose.setOnClickListener(this);
		 
		 edtCaption.setFocusable(false);
		edtCaption.setEnabled(false);
		 
		 tvLocationName = (TextView) findViewById(R.id.tvLocation);
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		InputFilter[] iFilter = new InputFilter[1];
		// Filter input title
		iFilter[0] = new InputFilter.LengthFilter(Constant.LENGTH_INPUT_POEM_TITLE);
		((EditText) findViewById(R.id.edtTitle)).setFilters(iFilter);
	}
	
	/**
	 * Set type face for controls
	 */
	private void setTypeFaceControls() {
		((TextView) findViewById(R.id.tvStar1)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvStar2)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvStar3)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvLanguage)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvCategory)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvLanguageEng)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvLanguageChina)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvLanguageMalay)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvLanguageTamil)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		
		((EditText) findViewById(R.id.edtTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvCategoryPublic)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvCategorySchool)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		
		((EditText) findViewById(R.id.edtContent)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((Button) findViewById(R.id.btnNext)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((Button) findViewById(R.id.btnSubmit)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((Button) findViewById(R.id.btnSave)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		tvCaption.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		edtCaption.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		tvLocationName.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
        
        LayoutParams lp1 = new LayoutParams(point.x/5, point.x/5);
		imgPhoto.setLayoutParams(lp1);
         
	}
	
	
//	/**
//	 * Show popup
//	 * @param view
//	 */
//	public void showPopupImage(View view) {
//		mPopupWindow = new PopupWindow(this);
//	    mPopupWindow.setTouchable(true);
//	    mPopupWindow.setFocusable(true);
//	    mPopupWindow.setOutsideTouchable(true);
//	    mPopupWindow.setTouchInterceptor(new OnTouchListener() {
//	        public boolean onTouch(View v, MotionEvent event) {
//	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
//	            	mPopupWindow.dismiss();
//	                return true;
//	            }
//	            return false;
//	        }
//	    });
//	    mPopupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
//	    mPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//	    mPopupWindow.setOutsideTouchable(false);
//	    mPopupWindow.setContentView(mViewPopup);
//	    
//	    mPopupWindow.showAsDropDown(view, 0, 0);
//	}
//	
//	/**
//	 * Show popup
//	 * @param view
//	 */
//	public void showPopupLocation(View view) {
//		mPopupWindowLocation = new PopupWindow(this);
//		mPopupWindowLocation.setTouchable(true);
//		mPopupWindowLocation.setFocusable(true);
//		mPopupWindowLocation.setOutsideTouchable(true);
//		mPopupWindowLocation.setTouchInterceptor(new OnTouchListener() {
//	        public boolean onTouch(View v, MotionEvent event) {
//	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
//	            	mPopupWindowLocation.dismiss();
//	                return true;
//	            }
//	            return false;
//	        }
//	    });
//		mPopupWindowLocation.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
//		mPopupWindowLocation.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//		mPopupWindowLocation.setOutsideTouchable(false);
//		mPopupWindowLocation.setContentView(mViewPopupLocation);
//	    
//		mPopupWindowLocation.showAsDropDown(view, 0, 0);
//	}
	
	/**
	 * Event click textview language
	 */
	private OnClickListener mEventLanguageClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.tvLanguageEng:
				mLanguageId = Constant.LANGUAGE_ID_ENG;
				
				processLanguageChoose(R.color.tab_meu_background_color, 
									R.color.tab_meu_background_focus_color, 
									R.color.tab_meu_background_focus_color, 
									R.color.tab_meu_background_focus_color, 
									View.VISIBLE, 
									View.VISIBLE, 
									View.GONE);
				
				break;
				
			case R.id.tvLanguageChina:
				mLanguageId = Constant.LANGUAGE_ID_CHINA;
				
				processLanguageChoose(R.color.tab_meu_background_focus_color,
									R.color.tab_meu_background_color, 
									R.color.tab_meu_background_focus_color, 
									R.color.tab_meu_background_focus_color, 
									View.GONE, 
									View.GONE, 
									View.VISIBLE);
				
				break;
			case R.id.tvLanguageMalay:
				mLanguageId = Constant.LANGUAGE_ID_MALAY;
				
				processLanguageChoose(R.color.tab_meu_background_focus_color,
									R.color.tab_meu_background_focus_color,
									R.color.tab_meu_background_color,  
									R.color.tab_meu_background_focus_color, 
									View.GONE, 
									View.GONE, 
									View.VISIBLE);
				
				break;
			case R.id.tvLanguageTamil:
				mLanguageId = Constant.LANGUAGE_ID_TAMIL;
				
				processLanguageChoose(R.color.tab_meu_background_focus_color,
									R.color.tab_meu_background_focus_color, 
									R.color.tab_meu_background_focus_color,
									R.color.tab_meu_background_color,  
									View.GONE, 
									View.GONE, 
									View.VISIBLE);
				
				break;

			default:
				break;
			}
		}
	};
	
	/**
	 * Event click textview category
	 */
	private OnClickListener mEventCategoryClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.tvCategoryPublic:
				mCategory = Constant.CATEGORY_ID_PUBLIC;
				
				((TextView) findViewById(R.id.tvCategoryPublic)).setTextColor(
						getResources().getColor(R.color.tab_meu_background_color));
				((TextView) findViewById(R.id.tvCategorySchool)).setTextColor(
						getResources().getColor(R.color.tab_meu_background_focus_color));
				
				break;
				
			case R.id.tvCategorySchool:
				mCategory = Constant.CATEGORY_ID_SCHOOL;
				
				((TextView) findViewById(R.id.tvCategoryPublic)).setTextColor(
						getResources().getColor(R.color.tab_meu_background_focus_color));
				((TextView) findViewById(R.id.tvCategorySchool)).setTextColor(
						getResources().getColor(R.color.tab_meu_background_color));
				
				break;

			default:
				break;
			}
		}
	};
	
	private void processLanguageChoose(int colorEng, int colorChina, 
									int colorMalay, int colorTamil, 
									int visibleSubmit, int visibleSave, 
									int visibleNext) {
		((TextView) findViewById(R.id.tvLanguageEng)).setTextColor(
				getResources().getColor(colorEng));
		((TextView) findViewById(R.id.tvLanguageChina)).setTextColor(
				getResources().getColor(colorChina));
		((TextView) findViewById(R.id.tvLanguageMalay)).setTextColor(
				getResources().getColor(colorMalay));
		((TextView) findViewById(R.id.tvLanguageTamil)).setTextColor(
				getResources().getColor(colorTamil));
		
		((Button) findViewById(R.id.btnNext)).setVisibility(visibleNext);
		((Button) findViewById(R.id.btnSave)).setVisibility(visibleSave);
		((Button) findViewById(R.id.btnSubmit)).setVisibility(visibleSubmit);
	}
	
	@Override
	public void onClick(View v) {
		Intent it;
		switch (v.getId()) {
		case R.id.btnNext:
			processUploadPoem(FLAG_NEXT);
			break;
		case R.id.btnGetImage:
//			openGallery(1);
			it = new Intent(SubmitActivity.this, AddImageActivity.class);
			it.putExtra(AddImageActivity.KEY_PHOTO, mUriAvatar);
			it.putExtra(AddImageActivity.KEY_CAPTION, edtCaption.getText().toString());
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivityForResult(it, CODE_PICK_IMAGE);
			overridePendingTransition(0,0);
			break;
		case R.id.btnClose:
			imgPhoto.setImageResource(R.drawable.no_image);
			mUriAvatar = "";
			rlImageLayout.setVisibility(View.GONE);
			break;
		case R.id.btnMarkerLocation:
			it = new Intent(SubmitActivity.this, LocationsActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivityForResult(it, LOCATION_CODE);
			overridePendingTransition(0,0);
			break;
		case R.id.btnSave:
			mFlagPostSattus = FLAG_SAVE;
			processUploadPoem(FLAG_SAVE);
			break;
		case R.id.btnSubmit:
			mFlagPostSattus = FLAG_SUBMIT;
			processUploadPoem(FLAG_SUBMIT);
			break;

		default:
			break;
		}
	}
	
	/**
	 * Process upload poem into server
	 * 
	 * @param isSubmit
	 */
	private void processUploadPoem(int flag) {
		
		EditText edtTitle = (EditText) findViewById(R.id.edtTitle);
		EditText edtContent = (EditText) findViewById(R.id.edtContent);
		
		if(edtTitle.getText().toString().trim().isEmpty()){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_fill_title_empty)).show();
			edtTitle.requestFocus();
			return;
		}
		
		if(edtContent.getText().toString().trim().isEmpty()){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_fill_content_empty)).show();
			edtContent.requestFocus();
			return;
		}
		
		if(mLocationId == 0){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_location_must_be_choose)).show();
			return;
		}
		
//		if(mUriAvatar.isEmpty() || edtPopupCaption.getText().toString().trim().isEmpty()){
//			createDialogMessage(getString(R.string.msg_error_title), 
//					getString(R.string.msg_error_must_be_choose_image)).show();
//			return;
//		}
		
		if(flag == FLAG_NEXT){

			Intent it = new Intent(SubmitActivity.this, SubmitInputEngActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			it.putExtra(KEY_LANGUAGE_ID, mLanguageId);
			it.putExtra(KEY_CATEGORY, mCategory);
			it.putExtra(KEY_TITLE, edtTitle.getText().toString().trim());
			it.putExtra(KEY_CONTENT, edtContent.getText().toString().trim());
			it.putExtra(KEY_LOCATION_ID, mLocationId);
			it.putExtra(KEY_CAPTION, edtCaption.getText().toString().trim());
			it.putExtra(KEY_URL_PHOTO, mUriAvatar);
			startActivity(it);
			overridePendingTransition(0,0);
			return;
		}
		
		try {
//			JSONObject jsonRequest = new JSONObject();
//			jsonRequest.put("location_id", ""+mLocationId);
//			jsonRequest.put("author", SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME));
//			jsonRequest.put("status_id", ""+(flag==FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH));
//			jsonRequest.put("category_id", ""+ mCategory);
//			jsonRequest.put("poem_contents[0][language_id]", mLanguageId+"");
//			jsonRequest.put("poem_contents[0][title]", edtTitle.getText().toString().trim());
//			jsonRequest.put("poem_contents[0][content]", edtContent.getText().toString().trim());
//			if(!mUriAvatar.isEmpty()){
//				jsonRequest.put("photo", ""+ mUriAvatar);
//			}
//			
//			Map<String, String> mapParams = new HashMap<String, String>();
//			mapParams.put("location_id", ""+mLocationId);
//			mapParams.put("author", SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME));
//			mapParams.put("status_id", ""+(flag==FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH));
//			mapParams.put("category_id", ""+ mCategory);
//			mapParams.put("language_id", mLanguageId+"");
//			mapParams.put("title", edtTitle.getText().toString().trim());
//			mapParams.put("content", edtContent.getText().toString().trim());
//			if(!mUriAvatar.isEmpty()){
//				mapParams.put("photo", ""+ mUriAvatar);
//				if(!edtCaption.getText().toString().trim().isEmpty()){
//					mapParams.put("caption", ""+ edtCaption.getText().toString().trim());
//				}
//			}
//			
//			new PoemCreateNewApi().createNewPoem(this, mapParams, mPoemCreateListener);
//			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			
			
			
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("location_id", ""+mLocationId));
			nameValuePairs.add(new BasicNameValuePair("author", SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME)));
			nameValuePairs.add(new BasicNameValuePair("status_id", ""+(flag==FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH)));
			nameValuePairs.add(new BasicNameValuePair("category_id", ""+ mCategory));
			nameValuePairs.add(new BasicNameValuePair("language_id", mLanguageId+""));
			nameValuePairs.add(new BasicNameValuePair("title", edtTitle.getText().toString().trim()));
			nameValuePairs.add(new BasicNameValuePair("content", edtContent.getText().toString().trim()));
			if(!mUriAvatar.isEmpty()){
				nameValuePairs.add(new BasicNameValuePair("photo", ""+ mUriAvatar));
				if(!edtCaption.getText().toString().trim().isEmpty()){
					nameValuePairs.add(new BasicNameValuePair("caption", ""+ edtCaption.getText().toString().trim()));
				}
			}
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			new PostPoemAsyncTask(this, nameValuePairs, false).execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * Get file image temp
	 * 
	 * @param type
	 * @return
	 */
	private File getTempFile(int type) {
		if (isSDCARDMounted()) {

			File f = new File(Environment.getExternalStorageDirectory(),
					"temp_" + type + ".jpg");
			try {
				f.createNewFile();
			} catch (IOException e) {

			}
			return f;
		} else {
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_no_sdcard)).show();;
			return null;
		}
	}

	/**
	 * Check SDCard is mounted
	 * 
	 * @return
	 */
	private boolean isSDCARDMounted() {
		String status = Environment.getExternalStorageState();
		if (status.equals(Environment.MEDIA_MOUNTED))
			return true;
		return false;
	}

}
