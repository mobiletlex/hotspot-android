package com.epsilon.textinthecity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcel;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;

public class TermsActivity extends BaseActivity implements OnClickListener {
	
	private int mTabIndex = Constant.TAB_MENU_INDEX_SUBMIT;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_term);
		
		mTabIndex = getIntent().getIntExtra(LoginActivity.TAB_KEY, Constant.TAB_MENU_INDEX_SUBMIT);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_term), true);
		
		addTabMenuLayout(R.id.bottom_layout, mTabIndex);
		
		initControls();
		
		setTypeFaceControls();
		
		setupWidthCenter();
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		((Button) findViewById(R.id.btnAccept)).setOnClickListener(this);
		
		 
		((TextView) findViewById(R.id.tvTerm3)).setMovementMethod(LinkMovementMethod.getInstance());

		String text = ((TextView) findViewById(R.id.tvTerm3)).getText().toString();
		Spannable mySpannable = new SpannableString(text);
		int i1 = text.indexOf("Privacy");
		int i2 = i1 + 14;
		ClickableSpan clickSpan = new ClickableSpan() {
			@Override
			public void onClick(View widget) {
				Common.openBrowser(TermsActivity.this, Constant.URL_SETTING_PRIVACY);
			}
		};
		
		mySpannable.setSpan(clickSpan, i1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		mySpannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.tab_meu_background_focus_color)), 
									i1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		NoUnderlineSpan noUnderline = new NoUnderlineSpan();
		mySpannable.setSpan(noUnderline, i1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		((TextView) findViewById(R.id.tvTerm3)).setText(mySpannable);
		
	}
	
	public static class NoUnderlineSpan extends UnderlineSpan {
	    public NoUnderlineSpan() {}

	    public NoUnderlineSpan(Parcel src) {}

	    @Override
	    public void updateDrawState(TextPaint ds) {
	        ds.setUnderlineText(false);
	    }
	}
	
	private void setTypeFaceControls() {
		
		((TextView) findViewById(R.id.tvTerm1)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvTerm2)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvTerm3)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvTerm4)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvTerm5)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvTermCompulsory)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvTermOptional)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		
		((Button) findViewById(R.id.btnAccept)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((Button) findViewById(R.id.btnAccept)).setClickable(false);
		((Button) findViewById(R.id.btnAccept)).setEnabled(false);
		try {
			((Button) findViewById(R.id.btnAccept)).setAlpha(0.5f);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		((CheckBox) findViewById(R.id.chkTerm1)).setOnCheckedChangeListener(new EventCheckbox());
		((CheckBox) findViewById(R.id.chkTerm2)).setOnCheckedChangeListener(new EventCheckbox());
		((CheckBox) findViewById(R.id.chkTerm3)).setOnCheckedChangeListener(new EventCheckbox());
		((CheckBox) findViewById(R.id.chkTerm4)).setOnCheckedChangeListener(new EventCheckbox());
		((CheckBox) findViewById(R.id.chkTerm5)).setOnCheckedChangeListener(new EventCheckbox());
	}
	
	private class EventCheckbox implements OnCheckedChangeListener{

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if(((CheckBox) findViewById(R.id.chkTerm1)).isChecked() 
					&& ((CheckBox) findViewById(R.id.chkTerm2)).isChecked()
					&& ((CheckBox) findViewById(R.id.chkTerm3)).isChecked()
					&& ((CheckBox) findViewById(R.id.chkTerm4)).isChecked()){
				((Button) findViewById(R.id.btnAccept)).setClickable(true);
				((Button) findViewById(R.id.btnAccept)).setEnabled(true);
				try {
					((Button) findViewById(R.id.btnAccept)).setAlpha(1f);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else {
				((Button) findViewById(R.id.btnAccept)).setClickable(false);
				((Button) findViewById(R.id.btnAccept)).setEnabled(false);
				try {
					((Button) findViewById(R.id.btnAccept)).setAlpha(0.5f);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
		
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnAccept:
			
			Intent it = new Intent(TermsActivity.this, SignupActivity.class);
			it.putExtra(LoginActivity.TAB_KEY, mTabIndex);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivityForResult(it, 300);
			overridePendingTransition(0,0);
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == 300 && resultCode == RESULT_OK){
			setResult(RESULT_OK);
			finish();
		}
		else if(requestCode == 300 && resultCode == RESULT_CANCELED){
			setResult(RESULT_CANCELED);
			finish();
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		try {
			setResult(RESULT_CANCELED);
			finish();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
