package com.epsilon.textinthecity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.epsilon.textinthecity.api.EventsApi;
import com.epsilon.textinthecity.api.LocationByAdminApi;
import com.epsilon.textinthecity.common.CleanUpUtil;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.inter_face.EventsLisener;
import com.epsilon.textinthecity.inter_face.LocationByAdminLisener;
import com.epsilon.textinthecity.object.Event;
import com.epsilon.textinthecity.object.Location;


public class SplashActivity extends Activity {
	
	public static final String KEY_TUTORIAL_LOADED = "KEY_TUTORIAL_LOADED";

	protected int _splashTime = 5000;
	private Thread splashTread;
//	private Handler mHandler;
//	private Runnable mRunnable;
//	private Runnable mRunnableFinish;
	
	private boolean isProcessSuccess = false;
	private boolean isWaitingFinish = false;
	
	ImageView imgLoading;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		if(! isTaskRoot()){
			finish();
			return;
		}
		
		setContentView(R.layout.activity_splash);
		
		imgLoading = (ImageView)findViewById(R.id.img3);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
//		mHandler = new Handler();
//		mRunnable = new Runnable() {
//			
//			@Override
//			public void run() {
//				rotateImage();
//			}
//		};
		
//		mRunnableFinish = new Runnable() {
//			
//			@Override
//			public void run() {
//				finishScreen();
//			}
//		};
		
//		rotateImage();
		
//		mHandler.postDelayed(mRunnableFinish, _splashTime);
		
		
		
//		List<Location> listLocations = (List<Location>)
//				Common.getObjFromInternalFile(this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
//		
//		if(null != listLocations && listLocations.size() > 0){
//			setupZoneByLocation();
//		}
//		else {
			new EventsApi().getEvents(this, mEventsListener);
			new LocationByAdminApi().getAllLocationsByAdmin(this, mLocationByAdminListener);
//		}
		
		// thread for displaying the SplashScreen
		splashTread = new Thread() {

			@Override
			public void run() {
				try {
					synchronized (this) {
						// Wait 2 sec
						wait(_splashTime);
					}

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					isWaitingFinish = true;
					finishScreen();
				}
			}
		};
		splashTread.start();

		
	}
	
	private EventsLisener mEventsListener = new EventsLisener() {
		
		@Override
		public void requestStarted() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void requestCompleted(List<Event> listEvents) {
			if(null != listEvents && listEvents.size() > 0){
				try {
					Collections.sort(listEvents, EventDescComparator);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Common.deleteFileFromInternal(SplashActivity.this, Constant.FILE_CACHE_LIST_ALL_EVENTS);
				Common.writeFileToInternal(SplashActivity.this, Constant.FILE_CACHE_LIST_ALL_EVENTS, listEvents);
				
			}
		}
	};
	
	//Comparator for Descending Order
    private Comparator<Event> EventDescComparator = new Comparator<Event>() {

        public int compare(Event app1, Event app2) {
        	
        	return app2.getId() > app1.getId() ? 1 : -1;
        }
    };
	
	private LocationByAdminLisener mLocationByAdminListener = new LocationByAdminLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			setupZoneByLocation();
		}
		
		@Override
		public void requestCompleted(List<Location> listLocation) {
			
			
			
			if(null != listLocation && listLocation.size() > 0){
								
				Common.deleteFileFromInternal(SplashActivity.this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
				Common.writeFileToInternal(SplashActivity.this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION, listLocation);
				
			}
			else {
				isProcessSuccess = true;
			}
			setupZoneByLocation();
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			setupZoneByLocation();
		}
	};
	
//	private void rotateImage() {
//		mRotate += 30;
//		((ImageView) findViewById(R.id.img3)).setRotation(mRotate);
//		mHandler.postDelayed(mRunnable, 100);
//	}
	
	private void finishScreen(){
		if(isProcessSuccess && isWaitingFinish){
			Intent it;
			if(SharePreference.getPreferenceByKeyInt(this, KEY_TUTORIAL_LOADED, 0) ==1){

				it = new Intent(SplashActivity.this, ExploreActivity.class);
			}
			else {

				it = new Intent(SplashActivity.this, TutorialActivity.class);
			}
			it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			finish();
		}
//		else {
//			_splashTime = 1000;
//			mHandler.postDelayed(mRunnableFinish, _splashTime);
//		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		try {
//			if(null != mHandler){
////				mHandler.removeCallbacks(mRunnable);
//				mHandler.removeCallbacks(mRunnableFinish);
//				
//				mHandler = null;
////				mRunnable = null;
//				mRunnableFinish = null;
//			}
			
			PoemApplication.getInstance().cancelPendingRequests(this);
			
			CleanUpUtil.cleanupView(findViewById(R.id.main_layout));
			
			if(null != splashTread){
				splashTread.interrupt();
				splashTread = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void setupZoneByLocation() {
//		try {
//			List<Location> listLocations = (List<Location>)
//					Common.getObjFromInternalFile(this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
//			
//			if(null != listLocations && listLocations.size() > 0){
//				for (int i = 0; i < listLocations.size(); i++) {
//					if(Constant.HASH_LOCATION_ZONENAME.containsKey(listLocations.get(i).getId())){
//						continue;
//					}
//					
//					try {
//						Constant.HASH_LOCATION_ZONENAME.put(listLocations.get(i).getId(), 
//								Constant.HASH_ZONES_BY_ID.get(listLocations.get(i).getZone_id()).getName());
//					} catch (Exception e) {
//						// Zone id not in app
//						e.printStackTrace();
//					}
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		isProcessSuccess = true;
		finishScreen();
	}

	
}