package com.epsilon.textinthecity;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.task.UpdatePoemAsyncTask;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class EditPoemEngActivity extends BaseActivity implements OnClickListener {
			
	public static final int FLAG_SAVE = 2;
	public static final int FLAG_SUBMIT = 3;
		
	private ImageView imgLoading;
	
	private Poem mPoem;
	
	private int mFlagPostSattus = FLAG_SAVE;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_eng_poem);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_edit), false);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_MY_ENTRIES);
		
		mPoem = (Poem)getIntent().getSerializableExtra(EditPoemActivity.KEY_POEM);
		
		initControls();
		
		
		setTypeFaceControls();
		
		setupWidthCenter();
		
		initData();
		
		PoemHelper.getInstance().register(this);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	@Override
	protected void onDestroy() {
		try {
			PoemHelper.getInstance().unregister(this);
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onDestroy();
	}
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {

		// Result for list location by zone
		if(null != event.getResult() && event.getResult().equals(Constant.POEM_UPDATE_ENG_ERROR_DEF)){
						
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_poem_update_fail)).show();
		}
		// add new poem success
		else if(null != event.getPoem()){
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
						
			Intent it = new Intent(EditPoemEngActivity.this, MyEntriesActivity.class);
			if(mFlagPostSattus == FLAG_SAVE){
				it.putExtra(ShareActivity.KEY_SHARE_SCREEN, false);
				Toast.makeText(this, getString(R.string.msg_poem_save_draft_successful), Toast.LENGTH_LONG).show();
			}
			else{
				if(mPoem.getStatus_id() == Constant.POEM_STATUS_DRAFT){
					Tracker t = PoemApplication.getInstance().getTracker(
			                TrackerName.APP_TRACKER);
			        t.send(new HitBuilders.EventBuilder()
			        	.setCategory(getString(R.string.google_analytics_event_category_poem))
			        	.setAction(getString(R.string.google_analytics_event_action_submit))
			        	.setLabel(
			        			SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME) + ", " +
			        			event.getPoem().getId()+"")
			        	.build());
				}
				
				it.putExtra(ShareActivity.KEY_SHARE_SCREEN, true);
				Toast.makeText(this, getString(R.string.msg_poem_update_successful), Toast.LENGTH_LONG).show();
			}
			it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			finish();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		((Button) findViewById(R.id.btnSave)).setOnClickListener(this);
		((Button) findViewById(R.id.btnSubmit)).setOnClickListener(this);
		((Button) findViewById(R.id.btnBack)).setOnClickListener(this);
//		((Button) findViewById(R.id.btnBack)).setVisibility(View.GONE);
			
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		InputFilter[] iFilter = new InputFilter[1];
		// Filter input title
		iFilter[0] = new InputFilter.LengthFilter(Constant.LENGTH_INPUT_POEM_TITLE);
		((EditText) findViewById(R.id.edtTitle)).setFilters(iFilter);
	}
	
	private void initData() {
		String languageName = Constant.HASH_LANGUAGES.get(Constant.LANGUAGE_ID_ENG).getName();
		String title = mPoem.getTitle_alt();
		String content = mPoem.getContent_alt();
		
		((TextView) findViewById(R.id.tvLanguageEng)).setText(languageName);
		((EditText) findViewById(R.id.edtTitle)).setText(title);
		((TextView) findViewById(R.id.edtContent)).setText(content);
		
		if(mPoem.getStatus_id() != Constant.POEM_STATUS_DRAFT){
			((Button) findViewById(R.id.btnSave)).setVisibility(View.GONE);
		}
		
	}
	
	/**
	 * Set type face for controls
	 */
	private void setTypeFaceControls() {
		((TextView) findViewById(R.id.tvStar1)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvStar2)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvLanguage)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvLanguageEng)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		
		((EditText) findViewById(R.id.edtTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((EditText) findViewById(R.id.edtContent)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((Button) findViewById(R.id.btnSubmit)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((Button) findViewById(R.id.btnSave)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((Button) findViewById(R.id.btnBack)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnBack:
			onBackPressed();
			break;
		case R.id.btnSave:
			mFlagPostSattus = FLAG_SAVE;
			processUploadPoem(FLAG_SAVE);
			break;
		case R.id.btnSubmit:
			mFlagPostSattus = FLAG_SUBMIT;
			processUploadPoem(FLAG_SUBMIT);
			break;

		default:
			break;
		}
	}
	
	/**
	 * Process upload poem into server
	 * 
	 * @param isSubmit
	 */
	private void processUploadPoem(int flag) {
		
		EditText edtTitle = (EditText) findViewById(R.id.edtTitle);
		EditText edtContent = (EditText) findViewById(R.id.edtContent);
		
		if(edtTitle.getText().toString().trim().isEmpty()){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_fill_title_empty)).show();
			edtTitle.requestFocus();
			return;
		}
				
		mPoem.setContent_alt(edtContent.getText().toString().trim());
		mPoem.setTitle_alt(edtTitle.getText().toString().trim());
				
		try {
//			JSONObject jsonRequest = new JSONObject();
//			jsonRequest.put("id", ""+mPoem.getId());
//			jsonRequest.put("location_id", ""+mPoem.getLocation_id());
//			jsonRequest.put("author", mPoem.getAuthor());
//			jsonRequest.put("status_id", ""+(flag==FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH));
//			jsonRequest.put("category_id", ""+ mPoem.getCategory_id());
//			jsonRequest.put("poem_contents[0][language_id]", mPoem.getContents().get(0).getLanguage_id()+"");
//			jsonRequest.put("poem_contents[0][title]", mPoem.getContents().get(0).getTitle());
//			jsonRequest.put("poem_contents[0][content]", mPoem.getContents().get(0).getContent());
//			jsonRequest.put("poem_contents[1][language_id]", mPoem.getContents().get(1).getLanguage_id()+"");
//			jsonRequest.put("poem_contents[1][title]", mPoem.getContents().get(1).getTitle());
//			jsonRequest.put("poem_contents[1][content]", mPoem.getContents().get(1).getContent());
//			if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0 
//					&& !mPoem.getPhotos()[0].getLink().isEmpty()){
//				jsonRequest.put("photo", ""+ mPoem.getPhotos()[0].getLink());
//			}
//			
//			Map<String, String> mapParams = new HashMap<String, String>();
//			mapParams.put("id", ""+mPoem.getId());
//			mapParams.put("location_id", ""+mPoem.getLocation_id());
//			mapParams.put("author", mPoem.getAuthor());
//			mapParams.put("status_id", ""+(flag==FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH));
//			mapParams.put("category_id", ""+ mPoem.getCategory_id());
//			mapParams.put("poem_contents[0][language_id]", mPoem.getContents().get(0).getLanguage_id()+"");
//			mapParams.put("poem_contents[0][title]", mPoem.getContents().get(0).getTitle());
//			mapParams.put("poem_contents[0][content]", mPoem.getContents().get(0).getContent());
//			mapParams.put("poem_contents[1][language_id]", mPoem.getContents().get(1).getLanguage_id()+"");
//			mapParams.put("poem_contents[1][title]", mPoem.getContents().get(1).getTitle());
//			mapParams.put("poem_contents[1][content]", mPoem.getContents().get(1).getContent());
//			if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0 
//					&& !mPoem.getPhotos()[0].getLink().isEmpty()){
//				mapParams.put("photo", ""+ mPoem.getPhotos()[0].getLink());
//			}
//			
//			Api.putUpdatePoem(this, mapParams, jsonRequest);
//			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			
			
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("id", ""+mPoem.getId()));
			nameValuePairs.add(new BasicNameValuePair("location_id", ""+mPoem.getLocation_id()));
			nameValuePairs.add(new BasicNameValuePair("author", mPoem.getAuthor()));
			nameValuePairs.add(new BasicNameValuePair("status_id", ""+(flag==FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH)));
			nameValuePairs.add(new BasicNameValuePair("category_id", ""+ mPoem.getCategory_id()));
			nameValuePairs.add(new BasicNameValuePair("language_id", mPoem.getLanguage_id()+""));
			nameValuePairs.add(new BasicNameValuePair("title", mPoem.getTitle()));
			nameValuePairs.add(new BasicNameValuePair("content", mPoem.getContent()));
			nameValuePairs.add(new BasicNameValuePair("title_alt", mPoem.getTitle_alt()));
			nameValuePairs.add(new BasicNameValuePair("content_alt", mPoem.getContent_alt()));
			if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0 
					&& !mPoem.getPhotos()[0].getLinks().getThumbnail().isEmpty()){
				if(! mPoem.getPhotos()[0].getLinks().getThumbnail().startsWith("http") 
						&& ! mPoem.getPhotos()[0].getLinks().getThumbnail().startsWith("https")){
					
					nameValuePairs.add(new BasicNameValuePair("photo", ""+ mPoem.getPhotos()[0].getLinks().getThumbnail()));
				}
				
				nameValuePairs.add(new BasicNameValuePair("caption", ""+ mPoem.getPhotos()[0].getCaption()));
			}
			else {
				nameValuePairs.add(new BasicNameValuePair("photo", "-1"));
				nameValuePairs.add(new BasicNameValuePair("caption", ""));
			}
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			new UpdatePoemAsyncTask(this, nameValuePairs, mPoem.getId(), true).execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
}
