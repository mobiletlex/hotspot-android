package com.epsilon.textinthecity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.toolbox.ImageRequest;
import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.object.Poem;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnPublishListener;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class ShareActivity extends BaseActivity implements OnClickListener {
	
	protected static final String TAG = ShareActivity.class.getName();
	
	public static final String KEY_SHARE_SCREEN = "KEY_SHARE_SCREEN";
	public static final String KEY_POEM = "KEY_POEM";
	public static final String KEY_SHARE_IMAGE = "KEY_SHARE_IMAGE";
	
	private SimpleFacebook mSimpleFacebook;
	
	public PopupWindow mPopupWindowShareFB;
	private View mViewPopupShareFB;
	
	private ImageView imgLoading;
	
	String mTextBody = "";
	String mTextSubject = "";
	String mTextLink = Constant.URL_GOOGLE_PLAY_STORE;
	String mImage = "";
	String mImageFace = "";
	private int mPoemId = 0;
	Poem mPoem;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_submit_thank), true);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SUBMIT);
		
		initControls();
		
		setTypeFaceControls();
		
		mPoem = (Poem)getIntent().getSerializableExtra(KEY_POEM);
		if(null != mPoem && null != mPoem.getContent()){
			mPoemId = mPoem.getId();
			mTextBody = mPoem.getContent();
			mTextSubject = mPoem.getTitle();
			mImage = getIntent().getStringExtra(KEY_SHARE_IMAGE);
			
			if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0){
				mImageFace = mPoem.getPhotos()[0].getLinks().getOrg();
			}
		}
		
		initPopup();
		
		mSimpleFacebook = SimpleFacebook.getInstance(this);
		
//		Log.e("CongVC", "====HashKey:" + Utils.getHashKey(this));
		
		if (mSimpleFacebook.isLogin()) {
			mSimpleFacebook.logout(onLogoutListener);
		}
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		((Button) findViewById(R.id.btnFace)).setOnClickListener(this);
		((Button) findViewById(R.id.btnT)).setOnClickListener(this);
		((Button) findViewById(R.id.btnTwitter)).setOnClickListener(this);
		((Button) findViewById(R.id.btnEmail)).setOnClickListener(this);
		
		((Button) findViewById(R.id.top_layout).findViewById(R.id.btnScreenBack)).setOnClickListener(this);
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
	}
	
	/**
	 * Init view for popup
	 */
	@SuppressLint("InflateParams")
	private void initPopup() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 		 
		 Point point = new Point(0, 0);
	        Display display = getWindowManager().getDefaultDisplay();
	        display.getSize(point);
	        	        
			 mViewPopupShareFB = inflater.inflate(R.layout.popup_share_fb, null, false);
			 String authorName = "";
			if(mPoem.getPoet() != null){
				try {
					authorName = ! mPoem.getPoet().getName().isEmpty() ? 
									mPoem.getPoet().getName() : 
										mPoem.getPoet().getPen_name();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			final String bodyFB = getString(R.string.share_poem_email_body, 
											mPoem.getTitle(), authorName, 
											Constant.URL_APPLE_STORE,
											Constant.URL_GOOGLE_PLAY_STORE);
			((TextView) mViewPopupShareFB.findViewById(R.id.tvContentFB)).setText(bodyFB);
			((TextView) mViewPopupShareFB.findViewById(R.id.tvContentFB)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
			((TextView) mViewPopupShareFB.findViewById(R.id.tvContentFB)).setMovementMethod(new ScrollingMovementMethod());
			((TextView) mViewPopupShareFB.findViewById(R.id.tvShareFBLabel)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
			
			
			((Button) mViewPopupShareFB.findViewById(R.id.btnShareFBClose)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mPopupWindowShareFB.dismiss();
				}
			});
			((Button) mViewPopupShareFB.findViewById(R.id.btnPopPost)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mPopupWindowShareFB.dismiss();
					if (mSimpleFacebook.isLogin()) {
						Log.w("CongVC", "====has Login=====");
						processShareFaceBook();
					}
					else {
						Log.w("CongVC", "====call Login=====");
						mSimpleFacebook.login(onLoginListener);
					}
				}
			});
			
			android.widget.LinearLayout.LayoutParams lpImg = new android.widget.LinearLayout.LayoutParams(
							(int)(point.x *0.9f), 
							(int)(point.x *0.9f * 0.564f));
			((ImageView) mViewPopupShareFB.findViewById(R.id.share_image)).setLayoutParams(lpImg);
			if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0){
				String imgLink = mPoem.getPhotos()[0].getLinks().getOrg();
				loadImageView(((ImageView) mViewPopupShareFB.findViewById(R.id.share_image)), 
							imgLink, 
							(int)(point.x *0.9f), 
							(int)(point.x *0.9f * 0.564f));
			}
			else {
				((ImageView) mViewPopupShareFB.findViewById(R.id.share_image)).setVisibility(View.GONE);
			}
	}
	
	private void setTypeFaceControls() {
		((TextView) findViewById(R.id.tvThank)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvShare)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
	}
	

	/**
	 * Show popup
	 * @param view
	 */
	private void showPopupShareFB(View view) {
		mPopupWindowShareFB = new PopupWindow(this);
		mPopupWindowShareFB.setTouchable(true);
		mPopupWindowShareFB.setFocusable(true);
		mPopupWindowShareFB.setOutsideTouchable(true);
		mPopupWindowShareFB.setTouchInterceptor(new OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	mPopupWindowShareFB.dismiss();
	                return true;
	            }
	            return false;
	        }
	    });
		mPopupWindowShareFB.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowShareFB.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowShareFB.setOutsideTouchable(false);
		mPopupWindowShareFB.setContentView(mViewPopupShareFB);
		mPopupWindowShareFB.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_gray_alpha));
	    
		mPopupWindowShareFB.showAsDropDown(view, 0, (int)(- findViewById(R.id.top_layout).getHeight()));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnScreenBack:
			onBackPressed();
			break;
		case R.id.btnFace:
//			processShare("com.facebook.katana");	
			showPopupShareFB(findViewById(R.id.top_layout));
//			if (mSimpleFacebook.isLogin()) {
//				Log.w("CongVC", "====has Login=====");
//				processShareFaceBook();
//			}
//			else {
//				Log.w("CongVC", "====call Login=====");
//				mSimpleFacebook.login(onLoginListener);
//			}
			break;
		case R.id.btnT:
			
			break;
		case R.id.btnTwitter:
			processShare("com.twitter.android");
			break;
		case R.id.btnEmail:
			processSendMail();
//			processShare("com.google.android.apps.plus");
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mSimpleFacebook.onActivityResult(this, requestCode, resultCode, data);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		Intent it = new Intent(ShareActivity.this, MyEntriesActivity.class);
		it.putExtra(KEY_SHARE_SCREEN, true);
		it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(it);
		overridePendingTransition(0,0);
		finish();
	}
	
	private void processShareFaceBook() {
		
		final Feed feed;
		
		String subjectMail = getString(R.string.share_poem_email_subject);
		String authorName = "";
		if(mPoem.getPoet() != null){
			try {
				authorName = ! mPoem.getPoet().getName().isEmpty() ? 
								mPoem.getPoet().getName() : 
									mPoem.getPoet().getPen_name();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String bodyMail = getString(R.string.share_poem_email_body, 
										mPoem.getTitle(), authorName, 
										Constant.URL_APPLE_STORE,
										Constant.URL_GOOGLE_PLAY_STORE);
		
		String mTextBody = bodyMail;
		String mTextSubject = subjectMail;
		String mTextLink = Constant.URL_GOOGLE_PLAY_STORE;
		String mImage = "";
		
		if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0){
			mImage = mPoem.getPhotos()[0].getLinks().getOrg();
		}
		
		if(!mImageFace.equals("")){
			feed = new Feed.Builder()
//			.setName(mTextSubject)
//			.setCaption(mTextLink)
//			.setDescription(mTextBody)
			.setPicture(mImageFace)
//			.setLink(mTextLink)
			.setMessage(mTextBody)
			.build();
		}
		else {
			feed = new Feed.Builder()
//			.setName(mTextSubject)
//			.setCaption(mTextLink)
//			.setDescription(mTextBody)
//			.setLink(mTextLink)
			.setMessage(mTextBody)
			.build();
		}
		
//		final Feed feed = new Feed.Builder()
//				.setMessage("Clone it out...")
//				.setName("Simple Facebook SDK for Android")
//				.setCaption("Code less, do the same.")
//				.setDescription("Login, publish feeds and stories, invite friends and more....")
//				.setPicture("https://raw.github.com/sromku/android-simple-facebook/master/Refs/android_facebook_sdk_logo.png")
//				.setLink("https://github.com/sromku/android-simple-facebook")
//				.build();
		
		SimpleFacebook.getInstance().publish(feed, new OnPublishListener() {
		
			@Override
			public void onException(Throwable throwable) {
//				mResult.setText(throwable.getMessage());
				try {
					((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				} catch (Exception e) {
					// TODO: handle exception
				}
				Toast.makeText(ShareActivity.this, 
						getString(R.string.msg_err_share_fb_duplicate), Toast.LENGTH_LONG).show();
			}
			
			@Override
			public void onThinking() {
				// TODO Auto-generated method stub
				super.onThinking();
				try {
					((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}



			@Override
			public void onFail(String reason) {
//				mResult.setText(reason);\
				try {
					((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				} catch (Exception e) {
					// TODO: handle exception
				}
				Toast.makeText(ShareActivity.this, 
						reason, 
						Toast.LENGTH_LONG).show();
			}
		
			@Override
			public void onComplete(String response) {
//				mResult.setText(response);
				try {
					((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				} catch (Exception e) {
					// TODO: handle exception
				}
				Toast.makeText(ShareActivity.this, 
						getString(R.string.msg_share_fb_successful), 
						Toast.LENGTH_LONG).show();
				trackShareGoogleAnalytics();
			}
		});		
	}
	
	public void trackShareGoogleAnalytics() {
		Tracker t = PoemApplication.getInstance().getTracker(
                TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
        	.setCategory(getString(R.string.google_analytics_event_category_poem))
        	.setAction(getString(R.string.google_analytics_event_action_share))
        	.setLabel(mPoemId+"").build());
	}
	
	
	private void processShare(String packageName) {
		
		String authorName = "";
		if(mPoem.getPoet() != null){
			try {
				authorName = ! mPoem.getPoet().getName().isEmpty() ? 
								mPoem.getPoet().getName() : 
									mPoem.getPoet().getPen_name();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		String strBody = "";
		
		
			strBody = getString(R.string.share_poem_twitter, 
					mPoem.getTitle(), 
					authorName, 
					"",
					"");
//			int lengthImage = 0;
//			if(null != image && !image.isEmpty() && image.length() > 0){
//				lengthImage = 23;
//			}
//			int lengthBody = strBody.length() + lengthImage + 46;
			int lengthBody = strBody.length() + 46;
			String titleTruncate = mPoem.getTitle();
			if(lengthBody > 140){
				try {
					titleTruncate = mPoem.getTitle().substring(0, mPoem.getTitle().length() - (lengthBody - 137)) + "...";
				} catch (Exception e) {
					titleTruncate = "...";
					e.printStackTrace();
				}
			}
			strBody = getString(R.string.share_poem_twitter, 
					titleTruncate, 
					authorName, 
					Constant.URL_APPLE_STORE,
					Constant.URL_GOOGLE_PLAY_STORE);
		
		
		boolean hasInstallApp = false;
		
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//		shareIntent.setType("text/plain");
//		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, mTextBody);
		shareIntent.setType("image/png");
		shareIntent.putExtra(Intent.EXTRA_TEXT, strBody);
		if(null != mImage && !mImage.isEmpty() && mImage.length() > 0){
			shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(mImage)));
		}
		shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		PackageManager pm = getPackageManager();
		List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
		for (final ResolveInfo app : activityList) {
		    if (app.activityInfo.packageName.contains(packageName)) {
		        		    	
		    	hasInstallApp = true;
				
				 final ActivityInfo activity = app.activityInfo;
		        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
		        shareIntent.setComponent(name);
		        startActivity(shareIntent);
		        
		        trackShareGoogleAnalytics();
		        
		        break;
		   }
		}
		
		if(! hasInstallApp){
			try {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
			} catch (android.content.ActivityNotFoundException anfe) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + packageName)));
			}
		}
	}
	
	private void processSendMail() {
		try {
			
			String subjectMail = getString(R.string.share_poem_email_subject);
			String authorName = "";
			if(mPoem.getPoet() != null){
				try {
					authorName = ! mPoem.getPoet().getName().isEmpty() ? 
									mPoem.getPoet().getName() : 
										mPoem.getPoet().getPen_name();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			String bodyMail = getString(R.string.share_poem_email_body, 
											mPoem.getTitle(), authorName, 
											Constant.URL_APPLE_STORE,
											Constant.URL_GOOGLE_PLAY_STORE);
			
			Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
			shareIntent.setType("message/rfc822");
			if(null != mImage && !mImage.isEmpty() && mImage.length() > 0){
				shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(mImage)));
			}
//			shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, mTextBody +"\n" + mTextLink);
//			shareIntent.putExtra(Intent.EXTRA_SUBJECT, mTextSubject);
			shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, bodyMail);
			shareIntent.putExtra(Intent.EXTRA_SUBJECT, subjectMail);
			shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			
			startActivity(Intent.createChooser(shareIntent, getString(R.string.send_email)));
			
			trackShareGoogleAnalytics();
		} catch (Exception e) {
			Toast.makeText(this, getString(R.string.msg_no_email_install), Toast.LENGTH_LONG).show();
		}
	}
	
	// Login listener
	final OnLoginListener onLoginListener = new OnLoginListener() {

		@Override
		public void onFail(String reason) {
			Log.w(TAG, "Failed to login");
			Log.w("CongVC", "====Login fali=====" + reason);
		}

		@Override
		public void onException(Throwable throwable) {
			Log.e(TAG, "Bad thing happened", throwable);
			Log.w("CongVC", "====Error login=====" + throwable);
		}

		@Override
		public void onThinking() {
			// show progress bar or something to the user while login is
			// happening
			Log.w("CongVC", "====Thinking=====");
		}

		@Override
		public void onLogin() {
			// change the state of the button or do whatever you want
			Log.w("CongVC", "====Login=====");
			processShareFaceBook();
		}

		@Override
		public void onNotAcceptingPermissions(Permission.Type type) {
			Toast.makeText(ShareActivity.this, 
					String.format("You didn't accept %s permissions", type.name()),
					Toast.LENGTH_LONG).show();
		}
	};
	
	final OnLogoutListener onLogoutListener = new OnLogoutListener() {

		@Override
		public void onFail(String reason) {
			Log.w(TAG, "Failed to login");
		}

		@Override
		public void onException(Throwable throwable) {
			Log.e(TAG, "Bad thing happened", throwable);
		}

		@Override
		public void onThinking() {
			// show progress bar or something to the user while login is
			// happening
		}

		@Override
		public void onLogout() {
			// change the state of the button or do whatever you want
		}

	};
	
	public void loadImageView(final ImageView imgView, String url, final int width, final int height) {
		try {
			
//			imgView.setBackgroundResource(R.drawable.no_image);
			
			Response.Listener<Bitmap> listener = new Response.Listener<Bitmap>() {
			    @Override
			    public void onResponse(Bitmap bitmap) {
			    	Log.e("CongVC", "====Bitmap:" + bitmap);
			        scaleAndSetImageBitmap(imgView, bitmap, width, height);
			        
			    }
			};
			
			ErrorListener eListener = new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError arg0) {
					
					Log.e("CongVC", "====Bitmap error:" + arg0.getMessage());
				}
			};
			
			ImageRequest imgRequest = new ImageRequest(
			        url,
			        listener,
			        width,
			        height,
			        Config.RGB_565,
			        //Config.ARGB_8888,
			        eListener);
			
//			PoemApplication.getInstance().getQueue().add(imgRequest);
			PoemApplication.getInstance().addToRequestQueue(imgRequest, this);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "static-access", "deprecation" })
	public void scaleAndSetImageBitmap(ImageView imgView, Bitmap bitmap, int width, int height) {
		try {
			
			int imgWidth = imgView.getWidth() > 0 ? imgView.getWidth() : width;
			int imgHeight = imgView.getHeight() > 0 ? imgView.getHeight() : height;
			
			float ratioW = (float)imgWidth/(float)bitmap.getWidth();
	        float ratioH = (float)imgHeight/(float)bitmap.getHeight();
	        float ratio = ratioW>ratioH ? ratioW : ratioH;
	        
	        Log.e("CongVC", "===imgView.getWidth():" + imgView.getWidth() + "==imgView.getHeight():" + imgView.getHeight());
	        Log.e("CongVC", "===ratioW:" + ratioW + "==ratioH:" + ratioH);
	        
	        bitmap = bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*ratio), (int)(bitmap.getHeight()*ratio), true);
	        	        
	        
	        bitmap = bitmap.createBitmap(bitmap, 0, 0, imgWidth, imgHeight);	
	        

	        BitmapDrawable ob = new BitmapDrawable(bitmap);
	        imgView.setBackgroundDrawable(ob);
	        imgView.setScaleType(ScaleType.FIT_START);
	        
	        
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
