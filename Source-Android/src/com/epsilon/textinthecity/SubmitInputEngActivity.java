package com.epsilon.textinthecity;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.task.PostPoemAsyncTask;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SubmitInputEngActivity extends BaseActivity implements OnClickListener {
	
	private int mLanguageId = 0;
	private int mCategory = 0;
	private int mLocationId = 0;
	private String mTitle = "";
	private String mContent = "";
	private String mCaption = "";
	private String mUrlPhoto = "";
	
	private ImageView imgLoading;
	
	private int mFlagPostSattus = SubmitActivity.FLAG_SAVE;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_submit_input_eng);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_submit), false);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SUBMIT);
		
		initControls();
		
		setTypeFaceControls();
		
		setupWidthCenter();
		
		PoemHelper.getInstance().register(this);
	}
	
	@Override
	protected void onDestroy() {
		try {
			PoemHelper.getInstance().unregister(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		Intent it = getIntent();
		if(null != it){
			mLanguageId = it.getIntExtra(SubmitActivity.KEY_LANGUAGE_ID, 0);
			mCategory = it.getIntExtra(SubmitActivity.KEY_CATEGORY, 0);
			mLocationId = it.getIntExtra(SubmitActivity.KEY_LOCATION_ID, 0);
			mTitle = it.getStringExtra(SubmitActivity.KEY_TITLE);
			mContent = it.getStringExtra(SubmitActivity.KEY_CONTENT);
			mCaption = it.getStringExtra(SubmitActivity.KEY_CAPTION);
			mUrlPhoto = it.getStringExtra(SubmitActivity.KEY_URL_PHOTO);
		}
		
//		((TextView) findViewById(R.id.edtContent)).setText(mContent);
		
		((Button) findViewById(R.id.btnBack)).setOnClickListener(this);
//		((Button) findViewById(R.id.btnBack)).setVisibility(View.GONE);
		((Button) findViewById(R.id.btnSubmit)).setOnClickListener(this);
		((Button) findViewById(R.id.btnSave)).setOnClickListener(this);
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		InputFilter[] iFilter = new InputFilter[1];
		// Filter input title
		iFilter[0] = new InputFilter.LengthFilter(Constant.LENGTH_INPUT_POEM_TITLE);
		((EditText) findViewById(R.id.edtTitle)).setFilters(iFilter);
	}
	
	private void setTypeFaceControls() {
		((TextView) findViewById(R.id.tvStar1)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvStar2)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvSubmitEngLabel)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.edtContent)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.edtTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((Button) findViewById(R.id.btnSubmit)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((Button) findViewById(R.id.btnSave)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((Button) findViewById(R.id.btnBack)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {

		// Result for list location by zone
		if(null != event.getResult() && event.getResult().equals(Constant.POEM_ADD_ENG_ERROR_DEF)){
						
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_poem_add_fail)).show();
		}
		// add new poem success
		else if(null != event.getPoem()){
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			Intent it;
			if(mFlagPostSattus == SubmitActivity.FLAG_SAVE){
				Toast.makeText(this, getString(R.string.msg_poem_save_draft_successful), Toast.LENGTH_LONG).show();
				it = new Intent(SubmitInputEngActivity.this, MyEntriesActivity.class);
			}
			else{
				Tracker t = PoemApplication.getInstance().getTracker(
		                TrackerName.APP_TRACKER);
		        t.send(new HitBuilders.EventBuilder()
		        	.setCategory(getString(R.string.google_analytics_event_category_poem))
		        	.setAction(getString(R.string.google_analytics_event_action_submit))
		        	.setLabel(
		        			SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME) + ", " +
		        			event.getPoem().getId()+"")
		        	.build());
				
				Toast.makeText(this, getString(R.string.msg_poem_add_successful), Toast.LENGTH_LONG).show();
				it = new Intent(SubmitInputEngActivity.this, ShareActivity.class);
				it.putExtra(ShareActivity.KEY_POEM, event.getPoem());
				it.putExtra(ShareActivity.KEY_SHARE_IMAGE, mUrlPhoto);
			}
			it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			finish();
		}
	}
	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnBack:
			onBackPressed();
			break;
		case R.id.btnSave:
			mFlagPostSattus = SubmitActivity.FLAG_SAVE;
			processUploadPoem(SubmitActivity.FLAG_SAVE);
			break;
		case R.id.btnSubmit:
			mFlagPostSattus = SubmitActivity.FLAG_SUBMIT;
			processUploadPoem(SubmitActivity.FLAG_SUBMIT);
			break;
		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	
	/**
	 * Process upload poem into server
	 * 
	 * @param isSubmit
	 */
	private void processUploadPoem(int flag) {
		
		EditText edtTitle = (EditText) findViewById(R.id.edtTitle);
		EditText edtContent = (EditText) findViewById(R.id.edtContent);
		
		if(edtTitle.getText().toString().trim().isEmpty()){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_fill_title_empty)).show();
			edtTitle.requestFocus();
			return;
		}
				
		try {
//			JSONObject jsonRequest = new JSONObject();
//			jsonRequest.put("location_id", ""+mLocationId);
//			jsonRequest.put("author", SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME));
//			jsonRequest.put("status_id", ""+(flag==SubmitActivity.FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH));
//			jsonRequest.put("category_id", ""+ mCategory);
//			jsonRequest.put("poem_contents[0][language_id]", mLanguageId+"");
//			jsonRequest.put("poem_contents[0][title]", mTitle);
//			jsonRequest.put("poem_contents[0][content]", mContent);
//			jsonRequest.put("poem_contents[1][language_id]", Constant.LANGUAGE_ID_ENG+"");
//			jsonRequest.put("poem_contents[1][title]", edtTitle.getText().toString().trim());
//			jsonRequest.put("poem_contents[1][content]", edtContent.getText().toString().trim());
//			if(!mUrlPhoto.isEmpty()){
//				jsonRequest.put("photo", ""+ mUrlPhoto);
//			}
//			
//			Map<String, String> mapParams = new HashMap<String, String>();
//			mapParams.put("location_id", ""+mLocationId);
//			mapParams.put("author", SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME));
//			mapParams.put("status_id", ""+(flag==SubmitActivity.FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH));
//			mapParams.put("category_id", ""+ mCategory);
//			mapParams.put("poem_contents[0][language_id]", mLanguageId+"");
//			mapParams.put("poem_contents[0][title]", mTitle);
//			mapParams.put("poem_contents[0][content]", mContent);
//			mapParams.put("poem_contents[1][language_id]", Constant.LANGUAGE_ID_ENG+"");
//			mapParams.put("poem_contents[1][title]", edtTitle.getText().toString().trim());
//			mapParams.put("poem_contents[1][content]", edtContent.getText().toString().trim());
//			if(!mUrlPhoto.isEmpty()){
//				mapParams.put("photo", ""+ mUrlPhoto);
//			}
//			
//			Api.postPoem(this, mapParams, jsonRequest);
//			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("location_id", ""+mLocationId));
			nameValuePairs.add(new BasicNameValuePair("author", SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME)));
			nameValuePairs.add(new BasicNameValuePair("status_id", ""+(flag==SubmitActivity.FLAG_SAVE?Constant.POEM_STATUS_DRAFT:Constant.POEM_STATUS_PUBLISH)));
			nameValuePairs.add(new BasicNameValuePair("category_id", ""+ mCategory));
			nameValuePairs.add(new BasicNameValuePair("language_id", mLanguageId+""));
			nameValuePairs.add(new BasicNameValuePair("title", mTitle));
			nameValuePairs.add(new BasicNameValuePair("content", mContent));
			nameValuePairs.add(new BasicNameValuePair("title_alt", edtTitle.getText().toString().trim()));
			nameValuePairs.add(new BasicNameValuePair("content_alt", edtContent.getText().toString().trim()));
			if(!mUrlPhoto.isEmpty()){
				nameValuePairs.add(new BasicNameValuePair("photo", ""+ mUrlPhoto));
			}
			if(!mCaption.isEmpty()){
				nameValuePairs.add(new BasicNameValuePair("caption", ""+ mCaption));
			}
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			new PostPoemAsyncTask(this, nameValuePairs, true).execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
