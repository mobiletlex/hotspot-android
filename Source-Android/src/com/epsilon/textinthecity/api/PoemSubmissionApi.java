package com.epsilon.textinthecity.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.arnx.jsonic.JSON;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.inter_face.PoemSubmissionLisener;
import com.epsilon.textinthecity.object.Poem;

public class PoemSubmissionApi extends AbstractApi {
	
	/**
	 * Get poem featured
	 * 
	 * @return
	 */
	public void getAllPoemsSubmission(final Context context, final PoemSubmissionLisener resultListener, 
									final Fragment fragment) {
		
		try {
			
			Response.Listener<String> slistener = new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Poem submission:" + response);
			    	   
			    	   Poem[] arrResult = JSON.decode(response, Poem[].class);
						
			    	   List<Poem> lstPoem = Arrays.asList(arrResult);
			    	   
			    	   ArrayList<Poem> aPoem = new ArrayList<Poem>();
			    	   aPoem.addAll(lstPoem);
			    	   
			    	   resultListener.requestCompleted(aPoem);
			    	   			    	   				    	   
					} catch (Exception e) {
						e.printStackTrace();
						resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Poem submission Err:" + error.getMessage());
			    	
			    	resultListener.requestEndedWithError(error);
			    }
			};
			
			String url = Constant.API_POEM_MY_ENTRIES.replace(Constant.REFIX_STATUS_ID, Constant.POEM_STATUS_PUBLISH+"");
			
			StringRequest sr = new StringRequest(Request.Method.GET, 
										url, 
										slistener, eListener){
			    

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        
			        params.put(KEY_X_API_KEY, 
							Constant.X_API_KEY_ENCODE);
					
					String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					if(!authToken.isEmpty()){
						params.put(KEY_X_AUTH_TOKEN, authToken);
					}
					
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			PoemApplication.getInstance().addToRequestQueue(sr, fragment);

		} catch (Exception e) {
			e.printStackTrace();
			resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
		}
	}

}
