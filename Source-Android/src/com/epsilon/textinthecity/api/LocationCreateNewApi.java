package com.epsilon.textinthecity.api;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import net.arnx.jsonic.JSON;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.inter_face.LocationCreateNewLisener;
import com.epsilon.textinthecity.object.LocationMap;

public class LocationCreateNewApi extends AbstractApi {
	
	private static final String KEY_ZONE_ID = "zone_id";
	private static final String KEY_NAME = "name";
	private static final String KEY_ADDRESS = "address";
	private static final String KEY_FUN_FACT = "fun_fact";
	private static final String KEY_LAT = "lat";
	private static final String KEY_LON = "lon";
	
	/**
	 * Get location Near by
	 * 
	 * @return
	 */
	public void createNewLocation(final Context context, final String locationname, 
			final String address, final int zoneId, final String funFact,
			final double lat, final double lon, 
			final LocationCreateNewLisener resultListener) {
		
		try {
			
			Response.Listener<String> slistener = new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Location add:" + response);
			    	   
			    	   if(! new JSONObject(response).isNull(KEY_ERROR)){
			    		   resultListener.requestCompleteWithErrorParseJson("");
			    	   }
			    	   else {
			    		   LocationMap locationMap = JSON.decode(response, LocationMap.class);
			    		   
			    		   resultListener.requestCompleted(locationMap);
			    	   }
			    	   			    	   				    	   
					} catch (Exception e) {
						e.printStackTrace();
						resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Location add Err:" + error.getMessage());
			    	
			    	resultListener.requestEndedWithError(error);
			    }
			};
			
			StringRequest sr = new StringRequest(Request.Method.POST, 
										Constant.API_LOCATION_ADD, 
										slistener, eListener){
			    

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        
			        params.put(KEY_X_API_KEY, 
							Constant.X_API_KEY_ENCODE);
					
					String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					if(!authToken.isEmpty()){
						params.put(KEY_X_AUTH_TOKEN, authToken);
					}
					
			        return params;
			    }
			    
			    @Override
			    protected Map<String,String> getParams(){
			    	Map<String,String> params = new HashMap<String, String>();
			        params.put(KEY_ZONE_ID, zoneId+"");
			        params.put(KEY_NAME, locationname);
			        params.put(KEY_ADDRESS, address+"");
			        params.put(KEY_FUN_FACT, funFact);
			        params.put(KEY_LAT, lat+"");
			        params.put(KEY_LON, lon+"");

			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			PoemApplication.getInstance().addToRequestQueue(sr, context);

		} catch (Exception e) {
			e.printStackTrace();
			resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
		}
	}

}
