package com.epsilon.textinthecity.api;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.net.Proxy;


public class AbstractApi {

	public static final int CONNECT_TIMEOUT = 15000;
	public static final int TIMEOUT = 20000;
	
	public static final String KEY_X_API_KEY = "X-Api-Key";
	public static final String KEY_X_AUTH_TOKEN = "X-Auth-Token";
	public static final String KEY_ERROR = "error";
	
	@SuppressWarnings("deprecation")
	protected void setProxy(HttpClient httpclient, Context aContext) {
		
		String host = Proxy.getHost(aContext);
		int port = Proxy.getPort(aContext);
		if (host == null || host.length() == 0 || port == 0) {
			return;
		}
		HttpHost proxy = new HttpHost(host, port);
		httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY,proxy);
		HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpclient.getParams(), 300);
	}
		
	protected static HttpClient getDefaultHttpClient() {
		
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, CONNECT_TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParameters, TIMEOUT);
		HttpClient httpclient = new DefaultHttpClient(httpParameters);
		return httpclient;
	}
	
	protected boolean isSuccess(HttpResponse response) {
		int code = response.getStatusLine().getStatusCode();
		return (code >= 200 && code < 400);
	}

}
