package com.epsilon.textinthecity.api;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import net.arnx.jsonic.JSON;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.inter_face.PoemCreateNewLisener;
import com.epsilon.textinthecity.object.Poem;
import com.nami.volleydemo.tootbox.MultiPartRequest;

public class PoemCreateNewApi extends AbstractApi {
	
	/**
	 * Get location by id
	 * 
	 * @return
	 */
	public void createNewPoem(final Context context, final Map<String,String> params, final PoemCreateNewLisener resultListener) {
		
		try {
			
			Response.Listener<JSONObject> sslistener = new Response.Listener<JSONObject>() {
			    @Override
			    public void onResponse(JSONObject response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Poem create:" + response);
			    	   
			    	   if(! response.isNull(KEY_ERROR)){
			    		   resultListener.requestCompleteWithErrorParseJson("");
			    	   }
			    	   else {
			    		   Poem poem = JSON.decode(response.toString(), Poem.class);
			    		   
			    		   resultListener.requestCompleted(poem);
			    	   }
			    	   
			    	   			    	   				    	   
					} catch (Exception e) {
						e.printStackTrace();
						resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Poem create Err:" + error.getMessage());
			    	
			    	resultListener.requestEndedWithError(error);
			    }
			};
			
			MultiPartRequest sr = new MultiPartRequest(Request.Method.POST, 
										Constant.API_POEM_CREATE, 
										null,
										sslistener, eListener){
			    

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params1 = new HashMap<String, String>();
			        
			        params1.put(KEY_X_API_KEY, 
							Constant.X_API_KEY_ENCODE);
					
					String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					if(!authToken.isEmpty()){
						params1.put(KEY_X_AUTH_TOKEN, authToken);
					}
					
			        return params1;
			    }
			    @Override
			    protected Map<String,String> getParams(){
			       
			        return params;
			    }
			};
			
			if(params.containsKey("photo")){
				
				File file = new File(params.get("photo"));

				sr.addFileUpload("photo", file);
			}
			
			sr.setStringUpload(params);
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			PoemApplication.getInstance().addToRequestQueue(sr, context);

		} catch (Exception e) {
			e.printStackTrace();
			resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
		}
	}

}
