package com.epsilon.textinthecity.api;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import net.arnx.jsonic.JSON;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.inter_face.LoginLisener;
import com.epsilon.textinthecity.object.User;

public class LoginApi extends AbstractApi {
	
	private static final String KEY_PASSWORD = "password";
	private static final String KEY_USER_EMAIL = "email";
	
	/**
	 * login user
	 * 
	 * @return
	 */
	public void login(final Context context, final String username, 
			final String password, final LoginLisener resultListener) {
		
		try {
			
			Response.Listener<String> slistener = new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Login:" + response);
			    	   if(! new JSONObject(response).isNull(KEY_ERROR)){
                   			resultListener.requestCompleteWithErrorParseJson("");
	                   	}
	                   	else {
	                   		User user = JSON.decode(response, User.class);
	                   		user.setPassword(password);
	                   		
	                   		resultListener.requestCompleted(user);
	                		
	                   	}
			    	   			    	   			    	   				    	   
					} catch (Exception e) {
						e.printStackTrace();
						resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Login Err:" + error.getMessage());
			    	
			    	resultListener.requestEndedWithError(error);
			    }
			};
			
			StringRequest sr = new StringRequest(Request.Method.POST, 
										Constant.API_USER_LOGIN, 
										slistener, eListener){
			    

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        
			        params.put(KEY_X_API_KEY, 
							Constant.X_API_KEY_ENCODE);
					
					String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					if(!authToken.isEmpty()){
						params.put(KEY_X_AUTH_TOKEN, authToken);
					}
					
			        return params;
			    }
			    
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        params.put(KEY_USER_EMAIL, username);
			        params.put(KEY_PASSWORD,password);

			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			PoemApplication.getInstance().addToRequestQueue(sr, context);

		} catch (Exception e) {
			e.printStackTrace();
			resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
		}
	}

}
