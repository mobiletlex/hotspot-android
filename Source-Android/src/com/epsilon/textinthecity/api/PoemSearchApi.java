package com.epsilon.textinthecity.api;

import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.arnx.jsonic.JSON;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.inter_face.PoemSearchLisener;
import com.epsilon.textinthecity.object.PoemPagging;

public class PoemSearchApi extends AbstractApi {
	
	/**
	 * Get poem search
	 * 
	 * @return
	 */
	public void getAllPoemsSearch(final Context context, int page, int perPage, String searchText, 
											final PoemSearchLisener resultListener) {
		
		try {
			
			Response.Listener<String> slistener = new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Poem Search:" + response);
			    	   
			    	   PoemPagging poemPagging = JSON.decode(response, PoemPagging.class);
			    	   
			    	   resultListener.requestCompleted(poemPagging);
			    	   			    	   				    	   
					} catch (Exception e) {
						e.printStackTrace();
						resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Poem Search Err:" + error.getMessage());
			    	
			    	resultListener.requestEndedWithError(error);
			    }
			};
			
			String url = Constant.API_POEM_SEARCH.replace(Constant.REFIX_TITLE, URLEncoder.encode(searchText, "UTF-8"));
			url = url.replace(Constant.REFIX_PAGE, ""+page)
                  .replace(Constant.REFIX_PER_PAGE, ""+perPage);
			
			URI uri = new URI(url);
			
			StringRequest sr = new StringRequest(Request.Method.GET, 
										uri.toString(), 
										slistener, eListener){
			    

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        
			        params.put(KEY_X_API_KEY, 
							Constant.X_API_KEY_ENCODE);
					
					String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					if(!authToken.isEmpty()){
						params.put(KEY_X_AUTH_TOKEN, authToken);
					}
					
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			PoemApplication.getInstance().addToRequestQueue(sr, context);

		} catch (Exception e) {
			e.printStackTrace();
			resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
		}
	}

}
