package com.epsilon.textinthecity.api;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.inter_face.LocationByGoogleLisener;
import com.epsilon.textinthecity.object.LocationMap;

public class LocationByGoogleApi extends AbstractApi {
	
	/**
	 * Get location by Google
	 * 
	 * @return
	 */
	public void getLocationsByGoogle(final Context context, double lat, double lon, 
											final LocationByGoogleLisener resultListener) {
		
		try {
			
			Response.Listener<String> slistener = new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Location Google:" + response);
			    	   
			    	   if(new JSONObject(response).getString("status").equals("OK")){
			    		   JSONArray arrResult = new JSONObject(response).getJSONArray("results");
			    		   if(null != arrResult && arrResult.length() > 0){
			    			   List<LocationMap> arrLocation = new ArrayList<LocationMap>();
			    			   for (int i = 0; i < arrResult.length(); i++) {
			    				   JSONObject jsonObj = arrResult.getJSONObject(i);
			    				   
			    				   LocationMap location = new LocationMap();
			    				   
			    				   location.setName(jsonObj.getString("name"));
			    				   location.setAddress(jsonObj.getString("vicinity"));
			    				   
			    				   arrLocation.add(location);
			    			   }
			    			   
			    			   resultListener.requestCompleted(arrLocation);
			    			   return;
			    		   }
			    	   }
			    	   
			    	   resultListener.requestCompleteWithErrorParseJson("");
			    	   			    	   				    	   
					} catch (Exception e) {
						e.printStackTrace();
						resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Location google err:" + error.getMessage());
			    	
			    	resultListener.requestEndedWithError(error);
			    }
			};
			
			String url = Constant.API_LOCATION_BY_GOOGLE.replace(Constant.REFIX_ZONE_LAT, lat+"")
					.replace(Constant.REFIX_ZONE_LON, lon+"")
					.replace(Constant.REFIX_KEY, context.getString(R.string.google_key_api_place));
			
			Log.e("CongVC", "======url==" + url);
			
			StringRequest sr = new StringRequest(Request.Method.GET, 
										url, 
										slistener, eListener){
			    

//			    @Override
//			    public Map<String, String> getHeaders() throws AuthFailureError {
//			        Map<String,String> params = new HashMap<String, String>();
//			        
//			        params.put(KEY_X_API_KEY, 
//							Constant.X_API_KEY_ENCODE);
//					
//					String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
//					if(!authToken.isEmpty()){
//						params.put(KEY_X_AUTH_TOKEN, authToken);
//					}
//					
//			        return params;
//			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			PoemApplication.getInstance().addToRequestQueue(sr, context);

		} catch (Exception e) {
			e.printStackTrace();
			resultListener.requestCompleteWithErrorParseJson(e.getLocalizedMessage());
		}
	}

}
