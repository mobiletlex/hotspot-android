package com.epsilon.textinthecity.api;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.inter_face.RegisterUserLisener;
import com.epsilon.textinthecity.object.User;

public class RegisterUserApi extends AbstractApi {
	
	private static final String KEY_USER_EMAIL = "email";
	private static final String KEY_PASSWORD = "password";
	private static final String KEY_FIRST_NAME = "first_name";
	private static final String KEY_LAST_NAME = "last_name";
	private static final String KEY_PEN_NAME = "pen_name";
	private static final String KEY_ACCESS_TOKEN = "access_token";
	private static final String KEY_PASSWORD_CONFIRMATION = "password_confirmation";
	
	/**
	 * register user
	 * 
	 * @return
	 */
	public void registerUser(final Context context, final User user, final RegisterUserLisener resultListener) {
		
		try {
			
			Response.Listener<String> slistener = new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Sign up:" + response);
			    	   if(! new JSONObject(response).isNull(KEY_ERROR)){
			    		   if(! new JSONObject(response).getJSONObject(KEY_ERROR).isNull(KEY_PEN_NAME)){
                   				resultListener.requestCompleteWithErrorParseJson(
                   						new JSONObject(response).getJSONObject(KEY_ERROR).getJSONArray(KEY_PEN_NAME).getString(0));
			    		   }
			    		   else if(! new JSONObject(response).getJSONObject(KEY_ERROR).isNull(KEY_PASSWORD)){
                  				resultListener.requestCompleteWithErrorParseJson(
                  						new JSONObject(response).getJSONObject(KEY_ERROR).getJSONArray(KEY_PASSWORD).getString(0));
			    		   }
			    		   else if(! new JSONObject(response).getJSONObject(KEY_ERROR).isNull(KEY_USER_EMAIL)){
                  				resultListener.requestCompleteWithErrorParseJson(
                  						new JSONObject(response).getJSONObject(KEY_ERROR).getJSONArray(KEY_USER_EMAIL).getString(0));
			    		   }
			    		   else {
			    			   resultListener.requestCompleteWithErrorParseJson(context.getString(R.string.msg_error_signup_fail));
			    		   }
	                   	}
	                   	else {
	                   		User userSuccess = new User();
	                   		userSuccess.setEmail(user.getEmail());
	                   		userSuccess.setPassword(user.getPassword());
	                   		userSuccess.setPen_name(user.getPen_name());
	                   		userSuccess.setAccess_token(new JSONObject(response).getString(KEY_ACCESS_TOKEN));
	                   		
	                   		resultListener.requestCompleted(userSuccess);
	                		
	                   	}
			    	   			    	   			    	   				    	   
					} catch (Exception e) {
						e.printStackTrace();
						resultListener.requestCompleteWithErrorParseJson(context.getString(R.string.msg_error_signup_fail));
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Sign up Err:" + error.getMessage());
			    	
			    	resultListener.requestEndedWithError(error);
			    }
			};
			
			StringRequest sr = new StringRequest(Request.Method.POST, 
										Constant.API_USER_REGISTER, 
										slistener, eListener){
			    

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        
			        params.put(KEY_X_API_KEY, 
							Constant.X_API_KEY_ENCODE);
					
					String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					if(!authToken.isEmpty()){
						params.put(KEY_X_AUTH_TOKEN, authToken);
					}
					
			        return params;
			    }
			    
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        params.put(KEY_FIRST_NAME, user.getFirst_name());
			        params.put(KEY_LAST_NAME, user.getLast_name());
			        params.put(KEY_PEN_NAME, user.getPen_name());
			        params.put(KEY_USER_EMAIL, user.getEmail());
			        params.put(KEY_PASSWORD, user.getPassword());
			        params.put(KEY_PASSWORD_CONFIRMATION, user.getPassword_confirmation());
//			        params.put(KEY_PHONE, user.getPhone());

			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			PoemApplication.getInstance().addToRequestQueue(sr, context);

		} catch (Exception e) {
			e.printStackTrace();
			resultListener.requestCompleteWithErrorParseJson(context.getString(R.string.msg_error_signup_fail));
		}
	}

}
