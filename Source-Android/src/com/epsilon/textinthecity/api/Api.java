package com.epsilon.textinthecity.api;

import java.io.File;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.arnx.jsonic.JSON;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.common.Constant.SortField;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.object.LocationMap;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.object.PoemPagging;
import com.epsilon.textinthecity.object.User;
//import com.epsilon.textinthecity.request.MultipartRequest;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.nami.volleydemo.tootbox.MultiPartStringRequest;

public class Api extends AbstractApi {
	
//	private static final String KEY_PAGE = "page";
//	private static final String KEY_PER_PAGE = "per_page";
	private static final String KEY_ZONE_ID = "zone_id";
	private static final String KEY_X_API_KEY = "X-Api-Key";
	private static final String KEY_X_AUTH_TOKEN = "X-Auth-Token";
//	private static final String KEY_USER_NAME = "username";
	private static final String KEY_USER_EMAIL = "email";
	private static final String KEY_PASSWORD = "password";
	private static final String KEY_FIRST_NAME = "first_name";
	private static final String KEY_LAST_NAME = "last_name";
	private static final String KEY_PEN_NAME = "pen_name";
	private static final String KEY_ACCESS_TOKEN = "access_token";
	private static final String KEY_ERROR = "error";
	private static final String KEY_GRADE = "grade";
	private static final String KEY_REASON = "reason";
	private static final String KEY_ID = "id";
	// Key add new location
	private static final String KEY_NAME = "name";
	private static final String KEY_ADDRESS = "address";
	private static final String KEY_FUN_FACT = "fun_fact";
	private static final String KEY_LAT = "lat";
	private static final String KEY_LON = "lon";
	

	/**
	 * Get location by id
	 * 
	 * @return
	 */
	public static void getLocationById(
			final List<NameValuePair> nameValuePairs, final Context context, final int id) {
		
		try {

			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			Response.Listener<String> listener = new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Location by Id Res:" + response);
			    	   
			    	   Location arrResult = JSON.decode(response, Location.class);
			    	   AsyncTaskResultEvent eventResult = new AsyncTaskResultEvent();
			    	   eventResult.setLocationById(arrResult);
			    	   PoemHelper.getInstance().post(eventResult);
			    	   				    	   
					} catch (Exception e) {
						e.printStackTrace();
						PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_GET_BY_ID_ERROR_DEF));
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Location by ID Err:" + error.getMessage());
			    	
			    	PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_GET_BY_ID_ERROR_DEF));
			    }
			};
			
			StringRequest sr = new StringRequest(Request.Method.GET, 
										Constant.API_LOCATION_BY_ID.replace(Constant.REFIX_ID, ""+id), 
										listener, eListener){
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        for (int index = 0; index < nameValuePairs.size(); index++) {
						String name = nameValuePairs.get(index).getName();
						String value = nameValuePairs.get(index).getValue();
						params.put(name, value);
			        }

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
//			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
			        		Constant.X_API_KEY_ENCODE);
			        String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					if(!authToken.isEmpty()){
						params.put(KEY_X_AUTH_TOKEN, 
				        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
					}
			        
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		

		} catch (Exception e) {
			e.printStackTrace();
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_GET_BY_ID_ERROR_DEF));
		}
	}
	
	/**
	 * List location by zone
	 * 
	 * @param zoneId
	 * @return
	 */
	public static List<LocationMap> getLocationsByZone(int zoneId, Context context){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			String result = getJsonDataAuthToken(
								Constant.API_LOCATION_BY_ZONE.replace(Constant.REFIX_ID, ""+zoneId),
								nameValuePairs, context);
			
			Log.e("CongVC", "===Location by Zone:" + result);
			
			LocationMap[] arrResult = JSON.decode(result, LocationMap[].class);
			return Arrays.asList(arrResult);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * List all location
	 * 
	 * @param zoneId
	 * @return
	 */
	public static List<Location> getAllLocations(Context context){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			String result = getJsonDataAuthToken(
								Constant.API_LOCATION_ALL,
								nameValuePairs, context);
			
			Log.e("CongVC", "===Locations All res:" + result);
						
			Location[] arrResult = JSON.decode(result, Location[].class);
			return Arrays.asList(arrResult);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * List all location by Admin
	 * 
	 * @param zoneId
	 * @return
	 */
	public static List<Location> getAllLocationsByAdmin(Context context){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			String result = getJsonDataAuthToken(
								Constant.API_LOCATION_BY_ADMIN,
								nameValuePairs, context);
			
			Log.e("CongVC", "===Locations Admin res:" + result);
						
			Location[] arrResult = JSON.decode(result, Location[].class);
			return Arrays.asList(arrResult);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * List location near by
	 * 
	 * @param zoneId
	 * @return
	 */
	public static List<LocationMap> getLocationsNearBy(double lat, double lon, Context context){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//			nameValuePairs.add(new BasicNameValuePair(KEY_LAT, lat+""));
//			nameValuePairs.add(new BasicNameValuePair(KEY_LON, lon+""));
			Log.e("CongVC", "===Location newr url:" + Constant.API_LOCATION_NEAR_BY.replace(Constant.REFIX_ZONE_LAT, lat+"")
					.replace(Constant.REFIX_ZONE_LON, lon+""));
			String result = getJsonDataAuthToken(
								Constant.API_LOCATION_NEAR_BY.replace(Constant.REFIX_ZONE_LAT, lat+"")
															.replace(Constant.REFIX_ZONE_LON, lon+""),
								nameValuePairs, context);
			Log.e("CongVC", "===Locatios near by:" + result);
			
			LocationMap[] arrResult = JSON.decode(result, LocationMap[].class);
			return Arrays.asList(arrResult);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * List random poems
	 * 
	 * @return
	 */
	public static List<Poem> getRandomPoems(Context context){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			String result = getJsonData(
								Constant.API_POEM_RANDOM,
								nameValuePairs, context);
			
			Log.e("CongVC", "===Poem random:" + result);
			
			Poem[] arrResult = JSON.decode(result, Poem[].class);
			return Arrays.asList(arrResult);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * List random poems
	 * 
	 * @return
	 */
	public static PoemPagging getPoemsByZoneId(int page, int perPage, int zoneId, Context context){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			String url = Constant.API_POEM_BY_ZONE.replace(Constant.REFIX_PAGE, ""+page)
					                              .replace(Constant.REFIX_PER_PAGE, ""+perPage)
					                              .replace(Constant.REFIX_ZONE_ID, ""+zoneId);
			
			String result = getJsonDataAuthToken(
								url,
								nameValuePairs, context);
			
			Log.e("CongVC", "===Poem by Zone:" + result);
									
//			Poem[] arrResult = JSON.decode(result, Poem[].class);
//			return Arrays.asList(arrResult);
			
			PoemPagging poemPagging = JSON.decode(result, PoemPagging.class);
			return poemPagging;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * List random poems
	 * 
	 * @return
	 */
	public static void postLocation1(final Context context, final String locationname, 
			final String address, final int zoneId, final String funFact,
			final double lat, final double lon){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair(KEY_ZONE_ID, zoneId+""));
			nameValuePairs.add(new BasicNameValuePair(KEY_NAME, locationname));
			nameValuePairs.add(new BasicNameValuePair(KEY_ADDRESS, address+""));
			nameValuePairs.add(new BasicNameValuePair(KEY_FUN_FACT, funFact));
			nameValuePairs.add(new BasicNameValuePair(KEY_LAT, lat+""));
			nameValuePairs.add(new BasicNameValuePair(KEY_LON, lon+""));
			
			String url = Constant.API_LOCATION_ADD;
			
			String result = postJsonData(
								url,
								nameValuePairs, context);
			
			Log.e("CongVC", "===Post Location:" + result);
									
//			Poem[] arrResult = JSON.decode(result, Poem[].class);
//			return Arrays.asList(arrResult);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Get json data
	 * 
	 * @return
	 */
	public static String getJsonData(String url,
			List<NameValuePair> nameValuePairs, Context context) {

		try {
			// Create a new HttpClient and Post Header
			HttpClient httpclient = getDefaultHttpClient();

			// API url
			HttpGet httpGet = new HttpGet(url);

//			String sessionId = Util.getSessionId(context);
			try {
//				String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
//				if(!authToken.isEmpty()){
//					httpGet.setHeader(KEY_X_AUTH_TOKEN, authToken);
//				}
				httpGet.setHeader(KEY_X_API_KEY, 
						Constant.X_API_KEY_ENCODE);
//				httpGet.setHeader("Content-Type",
//						"application/x-www-form-urlencoded");
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Set parameter
//			if (nameValuePairs != null) {
//				HttpEntity entity = new UrlEncodedFormEntity(nameValuePairs,
//						HTTP.UTF_8);
//				httppost.setEntity(entity);
//			}

			HttpResponse response = httpclient.execute(httpGet);

			HttpEntity responseEntity = response.getEntity();

			// Get result string
			if (responseEntity != null) {

				String reponseString = EntityUtils.toString(responseEntity);

				Log.i("RESPONSE", reponseString);
				return reponseString;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * Get json data
	 * 
	 * @return
	 */
	public static void getRandom(
			final List<NameValuePair> nameValuePairs, final Context context) {
		
		try {
//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			Response.Listener<String> listener = new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Random Res:" + response);
			    	   
			    	   Poem[] arrResult = JSON.decode(response, Poem[].class);
			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(Arrays.asList(arrResult)));
			    	   				    	   
					} catch (Exception e) {
						PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_GET_RANDOM_ERROR_DEF));
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Random Err:" + error.getMessage());
			    	
			    	PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_GET_RANDOM_ERROR_DEF));
			    }
			};
			
			StringRequest sr = new StringRequest(Request.Method.GET, Constant.API_POEM_RANDOM, listener, eListener){
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        for (int index = 0; index < nameValuePairs.size(); index++) {
						String name = nameValuePairs.get(index).getName();
						String value = nameValuePairs.get(index).getValue();
						params.put(name, value);
			        }

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
//			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
			        		Constant.X_API_KEY_ENCODE);
			        String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					if(!authToken.isEmpty()){
						params.put(KEY_X_AUTH_TOKEN, 
				        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
					}
			        
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		

		} catch (Exception e) {
			e.printStackTrace();
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_GET_RANDOM_ERROR_DEF));
		}
	}
	
	/**
	 * Get poem by id
	 * 
	 * @return
	 */
	public static void getPoemById(
			final List<NameValuePair> nameValuePairs, final Context context, final int id) {
		
		try {

			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			Response.Listener<String> listener = new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
			       try {
			    	   
			    	   Log.e("CongVC", "===Poem detail Res:" + response);
			    	   
			    	   Poem arrResult = JSON.decode(response, Poem.class);
			    	   AsyncTaskResultEvent eventResult = new AsyncTaskResultEvent();
			    	   eventResult.setPoemDetail(arrResult);
			    	   PoemHelper.getInstance().post(eventResult);
			    	   				    	   
					} catch (Exception e) {
						PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_GET_DETAIL_ERROR_DEF));
					}
			    }
			};
			
			Response.ErrorListener eListener = new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	
			    	Log.e("CongVC", "===Poem detail Err:" + error.getMessage());
			    	
			    	PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_GET_DETAIL_ERROR_DEF));
			    }
			};
			
			StringRequest sr = new StringRequest(Request.Method.GET, 
										Constant.API_POEM_DETAIL.replace(Constant.REFIX_ID, ""+id), 
										listener, eListener){
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        for (int index = 0; index < nameValuePairs.size(); index++) {
						String name = nameValuePairs.get(index).getName();
						String value = nameValuePairs.get(index).getValue();
						params.put(name, value);
			        }

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
//			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
			        		Constant.X_API_KEY_ENCODE);
			        String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					if(!authToken.isEmpty()){
						params.put(KEY_X_AUTH_TOKEN, 
				        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
					}
			        
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		

		} catch (Exception e) {
			e.printStackTrace();
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_GET_DETAIL_ERROR_DEF));
		}
	}
	
	
	
	public static String getJsonDataAuthToken(String url,
			List<NameValuePair> nameValuePairs, Context context) {

		try {
			// Create a new HttpClient and Post Header
			HttpClient httpclient = getDefaultHttpClient();

			// API url
			HttpGet httpGet = new HttpGet(url);

//			String sessionId = Util.getSessionId(context);
			try {
//				httpGet.setHeader("Content-type",
//						"application/x-www-form-urlencoded");
				

				httpGet.setHeader(KEY_X_API_KEY, 
						Constant.X_API_KEY_ENCODE);
				
				String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
				if(!authToken.isEmpty()){
					httpGet.setHeader(KEY_X_AUTH_TOKEN, authToken);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Set parameter
//			if (nameValuePairs != null) {
//				HttpEntity entity = new UrlEncodedFormEntity(nameValuePairs,
//						HTTP.UTF_8);
//				httppost.setEntity(entity);
//			}

			HttpResponse response = httpclient.execute(httpGet);

			HttpEntity responseEntity = response.getEntity();

			// Get result string
			if (responseEntity != null) {

				String reponseString = EntityUtils.toString(responseEntity);

				Log.i("RESPONSE", reponseString);
				return reponseString;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static Poem postPoemTest(final Context context, final List<NameValuePair> nameValuePairs){
		try {
						
			String result = postJsonDataWithImage(Constant.API_POEM_CREATE, nameValuePairs, context);
			
			Log.e("CongVC", "===Post poem:" + result);
									
//			Poem[] arrResult = JSON.decode(result, Poem[].class);
//			return Arrays.asList(arrResult);
			
			if(! new JSONObject(result).isNull(KEY_ERROR)){
        		return null;
        	}
			
			Poem poem = JSON.decode(result, Poem.class);
			return poem;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Poem updatePoemTest(final Context context, final List<NameValuePair> nameValuePairs, int id){
		try {
			
			String url = Constant.API_POEM_UPDATE.replace(Constant.REFIX_ID, ""+id);
						
			String result = updateJsonDataWithImage(url, nameValuePairs, context);
			
			Log.e("CongVC", "===Update poem:" + result);
									
//			Poem[] arrResult = JSON.decode(result, Poem[].class);
//			return Arrays.asList(arrResult);
			
			if(! new JSONObject(result).isNull(KEY_ERROR)){
        		return null;
        	}
			
			Poem poem = JSON.decode(result, Poem.class);
			return poem;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Post json data
	 * 
	 * @return
	 */
	public static String postJsonData(String url,
			List<NameValuePair> nameValuePairs, Context context) {

		try {
			// Create a new HttpClient and Post Header
			HttpClient httpclient = getDefaultHttpClient();

			// API url
			HttpPost httppost = new HttpPost(url);
			try {
				httppost.setHeader(KEY_X_API_KEY, 
						Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
				httppost.setHeader(KEY_X_AUTH_TOKEN, 
			        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Set parameter
			if (nameValuePairs != null) {
				HttpEntity entity = new UrlEncodedFormEntity(nameValuePairs,
						HTTP.UTF_8);
				httppost.setEntity(entity);
				
			}

			httppost.setHeader("Content-Type",
					"application/x-www-form-urlencoded");

			HttpResponse response = httpclient.execute(httppost);

			HttpEntity responseEntity = response.getEntity();

			// Get result string
			if (responseEntity != null) {

				String reponseString = EntityUtils.toString(responseEntity);

				Log.i("RESPONSE", reponseString);
				return reponseString;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * Get json data
	 * 
	 * @return
	 */
	public static String postJsonDataWithImage(String url,
			List<NameValuePair> nameValuePairs, Context context) {

		try {
		
			// Create a new HttpClient and Post Header
			HttpClient httpclient = getDefaultHttpClient();

			// API url
			HttpPost httppost = new HttpPost(url);
			
			try {
				
				httppost.setHeader(KEY_X_AUTH_TOKEN, 
		        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
				httppost.setHeader(KEY_X_API_KEY, 
						Constant.X_API_KEY_ENCODE);
				
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Set parameter
			MultipartEntity entity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
			
			for (int index = 0; index < nameValuePairs.size(); index++) {
				String name = nameValuePairs.get(index).getName();
				String value = nameValuePairs.get(index).getValue();
				if (value == null)
					continue;
				if ((name.indexOf("photo") >= 0)
						&& value.trim().length() > 0) {
					
						entity.addPart(name,
								new FileBody(new File(value), "binary/octet-stream"));

				} else if (value.trim().length() > 0) {
					// Normal string data
					entity.addPart(name,
							new StringBody(
									value,
									Charset.forName("UTF-8")));
				}
			}

			httpclient.getParams().setParameter(
					CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);


			httppost.setEntity(entity);
			
			HttpResponse response = httpclient.execute(httppost);
			
			HttpEntity responseEntity = response.getEntity();

			// Get result string
			if (responseEntity != null) {

				String reponseString = EntityUtils.toString(responseEntity);

				return reponseString;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * Get json data
	 * 
	 * @return
	 */
	public static String updateJsonDataWithImage(String url,
			List<NameValuePair> nameValuePairs, Context context) {

		try {
		
			// Create a new HttpClient and Post Header
			HttpClient httpclient = getDefaultHttpClient();

			// API url
			HttpPost httpput = new HttpPost(url);
			
			try {
				
				httpput.setHeader(KEY_X_AUTH_TOKEN, 
		        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
				httpput.setHeader(KEY_X_API_KEY, 
						Constant.X_API_KEY_ENCODE);
				
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Set parameter
			MultipartEntity entity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
			
			for (int index = 0; index < nameValuePairs.size(); index++) {
				String name = nameValuePairs.get(index).getName();
				String value = nameValuePairs.get(index).getValue();
				Log.e("CongVC", "======" + name + "=====" + value);
				if (value == null)
					continue;
				if ((name.indexOf("photo") >= 0 && !value.equals("-1"))
						&& value.trim().length() > 0) {
					
						entity.addPart(name,
								new FileBody(new File(value), "binary/octet-stream"));

				} else {//if (value.trim().length() > 0) {
					// Normal string data
					entity.addPart(name,
							new StringBody(
									value,
									Charset.forName("UTF-8")));
				}
			}

			httpclient.getParams().setParameter(
					CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);


			httpput.setEntity(entity);
			
			HttpResponse response = httpclient.execute(httpput);
			
			HttpEntity responseEntity = response.getEntity();

			// Get result string
			if (responseEntity != null) {

				String reponseString = EntityUtils.toString(responseEntity);

				return reponseString;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	
	
	/**
	 * Post login user 
	 * 
	 * @param context
	 * @param username
	 * @param password
	 */
	public static void postLogin(Context context, final String username, 
			final String password){//, final JsonResponseListener mResponseListener){
		try {

//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			StringRequest sr = new StringRequest(Request.Method.POST, Constant.API_USER_LOGIN, 
										new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
//			        mResponseListener.requestCompleted(response);
				       try {
				    	   
				    	   Log.e("CongVC", "===Login Res:" + response);
				    	   				    	   
//				    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(
//				    			   				new JSONObject(response).getString(KEY_ACCESS_TOKEN)));
				    	   
				    	   User user = JSON.decode(response, User.class);
				    	   
//				    	   User user = new User();
//				    	   user.setEmail(username);
				    	   user.setPassword(password);
//				    	   user.setPen_name("Cong VC");
//				    	   user.setAccess_token(new JSONObject(response).getString(KEY_ACCESS_TOKEN));
				    	   
				    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(user));
				    	   
					} catch (Exception e) {
						 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOGIN_ERROR_DEF));
					}
			    }
			}, new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	Log.e("CongVC", "===Login Err:" + error.getMessage());
//			    	mResponseListener.requestEndedWithError(error);
			    	 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOGIN_ERROR_DEF));
			    }
			}){
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        params.put(KEY_USER_EMAIL, username);
			        params.put(KEY_PASSWORD,password);

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOGIN_ERROR_DEF));
		}
	}
	
	public static void postRegisterUser(Context context, final User user){
		try {
//			mResponseListener.requestStarted();
//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			StringRequest sr = new StringRequest(Request.Method.POST, Constant.API_USER_REGISTER, 
										new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
//			        mResponseListener.requestCompleted(response);
			       try {
			    	   
			    	   Log.e("CongVC", "===Register Res:" + response);
			    	   
//			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(
//			    			   				new JSONObject(response).getString(KEY_ACCESS_TOKEN)));
			    	   
//			    	   User userSuccess = JSON.decode(response, User.class);
			    	   
			    	   User userSuccess = new User();
			    	   userSuccess.setEmail(user.getEmail());
			    	   userSuccess.setPassword(user.getPassword());
			    	   userSuccess.setPen_name(user.getPen_name());
			    	   userSuccess.setAccess_token(new JSONObject(response).getString(KEY_ACCESS_TOKEN));
			    	   
			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(userSuccess));
					} catch (Exception e) {
						 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.REGISTER_ERROR_DEF));
					}
			    }
			}, new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	Log.e("CongVC", "===Register Err:" + error.getMessage());
//			    	mResponseListener.requestEndedWithError(error);
			    	 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.REGISTER_ERROR_DEF));
			    }
			}){
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        params.put(KEY_FIRST_NAME, user.getFirst_name());
			        params.put(KEY_LAST_NAME, user.getLast_name());
			        params.put(KEY_PEN_NAME, user.getPen_name());
			        params.put(KEY_USER_EMAIL, user.getEmail());
			        params.put(KEY_PASSWORD, user.getPassword());

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.REGISTER_ERROR_DEF));
		}
	}
	
	public static void postLocation(final Context context, final String locationname, 
			final String address, final int zoneId, final String funFact,
			final double lat, final double lon){
		try {
//			mResponseListener.requestStarted();
//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			StringRequest sr = new StringRequest(Request.Method.POST, Constant.API_LOCATION_ADD, 
										new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {
//			        mResponseListener.requestCompleted(response);
			       try {
			    	   
			    	   Log.e("CongVC", "===Location add res:" + response);
			    	   
			    	   if(! new JSONObject(response).isNull(KEY_ERROR)){
			    		   PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
			    	   }
			    	   else {
			    		   LocationMap locationMap = JSON.decode(response, LocationMap.class);
				    	   
				    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(locationMap));
			    	   }
			    	   
				} catch (Exception e) {
					 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
				}
			    }
			}, new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	Log.e("CongVC", "===Location add err:" + error.getMessage());
//			    	mResponseListener.requestEndedWithError(error);
			    	 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
			    }
			}){
			    @Override
			    protected Map<String,String> getParams(){
			    	
			    	Log.e("CongVC", "===Post Location:" + zoneId + "==" + locationname + "==" + address + "==" + funFact + "==" + lat + "==" + lon);
			        Map<String,String> params = new HashMap<String, String>();
			        params.put(KEY_ZONE_ID, zoneId+"");
			        params.put(KEY_NAME, locationname);
			        params.put(KEY_ADDRESS, address+"");
			        params.put(KEY_FUN_FACT, funFact);
			        params.put(KEY_LAT, lat+"");
			        params.put(KEY_LON, lon+"");

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			    	
			    	String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
					Log.e("CongVC", "===token location:"+ authToken);
			    				    	
			        Map<String,String> params = new HashMap<String, String>();
			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
			        params.put(KEY_X_AUTH_TOKEN, 
			        		authToken);
			        return params;
			    }
			};
			
			
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
		}
	}
	
	public static void postLocationTest(final Context context, final String locationname, 
			final String address, final int zoneId, final String funFact,
			final double lat, final double lon){
		try {
//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
            Response.Listener<String> ssListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                	try {
 			    	   
 			    	   Log.e("CongVC", "===Location add res 11:" + response);
 			    	   
 			    	  if(! new JSONObject(response).isNull(KEY_ERROR)){
 			    		 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
 			    	  }
 			    	  else{
 			    		 LocationMap locationMap = JSON.decode(response, LocationMap.class);
 	 			    	   
 	 			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(locationMap));
 			    	  }
 			    	   
 			    	   
	 				} catch (Exception e) {
	 					 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
	 				}
                	
                }
            };

            Response.ErrorListener eListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                	Log.e("CongVC", "===Location add err:" + error.getMessage());
//			    	mResponseListener.requestEndedWithError(error);
			    	 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
                }
            };
			MultiPartStringRequest multipartRequest = new MultiPartStringRequest(Request.Method.POST, 
					"http://textinthecity.herokuapp.com/api/v1/locations/",  
					ssListener, 
					eListener) {
				@Override
			    protected Map<String,String> getParams(){
			       
					Map<String,String> params = new HashMap<String, String>();
			        params.put("zone_id", zoneId+"");
			        params.put("name", locationname);
			        params.put("address", address+"");
			        params.put("fun_fact", funFact);
			        params.put("lat", lat+"");
			        params.put("lon", lon+"");

			        return params;
			    }
			};
//			multipartRequest.setHeader("Content-Type","multipart/form-data");
			multipartRequest.setHeader(KEY_X_API_KEY, 
							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
			multipartRequest.setHeader(KEY_X_AUTH_TOKEN, 
	        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
			
			
			multipartRequest.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(multipartRequest);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
		}
	}
	
	public static void postPoem(final Context context, final Map<String,String> params, final JSONObject jsonRequest){
		try {
//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
            Response.Listener<String> ssListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                    	Log.e("CongVC", "===Post poem res:"+ response);
                    	if(! new JSONObject(response).isNull(KEY_ERROR)){
                    		PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ERROR_DEF));
                    	}
                    	else {
                    		Poem poem = JSON.decode(response, Poem.class);
       			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(poem));
                    	}
	  				} catch (Exception e) {
						
						PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ERROR_DEF));
					}
                }
            };

            Response.ErrorListener eListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                	Log.e("CongVC", "===Post poem err:"+ error.getMessage());
                	PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ERROR_DEF));
                }
            };
			
//			MultiPartRequest multipartRequest = new MultiPartRequest(Request.Method.POST, 
//																		Constant.API_POEM_CREATE, 
//																		jsonRequest, 
//																		sListener, 
//																		eListener) {
//				@Override
//			    protected Map<String,String> getParams(){
//			       
//
//			        return params;
//			    }
//			};
			MultiPartStringRequest multipartRequest = new MultiPartStringRequest(Request.Method.POST, 
					Constant.API_POEM_CREATE,  
					ssListener, 
					eListener){
				@Override
			    protected Map<String,String> getParams(){
			       

			        return params;
			    }
			};
			
			multipartRequest.setStringUpload(params);
			
			multipartRequest.setHeader(KEY_X_API_KEY, 
							Constant.X_API_KEY_ENCODE);
			multipartRequest.setHeader(KEY_X_AUTH_TOKEN, 
	        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
//			multipartRequest.setHeader("Content-Type","multipart/form-data");
			if(params.containsKey("photo")){
				
				File file = new File(params.get("photo"));

				Log.e("CongVC", "====file:" + file);
				multipartRequest.addFileUpload("photo", file);
			}
//			
//			StringRequest sr = new StringRequest(Request.Method.POST, Constant.API_POEM_CREATE, 
//										new Response.Listener<String>() {
//			    @Override
//			    public void onResponse(String response) {
////			        mResponseListener.requestCompleted(response);
//			       try {
//			    	   
//			    	   Log.e("CongVC", "===poem:"+ response);
//			    	   LocationMap locationMap = JSON.decode(response, LocationMap.class);
//			    	   
//			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(locationMap));
//				} catch (Exception e) {
//					 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
//				}
//			    }
//			}, new Response.ErrorListener() {
//			    @Override
//			    public void onErrorResponse(VolleyError error) {
//			    	Log.e("CongVC", "===poem err:"+ error.getMessage());
////			    	mResponseListener.requestEndedWithError(error);
//			    	 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.LOCATION_ADD_ERROR_DEF));
//			    }
//			}){
//			    @Override
//			    protected Map<String,String> getParams(){
//			       
//			        return params;
//			    }
//
//			    @Override
//			    public Map<String, String> getHeaders() throws AuthFailureError {
//			        Map<String,String> params = new HashMap<String, String>();
//			        params.put("Content-Type","application/x-www-form-urlencoded");
//			        params.put(KEY_X_API_KEY, 
//							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
//			        params.put(KEY_X_AUTH_TOKEN, 
//			        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
//			        return params;
//			    }
//			};
//			
//			queue.add(sr);
			
			multipartRequest.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			
			queue.add(multipartRequest);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ERROR_DEF));
		}
	}
	
//	public static void postPoemTest(final Context context, final List<NameValuePair> nameValuePairs){
//		try {
//			RequestQueue queue = Volley.newRequestQueue(context);
//			            
//            Response.Listener<String> ssListener = new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    try {
//                    	Log.e("CongVC", "===Post poem res:"+ response);
//                    	if(! new JSONObject(response).isNull(KEY_ERROR)){
//                    		PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ERROR_DEF));
//                    	}
//                    	else {
//                    		Poem poem = JSON.decode(response, Poem.class);
//       			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(poem));
//                    	}
//	  				} catch (Exception e) {
//						
//						PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ERROR_DEF));
//					}
//                }
//            };
//
//            Response.ErrorListener eListener = new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                	Log.e("CongVC", "===Post poem err:"+ error.getMessage());
//                	PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ERROR_DEF));
//                }
//            };
//						
//			MultipartRequest multipartRequest = new MultipartRequest(Constant.API_POEM_CREATE, nameValuePairs, ssListener, eListener){
//				@Override
//				public Map<String, String> getHeaders() throws AuthFailureError {
//					Map<String, String> mapHeader = new HashMap<String, String>();
//					mapHeader.put(KEY_X_API_KEY, 
//							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
//					mapHeader.put(KEY_X_AUTH_TOKEN, 
//			        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
////					mapHeader.put("enctype", "multipart/form-data");
////					mapHeader.put("Content-Type", "multipart/form-data");
//					return mapHeader;
//				}
//			};
//			
//			
//			multipartRequest.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
//	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
//	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//			
//			queue.add(multipartRequest);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_ADD_ERROR_DEF));
//		}
//	}
	
	public static void putUpdatePoem(final Context context, final Map<String,String> params, final JSONObject jsonRequest){
		try {
//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
            
            Response.Listener<String> ssListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                    	Log.e("CongVC", "===Update poem res:"+ response);
                    	if(! new JSONObject(response).isNull(KEY_ERROR)){
                    		PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_UPDATE_ERROR_DEF));
                    	}
                    	else {
                    		Poem poem = JSON.decode(response, Poem.class);
       			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(poem));
                    	}
	  				} catch (Exception e) {
						
						PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_UPDATE_ERROR_DEF));
					}
                }
            };

            Response.ErrorListener eListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                	Log.e("CongVC", "===Update poem err:"+ error.getMessage());
                	PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_UPDATE_ERROR_DEF));
                }
            };
			
			MultiPartStringRequest multipartRequest = new MultiPartStringRequest(Request.Method.PUT, 
					Constant.API_POEM_UPDATE.replace(Constant.REFIX_ID, params.get(KEY_ID)),  
					ssListener, 
					eListener) {
				@Override
			    protected Map<String,String> getParams(){
			       

			        return params;
			    }
			};
//			multipartRequest.setHeader("Content-Type","multipart/form-data");
			multipartRequest.setHeader(KEY_X_API_KEY, 
							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
			multipartRequest.setHeader(KEY_X_AUTH_TOKEN, 
	        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
			if(params.containsKey("photo")){

				multipartRequest.addFileUpload("photo", new File(params.get("photo")));
			}
			
			multipartRequest.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(multipartRequest);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_UPDATE_ERROR_DEF));
		}
	}
	
	/**
	 * Like poem by user
	 * 
	 * @param context
	 * @param poemId
	 */
	public static void postLikePoem(final Context context, final int poemId){
		try {

//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			StringRequest sr = new StringRequest(Request.Method.POST, 
										Constant.API_POEM_LIKE.replace(Constant.REFIX_ID, poemId+""), 
										new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {

				       try {
				    	   
				    	   Log.e("CongVC", "===Like poem Res:" + response);
				    	   				    	   
//				    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(
//				    			   				new JSONObject(response).getString(KEY_ACCESS_TOKEN)));
				    	   
				    	   
				    	   if(! new JSONObject(response).isNull(KEY_ERROR)){
	                    		PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_LIKE_ERROR_DEF));
	                    	}
	                    	else {
	                    		Poem poem = JSON.decode(response, Poem.class);
//	       			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_LIKE_SUCCESS_DEF));
	                    		
	                    		AsyncTaskResultEvent resultEvent = new AsyncTaskResultEvent();
	                    		resultEvent.setPoemLike(poem);
	                    		PoemHelper.getInstance().post(resultEvent);
	                    	}
				    	   
				    	   
					} catch (Exception e) {
						 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_LIKE_ERROR_DEF));
					}
			    }
			}, new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	Log.e("CongVC", "===Like poem Err:" + error.getMessage());
//			    	mResponseListener.requestEndedWithError(error);
			    	 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_LIKE_ERROR_DEF));
			    }
			}){
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
			        params.put(KEY_X_AUTH_TOKEN, 
			        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_LIKE_ERROR_DEF));
		}
	}
	
	/**
	 * Post Like
	 * 
	 * @param zoneId
	 * @return
	 */
	public static void postLike(final Context context, final int poemId){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			String result = postJsonData(
								Constant.API_POEM_LIKE.replace(Constant.REFIX_ID, poemId+""),
								nameValuePairs, context);
			
			Log.e("CongVC", "===Like poem res:" + result);
						
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_LIKE_SUCCESS_DEF));
			
		} catch (Exception e) {
			e.printStackTrace();
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_LIKE_ERROR_DEF));
		}
	}
	
	/**
	 * Like poem by user
	 * 
	 * @param context
	 * @param poemId
	 */
	public static void postRatePoem(final Context context, final int poemId, final int rating){
		try {

//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			StringRequest sr = new StringRequest(Request.Method.POST, 
										Constant.API_POEM_RATE.replace(Constant.REFIX_ID, poemId+""), 
										new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {

				       try {
				    	   
				    	   Log.e("CongVC", "===Rate poem Res:" + response);
				    	   				    	   
//				    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(
//				    			   				new JSONObject(response).getString(KEY_ACCESS_TOKEN)));
				    	   
				    	   
				    	   if(! new JSONObject(response).isNull(KEY_ERROR)){
	                    		PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_RATE_ERROR_DEF));
	                    	}
	                    	else {
	       			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_RATE_SUCCESS_DEF));
	                    	}
				    	   
				    	   
					} catch (Exception e) {
						 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_RATE_ERROR_DEF));
					}
			    }
			}, new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	Log.e("CongVC", "===Rate poem Err:" + error.getMessage());
//			    	mResponseListener.requestEndedWithError(error);
			    	 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_RATE_ERROR_DEF));
			    }
			}){
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        params.put(KEY_GRADE, rating+"");

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
			        params.put(KEY_X_AUTH_TOKEN, 
			        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_RATE_ERROR_DEF));
		}
	}
	
	/**
	 * report poem by user
	 * 
	 * @param context
	 * @param poemId
	 */
	public static void postReportPoem(final Context context, final int poemId, final String reason){
		try {

//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			StringRequest sr = new StringRequest(Request.Method.POST, 
										Constant.API_POEM_REPORT.replace(Constant.REFIX_ID, poemId+""), 
										new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {

				       try {
				    	   
				    	   Log.e("CongVC", "===Report poem Res:" + response);
				    	   		
				    	   
				    	   if(! new JSONObject(response).isNull(KEY_ERROR)){
	                    		PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_REPORT_ERROR_DEF));
	                    	}
	                    	else {
	       			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_REPORT_SUCCESS_DEF));
	                    	}
				    	   
				    	   
					} catch (Exception e) {
						 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_REPORT_ERROR_DEF));
					}
			    }
			}, new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	Log.e("CongVC", "===Report poem Err:" + error.getMessage());
//			    	mResponseListener.requestEndedWithError(error);
			    	 PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_REPORT_ERROR_DEF));
			    }
			}){
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        params.put(KEY_REASON, reason);

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
			        params.put(KEY_X_AUTH_TOKEN, 
			        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(Constant.POEM_REPORT_ERROR_DEF));
		}
	}
	
	/**
	 * delete user poem
	 * 
	 * @param context
	 * @param poemId
	 */
	public static void deletePoem(final Context context, final int poemId, final boolean isDraft){
		try {

//			RequestQueue queue = Volley.newRequestQueue(context);
			RequestQueue queue = PoemApplication.getInstance().getQueue();
			
			StringRequest sr = new StringRequest(Request.Method.DELETE, 
										Constant.API_POEM_DELETE.replace(Constant.REFIX_ID, poemId+""), 
										new Response.Listener<String>() {
			    @Override
			    public void onResponse(String response) {

				       try {
				    	   
				    	   Log.e("CongVC", "===Delete poem Res:" + response);
				    	   		
				    	   
				    	   if(! new JSONObject(response).isNull(KEY_ERROR)){
	                    		PoemHelper.getInstance().post(
	                    				new AsyncTaskResultEvent(isDraft ? Constant.POEM_DELETE_ERROR_DEF
	                    											: Constant.POEM_SUBMISSION_DELETE_ERROR_DEF));
	                    	}
	                    	else {
	       			    	   PoemHelper.getInstance().post(new AsyncTaskResultEvent(isDraft ? Constant.POEM_DELETE_SUCCESS_DEF
	       			    			   								: Constant.POEM_SUBMISSION_DELETE_SUCCESS_DEF));
	                    	}
				    	   
				    	   
					} catch (Exception e) {
						e.printStackTrace();
						 PoemHelper.getInstance().post(new AsyncTaskResultEvent(isDraft ? Constant.POEM_DELETE_ERROR_DEF
																		: Constant.POEM_SUBMISSION_DELETE_ERROR_DEF));
					}
			    }
			}, new Response.ErrorListener() {
			    @Override
			    public void onErrorResponse(VolleyError error) {
			    	Log.e("CongVC", "===Delete poem Err:" + error.getMessage());
//			    	mResponseListener.requestEndedWithError(error);
			    	 PoemHelper.getInstance().post(new AsyncTaskResultEvent(isDraft ? Constant.POEM_DELETE_ERROR_DEF
																		: Constant.POEM_SUBMISSION_DELETE_ERROR_DEF));
			    }
			}){
			    @Override
			    protected Map<String,String> getParams(){
			        Map<String,String> params = new HashMap<String, String>();
			        

			        return params;
			    }

			    @Override
			    public Map<String, String> getHeaders() throws AuthFailureError {
			        Map<String,String> params = new HashMap<String, String>();
			        params.put("Content-Type","application/x-www-form-urlencoded");
			        params.put(KEY_X_API_KEY, 
							Base64.encodeToString(Constant.X_API_KEY.getBytes(), Base64.DEFAULT));
			        params.put(KEY_X_AUTH_TOKEN, 
			        		SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN));
			        return params;
			    }
			};
			
			sr.setRetryPolicy(new DefaultRetryPolicy(AbstractApi.CONNECT_TIMEOUT, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			queue.add(sr);

		} catch (Exception e) {
			PoemHelper.getInstance().post(new AsyncTaskResultEvent(isDraft ? Constant.POEM_DELETE_ERROR_DEF
																: Constant.POEM_SUBMISSION_DELETE_ERROR_DEF));
		}
	}
	
	
	/**
	 * List random poems
	 * 
	 * @return
	 */
	public static List<Poem> getPoemSubmission(Context context){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			String url = Constant.API_POEM_MY_ENTRIES.replace(Constant.REFIX_STATUS_ID, Constant.POEM_STATUS_PUBLISH+"");
//			String url = Constant.API_POEM_RANDOM;
			
			String result = getJsonDataAuthToken(
								url,
								nameValuePairs,
								context);
			
			Log.e("CongVC", "===Poem Submission res:" + result);
			
			Poem[] arrResult = JSON.decode(result, Poem[].class);
			return Arrays.asList(arrResult);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * List random poems
	 * 
	 * @return
	 */
	public static List<Poem> getPoemDraft(Context context){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			String url = Constant.API_POEM_MY_ENTRIES.replace(Constant.REFIX_STATUS_ID, Constant.POEM_STATUS_DRAFT+"");
//			String url = Constant.API_POEM_RANDOM;
			
			String result = getJsonDataAuthToken(
								url,
								nameValuePairs,
								context);
			
			Log.e("CongVC", "===Poem Draft res:" + result);
			
			Poem[] arrResult = JSON.decode(result, Poem[].class);
			return Arrays.asList(arrResult);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * List feature poems
	 * 
	 * @return
	 */
	public static PoemPagging getPoemFeatured(Context context, int page, int perPage, SortField sortField, int filter){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			String url = Constant.API_POEM_CATEGORY;
			if(sortField != SortField.none){
				if(filter == 0) {
					url = Constant.API_POEM_CATEGORY_SORT;
					
				}
				else {
					url = Constant.API_POEM_CATEGORY_FILTER;
					if(sortField == SortField.language){
						url = url.replace(Constant.REFIX_FILTER_PARAM, "language_id")
									.replace(Constant.REFIX_FILTER_VALUE, filter+"");
						
					}
					else {
						url = url.replace(Constant.REFIX_FILTER_PARAM, "zone_id")
								.replace(Constant.REFIX_FILTER_VALUE, filter+"");
					}
					
				}
				url = url.replace(Constant.REFIX_SORT_FIELD, sortField.toString());
			}
			url = url.replace(Constant.REFIX_PAGE, ""+page)
                    .replace(Constant.REFIX_PER_PAGE, ""+perPage)
                    .replace(Constant.REFIX_CATEGORY_ID, Constant.CATEGORY_ID_FEATURE+"")
                    .replace(Constant.REFIX_STATUS_ID, Constant.POEM_STATUS_PUBLISH+"");
			
			
			String result = getJsonDataAuthToken(
								url,
								nameValuePairs,
								context);
			
			Log.e("CongVC", "===Poem Featured res:" + result);
			
			PoemPagging arrResult = JSON.decode(result, PoemPagging.class);
			return arrResult;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * List public poems
	 * 
	 * @return
	 */
	public static PoemPagging getPoemPublic(Context context, int page, int perPage, SortField sortField, int filter){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//			String url = Constant.API_POEM_CATEGORY.replace(Constant.REFIX_PAGE, ""+page)
//                    .replace(Constant.REFIX_PER_PAGE, ""+perPage)
//                    .replace(Constant.REFIX_CATEGORY_ID, Constant.CATEGORY_ID_PUBLIC+"")
//                    .replace(Constant.REFIX_STATUS_ID, Constant.POEM_STATUS_PUBLISH+"");
			
			String url = Constant.API_POEM_CATEGORY_PUBLIC;
			if(sortField != SortField.none){
				if(filter == 0) {
					url = Constant.API_POEM_CATEGORY_PUBLIC_SORT;
					
				}
				else {
					url = Constant.API_POEM_CATEGORY_PUBLIC_FILTER;
					if(sortField == SortField.language){
						url = url.replace(Constant.REFIX_FILTER_PARAM, "language_id")
									.replace(Constant.REFIX_FILTER_VALUE, filter+"");
						
					}
					else {
						url = url.replace(Constant.REFIX_FILTER_PARAM, "zone_id")
								.replace(Constant.REFIX_FILTER_VALUE, filter+"");
					}
					
				}
				url = url.replace(Constant.REFIX_SORT_FIELD, sortField.toString());
			}
			url = url.replace(Constant.REFIX_PAGE, ""+page)
                    .replace(Constant.REFIX_PER_PAGE, ""+perPage)
                    .replace(Constant.REFIX_CATEGORY_ID, Constant.CATEGORY_ID_PUBLIC+"")
                    .replace(Constant.REFIX_STATUS_ID, Constant.POEM_STATUS_PUBLISH+"");
			
			Log.e("CongVC", "=====Url filter:" + url);

			
			String result = getJsonDataAuthToken(
								url,
								nameValuePairs,
								context);
			
			Log.e("CongVC", "===Poem Public res:" + result);
			
			PoemPagging arrResult = JSON.decode(result, PoemPagging.class);
			return arrResult;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * List search poems
	 * 
	 * @return
	 */
	public static PoemPagging getPoemSearch(Context context, int page, int perPage, String searchText){
		
		try {
			// Set parameter
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
//			String url = Constant.API_POEM_CATEGORY;
//			url = url.replace(Constant.REFIX_PAGE, ""+page)
//                    .replace(Constant.REFIX_PER_PAGE, ""+perPage)
//                    .replace(Constant.REFIX_CATEGORY_ID, Constant.CATEGORY_ID_FEATURE+"")
//                    .replace(Constant.REFIX_STATUS_ID, Constant.POEM_STATUS_PUBLISH+"");
			
			String url = Constant.API_POEM_SEARCH.replace(Constant.REFIX_TITLE, URLEncoder.encode(searchText, "UTF-8"));
			url = url.replace(Constant.REFIX_PAGE, ""+page)
                  .replace(Constant.REFIX_PER_PAGE, ""+perPage);
			
			URI uri = new URI(url);
			
			String result = getJsonDataAuthToken(
					uri.toString(),
								nameValuePairs,
								context);
			
			Log.e("CongVC", "===Poem Search res:" + result);
			
			PoemPagging arrResult = JSON.decode(result, PoemPagging.class);
			return arrResult;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
}
