package com.epsilon.textinthecity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.epsilon.textinthecity.common.CleanUpUtil;
import com.epsilon.textinthecity.common.SharePreference;
import com.viewpagerindicator.CirclePageIndicator;

public class TutorialActivity extends Activity implements OnClickListener {
	
	public static final String KEY_FROM_SETTING = "KEY_FROM_SETTING";
	
	private String[] urls;
	
	private Button btnEnter;
	
	private boolean isFromSetting = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutorial);
		

//		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_events), true);
//		
//		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SETTING);
					
//		setupWidthCenter();
		
		if(null != getIntent()){
			isFromSetting = getIntent().getBooleanExtra(KEY_FROM_SETTING, false);
		}
		
		if(isFromSetting){
			Button btnBack = (Button) findViewById(R.id.btnScreenBack);
			btnBack.setVisibility(View.VISIBLE);
			btnBack.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
		}
		
		urls = new String[]{"tutorial/tutorial-01.jpg", "tutorial/tutorial-02.jpg",
				"tutorial/tutorial-03.jpg", "tutorial/tutorial-04.jpg",
				"tutorial/tutorial-05.jpg", "tutorial/tutorial-06.jpg", 
				"tutorial/tutorial-07.jpg", "tutorial/tutorial-08.jpg"};
		
		btnEnter = (Button) findViewById(R.id.btnEnter);
		btnEnter.setVisibility(View.GONE);
		btnEnter.setOnClickListener(this);
		
		loadViewPager();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
				
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		try {
			
			CleanUpUtil.cleanupView(findViewById(R.id.main_layout));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
//	/**
//	 * Set up width and height map image
//	 */
//	private void setupWidthCenter() {
//		
//		Point point = new Point(0, 0);
//        Display display = getWindowManager().getDefaultDisplay();
//        display.getSize(point);
//        
//        // Set padding for center layout
//        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
//        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
//        													0, 
//        													paddingCenterLayout, 
//        													0);
//         
//	}
	
	private void loadViewPager(){
		final GalleryPagerAdapter adapterGallery = new GalleryPagerAdapter(this);
   	 ((ViewPager) findViewById(R.id.viewpager)).setAdapter(adapterGallery);
   	 ((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(0, true);
   	 
   	 CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
		mIndicator.setViewPager((ViewPager) findViewById(R.id.viewpager));
		
		final Button btnNext = (Button) findViewById(R.id.btnNext);
       final Button btnPrev = (Button) findViewById(R.id.btnPrev);
       if(adapterGallery.getCount() > 1){
			btnNext.setVisibility(View.VISIBLE);
		}
       mIndicator.setOnPageChangeListener(
       		new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				if(adapterGallery.getCount() <= 1){
					btnPrev.setVisibility(View.GONE);
					btnNext.setVisibility(View.GONE);
				}
				else if(arg0 == 0){
					btnPrev.setVisibility(View.GONE);
					btnNext.setVisibility(View.VISIBLE);
				}
				else if(arg0 >= adapterGallery.getCount()-1) {
					btnPrev.setVisibility(View.VISIBLE);
					btnNext.setVisibility(View.GONE);
				}
				else {
					btnPrev.setVisibility(View.VISIBLE);
					btnNext.setVisibility(View.VISIBLE);
				}
				
				if((arg0 == adapterGallery.getCount()-1) && !isFromSetting){
					btnEnter.setVisibility(View.VISIBLE);
				}
				else {
					btnEnter.setVisibility(View.GONE);
				}
			}
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
       
       btnNext.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(
						((ViewPager) findViewById(R.id.viewpager)).getCurrentItem()+1, true);
			}
		});
       btnPrev.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(
						((ViewPager) findViewById(R.id.viewpager)).getCurrentItem()-1, true);
			}
		});
	}
	
	private class GalleryPagerAdapter extends android.support.v4.view.PagerAdapter {
		
		private Context context;
		
		public GalleryPagerAdapter(Context context) {
			
			this.context = context;
			
		}

		@SuppressLint("InflateParams")
		public Object instantiateItem(View collection, final int position) {
			LayoutInflater inflater = (LayoutInflater) collection.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.gallery_page, null);
			ImageView imageView =  (ImageView) view.findViewById(R.id.gallery_image);
			imageView.setScaleType(ScaleType.FIT_XY);
			if(null != urls && urls.length > 0){
//				
				Drawable drawable;
				try {
					drawable = getAssetImage(context, urls[position]);
					imageView.setBackground(drawable);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
			
			((ViewPager) collection).addView(view, 0);
			return view;
		}

		@Override
		public int getCount() {
			return urls == null ? 1 : urls.length;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == ((View) arg1);
		}
		
		@Override
		public void destroyItem(View collection, int position, Object o) {
		    View view = (View) o;
		    ((ViewPager) collection).removeView(view);
		    CleanUpUtil.cleanupView(view);
		    view = null;
		}
	}
	
	public Drawable getAssetImage(Context context, String filename) throws IOException {
	    AssetManager assets = context.getResources().getAssets();
	    InputStream buffer = new BufferedInputStream((assets.open(filename)));
	    Bitmap bitmap = BitmapFactory.decodeStream(buffer);
	    return new BitmapDrawable(context.getResources(), bitmap);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnEnter:
			
			SharePreference.setPreferenceInt(this, SplashActivity.KEY_TUTORIAL_LOADED, 1);
			
			Intent it = new Intent(TutorialActivity.this, ExploreActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			finish();
			break;

		default:
			break;
		}
	}
}
