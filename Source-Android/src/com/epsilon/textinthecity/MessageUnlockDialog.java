package com.epsilon.textinthecity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.epsilon.textinthecity.object.Location;

public class MessageUnlockDialog extends Dialog {

	private Context mContext;
	private Location mLocation;
	
	
	public MessageUnlockDialog(Context context, int theme, Location location) {
		super(context, theme);
		mContext = context;
		mLocation = location;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_unlock_location);
		
		
		Point point = new Point(0, 0);
        Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
        display.getSize(point);
   
        LayoutParams lp = new LayoutParams((int)(point.x*0.9f), LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        lp.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        ((LinearLayout) findViewById(R.id.lnMessage)).setLayoutParams(lp);
                
        ((TextView) findViewById(R.id.tvCongrat)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
        ((TextView) findViewById(R.id.tvMessage)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaOblique);
        ((Button) findViewById(R.id.btnOK)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaOblique);
        
        String message = mLocation.getName();//mContext.getString(R.string.msg_unlock) + " \"" + mLocation.getName() + "\"";
        ((TextView) findViewById(R.id.tvMessage)).setText(message);
        
        
        ((Button) findViewById(R.id.btnOK)).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				MessageUnlockDialog.this.dismiss();
				Dialog dl = new UnlockDialog(mContext, 
						android.R.style.Theme_Translucent_NoTitleBar,
						mLocation);
				dl.show();
			}
		});
        
	}
	

}
