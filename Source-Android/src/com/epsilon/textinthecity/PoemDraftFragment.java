package com.epsilon.textinthecity;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.VolleyError;
import com.costum.android.widget.LoadMoreListView;
import com.epsilon.textinthecity.adapter.ListPoemDraftAdapter;
import com.epsilon.textinthecity.api.PoemDraftApi;
import com.epsilon.textinthecity.common.CleanUpUtil;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.inter_face.PoemDraftDeleteLisener;
import com.epsilon.textinthecity.inter_face.PoemDraftLisener;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.squareup.otto.Subscribe;

public final class PoemDraftFragment extends Fragment {
    private static final String KEY_CONTENT = "TestFragment:Content";
    
    private LoadMoreListView lvPoems;
	private ImageView imgLoading;
	private LinearLayout lnLoading;
	private ListPoemDraftAdapter mAdapter;
	private TextView tvError;
	private ArrayList<Poem> mListPoems;
		
	private boolean isFirst = true;
	
	private boolean isDelete = false;
	
	private View mView;
	
    public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
		
		if(null != mAdapter){
			mAdapter.setDelete(this.isDelete);
			mAdapter.notifyDataSetChanged();
		}
	}

	public static PoemDraftFragment newInstance() {
        PoemDraftFragment fragment = new PoemDraftFragment();

        return fragment;
    }

    private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
        Log.e("CongVC", "===Create flagment");
        
        
        
//        PoemHelper.getInstance().register(this);
        
        isFirst = true;
        
        
        if(null == mListPoems){
        	loadListPoems();
        }
    }
    
    @Override
    public void onDestroy() {
//    	PoemHelper.getInstance().unregister(this);
    	super.onDestroy();
    	try {
			CleanUpUtil.cleanupView(mView);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	PoemApplication.getInstance().cancelPendingRequests(this);
    }

    @SuppressLint("InflateParams")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
    	View view = inflater.inflate(R.layout.poem_draft_view, null);
        	
        	lvPoems = (LoadMoreListView) view.findViewById(R.id.listPoems);
        	lnLoading = (LinearLayout) view.findViewById(R.id.lnLoading);
        	imgLoading = (ImageView) view.findViewById(R.id.imgLoading);
    		imgLoading.post(new Runnable() {
    			    @Override
    			    public void run() {
    			        try {
    			        	AnimationDrawable frameAnimation =
    				            (AnimationDrawable) imgLoading.getBackground();
    				        frameAnimation.start();
    					} catch (Exception e) {
    						e.printStackTrace();
    					}
    			    }
    			}
    		);
    		
    		tvError = (TextView) view.findViewById(R.id.tvError);
    		tvError.setTypeface(PoemApplication.getInstance().typeFaceFuturaLightOblique);
    		
//    		lnLoading.setVisibility(View.GONE);
    		if(null != mListPoems){
    			lnLoading.setVisibility(View.GONE);
    			tvError.setVisibility(View.GONE);
    			processShowListPoems(mListPoems);
    		}
    		else if(!isFirst){
    			lnLoading.setVisibility(View.GONE);
    			tvError.setVisibility(View.VISIBLE);
    		}
//    		else {
//    			lnLoading.setVisibility(View.VISIBLE);
//	        	loadListPoems();
//    		}
    	isFirst = false;
    	mView = view;
        return view;
    }
    
    /**
	 * Load list poems from server
	 */
	private void loadListPoems() {
		
//		new ListPoemsDraftAsyncTask(getActivity()).execute();
		
		new PoemDraftApi().getAllPoemsDraft(getActivity(), mPoemDraftListener, this);
	}
	
	private PoemDraftLisener mPoemDraftListener = new PoemDraftLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			lnLoading.setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			lnLoading.setVisibility(View.GONE);
		}
		
		public void requestCompleted(ArrayList<Poem> listPoem) {
			lnLoading.setVisibility(View.GONE);
			 processShowListPoems(listPoem);
		};
	};
	
	public PoemDraftDeleteLisener mPoemDraftDeleteListener = new PoemDraftDeleteLisener() {
		
		@Override
		public void requestStarted() {
			
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			lnLoading.setVisibility(View.GONE);
			((MyEntriesActivity)getActivity()).createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_poem_delete_error)).show();
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			lnLoading.setVisibility(View.GONE);
			((MyEntriesActivity)getActivity()).createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_poem_delete_error)).show();
		}
		
		@Override
		public void requestCompleted(String message) {
			lnLoading.setVisibility(View.GONE);
			Toast.makeText(getActivity(), getString(R.string.msg_poem_delete_successful), Toast.LENGTH_SHORT)
								.show();
			if(null != mAdapter && mAdapter.getData().size() > 0 
					&& mAdapter.getPosDelete() < mAdapter.getData().size()){
//				mAdapter.getData().remove(mAdapter.getPosDelete());
				mListPoems.remove(mAdapter.getPosDelete());
				mAdapter.notifyDataSetChanged();
				if(mListPoems.size() <= 0){
					tvError.setVisibility(View.VISIBLE);
				}
			}
		}
	};
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {
			
		// Result for get list poems draft error
		if(null != event.getResult() && event.getResult().equals(Constant.POEM_DRAFT_ERROR_DEF)){
	    		   
			lnLoading.setVisibility(View.GONE);
		}
		else if(null != event.getResult() && event.getResult().equals(Constant.POEM_DELETE_ERROR_DEF)){
 		   
			lnLoading.setVisibility(View.GONE);
			((MyEntriesActivity)getActivity()).createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_poem_delete_error)).show();
		}
		else if(null != event.getResult() && event.getResult().equals(Constant.POEM_DELETE_SUCCESS_DEF)){
	 		   
			lnLoading.setVisibility(View.GONE);
			Toast.makeText(getActivity(), getString(R.string.msg_poem_delete_successful), Toast.LENGTH_SHORT)
								.show();
			if(null != mAdapter && mAdapter.getData().size() > 0 
					&& mAdapter.getPosDelete() < mAdapter.getData().size()){
//				mAdapter.getData().remove(mAdapter.getPosDelete());
				mListPoems.remove(mAdapter.getPosDelete());
				mAdapter.notifyDataSetChanged();
			}
		}
		else if(null != event.getListPoemsDraft()){
			lnLoading.setVisibility(View.GONE);
			 processShowListPoems(event.getListPoemsDraft());
		}
	}
	
	public void showLoading() {
		if(null != lnLoading){
			lnLoading.setVisibility(View.VISIBLE);
		}
	}
	
	/**
	 * Process show list poems ti list view
	 */
	private void processShowListPoems(ArrayList<Poem> listPoemsTemp) {
		
		if(null != listPoemsTemp && listPoemsTemp.size() > 0){
//	    	if(null == mListPoems){
	    		mListPoems = listPoemsTemp;
	    		mAdapter = new ListPoemDraftAdapter(getActivity(), mListPoems, this);
	    		mAdapter.setDelete(isDelete);
	    		lvPoems.setAdapter(mAdapter);
//	    	}
	    	
	    }
		else {

			tvError.setVisibility(View.VISIBLE);
		}
	}

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
}
