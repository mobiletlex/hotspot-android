package com.epsilon.textinthecity;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsense.trace.BugSenseHandler;
import com.epsilon.textinthecity.common.CleanUpUtil;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.object.Location;

/**
 * Class common for activity
 * 
 * @author CongVo
 *
 */
public class BaseActivity extends Activity {
	
//	private static final long TIME_COUNTDOWN_CHECK_LOCATION = 1000*10;
	

	private CountDownTimer mCountDownTimer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
//		try {
//
//			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//			.detectDiskReads()
//			.detectDiskWrites()
//			.detectNetwork() // or .detectAll() for all detectable problems
//			.penaltyLog()
//			.build());
//			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//			.detectLeakedSqlLiteObjects()
//			.detectLeakedClosableObjects()
//			.penaltyLog()
//			.penaltyDeath()
//			.build());
//			
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
//		Tracker t = PoemApplication.getInstance().getTracker(
//                TrackerName.GLOBAL_TRACKER);
//		// Send a screen view.
//        t.send(new HitBuilders.AppViewBuilder().build());
		
		
		if(Constant.IS_DEBUG){
			BugSenseHandler.initAndStartSession(this, Constant.BUG_SENSE_AP_KEY);
		}
		
		if(!isNetworkAvailable()){
//			createDialogMessage(getString(R.string.msg_error_title), 
//					getString(R.string.msg_error_no_network)).show();
			Toast.makeText(this, getString(R.string.msg_error_no_network), 
					Toast.LENGTH_LONG).show();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		checkUnlockLocation();
//		resetCountDownTimer();
		
		List<Location> listLocations = (List<Location>)
				Common.getObjFromInternalFile(this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
		if(null != listLocations){
			Log.e("CongVC", "===SO pt:"+ listLocations.size());
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		if(null != mCountDownTimer){
			mCountDownTimer.cancel();
		}
	}
		
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if(Constant.IS_DEBUG){
			BugSenseHandler.closeSession(this);
		}
		
		try {
			
			CleanUpUtil.cleanupView(findViewById(R.id.main_layout));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PoemApplication.getInstance().cancelPendingRequests(this);
	}
	
	/**
	 * Method process reset count down timer
	 */
//	private void resetCountDownTimer(){
//		if(null != mCountDownTimer){
//			mCountDownTimer.cancel();
//		}
//		
//		// Init new count down timer 
//		mCountDownTimer = new CountDownTimer(TIME_COUNTDOWN_CHECK_LOCATION, 1000) {
//			
//			@Override
//			public void onTick(long millisUntilFinished) {
//				
//			}
//			
//			@Override
//			public void onFinish() {
//				checkUnlockLocation();
//				resetCountDownTimer();
//			}
//		}.start();
//	}
	
//	/**
//	 * Check current location for unlock in list location
//	 */
//	@SuppressWarnings("unchecked")
//	private void checkUnlockLocation() {
//		
//		if(null != PoemApplication.getInstance().lastKnownLocaton) {
//			
//			try {
//				List<Location> listLocations = (List<Location>)
//						Common.getObjFromInternalFile(this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
//							
//				
//				HashMap<String, Location> hashUnlockLoation = (HashMap<String, Location>)
//						Common.getObjFromInternalFile(this, Constant.FILE_CACHE_LIST_UNLOCK_LOCATION);
////				List<Location> listLocations = Constant.LIST_ALL_LOCATION;
////				HashMap<String, Location> hashUnlockLoation = Constant.HASH_UNLOCK_LOCATION;
//				if(null == hashUnlockLoation){
//					hashUnlockLoation = new HashMap<String, Location>();
//				}
//				
//				if(null != listLocations && listLocations.size() > 0){
//					boolean hasChanged = false;
//					for (int i = 0; i < listLocations.size(); i++) {
//						
//						final Location location = listLocations.get(i);
//						
//						if(!location.isUnlock_required() || hashUnlockLoation.containsKey("" + location.getId())){
//							continue;
//						}
//						
//						float[] resultDistance =  {1000.0f};
//						android.location.Location.distanceBetween(
//													PoemApplication.getInstance().lastKnownLocaton.getLatitude(),
//													PoemApplication.getInstance().lastKnownLocaton.getLongitude(),
//													location.getLat(), 
//													location.getLon(), resultDistance);
//						if(resultDistance[0] <= Constant.DISTANCE_UNLOCK ){
//							location.setUnlock_date(System.currentTimeMillis());
//							hashUnlockLoation.put("" + location.getId(), location);
////							Toast.makeText(this, getString(R.string.msg_unlock) + " \"" + location.getName() + "\"", Toast.LENGTH_LONG).show();
//							hasChanged = true;
//														
////							new AlertDialog.Builder(this)
////							.setTitle(getString(R.string.msg_unlock_title))
////							.setMessage(getString(R.string.msg_unlock) + " \"" + location.getName() + "\"")
////							.setPositiveButton(getString(R.string.ok),
////								new DialogInterface.OnClickListener() {
////									public void onClick(DialogInterface dialog,int whichButton) {
////										Dialog dl = new UnlockDialog(BaseActivity.this, 
////															android.R.style.Theme_Translucent_NoTitleBar,
////															location);
////										dl.show();
////									}
////								})
////							.create()
////							.show();
//							
//							Dialog dl = new MessageUnlockDialog(BaseActivity.this, 
//									android.R.style.Theme_Translucent_NoTitleBar,
//									location);
//							dl.show();
//						}
//						
////						Log.e("CongVC", "=====" + listLocations.get(i).getId() + "==" + resultDistance[0]);
//					}
//					
//					if(hasChanged){
//						Common.deleteFileFromInternal(this, Constant.FILE_CACHE_LIST_UNLOCK_LOCATION);
//						Common.writeFileToInternal(this, Constant.FILE_CACHE_LIST_UNLOCK_LOCATION, hashUnlockLoation);
////						Constant.HASH_UNLOCK_LOCATION = hashUnlockLoation;
//					}
//				}
//				
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
	
	/**
	 * Add top layout to screen
	 * 
	 * @param viewGroup
	 * @param title
	 * @param hasBack
	 */
	protected void addTitleLayout(int idViewGroup, String title, boolean hasBack) {
		RelativeLayout item = (RelativeLayout) findViewById(idViewGroup);
		LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		View view = inflater.inflate(R.layout.activity_title, item);
		
		((TextView)view.findViewById(R.id.tvScreenTitle)).setText(title);
		((TextView)view.findViewById(R.id.tvScreenTitle)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		
		Button btnBack = (Button) view.findViewById(R.id.btnScreenBack);
		if(hasBack){
			btnBack.setVisibility(View.VISIBLE);
			btnBack.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
		}
		else{
			btnBack.setVisibility(View.INVISIBLE);
		}
	}
	
	/**
	 * Add top layout to screen
	 * 
	 * @param viewGroup
	 * @param title
	 * @param hasBack
	 */
	protected void addTabMenuLayout(int idViewGroup, int tabIndex) {
		RelativeLayout item = (RelativeLayout) findViewById(idViewGroup);
		LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		View view = inflater.inflate(R.layout.tab_menu, item);
		
		View tabPoems = view.findViewById(R.id.tabPoems);
		View tabMyEntries = view.findViewById(R.id.tabMyEntries);
		View tabSubmit = view.findViewById(R.id.tabSubmit);
		View tabExplore = view.findViewById(R.id.tabExplore);
		View tabSetting = view.findViewById(R.id.tabSetting);
		
		((TextView)view.findViewById(R.id.tvTabExplore)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView)view.findViewById(R.id.tvTabPoem)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView)view.findViewById(R.id.tvTabSubmit)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView)view.findViewById(R.id.tvTabMyEntries)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView)view.findViewById(R.id.tvTabSetting)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		if(tabIndex == Constant.TAB_MENU_INDEX_POEMS){
			tabPoems.setBackgroundColor(getResources().getColor(R.color.tab_meu_background_focus_color));
			tabPoems.setClickable(false);
			tabPoems.setSelected(false);
			tabPoems.setEnabled(false);
		}
		else if(tabIndex == Constant.TAB_MENU_INDEX_MY_ENTRIES){
			tabMyEntries.setBackgroundColor(getResources().getColor(R.color.tab_meu_background_focus_color));
			tabMyEntries.setClickable(false);
			tabMyEntries.setSelected(false);
			tabMyEntries.setEnabled(false);
		}
		else if(tabIndex == Constant.TAB_MENU_INDEX_SUBMIT){
			tabSubmit.setBackgroundColor(getResources().getColor(R.color.tab_meu_background_focus_color));
			tabSubmit.setClickable(false);
			tabSubmit.setSelected(false);
			tabSubmit.setEnabled(false);
		}
		else if(tabIndex == Constant.TAB_MENU_INDEX_EXPLORE){
			tabExplore.setBackgroundColor(getResources().getColor(R.color.tab_meu_background_focus_color));
			tabExplore.setClickable(false);
			tabExplore.setSelected(false);
			tabExplore.setEnabled(false);
		}
		else if(tabIndex == Constant.TAB_MENU_INDEX_SETTING){
			tabSetting.setBackgroundColor(getResources().getColor(R.color.tab_meu_background_focus_color));
			tabSetting.setClickable(false);
			tabSetting.setSelected(false);
			tabSetting.setEnabled(false);
		}
		
		if(tabIndex != Constant.TAB_MENU_INDEX_SETTING){
			int countEventUnread = Common.countEventsUnread(this);
			if(countEventUnread > 0){
				((TextView)view.findViewById(R.id.tvTabSettingCountEvents)).setText(countEventUnread+"");
				((TextView)view.findViewById(R.id.tvTabSettingCountEvents)).setVisibility(View.VISIBLE);
			}
		}
		
		tabPoems.setOnClickListener(tabClickListener);
		tabMyEntries.setOnClickListener(tabClickListener);
		tabSubmit.setOnClickListener(tabClickListener);
		tabExplore.setOnClickListener(tabClickListener);
		tabSetting.setOnClickListener(tabClickListener);
	}
	
	/**
	 * Event when user click on tab menu
	 */
	private OnClickListener tabClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent it;
//			String trackerLabel = "";
			switch (v.getId()) {
			case R.id.tabPoems:
				it = new Intent(BaseActivity.this, PoemsActivity.class);
//				trackerLabel = getString(R.string.google_analytics_event_open_tab_poem);
				break;
			case R.id.tabMyEntries:
				it = new Intent(BaseActivity.this, MyEntriesActivity.class);
//				trackerLabel = getString(R.string.google_analytics_event_open_tab_myentries);
				break;
			case R.id.tabSubmit:
				it = new Intent(BaseActivity.this, SubmitActivity.class);
//				trackerLabel = getString(R.string.google_analytics_event_open_tab_submit);
				break;
			case R.id.tabExplore:
				it = new Intent(BaseActivity.this, ExploreActivity.class);
//				trackerLabel = getString(R.string.google_analytics_event_open_tab_explore);
				break;
			case R.id.tabSetting:
				it = new Intent(BaseActivity.this, SettingActivity.class);
//				trackerLabel = getString(R.string.google_analytics_event_open_tab_setting);
				break;

			default:
				it = new Intent(BaseActivity.this, ExploreActivity.class);
//				trackerLabel = getString(R.string.google_analytics_event_open_tab_explore);
				break;
			}
			
//			Tracker t = PoemApplication.getInstance().getTracker(
//	                TrackerName.APP_TRACKER);
//	        t.send(new HitBuilders.EventBuilder().setLabel(trackerLabel).build());
			
			it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			it.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//			it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			finish();
			
		}
	};
	
	/**
	 * Get unique id of device android
	 * 
	 * @return
	 */
	public String getDeviceUniqueId() {
		
		String result = "";
		
		TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE); 
		result = mngr.getDeviceId();
		
		if(null == result || result.equals("")){
			WifiManager wm = (WifiManager)getSystemService(Context.WIFI_SERVICE);
			result = wm.getConnectionInfo().getMacAddress();
		}
		
		if(null == result || result.equals("")){
			result = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
		}
		
		return result;
	}
	
	/**
	 * Create dialog show message report
	 * 
	 * @param title
	 * @param message
	 * @return
	 */
	public Dialog createDialogMessage(String title, String message) {
		return new AlertDialog.Builder(this)
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(getString(R.string.ok),
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int whichButton) {
					
				}
			})
		.create();
	}
	
	/**
	 * Check network connect
	 * 
	 * @param context
	 * @return true if network is connected
	 */
	public boolean isNetworkAvailable() {
		ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] infoset = mConnectivityManager.getAllNetworkInfo();
		for (NetworkInfo info : infoset) {
			if (info.isAvailable() && info.isConnected()){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Check valid email format
	 * 
	 * @param email
	 * @return
	 */
	public boolean checkEmailIsValid(String email) {
		final String ptnStr = "[\\w\\.\\-]+@(?:[\\w\\-]+\\.)+[\\w\\-]+"; // Email
																			
		final Pattern ptn = Pattern.compile(ptnStr);
		final Matcher mc = ptn.matcher(email);
		if (!mc.matches()) { // Invalid Email
			return false;
		} 
		else 
		{
			return true;
		}
	}


	/**
	 * Get list location unlock
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, Location> getListUnlockLocation() {
		try {
			HashMap<String, Location> hashUnlockLoation = (HashMap<String, Location>)
					Common.getObjFromInternalFile(this, Constant.FILE_CACHE_LIST_UNLOCK_LOCATION);
			if(null == hashUnlockLoation){
				hashUnlockLoation = new HashMap<String, Location>();
			}
			return hashUnlockLoation;
		} catch (Exception e) {
			return new HashMap<String, Location>();
		}
	}
	
	/**
	 * Animation slide in
	 * 
	 * @param v
	 * @param Con
	 * @param animationid
	 * @param StartOffset
	 * @return
	 */
	public Animation playAnim( View v, Context Con, int animationid, int StartOffset )
	{
	    
	    if( v != null )
	    {
	        Animation animation = AnimationUtils.loadAnimation(Con, animationid  );
	        animation.setStartOffset(StartOffset);
	        animation.setDuration(500);
	        v.startAnimation(animation);

	        return animation;
	    }
	    return null;
	}
	
	/**
	 * Get padding item screen
	 * 
	 * @return
	 */
	public int getPaddingScreen() {
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        // Set padding for center layout
        return (point.x - (int)(point.x*0.9f))/2;
	}
	
		
	
}
