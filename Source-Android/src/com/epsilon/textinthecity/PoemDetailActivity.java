package com.epsilon.textinthecity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.api.PoemByIdApi;
import com.epsilon.textinthecity.api.PoemLikeApi;
import com.epsilon.textinthecity.api.PoemRateApi;
import com.epsilon.textinthecity.api.PoemReportApi;
import com.epsilon.textinthecity.common.CleanUpUtil;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.helper.ShareHelper;
import com.epsilon.textinthecity.inter_face.PoemByIdLisener;
import com.epsilon.textinthecity.inter_face.PoemLikeLisener;
import com.epsilon.textinthecity.inter_face.PoemRateLisener;
import com.epsilon.textinthecity.inter_face.PoemReportLisener;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.object.PhotoPoem;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnPublishListener;
import com.sromku.simple.fb.utils.Utils;
import com.viewpagerindicator.CirclePageIndicator;

public class PoemDetailActivity extends BaseActivity implements OnClickListener {
	
	public static final String TAB_INDEX_POEM_DETAIL_KEY = "TAB_INDEX_POEM_DETAIL_KEY";
	
	private static final int LOGIN_CODE = 100;
	
//	private static final long TIME_COUNTDOWN_CHECK_UNLOCK = 1000*2;
	private CountDownTimer mCountDownTimer;
	
	private Poem mPoem;
	
	
	private PopupWindow mPopupWindow;
	private View mViewPopup;
	private TextView tvPopupReason;
	private EditText edtPopupReason;
	private Button btnPopupReport;
	private Button btnPopupClose;
	
	private PopupWindow mPopupWindowRating;
	private View mViewPopupRating;
	private RatingBar rbPopupRating;
	
	public PopupWindow mPopupWindowShareFB;
	private View mViewPopupShareFB;
	
	private ImageView imgHideInfo;
	private ImageView imgShowInfo;
	private LinearLayout lnHideInfo;
	private LinearLayout lnShowInfo;
	
//	private ImageView imgPoemDetail;
	private ImageView imgPoemWarning;
	
	private ImageView imgLoading;
	
//	private TextView tvContent;
//	private TextView tvContent1;
	private WebView wvContent;
	private WebView wvContent1;
	
	private TextView tvContentTitle;
	private TextView tvContent1Title;
	private TextView tvWarningUnlock;
	private Button btnBio;
	private Button btnPlayAudio;
	private Button btnLike;
	private Button btnShare;
	private Button btnRate;
	private Button btnReport;
	private LinearLayout lnPopup;
		
	private MediaPlayer mMediaPlayer;
	
	private SeekBar mSeekbar;
	private double startTime = 0;
    private double finalTime = 0;
    private Handler myHandler = new Handler();
   
    private boolean isStart = true;
    private boolean isPrepareSuccess = false;
    private Thread mThreadPrepareAudio;
    private boolean isPlayingAudio = false;
    private String mAudioUrl = "";
    
    private int mRating = 0;
    
    private String mShareImageUri = "";
    
    private int mTabIndex = Constant.TAB_MENU_INDEX_EXPLORE;
    
    private SimpleFacebook mSimpleFacebook;
   
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_poem_detail);
		
		mTabIndex = getIntent().getIntExtra(TAB_INDEX_POEM_DETAIL_KEY, Constant.TAB_MENU_INDEX_EXPLORE);
		
		mPoem = (Poem)getIntent().getSerializableExtra(ExploreActivity.KEY_POEM_OBJECT);
		
		Tracker t = PoemApplication.getInstance().getTracker(
                TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
        	.setCategory(getString(R.string.google_analytics_event_category_poem))
        	.setAction(getString(R.string.google_analytics_event_action_open))
        	.setLabel(mPoem.getId()+"").build());
		
		String title = mPoem.getCategory_id() == Constant.CATEGORY_ID_FEATURE ?
								getString(R.string.screen_title_featured):
								getString(R.string.screen_title_public);//mPoem.getTitle();
				
		addTitleLayout(R.id.top_layout, title, true);
		
		addTabMenuLayout(R.id.bottom_layout, mTabIndex);
		
		initControls();
		
		initPopup();
		
		setupWidthDetail();
		
		addUnlockLocationToArchivalPhoto();
		
		initAll();
		
		PoemHelper.getInstance().register(this);
		
		((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
		new PoemByIdApi().getPoemById(this, mPoem.getId(), mPoemByIdListener);
		
		initAudio();
		
		mSimpleFacebook = SimpleFacebook.getInstance(this);
		
		Log.e("CongVC", "====HashKey:" + Utils.getHashKey(this));
		
		if (mSimpleFacebook.isLogin()) {
			mSimpleFacebook.logout(onLogoutListener);
		}
		
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		checkUnlock();
//		try {
//			if(null != mMediaPlayer && !mAudioUrl.isEmpty()){
//				isPrepareSuccess = false;
//				mThreadPrepareAudio = new Thread(new Runnable() {
//					
//					@Override
//					public void run() {
//						try {
//							mMediaPlayer.prepare();
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//				});
//				mThreadPrepareAudio.start();
//				
//				if(isPlayingAudio){
//					isStart = true;
//					pause();
//					
//				}
//				mSeekbar.setVisibility(View.GONE);
//				
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		try {
			if(null != mMediaPlayer && !mAudioUrl.isEmpty() && !isPrepareSuccess){
				isPrepareSuccess = false;
				mThreadPrepareAudio = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							mMediaPlayer.prepare();
							Log.e("CongVC", "====start prepare===" + mAudioUrl);
						} catch (Exception e) {
							Log.e("CongVC", "====Error prepare===");
							e.printStackTrace();
						}
					}
				});
				mThreadPrepareAudio.start();
				
				if(isPlayingAudio){
					isStart = true;
					pause();
					
				}
				mSeekbar.setVisibility(View.GONE);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onPause() {
		
		super.onPause();
		
//		try {
//			if(null != mMediaPlayer){
//				pause();
//				mSeekbar.setVisibility(View.GONE);
//				isStart = true;
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
//		try {
//			if(null != mMediaPlayer){
//				mMediaPlayer.stop();
//			}
//			if(null != mThreadPrepareAudio){
//				mThreadPrepareAudio.interrupt();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}
	
	@Override
	protected void onDestroy() {
		PoemHelper.getInstance().unregister(this);
		try {
			
			if(null != mCountDownTimer){
				mCountDownTimer.cancel();
			}
			
			if(null != mMediaPlayer){
				mMediaPlayer.stop();
				mMediaPlayer.release();
			}
			if(null != myHandler){
				myHandler.removeCallbacks(UpdateSongTime);
				myHandler = null;
			}
			if(null != mThreadPrepareAudio){
				mThreadPrepareAudio.interrupt();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthDetail() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.bottom_detail_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													paddingCenterLayout/2);
        
        ((RelativeLayout) findViewById(R.id.rlMoreInfo)).setPadding(paddingCenterLayout, 
        														paddingCenterLayout/2, 
        														(int)(paddingCenterLayout*1.5f), 
																paddingCenterLayout/2);
        
        ((RelativeLayout) findViewById(R.id.rlMoreCaption)).setPadding(paddingCenterLayout, 
																paddingCenterLayout/2, 
																(int)(paddingCenterLayout*1.5f), 
																paddingCenterLayout/2);
        
        tvWarningUnlock.setPadding(paddingCenterLayout, 
								paddingCenterLayout/2, 
								paddingCenterLayout, 
								paddingCenterLayout/2);
        
        LayoutParams lp = new LayoutParams(point.x, (int)(point.x * 0.684f));
//        imgPoemDetail.setLayoutParams(lp);
        ((RelativeLayout) findViewById(R.id.top_detail_layout)).setLayoutParams(lp);
        ((ViewPager) findViewById(R.id.viewpager)).setLayoutParams(lp);
        
        
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls() {
		
		imgHideInfo = (ImageView) findViewById(R.id.imgPoemHideInfo);
		imgShowInfo = (ImageView) findViewById(R.id.imgPoemShowInfo);
		lnHideInfo = (LinearLayout) findViewById(R.id.lnPoemHideInfo);
		lnShowInfo = (LinearLayout) findViewById(R.id.lnPoemShowInfo);
		
//		imgPoemDetail = (ImageView) findViewById(R.id.imgPoemDetail);
		imgPoemWarning = (ImageView) findViewById(R.id.imgPoemWarning);
//		tvContent = (TextView) findViewById(R.id.tvPoemContent);
//		tvContent1 = (TextView) findViewById(R.id.tvPoemContent1);
		wvContent = (WebView) findViewById(R.id.webViewPoemContent);
		wvContent1 = (WebView) findViewById(R.id.webViewPoemContent1);
		tvContentTitle = (TextView) findViewById(R.id.tvPoemContentTitle);
		tvContent1Title = (TextView) findViewById(R.id.tvPoemContent1Title);
		tvWarningUnlock = (TextView) findViewById(R.id.tvWarningUnlock);
		btnBio = (Button) findViewById(R.id.btnBio);
		btnPlayAudio = (Button) findViewById(R.id.btnPlay);
		btnLike = (Button) findViewById(R.id.btnLike);
		btnShare = (Button) findViewById(R.id.btnShare);
		btnRate = (Button) findViewById(R.id.btnRate);
		btnReport = (Button) findViewById(R.id.btnReport);
		lnPopup = (LinearLayout) findViewById(R.id.lnPopup);
		
		mSeekbar = (SeekBar) findViewById(R.id.skbPlay);
        mSeekbar.setClickable(false);
        mSeekbar.setVisibility(View.GONE);
				
		btnBio.setOnClickListener(this);
		btnPlayAudio.setOnClickListener(this);
		btnLike.setOnClickListener(this);
		btnShare.setOnClickListener(this);
		btnRate.setOnClickListener(this);
		btnReport.setOnClickListener(this);
		
		imgHideInfo.setOnClickListener(this);
		imgShowInfo.setOnClickListener(this);
		lnHideInfo.setOnClickListener(this);
		lnShowInfo.setOnClickListener(this);
		
		findViewById(R.id.btnStar1).setOnClickListener(mRatingClickListener);
		findViewById(R.id.btnStar2).setOnClickListener(mRatingClickListener);
		findViewById(R.id.btnStar3).setOnClickListener(mRatingClickListener);
		findViewById(R.id.btnStar4).setOnClickListener(mRatingClickListener);
		findViewById(R.id.btnStar5).setOnClickListener(mRatingClickListener);
		
		((TextView) findViewById(R.id.tvMoreInfoAuthor)).setTypeface(
						PoemApplication.getInstance().typeFaceFuturaLightOblique);
		((TextView) findViewById(R.id.tvMoreInfoTitle)).setTypeface(
						PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
		((TextView) findViewById(R.id.tvMoreInfoTitle1)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
		((TextView) findViewById(R.id.tvMoreCaption)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
		((TextView) findViewById(R.id.tvWarningUnlock)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvLanguage)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvLanguage1)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
		((Button) findViewById(R.id.btnBio)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaOblique);
//		((TextView) findViewById(R.id.tvPoemContent)).setTypeface(
//				PoemApplication.getInstance().typeFaceFuturaOblique);
//		((TextView) findViewById(R.id.tvPoemContent1)).setTypeface(
//				PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvPoemContentTitle)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvPoemContent1Title)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvRating)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaOblique);
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		int seekValue = SharePreference.getPreferenceByKeyInt(this, Constant.KEY_FONT_SETTING, 50);
//		tvContent.setTextSize(Constant.SETTING_FONT_DEF + (seekValue-50)/5);
//		tvContent1.setTextSize(Constant.SETTING_FONT_DEF + (seekValue-50)/5);
		tvContentTitle.setTextSize(Constant.SETTING_FONT_DEF + (seekValue-50)/5);
		tvContent1Title.setTextSize(Constant.SETTING_FONT_DEF + (seekValue-50)/5);
		
		wvContent.setWebViewClient(new WebViewClient() {

			   @Override
			   public boolean shouldOverrideUrlLoading(WebView view, String url) {
				   if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
			            Common.openBrowser(PoemDetailActivity.this, url);
			            return true;
			        } else {
			            return false;
			        }
			   }
			  });
		wvContent1.setWebViewClient(new WebViewClient() {

			   @Override
			   public boolean shouldOverrideUrlLoading(WebView view, String url) {
				   if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
			            Common.openBrowser(PoemDetailActivity.this, url);
			            return true;
			        } else {
			            return false;
			        }
			   }
			  });
		
		wvContent.getSettings().setBuiltInZoomControls(true);
		wvContent.getSettings().setSupportZoom(true);
		wvContent.getSettings().setDisplayZoomControls(false);
		wvContent1.getSettings().setBuiltInZoomControls(true);
		wvContent1.getSettings().setSupportZoom(true);
		wvContent1.getSettings().setDisplayZoomControls(false);
		
		mSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (!fromUser || ! isPlayingAudio) {
	                // We're not interested in programmatically generated changes to
	                // the progress bar's position.
	                return;
	            }
				
				mMediaPlayer.seekTo(progress);
			}
		});
		
	}
	
	/**
	 * Add location to archival photo
	 */
	@SuppressWarnings("unchecked")
	private void addUnlockLocationToArchivalPhoto() {
		
		if(mPoem.getCategory_id() != Constant.CATEGORY_ID_FEATURE){
			return;
		}
		
		try {
			HashMap<String, Location> hashUnlockLoation = (HashMap<String, Location>)
					Common.getObjFromInternalFile(this, Constant.FILE_CACHE_LIST_UNLOCK_LOCATION);
			if(null == hashUnlockLoation){
				hashUnlockLoation = new HashMap<String, Location>();
			}
			
			
			final Location location = Common.getLocationById(mPoem.getLocation_id(), PoemDetailActivity.this);
					
			if(null == location || hashUnlockLoation.containsKey("" + location.getId())){
				return;
			}
				
			location.setUnlock_date(System.currentTimeMillis());
			hashUnlockLoation.put("" + location.getId(), location);
					
			Common.deleteFileFromInternal(this, Constant.FILE_CACHE_LIST_UNLOCK_LOCATION);
			Common.writeFileToInternal(this, Constant.FILE_CACHE_LIST_UNLOCK_LOCATION, hashUnlockLoation);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Init view for popup
	 */
	@SuppressLint("InflateParams")
	private void initPopup() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 mViewPopup = inflater.inflate(R.layout.popup_report_poem, null, false);
		 
		 tvPopupReason = (TextView) mViewPopup.findViewById(R.id.tvPopReason);
		 edtPopupReason = (EditText) mViewPopup.findViewById(R.id.edtPopReason);
		 btnPopupReport = (Button) mViewPopup.findViewById(R.id.btnPopReport);
		 btnPopupClose = (Button) mViewPopup.findViewById(R.id.btnReportClose);
		 
		 tvPopupReason.setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);		
		 btnPopupReport.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 edtPopupReason.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 btnPopupReport.setOnClickListener(this);
		 btnPopupClose.setOnClickListener(this);
		 
		 
		 mViewPopupRating = inflater.inflate(R.layout.popup_rating, null, false);
		 
		 rbPopupRating = (RatingBar) mViewPopupRating.findViewById(R.id.rbRating);
		 
		 ((TextView) mViewPopupRating.findViewById(R.id.tvRating))
		 		.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);	
		 
		 rbPopupRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
			
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				if(fromUser){
					resetCountTime();
					mRating = (int) rating;
					rbPopupRating.setRating(mRating);
				}
			}
		});
		 
		 Point point = new Point(0, 0);
	        Display display = getWindowManager().getDefaultDisplay();
	        display.getSize(point);
	        
	        // Set padding for center layout
	        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
	        ((LinearLayout) mViewPopupRating.findViewById(R.id.lnPopMain)).setPadding(paddingCenterLayout, 
	        													0, 
	        													paddingCenterLayout, 
	        													0);
	        
			 mViewPopupShareFB = inflater.inflate(R.layout.popup_share_fb, null, false);
			 String authorName = "";
			if(mPoem.getPoet() != null){
				try {
					authorName = ! mPoem.getPoet().getName().isEmpty() ? 
									mPoem.getPoet().getName() : 
										mPoem.getPoet().getPen_name();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			final String bodyFB = getString(R.string.share_poem_email_body, 
											mPoem.getTitle(), authorName, 
											Constant.URL_APPLE_STORE,
											Constant.URL_GOOGLE_PLAY_STORE);
			((TextView) mViewPopupShareFB.findViewById(R.id.tvContentFB)).setText(bodyFB);
			((TextView) mViewPopupShareFB.findViewById(R.id.tvContentFB)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
			((TextView) mViewPopupShareFB.findViewById(R.id.tvContentFB)).setMovementMethod(new ScrollingMovementMethod());
			((TextView) mViewPopupShareFB.findViewById(R.id.tvShareFBLabel)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
			
			
			((Button) mViewPopupShareFB.findViewById(R.id.btnShareFBClose)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mPopupWindowShareFB.dismiss();
				}
			});
			((Button) mViewPopupShareFB.findViewById(R.id.btnPopPost)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mPopupWindowShareFB.dismiss();
					if (mSimpleFacebook.isLogin()) {
						processShareFaceBook(bodyFB);
					}
					else {
						mSimpleFacebook.login(new onLoginListener(bodyFB, "", "", ""));
					}
				}
			});
			
			android.widget.LinearLayout.LayoutParams lpImg = new android.widget.LinearLayout.LayoutParams(
							(int)(point.x *0.9f), 
							(int)(point.x *0.9f * 0.564f));
			((ImageView) mViewPopupShareFB.findViewById(R.id.share_image)).setLayoutParams(lpImg);
			if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0){
				String imgLink = mPoem.getPhotos()[0].getLinks().getOrg();
				loadImageView(((ImageView) mViewPopupShareFB.findViewById(R.id.share_image)), 
							imgLink, 
							(int)(point.x *0.9f), 
							(int)(point.x *0.9f * 0.564f));
			}
			else {
				((ImageView) mViewPopupShareFB.findViewById(R.id.share_image)).setVisibility(View.GONE);
			}
	}
	
	private CountDownTimer mCountTimer;
	
	private void resetCountTime() {
		
		if(null != mCountTimer){
			mCountTimer.cancel();
		}
		
		mCountTimer = new CountDownTimer(1000, 1000) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				
			}
			
			@Override
			public void onFinish() {
				if(! Common.checkLogin(PoemDetailActivity.this)){
				Intent it = new Intent(PoemDetailActivity.this, LoginActivity.class);
				it.putExtra(LoginActivity.TAB_KEY, mTabIndex);
				it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivityForResult(it, LOGIN_CODE);
				overridePendingTransition(0,0);
				}
				else{
					if(mRating >=1){
						((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
//						Api.postRatePoem(PoemDetailActivity.this, mPoem.getId(), mRating);
						
						new PoemRateApi().ratingPoem(PoemDetailActivity.this, mPoem.getId(), mRating, mPoemRateListener);
					}
				}
			}
		};
		mCountTimer.start();
		
	}
	
	/**
	 * Show popup
	 * @param view
	 */
	public void showPopupImage(View view) {
		mPopupWindow = new PopupWindow(this);
	    mPopupWindow.setTouchable(true);
	    mPopupWindow.setFocusable(true);
	    mPopupWindow.setOutsideTouchable(true);
	    mPopupWindow.setTouchInterceptor(new OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	mPopupWindow.dismiss();
	                return true;
	            }
	            return false;
	        }
	    });
	    mPopupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
	    mPopupWindow.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
	    mPopupWindow.setOutsideTouchable(false);
	    mPopupWindow.setContentView(mViewPopup);
	    mPopupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_gray_alpha));
	    
	    mPopupWindow.showAsDropDown(view, 0, (int)(- findViewById(R.id.top_layout).getHeight()));
	}
	
	/**
	 * Show popup
	 * @param view
	 */
	public void showPopupRating(View view) {
		mPopupWindowRating = new PopupWindow(this);
		mPopupWindowRating.setTouchable(true);
		mPopupWindowRating.setFocusable(true);
		mPopupWindowRating.setOutsideTouchable(true);
		mPopupWindowRating.setTouchInterceptor(new OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	mPopupWindowRating.dismiss();
	                return true;
	            }
	            return false;
	        }
	    });
		mPopupWindowRating.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowRating.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		mPopupWindowRating.setOutsideTouchable(false);
		mPopupWindowRating.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_popup_transparent));
		mPopupWindowRating.setContentView(mViewPopupRating);
	    
		mPopupWindowRating.showAsDropDown(view, 0, (int)(btnRate.getY() - btnRate.getHeight()*3.5));
	}
	
	/**
	 * Show popup
	 * @param view
	 */
	private void showPopupShareFB(View view) {
		mPopupWindowShareFB = new PopupWindow(this);
		mPopupWindowShareFB.setTouchable(true);
		mPopupWindowShareFB.setFocusable(true);
		mPopupWindowShareFB.setOutsideTouchable(true);
		mPopupWindowShareFB.setTouchInterceptor(new OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	mPopupWindowShareFB.dismiss();
	                return true;
	            }
	            return false;
	        }
	    });
		mPopupWindowShareFB.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowShareFB.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowShareFB.setOutsideTouchable(false);
		mPopupWindowShareFB.setContentView(mViewPopupShareFB);
		mPopupWindowShareFB.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_gray_alpha));
	    
		mPopupWindowShareFB.showAsDropDown(view, 0, (int)(- findViewById(R.id.top_layout).getHeight()));
	}
	
	private PoemByIdLisener mPoemByIdListener = new PoemByIdLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleted(Poem poem) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			if(null != poem){
				mPoem = poem;
			}
			
			initAll();
		}
	};
	
	private PoemLikeLisener mPoemLikeListener = new PoemLikeLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_like_poem)).show();
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_like_poem)).show();
		}
		
		@Override
		public void requestCompleted(Poem poem) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			if(null != poem){
				if(poem.isIs_liked()){
					btnLike.setBackgroundResource(R.drawable.icon_like_focus);
				}
				else {
					btnLike.setBackgroundResource(R.drawable.icon_like);
				}
				Tracker t = PoemApplication.getInstance().getTracker(
		                TrackerName.APP_TRACKER);
		        t.send(new HitBuilders.EventBuilder()
		        	.setCategory(getString(R.string.google_analytics_event_category_poem))
		        	.setAction(getString(R.string.google_analytics_event_action_like))
		        	.setLabel(mPoem.getId()+"").build());
			}
			
		}
	};
	
	private PoemReportLisener mPoemReportListener = new PoemReportLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_report_poem)).show();
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_report_poem)).show();
		}
		
		@Override
		public void requestCompleted(Poem poem) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			Toast.makeText(PoemDetailActivity.this, getString(R.string.msg_report_poem_successful), Toast.LENGTH_LONG).show();
			
			btnReport.setBackgroundResource(R.drawable.icon_report_after);
			mPoem.setIs_reported(true);
		}
	};
	
	private PoemRateLisener mPoemRateListener = new PoemRateLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_rate_poem)).show();
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_rate_poem)).show();
		}
		
		@Override
		public void requestCompleted(Poem poem) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
						
			btnRate.setBackgroundResource(R.drawable.icon_rate);
		}
	};
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {
		
		if(null != event.getResult() && !event.getResult().isEmpty() ){

			if(event.getResult().equals(Constant.POEM_LIKE_ERROR_DEF)){
				
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				
				createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_like_poem)).show();
			}
			else if(event.getResult().equals(Constant.POEM_RATE_ERROR_DEF)){
				
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				
				createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_rate_poem)).show();
			}
			else if(event.getResult().equals(Constant.POEM_RATE_SUCCESS_DEF)){
				
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				
//				Toast.makeText(this, getString(R.string.msg_rate_poem_successful), Toast.LENGTH_LONG).show();
			}
			else if(event.getResult().equals(Constant.POEM_REPORT_ERROR_DEF)){
				
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				
				createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_report_poem)).show();
			}
			else if(event.getResult().equals(Constant.POEM_REPORT_SUCCESS_DEF)){
				
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				
				Toast.makeText(this, getString(R.string.msg_report_poem_successful), Toast.LENGTH_LONG).show();
			}
			else if(event.getResult().equals(Constant.POEM_GET_DETAIL_ERROR_DEF)){
				
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				
//				initAll();
			}
		   
		}
//		else if(event.getResult().equals(Constant.POEM_LIKE_SUCCESS_DEF)){
		else if(null != event.getPoemLike()){
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
//			Toast.makeText(this, getString(R.string.msg_like_poem_successful), Toast.LENGTH_LONG).show();
			
			if(event.getPoemLike().isIs_liked()){
				btnLike.setBackgroundResource(R.drawable.icon_like_focus);
			}
			else {
				btnLike.setBackgroundResource(R.drawable.icon_like);
			}
			
		}
		else if(null != event.getPoemDetail()){
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			mPoem = event.getPoemDetail();
			
			initAll();
		}
	}
	
	private OnClickListener mRatingClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			setRatingImage(Integer.parseInt(v.getTag().toString()));
		}
	};
	
	private void setRatingImage(int rating) {
		
		mRating = rating;
		
		findViewById(R.id.btnStar1).setBackgroundResource(
				rating>=1 ? R.drawable.icon_rate : R.drawable.icon_rate_empty);
		findViewById(R.id.btnStar2).setBackgroundResource(
				rating>=2 ? R.drawable.icon_rate : R.drawable.icon_rate_empty);
		findViewById(R.id.btnStar3).setBackgroundResource(
				rating>=3 ? R.drawable.icon_rate : R.drawable.icon_rate_empty);
		findViewById(R.id.btnStar4).setBackgroundResource(
				rating>=4 ? R.drawable.icon_rate : R.drawable.icon_rate_empty);
		findViewById(R.id.btnStar5).setBackgroundResource(
				rating>=5 ? R.drawable.icon_rate : R.drawable.icon_rate_empty);
	}
	
	private void initAll() {
		initData();
		
	}
	
	/**
	 * Init data poem detail
	 */
	private void initData() {
		
		((TextView) findViewById(R.id.tvMoreInfoAuthor)).setText(mPoem.getAuthor());
		
		
//		Photo[] photos = mPoem.getPhotos();
//		if(null != photos && photos.length > 0){
//			imageLoader.get(photos[0].getLink(), imageListener);
//		}
		if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0){
        	final GalleryPagerAdapter adapterGallery = new GalleryPagerAdapter(this, mPoem.getPhotos());
        	 ((ViewPager) findViewById(R.id.viewpager)).setAdapter(adapterGallery);
        	 
        	 ((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(0, true);
        	 
        	 CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
 			mIndicator.setViewPager((ViewPager) findViewById(R.id.viewpager));
 			
 			final Button btnNext = (Button) findViewById(R.id.btnNext);
	        final Button btnPrev = (Button) findViewById(R.id.btnPrev);
	        final LinearLayout lnNext = (LinearLayout) findViewById(R.id.lnNext);
	        final LinearLayout lnPrev = (LinearLayout) findViewById(R.id.lnPrev);
	        if(adapterGallery.getCount() > 1){
				btnNext.setVisibility(View.VISIBLE);
				lnNext.setVisibility(View.VISIBLE);
			}
	        mIndicator.setOnPageChangeListener(
	        		new ViewPager.OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int arg0) {
					if(adapterGallery.getCount() <= 1){
						btnPrev.setVisibility(View.GONE);
						btnNext.setVisibility(View.GONE);
						lnPrev.setVisibility(View.GONE);
						lnNext.setVisibility(View.GONE);
					}
					else if(arg0 == 0){
						btnPrev.setVisibility(View.GONE);
						btnNext.setVisibility(View.VISIBLE);
						lnPrev.setVisibility(View.GONE);
						lnNext.setVisibility(View.VISIBLE);
					}
					else if(arg0 >= adapterGallery.getCount()-1) {
						btnPrev.setVisibility(View.VISIBLE);
						btnNext.setVisibility(View.GONE);
						lnPrev.setVisibility(View.VISIBLE);
						lnNext.setVisibility(View.GONE);
					}
					else {
						btnPrev.setVisibility(View.VISIBLE);
						btnNext.setVisibility(View.VISIBLE);
						lnPrev.setVisibility(View.VISIBLE);
						lnNext.setVisibility(View.VISIBLE);
					}
					
					try {
						String caption = mPoem.getPhotos()[arg0].getCaption();
						if(null != caption && ! caption.equals("") && imgHideInfo.getVisibility() ==View.GONE){
							((TextView) findViewById(R.id.tvMoreCaption)).setText(caption);
							((RelativeLayout) findViewById(R.id.rlMoreCaption)).setVisibility(View.VISIBLE);
						}
						else {
							((RelativeLayout) findViewById(R.id.rlMoreCaption)).setVisibility(View.GONE);
						}
					} catch (Exception e) {
						e.printStackTrace();
						((RelativeLayout) findViewById(R.id.rlMoreCaption)).setVisibility(View.GONE);
					}
				}
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}
				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
			});
	        
	        btnNext.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(
							((ViewPager) findViewById(R.id.viewpager)).getCurrentItem()+1, true);
				}
			});
	        lnNext.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(
							((ViewPager) findViewById(R.id.viewpager)).getCurrentItem()+1, true);
				}
			});
	        btnPrev.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(
							((ViewPager) findViewById(R.id.viewpager)).getCurrentItem()-1, true);
				}
			});
	        lnPrev.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					((ViewPager) findViewById(R.id.viewpager)).setCurrentItem(
							((ViewPager) findViewById(R.id.viewpager)).getCurrentItem()-1, true);
				}
			});
        	
        }
		
		
		if(mPoem.isIs_liked()){
			btnLike.setBackgroundResource(R.drawable.icon_like_focus);
		}
		else {
			btnLike.setBackgroundResource(R.drawable.icon_like);
		}
		
		if(mPoem.isIs_reported()){
			btnReport.setBackgroundResource(R.drawable.icon_report_after);
		}
		else {
			btnReport.setBackgroundResource(R.drawable.icon_report);
		}
		
		if(mPoem.getAverage_rating() > 0){
			btnRate.setBackgroundResource(R.drawable.icon_rate);
		}
		else {
			btnRate.setBackgroundResource(R.drawable.icon_star);
		}
		
		// Not login
		if(!Common.checkLogin(this) || mPoem.getStatus_id() == Constant.POEM_STATUS_DRAFT){
			btnRate.setVisibility(View.GONE);
			btnReport.setVisibility(View.GONE);
			btnLike.setVisibility(View.GONE);
//			btnShare.setVisibility(View.GONE);
//			findViewById(R.id.vLineFeatured).setVisibility(View.GONE);
			findViewById(R.id.vLineShare).setVisibility(View.GONE);
			findViewById(R.id.lnRating).setVisibility(View.GONE);
		}
		else if(mPoem.getCategory_id() == Constant.CATEGORY_ID_FEATURE){
			btnRate.setVisibility(View.INVISIBLE);
			btnReport.setVisibility(View.INVISIBLE);
			
			findViewById(R.id.vLineFeatured).setVisibility(View.VISIBLE);
			findViewById(R.id.lnRating).setVisibility(View.GONE);
		}
		else {
			btnRate.setVisibility(View.VISIBLE);
			btnReport.setVisibility(View.VISIBLE);
			
			
//			findViewById(R.id.vLineFeatured).setVisibility(View.GONE);
			findViewById(R.id.lnRating).setVisibility(View.VISIBLE);
		}
		
		if(mPoem.getCategory_id() != Constant.CATEGORY_ID_FEATURE){
			btnPlayAudio.setVisibility(View.GONE);
			btnBio.setVisibility(View.GONE);
		}
		
		if(mPoem.getAudio_link().trim().isEmpty()){
			btnPlayAudio.setVisibility(View.GONE);
		}
		
		findViewById(R.id.lnRating).setVisibility(View.GONE);
		rbPopupRating.setRating(mPoem.getAverage_rating());
		
		initDataByLanguage();
		
	}
	
	/**
	 * Init audio
	 */
	private void initAudio() {
		mMediaPlayer = new MediaPlayer();
		mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mMediaPlayer.reset();

		try {
			String urlAudio = mPoem.getAudio_link();
			if(null != urlAudio && !urlAudio.isEmpty()){
				if(urlAudio.startsWith("https://")){
					urlAudio = urlAudio.replaceFirst("https", "http");
				}
				mAudioUrl = urlAudio;
				mMediaPlayer.setDataSource(urlAudio);//("https://textinthecity.sg/uploads//poems/72/audio.mp3");
				mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
					
					@Override
					public void onPrepared(MediaPlayer mp) {
						isPrepareSuccess = true;
						try {
							mThreadPrepareAudio.interrupt();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
					
					@Override
					public boolean onError(MediaPlayer mp, int what, int extra) {
						isPrepareSuccess = false;
						return false;
					}
				});
				mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						mMediaPlayer.seekTo(0);
						btnPlayAudio.setBackgroundResource(R.drawable.icon_play);
						mSeekbar.setVisibility(View.GONE);
						isStart = true;
						isPlayingAudio = false;
						
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void play(){
		   Toast.makeText(getApplicationContext(), "Playing sound", 
				   Toast.LENGTH_SHORT).show();
	
	mMediaPlayer.start();
    finalTime = mMediaPlayer.getDuration();
    startTime = mMediaPlayer.getCurrentPosition();
    

    mSeekbar.setMax((int) finalTime);
    mSeekbar.setProgress((int)startTime);
    myHandler.postDelayed(UpdateSongTime,100);
    btnPlayAudio.setBackgroundResource(R.drawable.icon_pause);
     isPlayingAudio = true;
   }
	
	private Runnable UpdateSongTime = new Runnable() {
	      public void run() {
	         try {
	        	 startTime = mMediaPlayer.getCurrentPosition();
		         mSeekbar.setProgress((int)startTime);
		         myHandler.postDelayed(this, 100);
			} catch (Exception e) {
				e.printStackTrace();
			}
	      }
	};
	   
   public void pause(){
//      Toast.makeText(getApplicationContext(), "Pausing sound", 
//      Toast.LENGTH_SHORT).show();

      mMediaPlayer.pause();
      btnPlayAudio.setBackgroundResource(R.drawable.icon_play);
      isPlayingAudio = false;
   }
	   
	/**
	 * Set up detail poem by language
	 */
	private void initDataByLanguage() {
		
		String header = Common.getStringFromAssets(this, "header.html");//"<head><style>@font-face {font-family: 'arial';src: url('file:///android_asset/fonts/Futura Oblique.ttf');}body {font-family: 'verdana';}</style></head>";
		int seekValue = SharePreference.getPreferenceByKeyInt(this, Constant.KEY_FONT_SETTING, 50);
		seekValue = Constant.SETTING_FONT_DEF + (seekValue-50)/5;
			
		String content = "";
		String title = "";
		String language = "";
		if(null != mPoem.getTitle()){

			content = mPoem.getContent();
			if((null == content || content.trim().isEmpty())){
				content = mPoem.getContent_alt();
			}
			title = mPoem.getTitle();
			language = Constant.HASH_LANGUAGES.containsKey(mPoem.getLanguage_id())?
							Constant.HASH_LANGUAGES.get(mPoem.getLanguage_id()).getName():"";
			
//			 String textContent="<html>"+header
//				     + "<body style=\"font-family: arial\">" + content.replace("\r\n", "<br/>")
//				     + "</body></html>";
			String textContent=header.replace("${size}", seekValue+"")
									.replace("${content}", content.replace("\r\n", "<br/>"));
//			tvContent.setText(content);
			wvContent.loadDataWithBaseURL("", textContent, "text/html", "utf-8", "");
			tvContentTitle.setText(title);
			((TextView) findViewById(R.id.tvMoreInfoTitle)).setText(title);
			((TextView) findViewById(R.id.tvLanguage)).setText(language);
			
			if(mPoem.getLanguage_id() != Constant.LANGUAGE_ID_ENG){
				
				((TextView) findViewById(R.id.tvMoreInfoTitle1)).setText(mPoem.getTitle_alt());
				
				content = mPoem.getContent_alt();
				if(null == content || content.trim().isEmpty()){
					content = getString(R.string.poem_detail_no_english_translation);
				}
				language = Constant.HASH_LANGUAGES.get(Constant.LANGUAGE_ID_ENG).getName();
				
//				textContent="<html>"+header
//					     + "<body style=\"font-family: arial\">" + content.replace("\r\n", "<br/>")
//					     + "</body></html>";
				
				textContent=header.replace("${size}", seekValue+"")
								.replace("${content}", content.replace("\r\n", "<br/>"));
//				tvContent1.setText(content);
				wvContent1.loadDataWithBaseURL("", textContent, "text/html", "utf-8", "");
				tvContent1Title.setText(mPoem.getTitle_alt());
				((TextView) findViewById(R.id.tvLanguage1)).setText(language);
				((TextView) findViewById(R.id.tvLanguage1)).setVisibility(View.VISIBLE);
				((ImageView) findViewById(R.id.vLineVertical3)).setVisibility(View.VISIBLE);
			}
			else {
				((TextView) findViewById(R.id.tvMoreInfoTitle1)).setVisibility(View.GONE);
//				tvContent1.setVisibility(View.GONE);
				wvContent1.setVisibility(View.GONE);
				tvContent1Title.setVisibility(View.GONE);
				((TextView) findViewById(R.id.tvLanguage1)).setVisibility(View.GONE);
				((ImageView) findViewById(R.id.vLineVertical3)).setVisibility(View.GONE);
			}
			
			((TextView) findViewById(R.id.tvLanguage)).setOnClickListener(this);
			((TextView) findViewById(R.id.tvLanguage1)).setOnClickListener(this);
			
		}
		else {
			((TextView) findViewById(R.id.tvMoreInfoTitle1)).setVisibility(View.GONE);
			
//			tvContent.setVisibility(View.GONE);
//			tvContent1.setVisibility(View.GONE);
			wvContent.setVisibility(View.GONE);
			wvContent1.setVisibility(View.GONE);
			tvContentTitle.setVisibility(View.GONE);
			tvContent1Title.setVisibility(View.GONE);
			
			((TextView) findViewById(R.id.tvLanguage)).setVisibility(View.GONE);
			((TextView) findViewById(R.id.tvLanguage1)).setVisibility(View.GONE);
			
			((ImageView) findViewById(R.id.vLineVertical2)).setVisibility(View.GONE);
			((ImageView) findViewById(R.id.vLineVertical3)).setVisibility(View.GONE);
		}
		((TextView) findViewById(R.id.tvMoreInfoTitle)).setText(title);
		
	}
	
//	/**
//	 * Method process reset count down timer
//	 */
//	private void resetCountDownTimer(){
//		if(null != mCountDownTimer){
//			mCountDownTimer.cancel();
//		}
//		
//		// Init new count down timer 
//		mCountDownTimer = new CountDownTimer(TIME_COUNTDOWN_CHECK_UNLOCK, 1000) {
//			
//			@Override
//			public void onTick(long millisUntilFinished) {
//				
//			}
//			
//			@Override
//			public void onFinish() {
//				checkUnlock();
//			}
//		}.start();
//	}
	
	/**
	 * Check unlock location
	 */
	private void checkUnlock() {
		
		if(mPoem.getCategory_id() != Constant.CATEGORY_ID_FEATURE){
			tvWarningUnlock.setVisibility(View.GONE);
			imgPoemWarning.setVisibility(View.GONE);
			return;
		}
		
		tvWarningUnlock.setVisibility(View.GONE);
		imgPoemWarning.setBackgroundResource(R.drawable.icon_warning_no_unlock);
		imgPoemWarning.setOnClickListener(this);
		return;
		
		/*
		if(mPoem.getCategory_id() != Constant.CATEGORY_ID_FEATURE){
			tvWarningUnlock.setVisibility(View.GONE);
			imgPoemWarning.setVisibility(View.GONE);
			return;
		}
		
		if(getListUnlockLocation().containsKey(mPoem.getLocation_id()+"")){
			tvWarningUnlock.setVisibility(View.GONE);
			imgPoemWarning.setVisibility(View.VISIBLE);
			imgPoemWarning.setBackgroundResource(R.drawable.icon_warning_unlock);
			imgPoemWarning.setOnClickListener(this);
			
		}
		else {
			Location local = Common.getLocationById(mPoem.getLocation_id(), this);
			
			if(null != local && ! local.isUnlock_required()){
				tvWarningUnlock.setVisibility(View.GONE);
				imgPoemWarning.setVisibility(View.VISIBLE);
				imgPoemWarning.setBackgroundResource(R.drawable.icon_warning_no_unlock);
				imgPoemWarning.setOnClickListener(this);
				return;
			}
			
			String distance = "...";
			if(null != PoemApplication.getInstance().lastKnownLocaton 
					&& null != Common.getLocationById(mPoem.getLocation_id(), this)){
				float[] resultDistance =  {-1.0f};
				
				try {
					android.location.Location.distanceBetween(
							PoemApplication.getInstance().lastKnownLocaton.getLatitude(),
							PoemApplication.getInstance().lastKnownLocaton.getLongitude(),
							local.getLat(), 
							local.getLon(), resultDistance);
				} catch (Exception e) {
					e.printStackTrace();
					resultDistance[0] = -1.0f;
				}
				if(resultDistance[0] >= 0){
//					int actualDistance = (resultDistance[0] - Constant.DISTANCE_UNLOCK) < 0 ? 0 :
//											(int)(resultDistance[0] - Constant.DISTANCE_UNLOCK);
					float actualDistance = (resultDistance[0] - Constant.DISTANCE_UNLOCK) < 0 ? 0 :
						(resultDistance[0] - Constant.DISTANCE_UNLOCK);
					if(actualDistance >= Constant.DISTANCE_TOO_FAR) {
						distance = getString(R.string.poem_detail_warning_unlock_distance_too_far);
					}
					else {
						
						distance = MixUtils.formatDist(actualDistance)+"";
						
					}
				}
			}
			
			if(distance.equals("...")){
				((TextView) findViewById(R.id.tvWarningUnlock)).setVisibility(View.GONE);
			}
			else {
				((TextView) findViewById(R.id.tvWarningUnlock)).setVisibility(View.VISIBLE);
				((TextView) findViewById(R.id.tvWarningUnlock)).setText(
							getString(R.string.poem_detail_warning_unlock_distance, distance));
			}
			resetCountDownTimer();
		}*/
	}
	
	/**
	 * Process show data popup
	 */
	private void processPopupUnlock(){
		
		Dialog dl = new UnlockDialog(PoemDetailActivity.this, 
				android.R.style.Theme_Translucent_NoTitleBar,
				Common.getLocationById(mPoem.getLocation_id(), PoemDetailActivity.this));
		dl.show();
	}
	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnLike:
			if(! Common.checkLogin(this)){
				Intent it = new Intent(PoemDetailActivity.this, LoginActivity.class);
				it.putExtra(LoginActivity.TAB_KEY, mTabIndex);
				it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivityForResult(it, LOGIN_CODE);
				overridePendingTransition(0,0);
			}
			else{
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
//				Api.postLikePoem(this, mPoem.getId());
				
				new PoemLikeApi().likePoem(this, mPoem.getId(), mPoemLikeListener);
			}
			
			break;
		case R.id.btnShare:
			
			String mTextBody = "";
			String mTextSubject = "";
			String mTextLink = "";
			String mImage = "";
			if(mPoem.getCategory_id() == Constant.CATEGORY_ID_FEATURE){
				mTextLink = mPoem.getUrl();
				mTextSubject = mPoem.getTitle();
			}
			else if(null != mPoem.getContent()){
				mTextLink = Constant.URL_GOOGLE_PLAY_STORE;
				mTextBody = mPoem.getContent();
				
				mTextSubject = mPoem.getTitle();
			}
			if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0 && !mShareImageUri.isEmpty()){
				mImage = mShareImageUri;
			}
			
			String userLogin = "Someone";
			if(Common.checkLogin(this)){
				userLogin = SharePreference.getPreferenceByKeyString(this, Constant.KEY_LOGIN_PEN_NAME);
			}
			
			String subjectMail = getString(R.string.share_poem_email_subject);
			String authorName = "";
			if(mPoem.getPoet() != null){
				try {
					authorName = ! mPoem.getPoet().getName().isEmpty() ? 
									mPoem.getPoet().getName() : 
										mPoem.getPoet().getPen_name();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			String bodyMail = getString(R.string.share_poem_email_body, 
											mPoem.getTitle(), authorName, 
											Constant.URL_APPLE_STORE,
											Constant.URL_GOOGLE_PLAY_STORE);

			new ShareHelper(this, mTextSubject, mTextBody, mTextLink, mImage, subjectMail, bodyMail).share1(mPoem.getTitle(), authorName);
			break;
		case R.id.btnRate:
//			if(! Common.checkLogin(this)){
//				Intent it = new Intent(PoemDetailActivity.this, LoginActivity.class);
//				it.putExtra(LoginActivity.TAB_KEY, mTabIndex);
//				it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//				startActivityForResult(it, LOGIN_CODE);
//				overridePendingTransition(0,0);
//			}
//			else{
//				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
//				Api.postRatePoem(this, mPoem.getId(), mRating);
//			}
			
			showPopupRating(findViewById(R.id.btnRate));
			
			break;
		case R.id.btnBio:
			Dialog dl = new PoetDialog(PoemDetailActivity.this, 
					android.R.style.Theme_Translucent_NoTitleBar,
					mPoem.getPoet());
			dl.show();
			break;
		case R.id.imgPoemWarning:
			processPopupUnlock();
			break;
		case R.id.btnPlay:
//			processPlayAudio();
			if(isPrepareSuccess){
				if(isStart){
					play();
					mSeekbar.setVisibility(View.VISIBLE);
//					((RelativeLayout) findViewById(R.id.rlMoreInfo)).setVisibility(View.GONE);
//					imgHideInfo.setVisibility(View.GONE);
//					imgShowInfo.setVisibility(View.VISIBLE);
					isStart = false;
				}
				else {
					pause();
					mSeekbar.setVisibility(View.GONE);
//					((RelativeLayout) findViewById(R.id.rlMoreInfo)).setVisibility(View.VISIBLE);
//					imgShowInfo.setVisibility(View.GONE);
//					imgHideInfo.setVisibility(View.VISIBLE);
					isStart = true;
				}
			}
			else {
				Toast.makeText(this, 
						getString(R.string.msg_waiting_prepare_audio), Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.imgPoemHideInfo:
			((RelativeLayout) findViewById(R.id.rlMoreInfo)).setVisibility(View.GONE);
			imgHideInfo.setVisibility(View.GONE);
			imgShowInfo.setVisibility(View.VISIBLE);
			lnHideInfo.setVisibility(View.GONE);
			lnShowInfo.setVisibility(View.VISIBLE);
			if(mPoem.getPhotos() != null && mPoem.getPhotos().length > 0) {
				
				int position = ((ViewPager) findViewById(R.id.viewpager)).getCurrentItem();
				if(position < mPoem.getPhotos().length && !mPoem.getPhotos()[position].getCaption().equals("")){

					((RelativeLayout) findViewById(R.id.rlMoreCaption)).setVisibility(View.VISIBLE);

					((TextView) findViewById(R.id.tvMoreCaption)).setText(mPoem.getPhotos()[position].getCaption());
					return;
				}
			}
			((RelativeLayout) findViewById(R.id.rlMoreCaption)).setVisibility(View.GONE);
			break;
		case R.id.imgPoemShowInfo:
			((RelativeLayout) findViewById(R.id.rlMoreInfo)).setVisibility(View.VISIBLE);
			((RelativeLayout) findViewById(R.id.rlMoreCaption)).setVisibility(View.GONE);
			imgShowInfo.setVisibility(View.GONE);
			imgHideInfo.setVisibility(View.VISIBLE);
			lnShowInfo.setVisibility(View.GONE);
			lnHideInfo.setVisibility(View.VISIBLE);
			break;
		case R.id.lnPoemHideInfo:
			((RelativeLayout) findViewById(R.id.rlMoreInfo)).setVisibility(View.GONE);
			imgHideInfo.setVisibility(View.GONE);
			imgShowInfo.setVisibility(View.VISIBLE);
			lnHideInfo.setVisibility(View.GONE);
			lnShowInfo.setVisibility(View.VISIBLE);
			if(mPoem.getPhotos() != null && mPoem.getPhotos().length > 0) {
				
				int position = ((ViewPager) findViewById(R.id.viewpager)).getCurrentItem();
				if(position < mPoem.getPhotos().length && !mPoem.getPhotos()[position].getCaption().equals("")){

					((RelativeLayout) findViewById(R.id.rlMoreCaption)).setVisibility(View.VISIBLE);

					((TextView) findViewById(R.id.tvMoreCaption)).setText(mPoem.getPhotos()[position].getCaption());
					return;
				}
			}
			((RelativeLayout) findViewById(R.id.rlMoreCaption)).setVisibility(View.GONE);
			break;
		case R.id.lnPoemShowInfo:
			((RelativeLayout) findViewById(R.id.rlMoreInfo)).setVisibility(View.VISIBLE);
			((RelativeLayout) findViewById(R.id.rlMoreCaption)).setVisibility(View.GONE);
			imgShowInfo.setVisibility(View.GONE);
			imgHideInfo.setVisibility(View.VISIBLE);
			lnShowInfo.setVisibility(View.GONE);
			lnHideInfo.setVisibility(View.VISIBLE);
			break;
		case R.id.tvLanguage:
//			tvContent1.setVisibility(View.GONE);
//			tvContent.setVisibility(View.VISIBLE);
			wvContent1.setVisibility(View.GONE);
			wvContent.setVisibility(View.VISIBLE);
			tvContent1Title.setVisibility(View.GONE);
			tvContentTitle.setVisibility(View.VISIBLE);
//			((TextView) findViewById(R.id.tvMoreInfoTitle)).setText(
//												mPoem.getContents().get(0).getTitle());
			((TextView) findViewById(R.id.tvLanguage)).setTextColor(
							getResources().getColor(R.color.tab_meu_background_color));
			((TextView) findViewById(R.id.tvLanguage1)).setTextColor(
					getResources().getColor(R.color.tab_meu_background_focus_color));
//			((TextView) findViewById(R.id.tvScreenTitle)).setText(
//												mPoem.getContents().get(0).getTitle());
			break;
		case R.id.tvLanguage1:
//			tvContent1.setVisibility(View.VISIBLE);
//			tvContent.setVisibility(View.GONE);
			wvContent1.setVisibility(View.VISIBLE);
			wvContent.setVisibility(View.GONE);
			tvContent1Title.setVisibility(View.VISIBLE);
			tvContentTitle.setVisibility(View.GONE);
//			((TextView) findViewById(R.id.tvMoreInfoTitle)).setText(
//												mPoem.getContents().get(1).getTitle());
			((TextView) findViewById(R.id.tvLanguage1)).setTextColor(
					getResources().getColor(R.color.tab_meu_background_color));
			((TextView) findViewById(R.id.tvLanguage)).setTextColor(
					getResources().getColor(R.color.tab_meu_background_focus_color));
//			((TextView) findViewById(R.id.tvScreenTitle)).setText(
//												mPoem.getContents().get(1).getTitle());
			break;
		case R.id.btnReport:
			if(mPoem.isIs_reported()){
				createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_report_poem)).show();
			}
			else {

				showPopupImage(findViewById(R.id.top_layout));
			}
			break;
		case R.id.btnPopReport:
			
			if(! isNetworkAvailable()){
				createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_no_network)).show();
			}
			else {
				processReport();
			}
			break;
		case R.id.btnReportClose:
			mPopupWindow.dismiss();
			break;
		default:
			break;
		}
		
	}
	
	@Override
	public void onBackPressed() {
		if(lnPopup.getVisibility() == View.VISIBLE){
			lnPopup.setVisibility(View.GONE);
			findViewById(R.id.vDisable).setVisibility(View.GONE);
		}
		else {
			super.onBackPressed();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mSimpleFacebook.onActivityResult(this, requestCode, resultCode, data);
	}
	
	public void processShareFacebook(String body, String subject, String linkImage, String link) {
		showPopupShareFB(findViewById(R.id.top_layout));
//		if (mSimpleFacebook.isLogin()) {
//			processShareFaceBook(body);
//		}
//		else {
//			mSimpleFacebook.login(new onLoginListener(body, subject, linkImage, link));
//		}
	}
	
	public void trackShareGoogleAnalytics() {
		Tracker t = PoemApplication.getInstance().getTracker(
                TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
        	.setCategory(getString(R.string.google_analytics_event_category_poem))
        	.setAction(getString(R.string.google_analytics_event_action_share))
        	.setLabel(mPoem.getId()+"").build());
	}
	
	private void processShareFaceBook(String body) {
		
		String mTextBody = body;
		String mImage = "";
//		if(mPoem.getCategory_id() == Constant.CATEGORY_ID_FEATURE){
//			mTextLink = mPoem.getUrl();
//			mTextSubject = mPoem.getTitle();
//		}
//		else if(null != mPoem.getContent()){
//			mTextLink = Constant.URL_GOOGLE_PLAY_STORE;
//			mTextBody = mPoem.getContent();
//			
//			mTextSubject = mPoem.getTitle();
//		}
		if(null != mPoem.getPhotos() && mPoem.getPhotos().length > 0){
			mImage = mPoem.getPhotos()[0].getLinks().getOrg();
		}
		
		final Feed feed;
		
		if(!mImage.equals("")){
			feed = new Feed.Builder()
//			.setName(mTextSubject)
//			.setCaption(mTextLink)
//			.setDescription(mTextBody)
			.setPicture(mImage)
//			.setLink(mTextLink)
			.setMessage(mTextBody)
			.build();
		}
		else {
			feed = new Feed.Builder()
//			.setName(mTextSubject)
//			.setCaption(mTextLink)
//			.setDescription(mTextBody)
//			.setLink(mTextLink)
			.setMessage(mTextBody)
			.build();
		}
				
		Log.e("CongVC", "===Post face book===");
		SimpleFacebook.getInstance().publish(feed, new OnPublishListener() {
		
			@Override
			public void onException(Throwable throwable) {
//				mResult.setText(throwable.getMessage());
				Log.e("CongVC", "====Error rrr:" + throwable.getMessage());
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				Toast.makeText(PoemDetailActivity.this, 
						getString(R.string.msg_err_share_fb_duplicate), Toast.LENGTH_LONG).show();
			}
		
			@Override
			public void onFail(String reason) {
//				mResult.setText(reason);
				Log.e("CongVC", "====Error:" + reason);
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				Toast.makeText(PoemDetailActivity.this, reason, Toast.LENGTH_LONG).show();
				
			}
			
			@Override
			public void onThinking() {
				// TODO Auto-generated method stub
				super.onThinking();
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
				
			}

			@Override
			public void onComplete(String response) {
//				mResult.setText(response);
				
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
				Toast.makeText(PoemDetailActivity.this, 
								getString(R.string.msg_share_fb_successful), 
								Toast.LENGTH_LONG).show();
				
				trackShareGoogleAnalytics();
			}
		});		
	}
	
	// Login listener
	private class onLoginListener implements OnLoginListener {
		String body;
		String subject;
		String linkImage;
		String link;
		public onLoginListener(String body, String subject, String linkImage, String link) {
			this.body = body;
			this.subject = subject;
			this.linkImage = linkImage;
			this.link = link;
		}

			@Override
			public void onFail(String reason) {
				Log.w("CongVC", "====Login fali=====" + reason);
			}

			@Override
			public void onException(Throwable throwable) {
				Log.w("CongVC", "====Error login=====" + throwable);
			}

			@Override
			public void onThinking() {
				// show progress bar or something to the user while login is
				// happening
				Log.w("CongVC", "====Thinking=====");
			}

			@Override
			public void onLogin() {
				// change the state of the button or do whatever you want
				Log.w("CongVC", "====Login=====");
				processShareFaceBook(body);
			}

			@Override
			public void onNotAcceptingPermissions(Permission.Type type) {
				Toast.makeText(PoemDetailActivity.this, 
							String.format("You didn't accept %s permissions", type.name()),
							Toast.LENGTH_LONG).show();
			}
		};
		
		final OnLogoutListener onLogoutListener = new OnLogoutListener() {

			@Override
			public void onFail(String reason) {
			}

			@Override
			public void onException(Throwable throwable) {
			}

			@Override
			public void onThinking() {
				// show progress bar or something to the user while login is
				// happening
			}

			@Override
			public void onLogout() {
				// change the state of the button or do whatever you want
			}

		};
	
	/**
	 * Process play audio
	 */
//	private void processPlayAudio() {
//		try {
//			mMediaPlayer.start();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	private void processReport(){
		if(! Common.checkLogin(this)){
			Intent it = new Intent(PoemDetailActivity.this, LoginActivity.class);
			it.putExtra(LoginActivity.TAB_KEY, mTabIndex);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivityForResult(it, LOGIN_CODE);
			overridePendingTransition(0,0);
		}
		else{
			
			if(edtPopupReason.getText().toString().trim().isEmpty()){
				createDialogMessage(getString(R.string.msg_error_title), 
									getString(R.string.msg_error_input_report_reason))
					.show();
				return;
			}
			
			mPopupWindow.dismiss();
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
//			Api.postReportPoem(this, mPoem.getId(), edtPopupReason.getText().toString().trim());
			
			new PoemReportApi().reportPoem(this, mPoem.getId(), 
									edtPopupReason.getText().toString().trim(), mPoemReportListener);
		}
	}
	
	private class GalleryPagerAdapter extends android.support.v4.view.PagerAdapter {
		private PhotoPoem[] urls;
//		private Context context;
//		private ImageLoader imageLoader;
//		private ImageListener imageListener;
		public GalleryPagerAdapter(Context context, PhotoPoem[] urls) {
			this.urls = urls;
//			this.context = context;
//			imageLoader = ((PoemApplication)context.getApplicationContext()).getImageLoader();
		}

		@SuppressLint("InflateParams")
		public Object instantiateItem(View collection, final int position) {
			LayoutInflater inflater = (LayoutInflater) collection.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.gallery_page, null);
			ImageView imageView =  (ImageView) view.findViewById(R.id.gallery_image);
//			imageListener = ImageLoader.getImageListener(imageView, R.drawable.no_image, R.drawable.no_image);
//			imageView.setScaleType(ScaleType.FIT_XY);
			
			if(null != urls && urls.length > 0 && urls.length > position){
//				imageLoader.get(urls[position].getLink(), imageListener);
				
				Point point = new Point(0, 0);
		        Display display = getWindowManager().getDefaultDisplay();
		        display.getSize(point);
		        if(null != urls[position] && null != urls[position].getLinks()){
		        	loadImageView(imageView, urls[position].getLinks().getOrg(), point.x, (int)(point.x * 0.684f));
		        }
			}
			
			((ViewPager) collection).addView(view, 0);
			return view;
		}

		@Override
		public int getCount() {
			return urls == null ? 1 : urls.length;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == ((View) arg1);
		}
		
		@Override
		public void destroyItem(View collection, int position, Object o) {
		    View view = (View) o;
		    ((ViewPager) collection).removeView(view);
		    CleanUpUtil.cleanupView(view);
		    view = null;
		}
				
	}
	
	@SuppressWarnings({ "static-access", "deprecation" })
	public void scaleAndSetImageBitmap(ImageView imgView, Bitmap bitmap, int width, int height) {
		try {
			
			int imgWidth = imgView.getWidth() > 0 ? imgView.getWidth() : width;
			int imgHeight = imgView.getHeight() > 0 ? imgView.getHeight() : height;
			
			float ratioW = (float)imgWidth/(float)bitmap.getWidth();
	        float ratioH = (float)imgHeight/(float)bitmap.getHeight();
	        float ratio = ratioW>ratioH ? ratioW : ratioH;
	        
	        Log.e("CongVC", "===imgView.getWidth():" + imgView.getWidth() + "==imgView.getHeight():" + imgView.getHeight());
	        Log.e("CongVC", "===ratioW:" + ratioW + "==ratioH:" + ratioH);
	        
	        bitmap = bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*ratio), (int)(bitmap.getHeight()*ratio), true);
	        
	        
	     // Store image to default external storage directory
		    if(null == mShareImageUri || mShareImageUri.isEmpty()) {
		    	try {
			        File file =  new File(Environment.getExternalStorageDirectory(), "textinthecity_share_image.png");
			        file.getParentFile().mkdirs();
			        FileOutputStream out = new FileOutputStream(file);
			        bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
			        out.close();
			        mShareImageUri = file.getPath();
			    } catch (IOException e) {
			    	mShareImageUri = "";
			    }
		    }
	        
	        
	        bitmap = bitmap.createBitmap(bitmap, 0, 0, imgWidth, imgHeight);	
	        

	        BitmapDrawable ob = new BitmapDrawable(bitmap);
	        imgView.setBackgroundDrawable(ob);
	        imgView.setScaleType(ScaleType.FIT_START);
	        
	        
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadImageView(final ImageView imgView, String url, final int width, final int height) {
		try {
			
//			imgView.setBackgroundResource(R.drawable.no_image);
			
			Response.Listener<Bitmap> listener = new Response.Listener<Bitmap>() {
			    @Override
			    public void onResponse(Bitmap bitmap) {
			    	Log.e("CongVC", "====Bitmap:" + bitmap);
			        scaleAndSetImageBitmap(imgView, bitmap, width, height);
			        
			    }
			};
			
			ErrorListener eListener = new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError arg0) {
					
					Log.e("CongVC", "====Bitmap error:" + arg0.getMessage());
				}
			};
			
			ImageRequest imgRequest = new ImageRequest(
			        url,
			        listener,
			        width,
			        height,
			        Config.RGB_565,
			        //Config.ARGB_8888,
			        eListener);
			
//			PoemApplication.getInstance().getQueue().add(imgRequest);
			PoemApplication.getInstance().addToRequestQueue(imgRequest, this);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
