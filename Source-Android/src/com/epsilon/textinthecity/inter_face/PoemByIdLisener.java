package com.epsilon.textinthecity.inter_face;

import com.epsilon.textinthecity.object.Poem;

public interface PoemByIdLisener extends BaseListener {
	
	public void requestCompleted(Poem poem);
}
