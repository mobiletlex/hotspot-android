package com.epsilon.textinthecity.inter_face;

import java.util.List;

import com.epsilon.textinthecity.object.LocationMap;

public interface LocationByZoneLisener extends BaseListener {
	
	public void requestCompleted(List<LocationMap> listLocation);
}
