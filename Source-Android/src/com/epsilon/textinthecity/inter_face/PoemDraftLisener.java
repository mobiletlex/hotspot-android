package com.epsilon.textinthecity.inter_face;

import java.util.ArrayList;
import com.epsilon.textinthecity.object.Poem;

public interface PoemDraftLisener extends BaseListener {
	
	public void requestCompleted(ArrayList<Poem> listPoem);
}
