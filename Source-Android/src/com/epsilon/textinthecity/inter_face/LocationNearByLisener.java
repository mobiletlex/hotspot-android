package com.epsilon.textinthecity.inter_face;

import java.util.List;

import com.epsilon.textinthecity.object.LocationMap;

public interface LocationNearByLisener extends BaseListener {
	
	public void requestCompleted(List<LocationMap> listLocation);
}
