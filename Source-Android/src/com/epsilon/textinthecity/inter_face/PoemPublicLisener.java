package com.epsilon.textinthecity.inter_face;

import com.epsilon.textinthecity.object.PoemPagging;

public interface PoemPublicLisener extends BaseListener {
	
	public void requestCompleted(PoemPagging poemPagging);
}
