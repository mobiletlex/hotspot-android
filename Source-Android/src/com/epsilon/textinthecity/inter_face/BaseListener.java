package com.epsilon.textinthecity.inter_face;

import com.android.volley.VolleyError;

public interface BaseListener {
	
	public void requestStarted();
	public void requestCompleteWithErrorParseJson(String errorMsg);
	public void requestEndedWithError(VolleyError error);

}
