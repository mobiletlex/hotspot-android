package com.epsilon.textinthecity.inter_face;

import java.util.List;

import com.epsilon.textinthecity.object.Event;

public interface EventsLisener extends BaseListener {
	
	public void requestCompleted(List<Event> listEvents);
}
