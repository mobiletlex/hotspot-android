package com.epsilon.textinthecity.inter_face;

import java.util.ArrayList;

import com.epsilon.textinthecity.object.Poem;

public interface PoemSubmissionLisener extends BaseListener {
	
	public void requestCompleted(ArrayList<Poem> listPoem);
}
