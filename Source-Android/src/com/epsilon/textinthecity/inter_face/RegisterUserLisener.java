package com.epsilon.textinthecity.inter_face;

import com.epsilon.textinthecity.object.User;

public interface RegisterUserLisener extends BaseListener {
	
	public void requestCompleted(User user);
}
