package com.epsilon.textinthecity.inter_face;

import java.util.List;

import com.epsilon.textinthecity.object.Poem;

public interface PoemRandomLisener extends BaseListener {
	
	public void requestCompleted(List<Poem> listPoem);
}
