package com.epsilon.textinthecity.inter_face;

import com.epsilon.textinthecity.object.Poem;

public interface PoemCreateNewLisener extends BaseListener {
	
	public void requestCompleted(Poem poem);
}
