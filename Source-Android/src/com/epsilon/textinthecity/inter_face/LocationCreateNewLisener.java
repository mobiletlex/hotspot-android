package com.epsilon.textinthecity.inter_face;

import com.epsilon.textinthecity.object.LocationMap;

public interface LocationCreateNewLisener extends BaseListener {
	
	public void requestCompleted(LocationMap lLocation);
}
