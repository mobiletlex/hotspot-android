package com.epsilon.textinthecity.inter_face;

import java.util.List;

import com.epsilon.textinthecity.object.Location;

public interface LocationByAdminLisener extends BaseListener {
	
	public void requestCompleted(List<Location> listLocation);
}
