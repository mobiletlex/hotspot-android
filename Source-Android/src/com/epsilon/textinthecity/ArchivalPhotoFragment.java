package com.epsilon.textinthecity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.costum.android.widget.LoadMoreListView;
import com.epsilon.textinthecity.adapter.ListArchivalPhotoAdapter;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.squareup.otto.Subscribe;

public final class ArchivalPhotoFragment extends Fragment {
    
    private LoadMoreListView lvPoems;
	private ImageView imgLoading;
	private LinearLayout lnLoading;
	private ListArchivalPhotoAdapter mAdapter;
	
	private List<String> listDate;
	private HashMap<String, List<String[]>> hashLocal;

    public static ArchivalPhotoFragment newInstance() {
        ArchivalPhotoFragment fragment = new ArchivalPhotoFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(null == listDate){
        	loadListPoems();
        }
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    }

    @SuppressLint("InflateParams")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
    	View view = inflater.inflate(R.layout.archival_photo_view, null);
    	
    	lvPoems = (LoadMoreListView) view.findViewById(R.id.listPoems);
    	lnLoading = (LinearLayout) view.findViewById(R.id.lnLoading);
    	imgLoading = (ImageView) view.findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
    	
		if(null != listDate){
			lnLoading.setVisibility(View.GONE);
			processShowListPoems();
		}
		else {
			loadListPoems();
		}
		
        return view;
    }
    
    /**
	 * Load list poems from server
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void loadListPoems() {
		
		List<Location> listLocations = (List<Location>)
				Common.getObjFromInternalFile(getActivity(), Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
					
		
		HashMap<String, Location> hashUnlockLoation = (HashMap<String, Location>)
				Common.getObjFromInternalFile(getActivity(), Constant.FILE_CACHE_LIST_UNLOCK_LOCATION);
		
		if(null == hashUnlockLoation || hashUnlockLoation.size() <=0 
				|| null == listLocations || listLocations.size() <= 0){
			if(null != lnLoading){
				lnLoading.setVisibility(View.GONE);
			}
			return;
		}
		
		listDate = new ArrayList<String>();
		hashLocal = new HashMap<String, List<String[]>>();
		
		HashMap<String, String> hashAllUrl = new HashMap<String, String>();
		for (int i = 0; i < listLocations.size(); i++) {
			if(null != listLocations.get(i).getPhotos() && 
					listLocations.get(i).getPhotos().length > 0){
				hashAllUrl.put(listLocations.get(i).getId()+"", listLocations.get(i).getPhotos()[0].getLink());
			}
			else {
				hashAllUrl.put(listLocations.get(i).getId()+"", "");
			}
		}
		
		SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd'th' yyyy"); 
		 
		Iterator iterator = hashUnlockLoation.keySet().iterator();
		while( iterator. hasNext() )
		{
			Location location = hashUnlockLoation.get(iterator.next().toString());
		    
			long dateUnlock = location.getUnlock_date();
						
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(dateUnlock);
			
			String daySuffix = getDayNumberSuffix(calendar.get(Calendar.DAY_OF_MONTH));
			
			postFormater = new SimpleDateFormat("MMMM dd'"+daySuffix+"' yyyy"); 
			
			String newDateStr = postFormater.format(new Date(dateUnlock)); 
			
			String url = "";
			if(hashAllUrl.containsKey(location.getId()+"")){
				url = hashAllUrl.get(location.getId()+"");
			}
			
			if(url.isEmpty() && null != location.getPhotos() && location.getPhotos().length > 0){
				url = location.getPhotos()[0].getLink();
			}
			
			String[] value = new String[2];
			value[0] = location.getId()+"";
			value[1] = url;
			
			if(!hashLocal.containsKey(newDateStr)){
				listDate.add(newDateStr);
				
				List<String[]> listValue = new ArrayList<String[]>();
				listValue.add(value);
				hashLocal.put(newDateStr, listValue);
			}
			else {
				hashLocal.get(newDateStr).add(value);
			}
		}
		
		if(null != lvPoems){
			processShowListPoems();
		}
	}
	
	private String getDayNumberSuffix(int day){
		if(day >= 11 && day <= 13){
			return "th";
		}
		switch (day%10) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";

		default:
			return "th";
		}
			
	}
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {
			    
		// Result for get list poems draft error
//		if(null != event.getResult() && event.getResult().equals(Constant.POEM_DRAFT_ERROR_DEF)){
//	    		   
//			lnLoading.setVisibility(View.GONE);
//		}
//		else if(null != event.getListPoems()){
//			lnLoading.setVisibility(View.GONE);
//			 processShowListPoems(event.getListPoems());
//		}
	}
	
	/**
	 * Process show list poems ti list view
	 */
	private void processShowListPoems() {
		
		if(null != listDate && listDate.size() > 0){
	    	
	    		mAdapter = new ListArchivalPhotoAdapter(getActivity(), listDate, hashLocal);
	    		lvPoems.setAdapter(mAdapter);
	    
	    }
	}

}
