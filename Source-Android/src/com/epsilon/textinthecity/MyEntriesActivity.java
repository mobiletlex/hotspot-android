package com.epsilon.textinthecity;

import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MyEntriesActivity extends BaseFlagmentActivity implements OnClickListener {
		
	private static final int LOGIN_CODE = 100;
	
	private TextView tvDraft;
	private TextView tvSubmission;
	private TextView tvArchivalPhoto;
	
	private Button btnEdit;
	
	private ViewPager mPager;
	private boolean isShareBack = false;
	
	private GoogleMusicAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_entries);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_my_entries), false);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_MY_ENTRIES);
		
		Tracker t = PoemApplication.getInstance().getTracker(
                TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
        	.setCategory(getString(R.string.google_analytics_event_category_tab_open))
        	.setAction(getString(R.string.google_analytics_event_action_open))
        	.setLabel(getString(R.string.google_analytics_event_open_tab_myentries)).build());
		
		isShareBack = getIntent().getBooleanExtra(ShareActivity.KEY_SHARE_SCREEN, false);
		
		initControls();
        setupWidthCenter();
		
        if(! Common.checkLogin(this)){
			Intent it = new Intent(MyEntriesActivity.this, LoginActivity.class);
			it.putExtra(LoginActivity.TAB_KEY, Constant.TAB_MENU_INDEX_MY_ENTRIES);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivityForResult(it, LOGIN_CODE);
			overridePendingTransition(0,0);
		}
        else {
        	setupPager();
        }
	}
	
	private void initControls(){
		tvDraft = (TextView) findViewById(R.id.tvDraft);
		tvSubmission = (TextView) findViewById(R.id.tvSubmission);
		tvArchivalPhoto = (TextView) findViewById(R.id.tvArchivalPhoto);
		
		tvDraft.setOnClickListener(this);
		tvSubmission.setOnClickListener(this);
		tvArchivalPhoto.setOnClickListener(this);
		
		tvDraft.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		tvSubmission.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		tvArchivalPhoto.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		tvDraft.setTextColor(getResources().getColor(R.color.tab_white));

        mPager = (ViewPager)findViewById(R.id.pager);
        
        btnEdit = (Button) findViewById(R.id.btnEdit);
        btnEdit.setBackgroundResource(R.drawable.bg_bio_btn);
        btnEdit.setText(getString(R.string.my_entries_edit));
        btnEdit.setOnClickListener(this);
        btnEdit.setVisibility(View.VISIBLE);
        btnEdit.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
        btnEdit.setPadding(getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_left_right), 
        									getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_top_bottom), 
        									getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_left_right), 
        									getResources().getDimensionPixelSize(R.dimen.poem_detail_bio_padding_top_bottom));
       
	}
	
	private void setupPager() {
		adapter = new GoogleMusicAdapter(getSupportFragmentManager());
		mPager.setAdapter(adapter);
	        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int position) {
					tvDraft.setTextColor(position==0 ? 
								getResources().getColor(R.color.tab_white) : 
								getResources().getColor(R.color.black_congrat));
					
					tvSubmission.setTextColor(position==1 ? 
							getResources().getColor(R.color.tab_white) : 
							getResources().getColor(R.color.black_congrat));
					
					tvArchivalPhoto.setTextColor(position==2 ? 
							getResources().getColor(R.color.tab_white) : 
							getResources().getColor(R.color.black_congrat));
					
					btnEdit.setVisibility(position==2?View.INVISIBLE:View.VISIBLE);
				}
				
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}
				
				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
			});
	    if(isShareBack){
	    	mPager.setCurrentItem(1);
	    }
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        mPager.setPadding(paddingCenterLayout, 
							0, 
							paddingCenterLayout, 
							0);
         
	}
	
	public class GoogleMusicAdapter extends FragmentPagerAdapter {
		private PoemDraftFragment draftFlagment = null;
		private PoemSubmissionFragment submissionFlagment = null;
		private ArchivalPhotoFragment archivalPhotoFlagment = null;
        public GoogleMusicAdapter(FragmentManager fm) {
            super(fm);
            if(null == draftFlagment){
    			draftFlagment = PoemDraftFragment.newInstance();
    		}
            if(null == submissionFlagment){
    			submissionFlagment = PoemSubmissionFragment.newInstance();
    		}
            if(null == archivalPhotoFlagment){
    			archivalPhotoFlagment = ArchivalPhotoFragment.newInstance();
    		}
        }
        
        public PoemDraftFragment getDraftFlagment() {
			return draftFlagment;
		}



		public PoemSubmissionFragment getSubmissionFlagment() {
			return submissionFlagment;
		}



		@Override
        public Fragment getItem(int position) {
        	if(position == 0){
        		
        		return draftFlagment;
        	}
        	else if(position == 1){
        		
        		return submissionFlagment;
        	}
        	else {
        		
        		return archivalPhotoFlagment;
        	}
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        @Override
        public int getCount() {
          return 3;
        }
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == LOGIN_CODE && resultCode == RESULT_CANCELED) {
			finish();
		}
		else if(requestCode == LOGIN_CODE && resultCode == RESULT_OK){
			setupPager();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvDraft:
			
			mPager.setCurrentItem(0, true);
			
			tvDraft.setTextColor(getResources().getColor(R.color.tab_white));
			tvSubmission.setTextColor(getResources().getColor(R.color.black_congrat));
			tvArchivalPhoto.setTextColor(getResources().getColor(R.color.black_congrat));
			btnEdit.setVisibility(View.VISIBLE);
			break;
			
		case R.id.tvSubmission:
			
			mPager.setCurrentItem(1, true);
			
			tvDraft.setTextColor(getResources().getColor(R.color.black_congrat));
			tvSubmission.setTextColor(getResources().getColor(R.color.tab_white));
			tvArchivalPhoto.setTextColor(getResources().getColor(R.color.black_congrat));
			btnEdit.setVisibility(View.VISIBLE);
			break;
			
		case R.id.tvArchivalPhoto:
			
			mPager.setCurrentItem(2, true);
			
			tvDraft.setTextColor(getResources().getColor(R.color.black_congrat));
			tvSubmission.setTextColor(getResources().getColor(R.color.black_congrat));
			tvArchivalPhoto.setTextColor(getResources().getColor(R.color.tab_white));
			btnEdit.setVisibility(View.INVISIBLE);
			break;
			
		case R.id.btnEdit:
			if(btnEdit.getText().toString().equals(getString(R.string.my_entries_edit))){
				btnEdit.setText(getString(R.string.my_entries_done));
				if(null != adapter){
					adapter.getDraftFlagment().setDelete(true);
					adapter.getSubmissionFlagment().setDelete(true);
				}
			}
			else {
				btnEdit.setText(getString(R.string.my_entries_edit));
				if(null != adapter){
					adapter.getDraftFlagment().setDelete(false);
					adapter.getSubmissionFlagment().setDelete(false);
				}
			}
			break;

		default:
			break;
		}
	}

}
