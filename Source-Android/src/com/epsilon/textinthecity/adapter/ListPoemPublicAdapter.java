package com.epsilon.textinthecity.adapter;

import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.epsilon.textinthecity.ExploreActivity;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.PoemDetailActivity;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.object.Poem;

public class ListPoemPublicAdapter extends BaseAdapter {
	
	private List<Poem> mData = new ArrayList<Poem>();
	private LayoutInflater mInflater;
	
	private Context mContext;
	
	public ListPoemPublicAdapter(Context context, List<Poem> data) {
		this.mContext = context;
		this.mData = data;
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void addItem(final Poem item) {
        mData.add(item);
    }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            
            convertView = mInflater.inflate(R.layout.poem_public_item, null);
            holder.tvZoneName = (TextView)convertView.findViewById(R.id.tvZoneName);
            holder.tvAuthor = (TextView)convertView.findViewById(R.id.tvAuthor);
            holder.tvPoemTitle = (TextView)convertView.findViewById(R.id.tvPoemsTitle);
            holder.tvPoemTitle1 = (TextView)convertView.findViewById(R.id.tvPoemsTitle1);
            holder.itemPoem = (View) convertView.findViewById(R.id.itemPoem);
            holder.itemData = (View) convertView.findViewById(R.id.lnData);
            holder.btnLike = (ImageView) convertView.findViewById(R.id.btnLike);
              
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        
        Point point = new Point(0, 0);
        Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        holder.itemData.setPadding(paddingCenterLayout, 
							mContext.getResources().getDimensionPixelSize(R.dimen.explore_item_padding_top_bottom), 
							0, 
							0);
        
       
        String title = mData.get(position).getTitle();
        String title1 = mData.get(position).getTitle_alt();
//        String zoneName = Common.getZoneNameByLocationId(mData.get(position).getLocation_id(), mContext);
        String zoneName = Common.getZoneNameById(mData.get(position).getZone_id());
        String author = mData.get(position).getAuthor();
        
        holder.tvPoemTitle.setText(title);
        if(! title1.isEmpty() && mData.get(position).getLanguage_id() != Constant.LANGUAGE_ID_ENG){
        	holder.tvPoemTitle1.setText(title1);
        	holder.tvPoemTitle1.setVisibility(View.VISIBLE);
        }
        else {
        	holder.tvPoemTitle1.setVisibility(View.GONE);
        }
        holder.tvZoneName.setText(zoneName);
        holder.tvAuthor.setText(author);
        
        holder.tvPoemTitle.setTypeface(PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
        holder.tvPoemTitle1.setTypeface(PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
        holder.tvZoneName.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
        holder.tvAuthor.setTypeface(PoemApplication.getInstance().typeFaceFuturaLightOblique);
        
        if(mData.get(position).isIs_liked()){
        	holder.btnLike.setVisibility(View.VISIBLE);
        	
        	 holder.tvZoneName.setTextColor(mContext.getResources().getColor(R.color.tab_meu_background_color));
        	 holder.tvAuthor.setTextColor(mContext.getResources().getColor(R.color.tab_meu_background_color));
        	 holder.tvPoemTitle.setTextColor(mContext.getResources().getColor(R.color.tab_meu_background_color));
        	 holder.tvPoemTitle1.setTextColor(mContext.getResources().getColor(R.color.tab_meu_background_color));
        }
        else {
        	holder.btnLike.setVisibility(View.INVISIBLE);
        	
        	holder.tvZoneName.setTextColor(mContext.getResources().getColor(R.color.gray_zone_name));
       	    holder.tvAuthor.setTextColor(mContext.getResources().getColor(R.color.gray_zone_name));
       	    holder.tvPoemTitle.setTextColor(mContext.getResources().getColor(R.color.gray_zone_name));
       	    holder.tvPoemTitle1.setTextColor(mContext.getResources().getColor(R.color.gray_zone_name));
        }
        
        holder.itemPoem.setOnClickListener(new ItemPoemListener(mData.get(position)));
         
        return convertView;
    }
	
	public static class ViewHolder {
        public TextView tvZoneName;
        public TextView tvAuthor;
        public TextView tvPoemTitle;
        public TextView tvPoemTitle1;
        public View itemPoem;
        public View itemData;
        public ImageView btnLike;
    }
	
	private class ItemPoemListener implements OnClickListener {
		
		Poem poem = null;
		public ItemPoemListener(Poem poem) {
			this.poem = poem;
		}

		@Override
		public void onClick(View v) {
			Intent it = new Intent(mContext, PoemDetailActivity.class);
			it.putExtra(PoemDetailActivity.TAB_INDEX_POEM_DETAIL_KEY, Constant.TAB_MENU_INDEX_POEMS);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			it.putExtra(ExploreActivity.KEY_POEM_OBJECT, this.poem);
			mContext.startActivity(it);
			((Activity)mContext).overridePendingTransition(0,0);
		}
		
	}
	

}
