package com.epsilon.textinthecity.adapter;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.epsilon.textinthecity.EditPoemActivity;
import com.epsilon.textinthecity.ExploreActivity;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.PoemDraftFragment;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.api.PoemDraftDeleteApi;
import com.epsilon.textinthecity.object.Poem;

public class ListPoemDraftAdapter extends BaseAdapter {
	
	private ArrayList<Poem> mData = new ArrayList<Poem>();
	private LayoutInflater mInflater;
	
	private Context mContext;
	private PoemDraftFragment mPoemDraftFlagment;
	
	private boolean isDelete = false;
	private int posDelete = 0;
	
	/**
	 * @return the isDelete
	 */
	public boolean isDelete() {
		return isDelete;
	}

	/**
	 * @param isDelete the isDelete to set
	 */
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	/**
	 * @return the posDelete
	 */
	public int getPosDelete() {
		return posDelete;
	}

	/**
	 * @param posDelete the posDelete to set
	 */
	public void setPosDelete(int posDelete) {
		this.posDelete = posDelete;
	}

	public ListPoemDraftAdapter(Context context, ArrayList<Poem> data, PoemDraftFragment poemDraftFlagment) {
		this.mContext = context;
		this.mData = data;
		this.mPoemDraftFlagment = poemDraftFlagment;
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void addItem(final Poem item) {
        mData.add(item);
    }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public ArrayList<Poem> getData() {
		return mData;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            
            convertView = mInflater.inflate(R.layout.poem_draft_item, null);
            holder.tvPoemContent = (TextView)convertView.findViewById(R.id.tvPoemContent);
            holder.tvPoemTitle = (TextView)convertView.findViewById(R.id.tvPoemsTitle);
            holder.itemPoem = (View) convertView.findViewById(R.id.itemPoem);
            holder.itemPoemLeft = (View) convertView.findViewById(R.id.itemPoemLeft);
            holder.btnEdit = (Button) convertView.findViewById(R.id.btnEdit);
            holder.btnDelete = (Button) convertView.findViewById(R.id.btnDelete);
              
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
       
        String content = mData.get(position).getContent();
        String title = mData.get(position).getTitle();
        
        holder.tvPoemContent.setText(content);
        holder.tvPoemTitle.setText(title);
         
        holder.tvPoemContent.setTypeface(PoemApplication.getInstance().typeFaceFuturaLightOblique);
		holder.tvPoemTitle.setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		holder.btnDelete.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		if(isDelete){
			holder.btnDelete.setVisibility(View.VISIBLE);
			holder.btnEdit.setVisibility(View.GONE);
		}
		else {
			holder.btnDelete.setVisibility(View.GONE);
			holder.btnEdit.setVisibility(View.VISIBLE);
		}
        
//        holder.itemPoem.setOnClickListener(new ItemPoemListener(mData.get(position)));
        holder.itemPoem.setOnClickListener(new ItemEditListener(mData.get(position)));
        holder.btnEdit.setOnClickListener(new ItemEditListener(mData.get(position)));
        holder.btnDelete.setOnClickListener(new ItemDeleteListener(position));
        
        return convertView;
    }
	
	public static class ViewHolder {
        public TextView tvPoemContent;
        public TextView tvPoemTitle;
        public View itemPoem;
        public View itemPoemLeft;
        public Button btnEdit;
        public Button btnDelete;
    }
	
	
	
	private class ItemEditListener implements OnClickListener {
		
		Poem poem = null;
		public ItemEditListener(Poem poem) {
			this.poem = poem;
		}

		@Override
		public void onClick(View v) {
			Intent it = new Intent(mContext, EditPoemActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			it.putExtra(ExploreActivity.KEY_POEM_OBJECT, this.poem);
			mContext.startActivity(it);
			((Activity)mContext).overridePendingTransition(0,0);
		}
		
	}
	
	private class ItemDeleteListener implements OnClickListener {
		
		int position = 0;
		public ItemDeleteListener(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			
			new AlertDialog.Builder(mContext)
//			.setTitle(getString(R.string.warning))
			.setMessage(mContext.getString(R.string.msg_confirm_delete))
			.setPositiveButton(mContext.getString(R.string.delete),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,int whichButton) {
						setPosDelete(position);
//						Api.deletePoem(mContext, mData.get(position).getId(), true);
						new PoemDraftDeleteApi().deletePoemsDraft(mContext, 
								mData.get(position).getId(), 
								mPoemDraftFlagment.mPoemDraftDeleteListener, mPoemDraftFlagment);
						mPoemDraftFlagment.showLoading();
					}
				})
			.setNegativeButton(mContext.getString(R.string.cancel), 
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				})
			.create()
			.show();
			
		}
		
	}
	

}
