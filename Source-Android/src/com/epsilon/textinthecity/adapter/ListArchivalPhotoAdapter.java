package com.epsilon.textinthecity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.UnlockDialog;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.object.Location;

public class ListArchivalPhotoAdapter extends BaseAdapter {
	
	private List<String> listDate = new ArrayList<String>();
	private HashMap<String, List<String[]>> hashLocal = new HashMap<String, List<String[]>>();
	private LayoutInflater mInflater;
	
	private Context mContext;
	
//	private ImageLoader imageLoader;
//	private ImageListener imageListener;
	
	Point point = new Point(0, 0);
	
	public ListArchivalPhotoAdapter(Context context, List<String> listDate, 
								HashMap<String, List<String[]>> hashLocal) {
		this.mContext = context;
		this.listDate = listDate;
		this.hashLocal = hashLocal;
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		imageLoader = ((PoemApplication)context.getApplicationContext()).getImageLoader();
		
		 
        Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
        display.getSize(point);
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listDate.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listDate.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            
            convertView = mInflater.inflate(R.layout.archival_photo_item, null);
            holder.tvDate = (TextView)convertView.findViewById(R.id.tvDate);
            holder.lnPhotos = (LinearLayout) convertView.findViewById(R.id.lnPhotos);
              
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
       
        
        holder.tvDate.setText(listDate.get(position));
        holder.tvDate.setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
        
        holder.lnPhotos.removeAllViews();
        holder.lnPhotos.removeAllViewsInLayout();
        ((LayoutParams)holder.lnPhotos.getLayoutParams()).bottomMargin = (point.x - (int)(point.x*0.9f))/2;
       List<String[]> value = hashLocal.get(listDate.get(position));
       if(null != value && value.size() > 0){
    	   LinearLayout lnItem = new LinearLayout(mContext);
    	   int i = 0;
    	   for ( ; i < value.size(); i++) {
    		   if(i%3 == 0){
    			   
    			   View v = new View(mContext);
    			   LayoutParams lpView = new LayoutParams(LayoutParams.MATCH_PARENT, (point.x - (int)(point.x*0.9f))/2);
    			   v.setLayoutParams(lpView);
    			   holder.lnPhotos.addView(v);
    			   
    			   LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    			   lnItem = new LinearLayout(mContext);
    			   lnItem.setLayoutParams(lp);
    			   lnItem.setOrientation(LinearLayout.HORIZONTAL);
    			   
    			   holder.lnPhotos.addView(lnItem);
    			   
    		   }
    		   else {
    			   View v = new View(mContext);
    			   LayoutParams lpView = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
    			   v.setLayoutParams(lpView);
    			   lnItem.addView(v);
    		   }
    		       		   
    		   ImageView img = new ImageView(mContext);
    		   LayoutParams lp1 = new LayoutParams(point.x/4, point.x/4);
    		   img.setLayoutParams(lp1);
    		   img.setScaleType(ScaleType.FIT_XY);
    		   img.setBackgroundResource(R.drawable.no_image);
//    		   imageListener = ImageLoader.getImageListener(img, R.drawable.no_image, R.drawable.no_image);
//   				imageLoader.get(value.get(i)[1], imageListener);
   				Common.loadImageView(img, value.get(i)[1], point.x/4, point.x/4, mContext);
   				img.setOnClickListener(new PhotoListener(value.get(i)[0]));
   				lnItem.addView(img);
    	   }
    	   if(i%3 != 0){
    		   for (int j = 0; j < 3 - (i%3); j++) {
    			   View v = new View(mContext);
    			   LayoutParams lpView = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
    			   v.setLayoutParams(lpView);
    			   lnItem.addView(v);
    			   
    			   ImageView img = new ImageView(mContext);
        		   LayoutParams lp1 = new LayoutParams(point.x/4, point.x/4);
        		   img.setLayoutParams(lp1);
        		   img.setScaleType(ScaleType.FIT_XY);
       				lnItem.addView(img);
    		   }
    	   }
       }
       
        
        return convertView;
    }
	
	public static class ViewHolder {
        public TextView tvDate;
        public LinearLayout lnPhotos;
    }
	
	private class PhotoListener implements OnClickListener {
		
		String idLocation = "";
		public PhotoListener(String idLocation) {
			this.idLocation = idLocation;
		}

		@Override
		public void onClick(View v) {
			
			Location lc = Common.getLocationById(Integer.parseInt(idLocation), mContext);
			if(null != lc){
				Dialog dl = new UnlockDialog(mContext, 
						android.R.style.Theme_Translucent_NoTitleBar,
						lc);
					dl.show();
			}
			
		}
		
	}
	
}
