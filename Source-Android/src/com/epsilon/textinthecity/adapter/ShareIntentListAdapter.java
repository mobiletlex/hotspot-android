package com.epsilon.textinthecity.adapter;

import java.util.HashMap;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.epsilon.textinthecity.R;

public class ShareIntentListAdapter extends ArrayAdapter < Object > {
    Activity context;
    private final Object[] items;

    int layoutId;


    public ShareIntentListAdapter(Activity context, int layoutId, Object[] items) {
        super(context, layoutId, items);

        this.context = context;
        this.items = items;
        this.layoutId = layoutId;


    }

    @SuppressLint("ViewHolder")
	@SuppressWarnings("unchecked")
	public View getView(int pos, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View row = inflater.inflate(layoutId, null);
        TextView label = (TextView) row.findViewById(R.id.titleApp);
        ImageView image = (ImageView) row.findViewById(R.id.imgApp);
        if(items[pos] instanceof ResolveInfo){
        	label.setText(((ResolveInfo) items[pos]).activityInfo.applicationInfo.loadLabel(context.getPackageManager()).toString());
            image.setImageDrawable(((ResolveInfo) items[pos]).activityInfo.applicationInfo.loadIcon(context.getPackageManager()));
        }
        else{
        	label.setText(context.getString(((HashMap<String, Integer>)items[pos]).get("title")));
        	image.setImageResource(((HashMap<String, Integer>)items[pos]).get("icon"));
        }
        System.out.println(layoutId);
        return (row);

    }


}