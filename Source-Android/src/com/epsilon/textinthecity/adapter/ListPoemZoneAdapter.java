package com.epsilon.textinthecity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.epsilon.textinthecity.ExploreActivity;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.PoemDetailActivity;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.object.Poem;

public class ListPoemZoneAdapter extends BaseAdapter {
	
	private List<Poem> mData = new ArrayList<Poem>();
	private HashMap<Integer, String> hashRefixLocation;
	
	private LayoutInflater mInflater;
	
	private Context mContext;
	
	public ListPoemZoneAdapter(Context context, List<Poem> data, HashMap<Integer, String> hashRefixLocation) {
		this.mContext = context;
		this.mData = data;
		this.hashRefixLocation = hashRefixLocation;
		
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void addItem(final Poem item) {
        mData.add(item);
    }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            
            convertView = mInflater.inflate(R.layout.explore_zonemap_item, null);
            holder.tvZoneName = (TextView)convertView.findViewById(R.id.tvZoneName);
            holder.tvZoneRefix = (TextView)convertView.findViewById(R.id.tvZoneRefix);
            holder.tvLandmark = (TextView)convertView.findViewById(R.id.tvLandmark);
            holder.tvTitle = (TextView)convertView.findViewById(R.id.tvPoemsName);
            holder.tvTitle1 = (TextView)convertView.findViewById(R.id.tvPoemsName1);
            holder.itemPoem = (View) convertView.findViewById(R.id.itemPoem);
            
            holder.tvZoneName.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
            holder.tvZoneRefix.setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
            holder.tvLandmark.setTypeface(PoemApplication.getInstance().typeFaceFuturaLightOblique);
    		holder.tvTitle.setTypeface(PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
    		holder.tvTitle1.setTypeface(PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
            
              
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
//        holder.tvZoneName.setText(
//        		Common.getZoneNameByLocationId(mData.get(position).getLocation_id(), mContext));
        holder.tvZoneName.setText(
        		Common.getZoneNameById(mData.get(position).getZone_id()));
        holder.tvLandmark.setText(mData.get(position).getAuthor());
        
        String title = mData.get(position).getTitle();
		
        holder.tvTitle.setText(title);
        String title1 = mData.get(position).getTitle_alt();
		
		if(! title1.isEmpty() && mData.get(position).getLanguage_id() != Constant.LANGUAGE_ID_ENG){
			 holder.tvTitle1.setText(title1);
			 holder.tvTitle1.setVisibility(View.VISIBLE);
		}
		else {
			 holder.tvTitle1.setVisibility(View.GONE);
		}
		
		String locationRefix = "";
		if(hashRefixLocation.containsKey(mData.get(position).getLocation_id())){
			locationRefix = hashRefixLocation.get(mData.get(position).getLocation_id());
		}
		holder.tvZoneRefix.setText(locationRefix);
        
        
        holder.itemPoem.setOnClickListener(new ItemPoemListener(mData.get(position)));
        
        return convertView;
    }
	
	public static class ViewHolder {
        public TextView tvZoneName;
        public TextView tvZoneRefix;
        public TextView tvLandmark;
        public TextView tvTitle;
        public TextView tvTitle1;
        public View itemPoem;
    }
	
	private class ItemPoemListener implements OnClickListener {
		
		Poem poem = null;
		public ItemPoemListener(Poem poem) {
			this.poem = poem;
		}

		@Override
		public void onClick(View v) {
			Intent it = new Intent(mContext, PoemDetailActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			it.putExtra(PoemDetailActivity.TAB_INDEX_POEM_DETAIL_KEY, Constant.TAB_MENU_INDEX_EXPLORE);
			it.putExtra(ExploreActivity.KEY_POEM_OBJECT, this.poem);
			mContext.startActivity(it);
			((Activity)mContext).overridePendingTransition(0,0);
		}
		
	}
	
//	private String getZoneNameByLocationId(int locationId, Context context) {
//		try {
//			List<Location> listLocations = (List<Location>)
//					Common.getObjFromInternalFile(context, Constant.FILE_CACHE_LIST_ALL_LOCATION);
////			List<Location> listLocations = Constant.LIST_ALL_LOCATION;
//			
//			if(null != listLocations && listLocations.size() > 0){
//				for (int i = 0; i < listLocations.size(); i++) {
//					Log.e("CongVC", "===List:"+ listLocations.get(i).getId() + "==local:" + locationId);
//					if(listLocations.get(i).getId() == locationId){
//						 Log.e("CongVC", "=====Zone:" + listLocations.get(i).getZone_id() + "==="+getZoneNameById(listLocations.get(i).getZone_id()));
//						return getZoneNameById(listLocations.get(i).getZone_id());
//					}
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			return "";
//		}
//		return "";
//	}
//	
//	private String getZoneNameById(int zoneId){
//		
//		try {
//			Iterator iterator = Constant.HASH_ZONES.keySet().iterator();
//			while( iterator. hasNext() )
//			{
//				Zone zone = Constant.HASH_ZONES.get(Integer.parseInt(iterator.next().toString()));
//			    if(zone.getId() == zoneId){
//			    	return zone.getName();
//			    }
//			}
//		} catch (Exception e) {
//			return "";
//		}
//		return "";
//	}

}
