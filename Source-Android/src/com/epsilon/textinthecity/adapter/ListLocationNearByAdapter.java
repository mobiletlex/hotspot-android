package com.epsilon.textinthecity.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.epsilon.textinthecity.LocationsActivity;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.object.LocationMap;

public class ListLocationNearByAdapter extends BaseAdapter {
	
	private List<LocationMap> workList = null;
	private List<LocationMap> mData = new ArrayList<LocationMap>();
	private LayoutInflater mInflater;
	
	private Context mContext;
	
	public ListLocationNearByAdapter(Context context, List<LocationMap> data) {
		this.mContext = context;
		this.workList = data;
		this.mData.addAll(workList);
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void addItem(final LocationMap item) {
		mData.add(item);
    }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return workList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return workList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            
            convertView = mInflater.inflate(R.layout.location_item, null);
            holder.tvLocationName = (TextView)convertView.findViewById(R.id.tvLocationName);
            holder.tvLocationAddress = (TextView)convertView.findViewById(R.id.tvLocationAddress);
            holder.itemLocation = (View) convertView.findViewById(R.id.itemLocation);
              
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.tvLocationName.setText(workList.get(position).getName());
        holder.tvLocationAddress.setText(workList.get(position).getAddress());
        
        holder.tvLocationName.setTypeface(PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
        holder.tvLocationAddress.setTypeface(PoemApplication.getInstance().typeFaceFuturaLightOblique);
        
        holder.itemLocation.setOnClickListener(new ItemLocationListener(workList.get(position)));
        
        return convertView;
    }
	
	public static class ViewHolder {
        public TextView tvLocationName;
        public TextView tvLocationAddress;
        public View itemLocation;
    }
	
	private class ItemLocationListener implements OnClickListener {
		
		LocationMap locationMap = null;
		public ItemLocationListener(LocationMap locationMap) {
			this.locationMap = locationMap;
		}

		@Override
		public void onClick(View v) {
			
			Intent it = ((LocationsActivity) mContext).getIntent();
			if(null == it){
				it = new Intent();
			}
			it.putExtra(LocationsActivity.KEY_LOCATION_ID, locationMap.getId());
			it.putExtra(LocationsActivity.KEY_LOCATION_NAME, locationMap.getName());
			it.putExtra(LocationsActivity.KEY_LOCATION_ADDRESS, locationMap.getAddress());
			
			((LocationsActivity) mContext).setResult(Activity.RESULT_OK, it);
		
			((LocationsActivity) mContext).finish();
		}
		
	}
	
	// Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        workList = new ArrayList<LocationMap>();
        if (charText.length() == 0) {
        	workList.addAll(mData);
        }
        else
        {
            for (LocationMap wp : mData)
            {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    workList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
	
}
