package com.epsilon.textinthecity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.object.Event;

public class ListEventsAdapter extends BaseAdapter {
	
	private List<Event> workList = null;
	private List<Event> mData = new ArrayList<Event>();
	private LayoutInflater mInflater;
	
	private Context mContext;
	
	public ListEventsAdapter(Context context, List<Event> data) {
		this.mContext = context;
		this.workList = data;
		this.mData.addAll(workList);
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void addItem(final Event item) {
		mData.add(item);
    }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return workList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return workList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            
            convertView = mInflater.inflate(R.layout.event_item, null);
            holder.tvEventTitle = (TextView)convertView.findViewById(R.id.tvEventTitle);
            holder.tvEventDescription = (TextView)convertView.findViewById(R.id.tvEventDescription);
            holder.itemEvent = (View) convertView.findViewById(R.id.itemEvent);
              
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        
        holder.tvEventTitle.setText(workList.get(position).getTitle());
        holder.tvEventDescription.setText(workList.get(position).getShort_content());
        
        holder.tvEventTitle.setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
        holder.tvEventDescription.setTypeface(PoemApplication.getInstance().typeFaceFuturaLight);
        
        HashMap<Integer, Event> hashReadEvents = (HashMap<Integer, Event>)
				Common.getObjFromInternalFile(mContext, Constant.FILE_CACHE_LIST_READ_EVENTS);
        
        if(null != hashReadEvents && hashReadEvents.containsKey(workList.get(position).getId())){
        	holder.tvEventTitle.setTextColor(mContext.getResources().getColor(R.color.black_congrat));
        }
        else {
        	holder.tvEventTitle.setTextColor(mContext.getResources().getColor(R.color.tab_meu_background_color));
        }
        
        holder.itemEvent.setOnClickListener(new ItemEventListener(workList.get(position)));
        
        return convertView;
    }
	
	public static class ViewHolder {
        public TextView tvEventTitle;
        public TextView tvEventDescription;
        public View itemEvent;
    }
	
	private class ItemEventListener implements OnClickListener {
		
		Event event = null;
		public ItemEventListener(Event event) {
			this.event = event;
		}

		@SuppressLint("UseSparseArrays")
		@SuppressWarnings("unchecked")
		@Override
		public void onClick(View v) {
			
			HashMap<Integer, Event> hashReadEvents = (HashMap<Integer, Event>)
					Common.getObjFromInternalFile(mContext, Constant.FILE_CACHE_LIST_READ_EVENTS);
			
			if(null == hashReadEvents){
				hashReadEvents = new HashMap<Integer, Event>();
			}
			if(!hashReadEvents.containsKey(event.getId())){
				hashReadEvents.put(event.getId(), event);
				
				Common.deleteFileFromInternal(mContext, Constant.FILE_CACHE_LIST_READ_EVENTS);
				Common.writeFileToInternal(mContext, Constant.FILE_CACHE_LIST_READ_EVENTS, hashReadEvents);
			}
			
			Common.openBrowser(mContext, event.getLink());	
			
		}
		
	}
	
	
}
