package com.epsilon.textinthecity.adapter;

import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.epsilon.textinthecity.LocationsActivity;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.object.Zone;

public class ListZoneAdapter extends BaseAdapter {
	
	private List<Zone> workList = null;
	private LayoutInflater mInflater;
	
	private Context mContext;
	
	public ListZoneAdapter(Context context, List<Zone> data) {
		this.mContext = context;
		this.workList = data;
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return workList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return workList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            
            convertView = mInflater.inflate(R.layout.zone_item, null);
            holder.tvZoneName = (TextView)convertView.findViewById(R.id.tvZoneName);
            holder.itemLocation = (View) convertView.findViewById(R.id.itemLocation);
              
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.tvZoneName.setText(workList.get(position).getName());
        
        holder.tvZoneName.setTypeface(PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
        
        holder.itemLocation.setOnClickListener(new ItemLocationListener(workList.get(position)));
        
        return convertView;
    }
	
	public static class ViewHolder {
        public TextView tvZoneName;
        public View itemLocation;
    }
	
	private class ItemLocationListener implements OnClickListener {
		
		Zone locationMap = null;
		public ItemLocationListener(Zone locationMap) {
			this.locationMap = locationMap;
		}

		@Override
		public void onClick(View v) {
			
			((LocationsActivity) mContext).setZone(locationMap);
		
		}
		
	}
	
	
}
