package com.epsilon.textinthecity.common;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.epsilon.textinthecity.PoemApplication;
import com.epsilon.textinthecity.object.Event;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.object.Zone;

public class Common {
	
	/**
	 * write File to internal storage
	 * 
	 * @param context
	 * @param fileName
	 * @param data
	 * 
	 * @return boolean : true is write file success
	 * 
	 */
	public static boolean writeFileToInternal(Context context, 
												String fileName,
												Object data){
		FileOutputStream fos;
		try {
			fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);			
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(data);
			os.close();
			
			return true;
		} catch (FileNotFoundException e) {
			return false;
		}
		catch (IOException e) {
			return false;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * get object data from file
	 * 
	 *  @param context
	 *  @param fileName
	 * 
	 * @return object from file
	 */
	public static Object getObjFromInternalFile(Context context, String fileName) {
		try {
			InputStream fis = context.openFileInput(fileName);
			ObjectInputStream is = new ObjectInputStream(fis);
			Object result = is.readObject();	
			is.close();
			return result;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Delete cache total history file
	 * 
	 * @param mContext
	 * @param fileName
	 */
	public static void deleteFileFromInternal(Context mContext, String fileName) {
		try {
			mContext.deleteFile(fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	public static String getZoneNameByLocationId(int locationId, Context context) {
//		try {
//			List<Location> listLocations = (List<Location>)
//					Common.getObjFromInternalFile(context, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
////			List<Location> listLocations = Constant.LIST_ALL_LOCATION;
//			
//			if(null != listLocations && listLocations.size() > 0){
//				for (int i = 0; i < listLocations.size(); i++) {
//					if(listLocations.get(i).getId() == locationId){
//						return getZoneNameById(listLocations.get(i).getZone_id());
//					}
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			return "";
//		}
//		return "";
//	}
	
//	public static String getZoneNameByLocationId(int locationId, Context context) {
//		try {
//			return Constant.HASH_LOCATION_ZONENAME.get(locationId);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return "";
//		}
////		return "";
//	}
	
	public static String getZoneNameById(int zoneId){
		
		try {
			if(Constant.HASH_ZONES_BY_ID.containsKey(zoneId)){
				return Constant.HASH_ZONES_BY_ID.get(zoneId).getName();
			}
//			Iterator iterator = Constant.HASH_ZONES.keySet().iterator();
//			while( iterator. hasNext() )
//			{
//				Zone zone = Constant.HASH_ZONES.get(Integer.parseInt(iterator.next().toString()));
//			    if(zone.getId() == zoneId){
//			    	return zone.getName();
//			    }
//			}
		} catch (Exception e) {
			return "";
		}
		return "";
	}
	
	@SuppressWarnings("rawtypes")
	public static int getNearestZoneByLocation(android.location.Location location){
		int zoneId = 0;
		float actualDistance = 0;
		try {
			Iterator iterator = Constant.HASH_ZONES_BY_ID.keySet().iterator();
			while( iterator. hasNext() )
			{
				Zone zone = Constant.HASH_ZONES_BY_ID.get(Integer.parseInt(iterator.next().toString()));
							    
				float[] resultDistance =  {-1.0f};
				
				try {
					android.location.Location.distanceBetween(
							location.getLatitude(),
							location.getLongitude(),
							zone.getLat(), 
							zone.getLon(), resultDistance);
				} catch (Exception e) {
					e.printStackTrace();
					resultDistance[0] = -1.0f;
				}
				if(resultDistance[0] >= 0){
					float distance = resultDistance[0];
					if(zoneId == 0){
						zoneId = zone.getId();
						actualDistance = distance;
					}
					else if(distance < actualDistance) {
						zoneId = zone.getId();
						actualDistance = distance;
					}
				}
			}
		} catch (Exception e) {
			return zoneId;
		}
		return zoneId;
	}
	
	@SuppressWarnings("unchecked")
	public static Location getLocationById(int locationId, Context context) {
		try {
			List<Location> listLocations = (List<Location>)
					Common.getObjFromInternalFile(context, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
//			List<Location> listLocations = Constant.LIST_ALL_LOCATION;
			
			if(null != listLocations && listLocations.size() > 0){
				for (int i = 0; i < listLocations.size(); i++) {
					if(listLocations.get(i).getId() == locationId){
						return listLocations.get(i);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static int countEventsUnread(Context context) {
		try {
			List<Event> listEvents = (List<Event>)
					Common.getObjFromInternalFile(context, Constant.FILE_CACHE_LIST_ALL_EVENTS);
			
			HashMap<Integer, Event> hashReadEvents = (HashMap<Integer, Event>)
					Common.getObjFromInternalFile(context, Constant.FILE_CACHE_LIST_READ_EVENTS);
			
			if(null != listEvents && listEvents.size() > 0){
				if(null != hashReadEvents && hashReadEvents.size() > 0){
					int count = 0;
					for (int i = 0; i < listEvents.size(); i++) {
						if(hashReadEvents.containsKey(listEvents.get(i).getId())){
							continue;
						}
						count++;
					}
					return count;
				}
				else {
					return listEvents.size();
				}
			}
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * CHeck user login
	 * 
	 * @param context
	 * @return true if user has already login
	 */
	public static boolean checkLogin(Context context) {
		
		String userName = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_USER_NAME);
		String password = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_PASSWORD);
		String authToken = SharePreference.getPreferenceByKeyString(context, Constant.KEY_LOGIN_AUTH_TOKEN);
		
		Log.e("CongVC", "===Set Login:" + userName + "==" + password + "===" + authToken);
		
		return (!userName.isEmpty() && !password.isEmpty() && !authToken.isEmpty());
		
	}
	
	public static void setLogin(Context context, String userName, String password, String authToken, String penName){
		
		Log.e("CongVC", "===Set Login:" + userName + "==" + password + "===" + authToken + "===" + penName);
		
		SharePreference.setPreferenceString(context, Constant.KEY_LOGIN_USER_NAME, userName);
		SharePreference.setPreferenceString(context, Constant.KEY_LOGIN_PASSWORD, password);
		SharePreference.setPreferenceString(context, Constant.KEY_LOGIN_AUTH_TOKEN, authToken);
		SharePreference.setPreferenceString(context, Constant.KEY_LOGIN_PEN_NAME, penName);
	}
	
	public static void clearLogin(Context context) {
		SharePreference.removePreferenceByKey(context, Constant.KEY_LOGIN_USER_NAME);
		SharePreference.removePreferenceByKey(context, Constant.KEY_LOGIN_PASSWORD);
		SharePreference.removePreferenceByKey(context, Constant.KEY_LOGIN_AUTH_TOKEN);
	}
	
	@SuppressWarnings({ "static-access", "deprecation" })
	public static void scaleAndSetImageBitmap(ImageView imgView, Bitmap bitmap, int width, int height) {
		try {
			
			int imgWidth = imgView.getWidth() > 0 ? imgView.getWidth() : width;
			int imgHeight = imgView.getHeight() > 0 ? imgView.getHeight() : height;
			
			float ratioW = (float)imgWidth/(float)bitmap.getWidth();
	        float ratioH = (float)imgHeight/(float)bitmap.getHeight();
	        float ratio = ratioW>ratioH ? ratioW : ratioH;
	        
	        Log.e("CongVC", "===imgView.getWidth():" + imgView.getWidth() + "==imgView.getHeight():" + imgView.getHeight());
	        Log.e("CongVC", "===ratioW:" + ratioW + "==ratioH:" + ratioH);
	        
	        bitmap = bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*ratio), (int)(bitmap.getHeight()*ratio), true);
	        bitmap = bitmap.createBitmap(bitmap, 0, 0, imgWidth, imgHeight);	
	        

	        BitmapDrawable ob = new BitmapDrawable(bitmap);
	        imgView.setBackgroundDrawable(ob);
	        imgView.setScaleType(ScaleType.FIT_START);
	        
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void loadImageView(final ImageView imgView, String url, 
									final int width, final int height, final Context context) {
		try {
			
//			imgView.setBackgroundResource(R.drawable.no_image);
			
			Response.Listener<Bitmap> listener = new Response.Listener<Bitmap>() {
			    @Override
			    public void onResponse(Bitmap bitmap) {
			    	Log.e("CongVC", "====Bitmap:" + bitmap);
			        scaleAndSetImageBitmap(imgView, bitmap, width, height);
			    }
			};
			
			ErrorListener eListener = new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError arg0) {
					
					Log.e("CongVC", "====Bitmap error:" + arg0.getMessage());
				}
			};
			
			ImageRequest imgRequest = new ImageRequest(
			        url,
			        listener,
			        width,
			        height,
			        Config.RGB_565,
//			        Config.ARGB_8888,
			        eListener);
			
//			PoemApplication.getInstance().getQueue().add(imgRequest);
			PoemApplication.getInstance().addToRequestQueue(imgRequest, context);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static final String HTTPS = "https://";
	private static final String HTTP = "http://";
	public static void openBrowser(final Context context, String url) {

	     try {
	    	 if (!url.startsWith(HTTP) && !url.startsWith(HTTPS)) {
		            url = HTTP + url;
		     }

		     Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		     context.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/*
	 * Read an action token file from the assets directory
	 */
	public static String getStringFromAssets(Context context, String tokenFileName) {
		String token = null;
		byte[] readBuffer = new byte[1024];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		int bytesRead = 0;

		try {
			is = context.getAssets()
			        .open(tokenFileName, Context.MODE_PRIVATE);
			while ((bytesRead = is.read(readBuffer)) != -1) {
				baos.write(readBuffer, 0, bytesRead);
			}
			baos.close();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		token = new String(baos.toByteArray());
		return token;
	}

}
