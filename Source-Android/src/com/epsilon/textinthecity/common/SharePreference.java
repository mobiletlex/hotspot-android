package com.epsilon.textinthecity.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * Class save and get share preferences
 * 
 * @author CongVo
 *
 */
public class SharePreference {
	
	/**
	 * Get value preference by key
	 * 
	 * @param context
	 * @param key
	 * @return String value
	 */
	public static String getPreferenceByKeyString(Context context, String key)
	{
		SharedPreferences mPrefer = PreferenceManager.getDefaultSharedPreferences(context);
		return mPrefer.getString(key, "");
	}
	
	/**
	 * Get value preference by key
	 * 
	 * @param context
	 * @param key
	 * @return integer value
	 */
	public static int getPreferenceByKeyInt(Context context,String key)
	{
		SharedPreferences mPrefer = PreferenceManager.getDefaultSharedPreferences(context);
		return mPrefer.getInt(key, 0);
	}
	
	/**
	 * Get value preference by key
	 * 
	 * @param context
	 * @param key
	 * @return integer value
	 */
	public static int getPreferenceByKeyInt(Context context,String key, int valueDef)
	{
		SharedPreferences mPrefer = PreferenceManager.getDefaultSharedPreferences(context);
		return mPrefer.getInt(key, valueDef);
	}
	
	/**
	 * Set value into preference by String value
	 * 
	 * @param context
	 * @param key
	 * @param value
	 */
	public  static void setPreferenceString( Context context, String key, String value)
	{
		SharedPreferences mPrefer =PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = mPrefer.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	/**
	 * Set value into preference by integer value
	 * 
	 * @param context
	 * @param key
	 * @param value
	 */
	public  static void setPreferenceInt( Context context, String key, int value)
	{
		SharedPreferences mPrefer =PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = mPrefer.edit();
		editor.putInt(key, value);
		editor.commit(); 
	}
	
	
	
	/**
	 * Clear all preference
	 * 
	 * @param context
	 */
	public static void  clearAllPreference(Context context){
		SharedPreferences mPrefer =PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = mPrefer.edit();
		editor.clear();
	    editor.commit();
	}

	/**
	 * remove preference by key
	 * @param context
	 * @param key
	 */
	public static void  removePreferenceByKey(Context context, String key){
		SharedPreferences mPrefer =PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = mPrefer.edit();
		editor.remove(key);
	    editor.commit();
	}
}
