package com.epsilon.textinthecity.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.epsilon.textinthecity.R;
import com.epsilon.textinthecity.object.Language;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.object.Zone;
import com.google.android.gms.maps.model.LatLng;


/**
 * Class create static variable using in application
 * 
 * @author CongVo
 *
 */
public class Constant {
	
	public static final boolean IS_DEBUG = false;
	
	public static final String BUG_SENSE_AP_KEY = "3e533e2e";
	
	public static final String REFIX_ID = "{id}";
	public static final String REFIX_PAGE = "{page}";
	public static final String REFIX_PER_PAGE = "{per_page}";
	public static final String REFIX_ZONE_ID = "{zone_id}";
	public static final String REFIX_ZONE_LAT = "{lat}";
	public static final String REFIX_ZONE_LON = "{lon}";
	public static final String REFIX_STATUS_ID = "{status_id}";
	public static final String REFIX_CATEGORY_ID = "{category_id}";
	public static final String REFIX_SORT_FIELD = "{sort_field}";
	public static final String REFIX_TITLE = "{title}";
	public static final String REFIX_FILTER_PARAM = "{filter_param}";
	public static final String REFIX_FILTER_VALUE = "{filter_value}";
	public static final String REFIX_KEY = "{key}";
	public static final String REFIX_LOCATION_ID = "{location_id}";
	/*** Config server  ***/
//	public static final String APP_SERVER = "https://textinthecity.sg"; // Release
	public static final String APP_SERVER = "http://textinthecity.epsilon-mobile.com"; // DEV
	public static final String APP_API_VERSION_SERVER = APP_SERVER + "/api/v2"; 
	public static final String API_ZONE_ALL = APP_API_VERSION_SERVER + "/zones";
	public static final String API_LOCATION_ALL = APP_API_VERSION_SERVER + "/locations";
	public static final String API_LOCATION_BY_ADMIN = APP_API_VERSION_SERVER + "/locations/featured";
	public static final String API_LOCATION_BY_ID = APP_API_VERSION_SERVER + "/locations/"+REFIX_ID;
	public static final String API_LOCATION_ADD = APP_API_VERSION_SERVER + "/locations";
	public static final String API_LOCATION_NEAR_BY = APP_API_VERSION_SERVER + "/locations/near_by?lat={lat}&lon={lon}";
	public static final String API_LOCATION_BY_ZONE = APP_API_VERSION_SERVER + "/zones/" + REFIX_ID+"/locations";
	public static final String API_POEM_RANDOM = APP_API_VERSION_SERVER + "/poems/random";
	public static final String API_POEM_DETAIL = APP_API_VERSION_SERVER + "/poems/" + REFIX_ID;
	public static final String API_POEM_BY_ZONE = APP_API_VERSION_SERVER + "/poems?page="+REFIX_PAGE+"&per_page="+REFIX_PER_PAGE+"&category_id="+REFIX_CATEGORY_ID+"&zone_id="+REFIX_ZONE_ID+"&sort=location";
	public static final String API_POEM_ALL = APP_API_VERSION_SERVER + "/poems?page="+REFIX_PAGE+"&per_page="+REFIX_PER_PAGE;
	public static final String API_POEM_CREATE = APP_API_VERSION_SERVER + "/poems";
	public static final String API_POEM_UPDATE = APP_API_VERSION_SERVER + "/poems/" + REFIX_ID;
	public static final String API_POEM_DELETE = APP_API_VERSION_SERVER + "/poems/" + REFIX_ID;
	public static final String API_POEM_LIKE = APP_API_VERSION_SERVER + "/poems/"+REFIX_ID+"/like";
	public static final String API_POEM_RATE = APP_API_VERSION_SERVER + "/poems/"+REFIX_ID+"/rate";
	public static final String API_POEM_REPORT = APP_API_VERSION_SERVER + "/poems/"+REFIX_ID+"/report";
	public static final String API_POEM_BY_LOCATION = APP_API_VERSION_SERVER + "/poems?location_id=" + REFIX_LOCATION_ID+"&category_id="+Constant.CATEGORY_ID_FEATURE+"&sort=created_at&order=desc";
	public static final String API_POEM_MY_ENTRIES = APP_API_VERSION_SERVER + "/poems/my_entries?status_id="+ REFIX_STATUS_ID;
	public static final String API_POEM_CATEGORY = APP_API_VERSION_SERVER + "/poems?page={page}&per_page={per_page}&status_id="+REFIX_STATUS_ID+"&category_id="+REFIX_CATEGORY_ID+"&sort=created_at&order=desc";
	public static final String API_POEM_CATEGORY_PUBLIC = APP_API_VERSION_SERVER + "/poems?page={page}&per_page={per_page}&status_id="+REFIX_STATUS_ID+"&sort=created_at&order=desc";
	public static final String API_POEM_CATEGORY_DEF = APP_API_VERSION_SERVER + "/poems?page=1&per_page=10&status_id="+1+"&category_id="+1+"&order=desc";
	public static final String API_POEM_CATEGORY_SORT = APP_API_VERSION_SERVER + "/poems?page={page}&per_page={per_page}&status_id="+REFIX_STATUS_ID+"&category_id="+REFIX_CATEGORY_ID+"&sort="+REFIX_SORT_FIELD+"&order=asc";
	public static final String API_POEM_CATEGORY_FILTER = APP_API_VERSION_SERVER + "/poems?page={page}&per_page={per_page}&status_id="+REFIX_STATUS_ID+"&category_id="+REFIX_CATEGORY_ID+"&sort="+REFIX_SORT_FIELD+"&"+REFIX_FILTER_PARAM+"="+REFIX_FILTER_VALUE+"&order=asc";
	public static final String API_POEM_CATEGORY_PUBLIC_SORT = APP_API_VERSION_SERVER + "/poems?page={page}&per_page={per_page}&status_id="+REFIX_STATUS_ID+"&sort="+REFIX_SORT_FIELD+"&order=asc";
	public static final String API_POEM_CATEGORY_PUBLIC_FILTER = APP_API_VERSION_SERVER + "/poems?page={page}&per_page={per_page}&status_id="+REFIX_STATUS_ID+"&sort="+REFIX_SORT_FIELD+"&"+REFIX_FILTER_PARAM+"="+REFIX_FILTER_VALUE+"&order=asc";
	public static final String API_POEM_SEARCH = APP_API_VERSION_SERVER + "/poems/search?page={page}&per_page={per_page}&title="+REFIX_TITLE;
	public static final String API_USER_LOGIN = APP_API_VERSION_SERVER + "/login";
	public static final String API_USER_REGISTER = APP_API_VERSION_SERVER + "/register";
	public static final String API_USER_RESET_PASSWORD = APP_API_VERSION_SERVER + "/request_reset_password";
	public static final String API_EVENTS = APP_API_VERSION_SERVER + "/events";
	public static final String API_LOCATION_BY_GOOGLE = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={lat},{lon}&radius=100&key={key}";
//	public static final String API_LOCATION_BY_GOOGLE = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key={key}";

	
	public static final String X_API_KEY = "##textinthecity##";
	public static final String X_API_KEY_ENCODE = "IyN0ZXh0aW50aGVjaXR5IyM=";
	
	public static final String URL_SETTING_TERM_CONDITION = APP_SERVER + "/terms";
	public static final String URL_SETTING_PRIVACY = APP_SERVER + "/terms";
	public static final String URL_SETTING_COMPETITION = APP_SERVER + "/competition";
	
	public static final String URL_GOOGLE_PLAY_STORE = "https://play.google.com/store/apps/details?id=com.epsilon.textinthecity";
	public static final String URL_APPLE_STORE = "https://itunes.apple.com/us/app/text-in-the-city/id917615665?ls=1&mt=8";
	
	public static final String LOGIN_ERROR_DEF = "ERROR";
	public static final String LOCATION_ADD_ERROR_DEF = "LOCATION_ADD_ERROR";
	
	public static final String POEM_GET_RANDOM_ERROR_DEF = "POEM_GET_RANDOM_ERROR";
	public static final String POEM_GET_DETAIL_ERROR_DEF = "POEM_GET_DETAIL_ERROR";
	public static final String POEM_ADD_ERROR_DEF = "POEM_ADD_ERROR";
	public static final String POEM_ADD_ENG_ERROR_DEF = "POEM_ADD_ENG_ERROR";
	public static final String POEM_UPDATE_ERROR_DEF = "POEM_UPDATE_ERROR";
	public static final String POEM_UPDATE_ENG_ERROR_DEF = "POEM_UPDATE_ENG_ERROR";
	public static final String REGISTER_ERROR_DEF = "REGISTER_ERROR";
	public static final String POEM_LIKE_ERROR_DEF = "POEM_LIKE_ERROR";
	public static final String POEM_LIKE_SUCCESS_DEF = "POEM_LIKE_SUCCESS";
	public static final String POEM_RATE_ERROR_DEF = "POEM_RATE_ERROR";
	public static final String POEM_RATE_SUCCESS_DEF = "POEM_RATE_SUCCESS";
	public static final String POEM_DRAFT_ERROR_DEF = "POEM_DRAFT_ERROR";
	public static final String POEM_SUBMISSION_ERROR_DEF = "POEM_SUBMISSION_ERROR";
	public static final String POEM_FEATURED_ERROR_DEF = "POEM_FEATURED_ERROR";
	public static final String POEM_PUBLIC_ERROR_DEF = "POEM_PUBLIC_ERROR";
	public static final String POEM_SEARCH_ERROR_DEF = "POEM_SEARCH_ERROR";
	public static final String POEM_REPORT_ERROR_DEF = "POEM_REPORT_ERROR";
	public static final String POEM_REPORT_SUCCESS_DEF = "POEM_REPORT_SUCCESS";
	public static final String POEM_DELETE_ERROR_DEF = "POEM_DELETE_ERROR";
	public static final String POEM_DELETE_SUCCESS_DEF = "POEM_DELETE_SUCCESS";
	public static final String POEM_SUBMISSION_DELETE_ERROR_DEF = "POEM_SUBMISSION_DELETE_ERROR";
	public static final String POEM_SUBMISSION_DELETE_SUCCESS_DEF = "POEM_SUBMISSION_DELETE_SUCCESS";
	public static final String LOCATION_GET_BY_ID_ERROR_DEF = "LOCATION_GET_BY_ID_ERROR";
	
	public static final String KEY_LOGIN_USER_NAME = "KEY_LOGIN_USER_NAME";
	public static final String KEY_LOGIN_PASSWORD = "KEY_LOGIN_PASSWORD";
	public static final String KEY_LOGIN_AUTH_TOKEN = "KEY_LOGIN_AUTH_TOKEN";
	public static final String KEY_LOGIN_PEN_NAME = "KEY_LOGIN_PEN_NAME";
	public static final String KEY_FONT_SETTING = "KEY_FONT_SETTING";
	
	// Setting font default
	public static final int SETTING_FONT_DEF = 16;
	
	// Length input text user
	public static final int LENGTH_INPUT_USER = 50;
	public static final int LENGTH_INPUT_EMAIL = 50;
	public static final int LENGTH_INPUT_POEM_TITLE = 120;
	public static final int LENGTH_INPUT_LOCATION_NAME = 100;
	public static final int LENGTH_INPUT_LOCATION = 250;

	// Set default number item poems when api return
	public static final int PER_PAGE_POEM_DEF = 10;
	
	// Status poem
	public static final int POEM_STATUS_PUBLISH = 1;
	public static final int POEM_STATUS_DRAFT = 2;
	
	// Category poem
	public static final int CATEGORY_ID_FEATURE = 1;
	public static final int CATEGORY_ID_PUBLIC = 2;
	public static final int CATEGORY_ID_SCHOOL = 3;
	
	
	/*** SETTING TAB MENU INDEX **/
	public static final int TAB_MENU_INDEX_POEMS = 0;
	public static final int TAB_MENU_INDEX_MY_ENTRIES = 1;
	public static final int TAB_MENU_INDEX_SUBMIT = 2;
	public static final int TAB_MENU_INDEX_EXPLORE = 3;
	public static final int TAB_MENU_INDEX_SETTING = 4;
	
	// File cache location admin
	public static final String FILE_CACHE_LIST_ALL_ADMIN_LOCATION = "list_all_admin_location.dat";
	public static final String FILE_CACHE_LIST_UNLOCK_LOCATION = "list_unlock_location.dat";
	
	// File cache events
	public static final String FILE_CACHE_LIST_ALL_EVENTS = "list_all_events.dat";
	public static final String FILE_CACHE_LIST_READ_EVENTS = "list_read_events.dat";
	
	public static final int DISTANCE_UNLOCK = 200;
	public static final long DISTANCE_TOO_FAR = 1000 * 10000;
	public static final int CONST_CROP_IMAGE_SIZE = 500;
	
	public static final LatLng THUYTRUC = new LatLng(10.8103172, 106.7088747);
	public static final LatLng BUIDINHTUY = new LatLng(10.808365, 106.709233);
	public static final LatLng CAUCHUVANAN = new LatLng(10.811135, 106.704154);
	
	/**
	 * Enum error from API server
	 */
	public static enum ErrorServer implements Serializable{
		NONE,
		ERROR_GENERAL,
		ERROR_SOCKET_TIMEOUT,
		ERROR_CONNECT_TIMEOUT,
		ERROR_NO_NETWORK
	}
	
	/**
	 * Enum sort field
	 */
	public static enum SortField implements Serializable{
		author,
		created_at,
		favourite,
		title,
		language,
		location,
		language_eng,
		language_china,
		language_malayu,
		language_tamil,
		zone_central,
		zone_east,
		zone_islands,
		zone_north,
		zone_north_east,
		zone_west,
		none
	}
	
//	public static final int ZONE_ID_TANBINH = 1;
//	public static final int ZONE_ID_BINHTHANH = 2;
	public static final int ZONE_ID_NORTH_EAST = 4;
	public static final int ZONE_ID_CENTRAL = 5;
	public static final int ZONE_ID_NORTH = 6;
	public static final int ZONE_ID_WEST = 7;
	public static final int ZONE_ID_EAST = 8;
	public static final int ZONE_ID_ISLANDS = 10;
	
	public static final HashMap<Integer, Zone> HASH_ZONES = new HashMap<Integer, Zone>();
	static {
//		HASH_ZONES.put(0, new Zone(ZONE_ID_TANBINH, "Tan Binh", 10.800216674805, 106.65538024902, "T"));
//		HASH_ZONES.put(0, new Zone(ZONE_ID_BINHTHANH, "Binh Thanh", 10.81031703949, 106.70887756348, "B"));
		HASH_ZONES.put(R.id.area5, new Zone(ZONE_ID_NORTH_EAST, "North - East", 1.376741, 103.951431, "NE"));
		HASH_ZONES.put(R.id.area1, new Zone(ZONE_ID_CENTRAL, "Central", 1.354539, 103.821152, "C"));
		HASH_ZONES.put(R.id.area3, new Zone(ZONE_ID_NORTH, "North", 1.434293, 103.857521, "N"));
		HASH_ZONES.put(R.id.area2, new Zone(ZONE_ID_WEST, "West", 1.386843, 103.677277, "W"));
		HASH_ZONES.put(R.id.area6, new Zone(ZONE_ID_EAST, "East", 1.361028, 103.977463, "E"));
		HASH_ZONES.put(R.id.area4, new Zone(ZONE_ID_ISLANDS, "Islands", 1.298591, 103.926490, "I"));
		HASH_ZONES.put(R.id.area7, new Zone(ZONE_ID_ISLANDS, "Islands", 1.298591, 103.926490, "I"));
		
	};
	
	public static final HashMap<Integer, Zone> HASH_ZONES_BY_ID = new HashMap<Integer, Zone>();
	static {
//		HASH_ZONES_BY_ID.put(ZONE_ID_TANBINH, new Zone(ZONE_ID_TANBINH, "Tan Binh", 10.800216674805, 106.65538024902, "T"));
//		HASH_ZONES_BY_ID.put(ZONE_ID_BINHTHANH, new Zone(ZONE_ID_BINHTHANH, "Binh Thanh", 10.81031703949, 106.70887756348, "B"));
		HASH_ZONES_BY_ID.put(ZONE_ID_NORTH_EAST, new Zone(ZONE_ID_NORTH_EAST, "North - East", 1.376741, 103.951431, "NE"));
		HASH_ZONES_BY_ID.put(ZONE_ID_CENTRAL, new Zone(ZONE_ID_CENTRAL, "Central", 1.354539, 103.821152, "C"));
		HASH_ZONES_BY_ID.put(ZONE_ID_NORTH, new Zone(ZONE_ID_NORTH, "North", 1.434293, 103.857521, "N"));
		HASH_ZONES_BY_ID.put(ZONE_ID_WEST, new Zone(ZONE_ID_WEST, "West", 1.386843, 103.677277, "W"));
		HASH_ZONES_BY_ID.put(ZONE_ID_EAST, new Zone(ZONE_ID_EAST, "East", 1.361028, 103.977463, "E"));
		HASH_ZONES_BY_ID.put(ZONE_ID_ISLANDS, new Zone(ZONE_ID_ISLANDS, "Islands", 1.298591, 103.926490, "I"));
		
	};
	
//	public static final HashMap<Integer, String> HASH_LOCATION_ZONENAME = new HashMap<Integer, String>();
		
	public static final int LANGUAGE_ID_ENG = 1;
	public static final int LANGUAGE_ID_CHINA = 2;
	public static final int LANGUAGE_ID_TAMIL = 3;
	public static final int LANGUAGE_ID_MALAY = 4;
	public static final HashMap<Integer, Language> HASH_LANGUAGES = new HashMap<Integer, Language>();
	static {
		HASH_LANGUAGES.put(LANGUAGE_ID_ENG, new Language(LANGUAGE_ID_ENG, "eng", "Eng"));
		HASH_LANGUAGES.put(LANGUAGE_ID_CHINA, new Language(LANGUAGE_ID_CHINA, "chi", "中文"));
		HASH_LANGUAGES.put(LANGUAGE_ID_MALAY, new Language(LANGUAGE_ID_MALAY, "may", "Melayu"));
		HASH_LANGUAGES.put(LANGUAGE_ID_TAMIL, new Language(LANGUAGE_ID_TAMIL, "tam", "Tamil"));
	}
	
//	public static HashMap<String, Location> HASH_UNLOCK_LOCATION = new HashMap<String, Location>();
//	public static List<Location> LIST_ALL_LOCATION = new ArrayList<Location>();
	
	
	
	public static List<Location> CreateListAllLocationsTest() {
		List<Location> listLocations = new ArrayList<Location>();
		
		Location location = new Location();
		location.setId(1);
		location.setLat(THUYTRUC.latitude);
		location.setLon(THUYTRUC.longitude);
		location.setName("Thuy Truc Cafe");
		location.setZone_id(11);
		listLocations.add(location);
		
		location = new Location();
		location.setId(2);
		location.setLat(BUIDINHTUY.latitude);
		location.setLon(BUIDINHTUY.longitude);
		location.setName("Bui Dinh Tuy Street");
		location.setZone_id(11);
		listLocations.add(location);
		
		location = new Location();
		location.setId(3);
		location.setLat(CAUCHUVANAN.latitude);
		location.setLon(CAUCHUVANAN.longitude);
		location.setName("Chu Van An Bridge");
		location.setZone_id(11);
		listLocations.add(location);
		
		return listLocations;
	}
	
//	Api key:IyN0ZXh0aW50aGVjaXR5IyM=
//	Auth token:5c90f187b97c6fb1223917a8c6b4e379f432d96f20cf7ab4507b60ae3bff58bf

	
	//Twitter
//	API key	Bpc5E4e9wHQpOeWteiqW8Mwa2
//	API secret	uLGC2QxdQzQhPU0t7ntMbpWhmKKPnjMkolX61p8VOYTqhX0VgV
}


