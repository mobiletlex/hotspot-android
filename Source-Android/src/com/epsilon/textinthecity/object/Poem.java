package com.epsilon.textinthecity.object;

import java.io.Serializable;

public class Poem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int location_id;
	private int status_id;
	private int category_id;
	private int language_id;
	private String title;
	private String content;
	private String title_alt;
	private String content_alt;
	private String audio;
	private String created_at;
	private String updated_at;
	private String audio_link;
	private int total_likes;
	private float average_rating;
	private String url;
	private String author;
	private boolean is_liked;
	private boolean is_reported;
	private Poet poet;
	private int zone_id;
	
	private PhotoPoem[] photos;
		
	public Poet getPoet() {
		return poet;
	}
	public void setPoet(Poet poet) {
		this.poet = poet;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getStatus_id() {
		return status_id;
	}
	public void setStatus_id(int status_id) {
		this.status_id = status_id;
	}
	public String getAudio_link() {
		return audio_link;
	}
	public void setAudio_link(String audio_link) {
		this.audio_link = audio_link;
	}
	public PhotoPoem[] getPhotos() {
		return photos;
	}
	public void setPhotos(PhotoPoem[] photos) {
		this.photos = photos;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLocation_id() {
		return location_id;
	}
	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getTotal_likes() {
		return total_likes;
	}
	public void setTotal_likes(int total_likes) {
		this.total_likes = total_likes;
	}
	public float getAverage_rating() {
		return average_rating;
	}
	public void setAverage_rating(float average_rating) {
		this.average_rating = average_rating;
	}
	public boolean isIs_liked() {
		return is_liked;
	}
	public void setIs_liked(boolean is_liked) {
		this.is_liked = is_liked;
	}
	public int getLanguage_id() {
		return language_id;
	}
	public void setLanguage_id(int language_id) {
		this.language_id = language_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTitle_alt() {
		return title_alt;
	}
	public void setTitle_alt(String title_alt) {
		this.title_alt = title_alt;
	}
	public String getContent_alt() {
		return content_alt;
	}
	public void setContent_alt(String content_alt) {
		this.content_alt = content_alt;
	}
	public String getAudio() {
		return audio;
	}
	public void setAudio(String audio) {
		this.audio = audio;
	}
	public int getZone_id() {
		return zone_id;
	}
	public void setZone_id(int zone_id) {
		this.zone_id = zone_id;
	}
	public boolean isIs_reported() {
		return is_reported;
	}
	public void setIs_reported(boolean is_reported) {
		this.is_reported = is_reported;
	}
	
}
