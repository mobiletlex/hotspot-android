package com.epsilon.textinthecity.object;

public class Language {

	private int id;
	private String code_iso639;
	private String name;
	
	public Language(int id, String code_iso639, String name) {
		
		this.id = id;
		this.code_iso639 = code_iso639;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode_iso639() {
		return code_iso639;
	}
	public void setCode_iso639(String code_iso639) {
		this.code_iso639 = code_iso639;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
