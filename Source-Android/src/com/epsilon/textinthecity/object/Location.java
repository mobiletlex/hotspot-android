package com.epsilon.textinthecity.object;

import java.io.Serializable;

public class Location implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int zone_id;
	private double lat;
	private double lon;
	private String name;
	private String address;
	private String fun_fact;
	private Photo[] photos;
	private long unlock_date;
	private boolean unlock_required;
	
	public long getUnlock_date() {
		return unlock_date;
	}
	public void setUnlock_date(long unlock_date) {
		this.unlock_date = unlock_date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getZone_id() {
		return zone_id;
	}
	public void setZone_id(int zone_id) {
		this.zone_id = zone_id;
	}
	public String getFun_fact() {
		return fun_fact;
	}
	public void setFun_fact(String fun_fact) {
		this.fun_fact = fun_fact;
	}
	public Photo[] getPhotos() {
		return photos;
	}
	public void setPhotos(Photo[] photos) {
		this.photos = photos;
	}
	public boolean isUnlock_required() {
		return unlock_required;
	}
	public void setUnlock_required(boolean unlock_required) {
		this.unlock_required = unlock_required;
	}
	
}
