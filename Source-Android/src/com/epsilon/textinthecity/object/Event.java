package com.epsilon.textinthecity.object;

import java.io.Serializable;

public class Event implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String title;
	private String content;
	private String short_content;
	private String link;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getShort_content() {
		return short_content;
	}
	public void setShort_content(String short_content) {
		this.short_content = short_content;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
//	public static List<Event> generateListEvents() {
//		
//		Event[] arrEvents = new Event[5];
//		for (int i = 0; i < 5; i++) {
//			Event event = new Event();
//			event.setId(i+1);
//			event.setTitle("The standard Lorem Ipsum passage, used since the 1500s");
//			event.setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
//			event.setLink("http://google.com.vn");
//			
//			arrEvents[i] = event;
//		}
//		return Arrays.asList(arrEvents);
//	}

}
