package com.epsilon.textinthecity.object;

import java.io.Serializable;

public class Zone implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private double lat;
	private double lon;
	private String refix;
	
	public Zone(int id, String name, double lat, double lon, String refix) {
		this.id = id;
		this.name = name;
		this.lat = lat;
		this.lon = lon;
		this.refix = refix;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getRefix() {
		return refix;
	}

	public void setRefix(String refix) {
		this.refix = refix;
	}
	
}
