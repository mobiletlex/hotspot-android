package com.epsilon.textinthecity.object;

import java.io.Serializable;

public class Poet implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String pen_name;
	private String bio;
	private PhotoPoet photo_links;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPen_name() {
		return pen_name;
	}
	public void setPen_name(String pen_name) {
		this.pen_name = pen_name;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	public PhotoPoet getPhoto_links() {
		return photo_links;
	}
	public void setPhoto_links(PhotoPoet photo_links) {
		this.photo_links = photo_links;
	}
	
}
