package com.epsilon.textinthecity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.object.PhotoPoet;
import com.epsilon.textinthecity.object.Poet;

public class PoetDialog extends Dialog {

	private Context mContext;
	private Poet mPoet;
	
	
	public PoetDialog(Context context, int theme, Poet poet) {
		super(context, theme);
		mContext = context;
		mPoet = poet;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.poet_dialog);
		
		
		Point point = new Point(0, 0);
        Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
        display.getSize(point);
   
        LayoutParams lp = new LayoutParams((int)(point.x*0.9f), (int)(point.x*0.9f*1.48f));
        lp.addRule(RelativeLayout.ABOVE, R.id.vBottom);
        lp.addRule(RelativeLayout.BELOW, R.id.vTop);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        lp.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        ((LinearLayout) findViewById(R.id.unlock_layout)).setLayoutParams(lp);
        
        LayoutParams lp1 = new LayoutParams((int)(point.x/3f), (int)(point.x/3f));
        ((ImageView) findViewById(R.id.imgBio)).setLayoutParams(lp1);
        ((ImageView) findViewById(R.id.imgBio)).setScaleType(ScaleType.FIT_XY);
//        LayoutParams lpPager = new LayoutParams((int)(point.x*0.9f), (int)(point.x*0.9f*0.63f));
//        ((ViewPager) findViewById(R.id.viewpager)).setLayoutParams(lpPager);
        
        int paddingCenterLayout = (point.x - (int)(point.x*0.95f))/2;
//        
        android.widget.LinearLayout.LayoutParams lpRlPager = 
        		new android.widget.LinearLayout.LayoutParams((int)(point.x*0.9f), 
        				android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
        ((RelativeLayout) findViewById(R.id.rlViewPager)).setLayoutParams(lpRlPager);
        ((RelativeLayout) findViewById(R.id.rlViewPager)).setPadding(paddingCenterLayout, 
													0, 
													paddingCenterLayout, 
													paddingCenterLayout/2);
        
        ((LinearLayout) findViewById(R.id.lnContent)).setPadding(paddingCenterLayout, 
													0, 
													paddingCenterLayout, 
													paddingCenterLayout/2);

        
        ((Button) findViewById(R.id.btnUnlockClose)).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PoetDialog.this.dismiss();
			}
		});
        
        ((TextView) findViewById(R.id.tvContent)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaOblique);
        
        ((TextView) findViewById(R.id.tvAuthorName)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
       
        ((TextView) findViewById(R.id.tvPoet)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
        
        String author = "";
        String content = "";
        PhotoPoet photoPoet = null;
        if(null != mPoet){
        	author = ! mPoet.getName().isEmpty() ? mPoet.getName() : mPoet.getPen_name();
        	content = mPoet.getBio();
        	photoPoet = mPoet.getPhoto_links();
        }
        
        ((TextView) findViewById(R.id.tvContent)).setText(content);
        ((TextView) findViewById(R.id.tvAuthorName)).setText(author);
        
        if(null != photoPoet && !photoPoet.getThumbnail().isEmpty()){
        	
        	Common.loadImageView(((ImageView) findViewById(R.id.imgBio)), 
        						photoPoet.getAvatar(), 
        						(int)(point.x/3f), (int)(point.x/3f), mContext);
        }
        
	}
	

}
