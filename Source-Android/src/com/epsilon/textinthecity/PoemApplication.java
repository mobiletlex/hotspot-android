package com.epsilon.textinthecity;

import java.io.File;
import java.util.HashMap;

import android.app.Activity;
import android.app.Application;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.epsilon.textinthecity.ArchitectViewHolderInterface.ILocationProvider;
import com.epsilon.textinthecity.common.ImageLruCache;
import com.epsilon.textinthecity.location.LocationProvider;
import com.epsilon.textinthecity.ssl._FakeX509TrustManager;
import com.facebook.SessionDefaultAudience;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

public class PoemApplication extends Application {
	
	/**
     * Log or request TAG
     */
    public static final String TAG_QUEUE = "VolleyTag";
	
	private ImageLruCache imageCache;
	private RequestQueue queue;
	private ImageLoader imageLoader;
	
	
	private static PoemApplication mInstance;
	
	/**
	 * last known location of the user, used internally for content-loading after user location was fetched
	 */
	protected Location 						lastKnownLocaton;
	
	// sample location strategy
	protected ILocationProvider	locationProvider;
	// location listener receives location updates and must forward them to the architectView
	protected LocationListener locationListener;
	
	public Typeface typeFace;
	public Typeface typeFaceFuturaCondensedExtraBold;
	public Typeface typeFaceFuturaCondensedMedium;
	public Typeface typeFaceFuturaMedium;
	public Typeface typeFaceFuturaMediumItalic;
	
	public Typeface typeFaceFuturaLight;
	public Typeface typeFaceFuturaBold;
	public Typeface typeFaceFuturaKoyuItalic;
	public Typeface typeFaceFuturaLightOblique;
	public Typeface typeFaceFuturaOblique;
	public Typeface typeFaceFUTURAB;
	public Typeface typeFaceFUTURABB;
	public Typeface typeFaceFUTURAEXTENDED;
	public Typeface typeFaceHelveticaiDesignVn_Bd;
	public Typeface typeFaceHelveticaiDesignVn_Lt;
	public Typeface typeFaceUfontsComFutura_book;
	
	private static final String APP_ID = "1601670256726348";//"579927505450217";//"211619498871712";//"728615400528729";
	private static final String APP_NAMESPACE = "textinthecity";//"textinthe_city";
	
	// The following line should be changed to include the correct property id.
	private static final String PROPERTY_ID = "UA-55451450-1"; // Release
//    private static final String PROPERTY_ID = "UA-55421420-1";//  Dev

    public static int GENERAL_TRACKER = 0;

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
    
    public PoemApplication() {
    	super();
    }
		
	@Override
	public void onCreate() {
		
		super.onCreate();
		
		if (mInstance == null) {
			mInstance = this;
		}
		
		typeFace = Typeface.createFromAsset(getAssets(),"fonts/Futura.ttc"); 
		typeFaceFuturaCondensedExtraBold = Typeface.createFromAsset(getAssets(),"fonts/Futura-CondensedExtraBold.ttf"); 
		typeFaceFuturaCondensedMedium = Typeface.createFromAsset(getAssets(),"fonts/Futura-CondensedMedium.ttf"); 
		typeFaceFuturaMedium = Typeface.createFromAsset(getAssets(),"fonts/Futura-Medium.ttf"); 
		typeFaceFuturaMediumItalic = Typeface.createFromAsset(getAssets(),"fonts/Futura-MediumItalic.ttf"); 
		
		typeFaceFuturaLight = Typeface.createFromAsset(getAssets(),"fonts/FUTURA (LIGHT).TTF"); 
		typeFaceFuturaBold = Typeface.createFromAsset(getAssets(),"fonts/Futura Bold.ttf"); 
		typeFaceFuturaKoyuItalic = Typeface.createFromAsset(getAssets(),"fonts/Futura Koyu Italic.ttf"); 
		typeFaceFuturaLightOblique = Typeface.createFromAsset(getAssets(),"fonts/Futura LightOblique.ttf"); 
		typeFaceFuturaOblique = Typeface.createFromAsset(getAssets(),"fonts/Futura Oblique.ttf"); 
		typeFaceFUTURAB = Typeface.createFromAsset(getAssets(),"fonts/FUTURAB.TTF"); 
		typeFaceFUTURABB = Typeface.createFromAsset(getAssets(),"fonts/FUTURABB.TTF"); 
		typeFaceFUTURAEXTENDED = Typeface.createFromAsset(getAssets(),"fonts/FUTURAEXTENDED.TTF"); 
		typeFaceHelveticaiDesignVn_Bd = Typeface.createFromAsset(getAssets(),"fonts/HelveticaiDesignVn-Bd.ttf"); 
		typeFaceHelveticaiDesignVn_Lt = Typeface.createFromAsset(getAssets(),"fonts/HelveticaiDesignVn-Lt.ttf"); 
		typeFaceUfontsComFutura_book = Typeface.createFromAsset(getAssets(),"fonts/ufonts.com_futura_book.ttf"); 
		
		imageCache = new ImageLruCache(ImageLruCache.getDefaultLruCacheSize());
//		queue =Volley.newRequestQueue(this);
		queue = newRequestQueue();
		imageLoader = new ImageLoader(queue, imageCache);
		
		processEventLocationChanged();
		
		this.locationProvider = new LocationProvider( this, this.locationListener );
		
		registerActivityLifecycleCallbacks(new MyLifecycleHandler());
		
//		List<Location> listLocations = (List<Location>)
//				Common.getObjFromInternalFile(this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
////		List<com.epsilon.textinthecity.object.Location> listLocations = Constant.LIST_ALL_LOCATION;
//		
//		if(null == listLocations || listLocations.size() <=0){
//		
//			PoemHelper.getInstance().register(this);
//			
//			// Get List all location for check unlock
//			new ListLocationByAdminAsyncTask(this).execute();
//		}
		
		// initialize facebook configuration
		Permission[] permissions = new Permission[] { 
				Permission.PUBLIC_PROFILE, 
				Permission.USER_GROUPS,
				Permission.USER_BIRTHDAY, 
				Permission.USER_LIKES, 
				Permission.USER_PHOTOS,
				Permission.USER_VIDEOS,
				Permission.USER_FRIENDS,
				Permission.USER_EVENTS,
				Permission.USER_VIDEOS,
				Permission.USER_RELATIONSHIPS,
				Permission.READ_STREAM, 
				Permission.PUBLISH_ACTION
				};

		SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
//			.setAppId(getString(R.string.app_face_id))
//			.setNamespace(getString(R.string.app_face_namespace))
			.setAppId(APP_ID)
			.setNamespace(APP_NAMESPACE)
			.setPermissions(permissions)
			.setDefaultAudience(SessionDefaultAudience.FRIENDS)
			.setAskForAllPermissionsAtOnce(false)
			.build();

		SimpleFacebook.setConfiguration(configuration);
				
	}
	
	public static synchronized PoemApplication getInstance() {
		return mInstance;
	}
	
	public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(
                            R.xml.global_tracker)
                            : analytics.newTracker(R.xml.ecommerce_tracker);
            t.enableAdvertisingIdCollection(true);
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }
		
	/**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (queue == null) {
        	queue = newRequestQueue();
        }

        return queue;
    }
	
	/**
	 * Used to return the singleton Image cache
	 * We do this so that if the same image is loaded 
	 * twice on two different activities, the cache still remains
	 * @return ImageLruCach
	 */
	public ImageLruCache getCache() {
		return imageCache;
	}
	/**
	 * Used to return the singleton RequestQueue
	 * @return RequestQueue
	 */
	public RequestQueue getQueue() {
		return queue;
	}
	/**
	 * Used to return the singleton imageloader 
	 * that utilizes the image lru cache. 
	 * @return ImageLoader
	 */
	public ImageLoader getImageLoader(){
		return imageLoader;
	}
	
	public class MyLifecycleHandler implements ActivityLifecycleCallbacks {
	    // I use two separate variables here. You can, of course, just use one and
	    // increment/decrement it instead of using two and incrementing both.
	    private int resumed;
	    private int stopped;
	    
	    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
	    }

	    public void onActivityDestroyed(Activity activity) {
	    }
	    
	    public void onActivityResumed(Activity activity) {
	        ++resumed;
	        if ( getInstance().locationProvider != null ) {
				getInstance().locationProvider.onResume();
			}
	    }
	    
	    public void onActivityPaused(Activity activity) {
	    	if ( getInstance().locationProvider != null ) {
				getInstance().locationProvider.onPause();
			}
	    }

	    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
	    }

	    public void onActivityStarted(Activity activity) {
	    }

	    public void onActivityStopped(Activity activity) {
	        ++stopped;
	        android.util.Log.w("test", "application is being backgrounded: " + (resumed == stopped));
	    }
	}

	/**
 	 * Process when location changed
 	 */
 	private void processEventLocationChanged() {
 		this.locationListener = new LocationListener() {

			@Override
			public void onStatusChanged( String provider, int status, Bundle extras ) {
			}

			@Override
			public void onProviderEnabled( String provider ) {
			}

			@Override
			public void onProviderDisabled( String provider ) {
			}

			@Override
			public void onLocationChanged( final Location location ) {
				if (location!=null) {
					lastKnownLocaton = location; 
				}
			}
		};
 	}
 	
// 	@Subscribe 
//	public void onAsyncTaskResult(AsyncTaskResultEvent event) {
//		
//		if(null != event.getListLocations() && event.getListLocations().size() > 0){
//			
//			final List<com.epsilon.textinthecity.object.Location> listLocations = event.getListLocations();
//			
//			Common.deleteFileFromInternal(this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
//			Common.writeFileToInternal(this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION, listLocations);
//
//		}
//	    
//	}
 	
 	// Default maximum disk usage in bytes
 	private static final int DEFAULT_DISK_USAGE_BYTES = 25 * 1024 * 1024;

 	// Default cache folder name
 	private static final String DEFAULT_CACHE_DIR = "textinthecity";

 	// Most code copied from "Volley.newRequestQueue(..)", we only changed cache directory
 	private RequestQueue newRequestQueue() {
 	    // define cache folder
 	    File rootCache = getExternalCacheDir();
 	    if (rootCache == null) {
 	        rootCache = getCacheDir();
 	    }

 	    File cacheDir = new File(rootCache, DEFAULT_CACHE_DIR);
 	    cacheDir.mkdirs();

 	    HttpStack stack = new HurlStack();
 	    Network network = new BasicNetwork(stack);
 	    DiskBasedCache diskBasedCache = new DiskBasedCache(cacheDir, DEFAULT_DISK_USAGE_BYTES);
 	    RequestQueue queue = new RequestQueue(diskBasedCache, network);
 	    queue.start();

 	    return queue;
 	}
 	
 	/**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     * 
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, Object tag) {
        // set the default tag if tag is empty
        req.setTag(null==tag ? TAG_QUEUE : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());
        
        _FakeX509TrustManager.allowAllSSL();

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     * 
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG_QUEUE);

        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     * 
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (getRequestQueue() != null) {
        	getRequestQueue().cancelAll(tag);
        }
    }
    
    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     * 
     * @param tag
     */
    public void cancelPendingRequests() {
        if (getRequestQueue() != null) {
        	getRequestQueue().cancelAll(TAG_QUEUE);
        	
        }
    }

}
