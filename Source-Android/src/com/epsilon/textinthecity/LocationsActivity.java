package com.epsilon.textinthecity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.VolleyError;
import com.costum.android.widget.LoadMoreListView;
import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.adapter.ListLocationByGoogleAdapter;
import com.epsilon.textinthecity.adapter.ListLocationNearByAdapter;
import com.epsilon.textinthecity.adapter.ListZoneAdapter;
import com.epsilon.textinthecity.api.LocationByGoogleApi;
import com.epsilon.textinthecity.api.LocationCreateNewApi;
import com.epsilon.textinthecity.api.LocationNearByApi;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.inter_face.LocationByGoogleLisener;
import com.epsilon.textinthecity.inter_face.LocationCreateNewLisener;
import com.epsilon.textinthecity.inter_face.LocationNearByLisener;
import com.epsilon.textinthecity.object.LocationMap;
import com.epsilon.textinthecity.object.Zone;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;

@SuppressLint("InflateParams")
public class LocationsActivity extends BaseActivity implements OnClickListener {
	
	public static final String KEY_LOCATION_ID = "KEY_LOCATION_ID";
	public static final String KEY_LOCATION_NAME = "KEY_LOCATION_NAME";
	public static final String KEY_LOCATION_ADDRESS = "KEY_LOCATION_ADDRESS";
	
	private ImageView imgLoading;
	
	private EditText edtSearch;
	private Button btnClose;
	private Button btnAdd;
	private LoadMoreListView lvLocation;
	private ListLocationNearByAdapter mAdapter;
	private List<LocationMap> mListLocations;
	
//	private PopupWindow mPopupWindow;
//	private View mViewPopup;
	private TextView tvPopupLocationName;
	private TextView tvPopupLocationAddress;
	private TextView tvPopupLocationFunFact;
	private TextView tvPopupLocationZone;
	private TextView edtPopupLocationZone;
	private EditText edtPopupLocationName;
	private EditText edtPopupLocationFunFact;
	private EditText edtPopupLocationAddress;
	private Button btnPopupDetectLocation;
	
	private PopupWindow mPopupWindowLocationGoogle;
	private View mViewPopupLocationGoogle;
	private TextView tvPopupLocationGoogleTitle;
	private LoadMoreListView lvLocationGoogle;
	private Button btnPopupLocationGoogleClose;
	private ListLocationByGoogleAdapter mAdapterLocationGoogle;
	
	private PopupWindow mPopupWindowZone;
	private View mViewPopupZone;
	private TextView tvPopupZoneTitle;
	private LoadMoreListView lvZone;
	private Button btnPopupZoneClose;
	private ListZoneAdapter mAdapterZone;
	
	private RelativeLayout rlAddLocation;
	private Button btnCancel;
	private Button btnDone;
	
	private int mZoneId = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_location), true);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SUBMIT);
		
		initControls();
		
		initPopup();
		
		setTypeFaceControls();
		
		setupWidthCenter();
		
		PoemHelper.getInstance().register(this);
		
		loadListLocation();
	}
	
	@Override
	protected void onDestroy() {
		try {
			PoemHelper.getInstance().unregister(this);
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onDestroy();
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		edtSearch = (EditText) findViewById(R.id.edtSearch);
		btnClose = (Button) findViewById(R.id.btnClose);
		btnAdd = (Button) findViewById(R.id.btnAddLocation);
		lvLocation = (LoadMoreListView) findViewById(R.id.listLocations);
		
		rlAddLocation = (RelativeLayout) findViewById(R.id.rlAddLocation);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnDone = (Button) findViewById(R.id.btnDone);
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		btnClose.setOnClickListener(this);
		btnAdd.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		btnDone.setOnClickListener(this);
		
		// Capture Text in EditText
		edtSearch.addTextChangedListener(new TextWatcher() {
 
            @Override
            public void afterTextChanged(Editable arg0) {
                if(null != mAdapter && null != mListLocations && mListLocations.size() > 0){
                	// TODO Auto-generated method stub
                    String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                    mAdapter.filter(text);
                }
            }
 
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                    int arg2, int arg3) {
                // TODO Auto-generated method stub
            }
 
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
                // TODO Auto-generated method stub
            }
        });
	}
	
	private void setTypeFaceControls() {
		edtSearch.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		btnAdd.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	/**
	 * Init view for popup
	 */
	private void initPopup() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 mViewPopupLocationGoogle = inflater.inflate(R.layout.popup_location_google, null, false);
		 
		 tvPopupLocationGoogleTitle = (TextView) mViewPopupLocationGoogle.findViewById(R.id.tvLocationGoogleTitle);
		 lvLocationGoogle = (LoadMoreListView) mViewPopupLocationGoogle.findViewById(R.id.listLocationsGoogle);
		 btnPopupLocationGoogleClose = (Button) mViewPopupLocationGoogle.findViewById(R.id.btnLocationGoogleClose);
		 
		 tvPopupLocationGoogleTitle.setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		 btnPopupLocationGoogleClose.setOnClickListener(this);
		 
		 
		 inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 mViewPopupZone = inflater.inflate(R.layout.popup_zone, null, false);
		 tvPopupZoneTitle = (TextView) mViewPopupZone.findViewById(R.id.tvLocationZoneTitle);
		 lvZone = (LoadMoreListView) mViewPopupZone.findViewById(R.id.listZone);
		 btnPopupZoneClose = (Button) mViewPopupZone.findViewById(R.id.btnZoneClose);
		 
		 tvPopupZoneTitle.setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		 btnPopupZoneClose.setOnClickListener(this);
		 
		 List<Zone> lstZone = new ArrayList<Zone>();
		 lstZone.addAll(Constant.HASH_ZONES_BY_ID.values());
		 mAdapterZone = new ListZoneAdapter(this, lstZone);
		 lvZone.setAdapter(mAdapterZone);
		 
		 
		 
		 tvPopupLocationName = (TextView) findViewById(R.id.tvLocationName);
		 tvPopupLocationAddress = (TextView) findViewById(R.id.tvLocationAddress);
		 tvPopupLocationFunFact = (TextView) findViewById(R.id.tvLocationFunFact);
		 tvPopupLocationZone = (TextView) findViewById(R.id.tvLocationZone);
		 edtPopupLocationZone = (TextView) findViewById(R.id.edtLocationZone);
		 edtPopupLocationName = (EditText) findViewById(R.id.edtLocationName);
		 edtPopupLocationAddress = (EditText) findViewById(R.id.edtLocationAddress);
		 edtPopupLocationFunFact = (EditText) findViewById(R.id.edtLocationFunFact);
		 btnPopupDetectLocation = (Button) findViewById(R.id.btnDetectLocation);
		
		
		 tvPopupLocationName.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvPopupLocationAddress.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvPopupLocationFunFact.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvPopupLocationZone.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 edtPopupLocationZone.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 edtPopupLocationName.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 edtPopupLocationAddress.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 edtPopupLocationFunFact.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 btnCancel.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 btnDone.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 edtPopupLocationZone.setOnClickListener(this);
		 btnPopupDetectLocation.setOnClickListener(this);
		 ((ImageView) findViewById(R.id.imgZoneList)).setOnClickListener(this);
		 
		 inputFilter();
	}
	
	/**
	 * Input filter controls
	 */
	private void inputFilter() {
		InputFilter[] iFilter = new InputFilter[1];
		// Filter input email
		iFilter[0] = new InputFilter.LengthFilter(Constant.LENGTH_INPUT_LOCATION_NAME);
		edtPopupLocationName.setFilters(iFilter);
		
		// Filter input password, first name, last name, pen name
		iFilter = new InputFilter[1];
		iFilter[0] = new InputFilter.LengthFilter(Constant.LENGTH_INPUT_LOCATION);
		edtPopupLocationAddress.setFilters(iFilter);
		edtPopupLocationFunFact.setFilters(iFilter);
	}
	
	/**
	 * Show popup
	 * @param view
	 */
	private void showPopupImage(View view) {
		mPopupWindowLocationGoogle = new PopupWindow(this);
		mPopupWindowLocationGoogle.setTouchable(true);
		mPopupWindowLocationGoogle.setFocusable(true);
		mPopupWindowLocationGoogle.setOutsideTouchable(true);
		mPopupWindowLocationGoogle.setTouchInterceptor(new OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	mPopupWindowLocationGoogle.dismiss();
	                return true;
	            }
	            return false;
	        }
	    });
		mPopupWindowLocationGoogle.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowLocationGoogle.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowLocationGoogle.setOutsideTouchable(false);
		mPopupWindowLocationGoogle.setContentView(mViewPopupLocationGoogle);
		mPopupWindowLocationGoogle.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_gray_alpha));
	    
		mPopupWindowLocationGoogle.showAsDropDown(view, 0, (int)(- findViewById(R.id.top_layout).getHeight()));
	}
	
	/**
	 * Show popup
	 * @param view
	 */
	private void showPopupZone(View view) {
		mPopupWindowZone = new PopupWindow(this);
		mPopupWindowZone.setTouchable(true);
		mPopupWindowZone.setFocusable(true);
		mPopupWindowZone.setOutsideTouchable(true);
		mPopupWindowZone.setTouchInterceptor(new OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	mPopupWindowZone.dismiss();
	                return true;
	            }
	            return false;
	        }
	    });
		mPopupWindowZone.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowZone.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
		mPopupWindowZone.setOutsideTouchable(false);
		mPopupWindowZone.setContentView(mViewPopupZone);
		mPopupWindowZone.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_gray_alpha));
	    
		mPopupWindowZone.showAsDropDown(view, 0, (int)(- findViewById(R.id.top_layout).getHeight()));
	}
	
	/**
	 * Load list poems from server
	 */
	private void loadListLocation() {
		
		if(null != PoemApplication.getInstance().lastKnownLocaton){
			
			Location local = PoemApplication.getInstance().lastKnownLocaton;
//			new ListLocationNearByAsyncTask(local.getLatitude(), local.getLongitude(), this).execute();
			new LocationNearByApi().getAllLocationsNearBy(this, 
										local.getLatitude(), 
										local.getLongitude(), mLocationNearByListener);
		}
		else {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		}
				
	}
	
	private LocationNearByLisener mLocationNearByListener = new LocationNearByLisener() {
		
		@Override
		public void requestStarted() {
			
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleted(List<LocationMap> listLocation) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			if(null != listLocation && listLocation.size() > 0){
	    		mListLocations = listLocation;
	    		mAdapter = new ListLocationNearByAdapter(LocationsActivity.this, mListLocations);
	    		lvLocation.setAdapter(mAdapter);
	    		
		    }
		}
	};
	
	private LocationCreateNewLisener mLocationCreateListener = new LocationCreateNewLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_location_add_fail)).show();
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_location_add_fail)).show();
		}
		
		@Override
		public void requestCompleted(LocationMap lLocation) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			if(null != lLocation){
				
				String labelTracker = lLocation.getName();
				if(null != lLocation.getAddress() && !lLocation.getAddress().equals("")){
					labelTracker += " - " + lLocation.getAddress();
				}
				
				Tracker t = PoemApplication.getInstance().getTracker(
		                TrackerName.APP_TRACKER);
		        t.send(new HitBuilders.EventBuilder()
		        	.setCategory(getString(R.string.google_analytics_event_category_location))
		        	.setAction(getString(R.string.google_analytics_event_action_submit))
		        	.setLabel(labelTracker)
		        	.build());
				
				Toast.makeText(LocationsActivity.this, getString(R.string.msg_location_add_successful), Toast.LENGTH_LONG).show();
				Intent it = getIntent();
				if(null == it){
					it = new Intent();
				}
				it.putExtra(LocationsActivity.KEY_LOCATION_ID, lLocation.getId());
				it.putExtra(LocationsActivity.KEY_LOCATION_NAME, lLocation.getName());
				it.putExtra(LocationsActivity.KEY_LOCATION_ADDRESS, lLocation.getAddress());
				setResult(RESULT_OK, it);
				finish();
			}
		}
	};
	
	private LocationByGoogleLisener mLocationByGoogleListener = new LocationByGoogleLisener() {
		
		@Override
		public void requestStarted() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
		}
		
		@Override
		public void requestCompleted(List<LocationMap> listLocation) {
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			if(null != listLocation && listLocation.size() > 0){
				mAdapterLocationGoogle = new ListLocationByGoogleAdapter(LocationsActivity.this, listLocation);
				lvLocationGoogle.setAdapter(mAdapterLocationGoogle);
				showPopupImage(findViewById(R.id.top_layout));
			}			
		}
	};
	
	public void setLocationGoogle(LocationMap location) {
		if(null != location){
			edtPopupLocationName.setText(location.getName());
			edtPopupLocationAddress.setText(location.getAddress());
		}
		mPopupWindowLocationGoogle.dismiss();
	}
	
	public void setZone(Zone zone) {
		if(null != zone){
			edtPopupLocationZone.setText(zone.getName());
			mZoneId = zone.getId();
		}
		mPopupWindowZone.dismiss();
	}
	
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {

		// Result for list location by zone
		if(null != event.getListLocationsMap()){
						
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			List<LocationMap> listLocation = event.getListLocationsMap();
			if(null != listLocation && listLocation.size() > 0){
	    		mListLocations = listLocation;
	    		mAdapter = new ListLocationNearByAdapter(this, mListLocations);
	    		lvLocation.setAdapter(mAdapter);
	    		
		    }
		}
		// Add new location fail
		else if(null != event.getResult() && event.getResult().equals(Constant.LOCATION_ADD_ERROR_DEF)){
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_location_add_fail)).show();
		}
		// add new location success
		else if(null != event.getLocationMap()){
			
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			
			Toast.makeText(this, getString(R.string.msg_location_add_successful), Toast.LENGTH_LONG).show();
			Intent it = getIntent();
			if(null == it){
				it = new Intent();
			}
			it.putExtra(LocationsActivity.KEY_LOCATION_ID, event.getLocationMap().getId());
			it.putExtra(LocationsActivity.KEY_LOCATION_NAME, event.getLocationMap().getName());
			it.putExtra(LocationsActivity.KEY_LOCATION_ADDRESS, event.getLocationMap().getAddress());
			setResult(RESULT_OK, it);
			finish();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnClose:
			edtSearch.setText("");
			break;
		case R.id.btnAddLocation:
//			showPopupImage(findViewById(R.id.btnAddLocation));
			if(null == PoemApplication.getInstance().lastKnownLocaton){
				createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_location_not_found)).show();
				return;
			}
			
			Location location = PoemApplication.getInstance().lastKnownLocaton;
			mZoneId = Common.getNearestZoneByLocation(location);
			if(mZoneId != 0){
				edtPopupLocationZone.setText(Constant.HASH_ZONES_BY_ID.get(mZoneId).getName());
			}
			else {
				edtPopupLocationZone.setText("");
			}
			rlAddLocation.setVisibility(View.VISIBLE);
			edtPopupLocationName.setText("");
			edtPopupLocationAddress.setText("");
			edtPopupLocationFunFact.setText("");
			break;
		case R.id.btnPopAdd:
			processAddLocation();
			break;
		case R.id.btnCancel:
			rlAddLocation.setVisibility(View.GONE);
			break;
		case R.id.btnDone:
			processAddLocation();
			break;
		case R.id.btnLocationGoogleClose:
			mPopupWindowLocationGoogle.dismiss();
			break;
		case R.id.btnZoneClose:
			mPopupWindowZone.dismiss();
			break;
		case R.id.edtLocationZone:
		case R.id.imgZoneList:
			showPopupZone(findViewById(R.id.top_layout));
			break;
		case R.id.btnDetectLocation:
			if(null == PoemApplication.getInstance().lastKnownLocaton){
				createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_location_not_found)).show();
				return;
			}
			
			Location location1 = PoemApplication.getInstance().lastKnownLocaton;
			((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
			new LocationByGoogleApi().getLocationsByGoogle(this, location1.getLatitude(), 
					location1.getLongitude(), mLocationByGoogleListener);
			break;
		default:
			break;
		}
	}
	
	private void processAddLocation() {
		if(edtPopupLocationName.getText().toString().trim().isEmpty()
				|| edtPopupLocationFunFact.getText().toString().trim().isEmpty()
				|| edtPopupLocationAddress.getText().toString().trim().isEmpty()){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_location_name_empty)).show();
			return;
		}
		
		if(null == PoemApplication.getInstance().lastKnownLocaton){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_location_not_found)).show();
			return;
		}
		
//		mPopupWindow.dismiss();
		((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.VISIBLE);
		
//		Api.postLocation(this, edtPopupLocationName.getText().toString().trim(), 
//				edtPopupLocationAddress.getText().toString().trim(), 
//				Common.getNearestZoneByLocation(PoemApplication.getInstance().lastKnownLocaton), 
//				edtPopupLocationFunFact.getText().toString().trim(), 
//				PoemApplication.getInstance().lastKnownLocaton.getLatitude(),
//				PoemApplication.getInstance().lastKnownLocaton.getLongitude());
		
		new LocationCreateNewApi().createNewLocation(this, edtPopupLocationName.getText().toString().trim(), 
						edtPopupLocationAddress.getText().toString().trim(), 
						mZoneId, 
						edtPopupLocationFunFact.getText().toString().trim(), 
						PoemApplication.getInstance().lastKnownLocaton.getLatitude(),
						PoemApplication.getInstance().lastKnownLocaton.getLongitude(),
						mLocationCreateListener);
	}

}
