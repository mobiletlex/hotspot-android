package com.epsilon.textinthecity;

import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.android.volley.VolleyError;
import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.api.LocationByAdminApi;
import com.epsilon.textinthecity.api.PoemByRandomApi;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.helper.PoemHelper;
import com.epsilon.textinthecity.inter_face.LocationByAdminLisener;
import com.epsilon.textinthecity.inter_face.PoemRandomLisener;
import com.epsilon.textinthecity.object.Location;
import com.epsilon.textinthecity.object.Poem;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class ExploreActivity extends BaseActivity {
	
	public static List<Poem> CURRENT_POEMS = new ArrayList<Poem>();
	public static long TIME_MOVE_TAB = System.currentTimeMillis();
	
	public static final String KEY_ZONE = "KEY_ZONE";
	public static final String KEY_POEM_OBJECT = "KEY_POEM_OBJECT";
//	private static final long TIME_COUNTDOWN = 10000;
	
	private ImageView imgLoading;
	private LinearLayout lnListItem;
	private ImageMap mImageMap;
	private List<Poem> mListPoems;
	private int mIndexPoem = 0;
	
	private CountDownTimer mCountDownTimer;
	
	private boolean isLoadPoemCache = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_explore);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_explore), false);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_EXPLORE);
		
		Tracker t = PoemApplication.getInstance().getTracker(
                TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
        	.setCategory(getString(R.string.google_analytics_event_category_tab_open))
        	.setAction(getString(R.string.google_analytics_event_action_open))
        	.setLabel(getString(R.string.google_analytics_event_open_tab_explore)).build());
	
		setupMapZone();
		
		initControls();
		
		initImageMap();
				
		PoemHelper.getInstance().register(this);
				
		// Get List all location for check unlock
//		new ListLocationByAdminAsyncTask(this).execute();
		new LocationByAdminApi().getAllLocationsByAdmin(this, mLocationByAdminListener);
		
		mIndexPoem = 0;
		mListPoems = null;
	}
	
	@Override
	protected void onResume() {
		
		super.onResume();
		
		if(null == mListPoems){
			// Get List poems for show to list
			loadListPoems();
		}
		long currentTime = System.currentTimeMillis();
		isLoadPoemCache = false;
		if(null != CURRENT_POEMS && CURRENT_POEMS.size() > 0 && 
				(currentTime - TIME_MOVE_TAB) <= 24*60*60*1000){
			isLoadPoemCache = true;

			lnListItem.removeAllViews();
			lnListItem.removeAllViewsInLayout();
			
			showListPoems();
		}
		else {
			CURRENT_POEMS = new ArrayList<Poem>();
		}
	}
	
	@Override
	protected void onPause() {
		
		super.onPause();
		
//		TIME_MOVE_TAB = System.currentTimeMillis();
		if(null != mCountDownTimer){
			mCountDownTimer.cancel();
		}
		
	}
	
	@Override 
	protected void onDestroy() {
		PoemHelper.getInstance().unregister(this);
	    super.onDestroy();
	    
	    if(null != mImageMap){
	    	try {
//	    		((ImageView) mImageMap).setImageDrawable(null);
//				((ImageView) mImageMap).setImageBitmap(null);
//				((ImageView) mImageMap).setBackgroundDrawable(null);
//				CleanUpUtil.recycle(mImageMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	mImageMap = null;
	    }
	  }
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		lnListItem = (LinearLayout) findViewById(R.id.lnListItem);
		
		((TextView) findViewById(R.id.tvFeaturedPoems)).setTypeface(
				PoemApplication.getInstance().typeFaceFuturaBold);
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		
	}
	
	private void initImageMap() {
		// find the image map in the view
        mImageMap = (ImageMap)findViewById(R.id.imgMapZone);
//	    mImageMap.setImageResource(R.drawable.map_zone);
        mImageMap.setImageResource(R.drawable.map_zone_1);
	    
	    // add a click handler to react when areas are tapped
        mImageMap.addOnImageMapClickedHandler(new ImageMap.OnImageMapClickedHandler()
        {
			@Override
			public void onImageMapClicked(int id, ImageMap imageMap)
			{
				// when the area is tapped, show the name in a 
				// text bubble
//				mImageMap.showBubble(id);
				
				Tracker t = PoemApplication.getInstance().getTracker(
		                TrackerName.APP_TRACKER);
		        t.send(new HitBuilders.EventBuilder()
		        	.setCategory(getString(R.string.google_analytics_event_category_zone))
		        	.setAction(getString(R.string.google_analytics_event_action_open))
		        	.setLabel(Constant.HASH_ZONES.get(id).getName()).build());
				
				Intent it = new Intent(ExploreActivity.this, ExploreZonemapActivity.class);
				it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				it.putExtra(KEY_ZONE, Constant.HASH_ZONES.get(id));
				startActivity(it);
				overridePendingTransition(0,0);
			}

			@Override
			public void onBubbleClicked(int id)
			{
				// react to info bubble for area being tapped
			}
		});
	}
	
	
	/**
	 * Set up width and height map image
	 */
	private void setupMapZone() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((LinearLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													paddingCenterLayout/2);
        
//        ((TextView) findViewById(R.id.tvFeaturedPoems)).setPadding(paddingCenterLayout, 
//															0, 
//															0, 
//															0);
        
        // Set width and height for image map
//        LayoutParams lp = new LayoutParams((int)(point.x*0.9f), (int)(point.x*0.9f*0.841f));
        LayoutParams lp = new LayoutParams((int)(point.x*0.905f), (int)(point.x*0.905f*0.696f));
        ((ImageMap) findViewById(R.id.imgMapZone)).setLayoutParams(lp);
         
	}
	
	/**
	 * Load list poems from server
	 */
	private void loadListPoems() {
		
		mListPoems = null;
		mIndexPoem = 0;
		
//		Api.getRandom(new ArrayList<NameValuePair>(), this);
		
		new PoemByRandomApi().getPoemByRandom(this, mPoemRandomListener);
				
//		new ListPoemsAsyncTask(this).execute();
	}
	
	private PoemRandomLisener mPoemRandomListener = new PoemRandomLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			processGetPoemRandomError();
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			processGetPoemRandomError();
		}
		
		@Override
		public void requestCompleted(List<Poem> listPoem) {
			
			if(null != listPoem){

			    mListPoems = listPoem;
			    
			    if(isLoadPoemCache){
			    	isLoadPoemCache = false;
			    	return;
			    }
			    	    
			    showListPoems();
			}
		}
	};
	
	private LocationByAdminLisener mLocationByAdminListener = new LocationByAdminLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
		}
		
		@Override
		public void requestCompleted(List<Location> listLocation) {
						
			if(null != listLocation && listLocation.size() > 0){
				Common.deleteFileFromInternal(ExploreActivity.this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
				Common.writeFileToInternal(ExploreActivity.this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION, listLocation);
				
//				if(null != listLocation && listLocation.size() >0){
//					for (int i = 0; i < listLocation.size(); i++) {
//						
//						if(Constant.HASH_LOCATION_ZONENAME.containsKey(listLocation.get(i).getId())){
//							continue;
//						}
//						try {
//							Constant.HASH_LOCATION_ZONENAME.put(listLocation.get(i).getId(), 
//									Constant.HASH_ZONES_BY_ID.get(listLocation.get(i).getZone_id()).getName());
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//				}
			}
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
		}
	};
	
	private void processGetPoemRandomError(){
		try {
			if(isLoadPoemCache){
				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
			}
			else {
				imgLoading.setVisibility(View.GONE);
				findViewById(R.id.imgRefresh).setVisibility(View.VISIBLE);
				findViewById(R.id.imgRefresh).setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(isNetworkAvailable()){
							if(null == mListPoems){
								// Get List poems for show to list
								loadListPoems();
							}
							
							imgLoading.setVisibility(View.VISIBLE);
							findViewById(R.id.imgRefresh).setVisibility(View.GONE);
						}
						else {
							Toast.makeText(ExploreActivity.this, getString(R.string.msg_error_no_network), 
									Toast.LENGTH_LONG).show();
						}
					}
				});
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	@Subscribe 
//	public void onAsyncTaskResult(AsyncTaskResultEvent event) {
//		
//		if(null != event.getListPoems()){
//
//		    mListPoems = event.getListPoems();
//		    
//		    if(isLoadPoemCache){
//		    	isLoadPoemCache = false;
//		    	return;
//		    }
//		    	    
//		    showListPoems();
//		}
//		else if(null != event.getListLocations() && event.getListLocations().size() > 0){
//			
//			final List<Location> listLocations = event.getListLocations();
//			
//			Common.deleteFileFromInternal(this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION);
//			Common.writeFileToInternal(this, Constant.FILE_CACHE_LIST_ALL_ADMIN_LOCATION, listLocations);
////			Constant.LIST_ALL_LOCATION = listLocations;
//			
//			
//			if(null != listLocations && listLocations.size() >0){
//				for (int i = 0; i < listLocations.size(); i++) {
//					if(Constant.HASH_LOCATION_ZONENAME.containsKey(listLocations.get(i).getId())){
//						continue;
//					}
//					
//					try {
//						Constant.HASH_LOCATION_ZONENAME.put(listLocations.get(i).getId(), 
//								Constant.HASH_ZONES_BY_ID.get(listLocations.get(i).getZone_id()).getName());
//					} catch (Exception e) {
//						// Zone id not in app
//					}
//				}
//			}
//		}
//		else if(null != event.getResult() && event.getResult().equals(Constant.POEM_GET_RANDOM_ERROR_DEF)){
//			if(isLoadPoemCache){
//				((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
//			}
//			else {
//				imgLoading.setVisibility(View.GONE);
//				findViewById(R.id.imgRefresh).setVisibility(View.VISIBLE);
//				findViewById(R.id.imgRefresh).setOnClickListener(new OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						if(isNetworkAvailable()){
//							if(null == mListPoems){
//								// Get List poems for show to list
//								loadListPoems();
//							}
//							
//							imgLoading.setVisibility(View.VISIBLE);
//							findViewById(R.id.imgRefresh).setVisibility(View.GONE);
//						}
//						else {
//							Toast.makeText(ExploreActivity.this, getString(R.string.msg_error_no_network), 
//									Toast.LENGTH_LONG).show();
//						}
//					}
//				});
//				
//			}
//		}
//	    
//	}
	
	@SuppressLint("InflateParams")
	private void showListPoems() {
		
		if(isLoadPoemCache && lnListItem.getChildCount() <= 0){
			mIndexPoem = 0;
			lnListItem.removeAllViews();
	    	lnListItem.removeAllViewsInLayout();
			for (int i = 0; i < CURRENT_POEMS.size(); i++) {
				
		    	Poem poem = CURRENT_POEMS.get(i);
	    		LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
				View view = inflater.inflate(R.layout.explore_item, null);
//				((TextView)view.findViewById(R.id.tvZoneName)).setText(
//										Common.getZoneNameByLocationId(poem.getLocation_id(), this));
				((TextView)view.findViewById(R.id.tvZoneName)).setText(
						Common.getZoneNameById(poem.getZone_id()));
				((TextView)view.findViewById(R.id.tvZoneName)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
				((TextView)view.findViewById(R.id.tvLandmark)).setText(poem.getAuthor());
				((TextView)view.findViewById(R.id.tvLandmark)).setTypeface(PoemApplication.getInstance().typeFaceFuturaLightOblique);
				((TextView)view.findViewById(R.id.tvPoemsName)).setTypeface(
																	PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
				((TextView)view.findViewById(R.id.tvPoemsName1)).setTypeface(
																		PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
				
				String title = poem.getTitle();
				String title1 = poem.getTitle_alt();				
				((TextView)view.findViewById(R.id.tvPoemsName)).setText(title);
				if(! title1.isEmpty() && poem.getLanguage_id() != Constant.LANGUAGE_ID_ENG){
					((TextView)view.findViewById(R.id.tvPoemsName1)).setText(title1);
				}
				else {
					((TextView)view.findViewById(R.id.tvPoemsName1)).setVisibility(View.GONE);
				}
				
				((View) view.findViewById(R.id.itemPoem)).setOnClickListener(new ItemPoemListener(poem));
				
				lnListItem.addView(view);
			}
		}
		else if(null != mListPoems && mListPoems.size() > 0) {
			TIME_MOVE_TAB = System.currentTimeMillis();
	    	lnListItem.removeAllViews();
	    	lnListItem.removeAllViewsInLayout();
	    	CURRENT_POEMS = new ArrayList<Poem>();
	    	
	    	if(mIndexPoem >= mListPoems.size()){
	    		mIndexPoem = 0;
	    	}
    		Poem poem = mListPoems.get(mIndexPoem);
    		CURRENT_POEMS.add(poem);
    		LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
			View view = inflater.inflate(R.layout.explore_item, null);
//			((TextView)view.findViewById(R.id.tvZoneName)).setText(
//									Common.getZoneNameByLocationId(poem.getLocation_id(), this));
			((TextView)view.findViewById(R.id.tvZoneName)).setText(
									Common.getZoneNameById(poem.getZone_id()));
			((TextView)view.findViewById(R.id.tvLandmark)).setText(poem.getAuthor());
			
			String title = poem.getTitle();
			String title1 = poem.getTitle_alt();	
			
			((TextView)view.findViewById(R.id.tvPoemsName)).setText(title);
			if(! title1.isEmpty() && poem.getLanguage_id() != Constant.LANGUAGE_ID_ENG){
				((TextView)view.findViewById(R.id.tvPoemsName1)).setText(title1);
			}
			else {
				((TextView)view.findViewById(R.id.tvPoemsName1)).setVisibility(View.GONE);
			}
			
			((TextView)view.findViewById(R.id.tvZoneName)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
			((TextView)view.findViewById(R.id.tvLandmark)).setTypeface(PoemApplication.getInstance().typeFaceFuturaLightOblique);
			((TextView)view.findViewById(R.id.tvPoemsName)).setTypeface(
																PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
			((TextView)view.findViewById(R.id.tvPoemsName1)).setTypeface(
					PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
			
			((View) view.findViewById(R.id.itemPoem)).setOnClickListener(new ItemPoemListener(poem));
			
			lnListItem.addView(view);
			playAnim(view, (Context) this, android.R.anim.slide_in_left, 0);
				
			mIndexPoem++;
			
			if(mIndexPoem >= mListPoems.size() && mListPoems.size() > 1){
	    		mIndexPoem = 0;
	    	}
	    	
	    	if (mIndexPoem < mListPoems.size()) {
	    		poem = mListPoems.get(mIndexPoem);
	    		CURRENT_POEMS.add(poem);
	    		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
				view = inflater.inflate(R.layout.explore_item, null);
//				((TextView)view.findViewById(R.id.tvZoneName)).setText(
//						Common.getZoneNameByLocationId(poem.getLocation_id(), this));
				((TextView)view.findViewById(R.id.tvZoneName)).setText(
											Common.getZoneNameById(poem.getZone_id()));
				((TextView)view.findViewById(R.id.tvLandmark)).setText(poem.getAuthor());
				
				title = poem.getTitle();
				title1 = poem.getTitle_alt();	
				
				((TextView)view.findViewById(R.id.tvPoemsName)).setText(title);
				if(! title1.isEmpty() && poem.getLanguage_id() != Constant.LANGUAGE_ID_ENG){
					((TextView)view.findViewById(R.id.tvPoemsName1)).setText(title1);
				}
				else {
					((TextView)view.findViewById(R.id.tvPoemsName1)).setVisibility(View.GONE);
				}
				
				((TextView)view.findViewById(R.id.tvZoneName)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
				((TextView)view.findViewById(R.id.tvLandmark)).setTypeface(PoemApplication.getInstance().typeFaceFuturaLightOblique);
				((TextView)view.findViewById(R.id.tvPoemsName)).setTypeface(
											PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
				((TextView)view.findViewById(R.id.tvPoemsName1)).setTypeface(
						PoemApplication.getInstance().typeFaceFuturaKoyuItalic);
				
				((View) view.findViewById(R.id.itemPoem)).setOnClickListener(new ItemPoemListener(poem));
				
				playAnim(view, (Context) this, android.R.anim.slide_in_left, 0);
				lnListItem.addView(view);
				
				mIndexPoem++;
	    	}
	    }
		((LinearLayout) findViewById(R.id.lnLoading)).setVisibility(View.GONE);
//		resetCountDownTimer();
	}
	
//	/**
//	 * Method process reset count down timer
//	 */
//	private void resetCountDownTimer(){
//		if(null != mCountDownTimer){
//			mCountDownTimer.cancel();
//		}
//		
//		// Init new count down timer 
//		mCountDownTimer = new CountDownTimer(TIME_COUNTDOWN, 1000) {
//			
//			@Override
//			public void onTick(long millisUntilFinished) {
//				
//			}
//			
//			@Override
//			public void onFinish() {
//				if(null != mListPoems && mListPoems.size() > 0){
//					showListPoems();
//				}
//				else {
//					loadListPoems();
//				}
//			}
//		}.start();
//	}
	
	private class ItemPoemListener implements OnClickListener {
		
		Poem poem = null;
		public ItemPoemListener(Poem poem) {
			this.poem = poem;
		}

		@Override
		public void onClick(View v) {
			Intent it = new Intent(ExploreActivity.this, PoemDetailActivity.class);
			it.putExtra(PoemDetailActivity.TAB_INDEX_POEM_DETAIL_KEY, Constant.TAB_MENU_INDEX_EXPLORE);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			it.putExtra(KEY_POEM_OBJECT, this.poem);
			startActivity(it);
			overridePendingTransition(0,0);
		}
		
	}

}
