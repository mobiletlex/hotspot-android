package com.epsilon.textinthecity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.VolleyError;
import com.epsilon.textinthecity.api.LoginApi;
import com.epsilon.textinthecity.api.ResetPasswordApi;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.inter_face.LoginLisener;
import com.epsilon.textinthecity.inter_face.ResetPasswordLisener;
import com.epsilon.textinthecity.object.User;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.squareup.otto.Subscribe;

@SuppressLint("InflateParams")
public class LoginActivity extends BaseActivity implements OnClickListener {
	
	public static final String TAB_KEY = "TAB_KEY";
	
	private PopupWindow mPopupWindow;
	private View mViewPopup;
	private TextView tvPopupTitle;
	private TextView tvPopupDescription;
	private TextView tvPopupEmail;
	private EditText edtPopupEmail;
	private Button btnPopupSubmit;
	private Button btnPopupClose;
	
	private int mPressLogin = 0;
	
	private int mTabIndex = Constant.TAB_MENU_INDEX_EXPLORE;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		mTabIndex = getIntent().getIntExtra(TAB_KEY, Constant.TAB_MENU_INDEX_EXPLORE);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_login), false);
		
		addTabMenuLayout(R.id.bottom_layout, mTabIndex);
		
		initControls();
		
		setTypeFaceControls();
		
		setupWidthCenter();
		
		initPopup();
		
//		PoemHelper.getInstance().register(this);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		mPressLogin = 0;
	}
	
	@Override
	protected void onDestroy() {
//		PoemHelper.getInstance().unregister(this);
		super.onDestroy();
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		final ImageView imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		((Button) findViewById(R.id.btnLogin)).setOnClickListener(this);
		((TextView) findViewById(R.id.tvSignUp)).setOnClickListener(this);
		((TextView) findViewById(R.id.tvLabelForgotPassword)).setOnClickListener(this);
	}
	
	private void setTypeFaceControls() {
		
		((TextView) findViewById(R.id.tvEmail)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvPass)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.edtEmail)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.edtPass)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		((TextView) findViewById(R.id.tvLabelForgotPassword)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvLabelCondition)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvLabelPlease)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvSignUp)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		
		((Button) findViewById(R.id.btnLogin)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	/**
	 * Init view for popup
	 */
	private void initPopup() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 mViewPopup = inflater.inflate(R.layout.popup_reset_password, null, false);
		 
		 tvPopupTitle = (TextView) mViewPopup.findViewById(R.id.tvPopResetPassTitle);
		 tvPopupDescription = (TextView) mViewPopup.findViewById(R.id.tvPopDescription);
		 tvPopupEmail = (TextView) mViewPopup.findViewById(R.id.tvPopEmail);
		 edtPopupEmail = (EditText) mViewPopup.findViewById(R.id.edtPopEmail);
		 btnPopupSubmit = (Button) mViewPopup.findViewById(R.id.btnPopSubmit);
		 btnPopupClose = (Button) mViewPopup.findViewById(R.id.btnResetPassClose);
		 
		 tvPopupTitle.setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		 tvPopupDescription.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvPopupEmail.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 btnPopupSubmit.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 edtPopupEmail.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 btnPopupSubmit.setOnClickListener(this);
		 btnPopupClose.setOnClickListener(this);
		 
	}
	
	/**
	 * Show popup
	 * @param view
	 */
	public void showPopupImage(View view) {
		mPopupWindow = new PopupWindow(this);
	    mPopupWindow.setTouchable(true);
	    mPopupWindow.setFocusable(true);
	    mPopupWindow.setOutsideTouchable(true);
	    mPopupWindow.setTouchInterceptor(new OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	mPopupWindow.dismiss();
	                return true;
	            }
	            return false;
	        }
	    });
	    mPopupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
	    mPopupWindow.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
	    mPopupWindow.setOutsideTouchable(false);
	    mPopupWindow.setContentView(mViewPopup);
	    mPopupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_gray_alpha));
	    
	    mPopupWindow.showAsDropDown(view, 0, (int)(- findViewById(R.id.top_layout).getHeight()));
	}
	
	private LoginLisener mLoginListener = new LoginLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_login_fail)).show();
    		
    		mPressLogin = 0;
			
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_login_fail)).show();
    		
    		mPressLogin = 0;
			
		}
		
		@Override
		public void requestCompleted(User user) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			
			if(null != user && mPressLogin == 1){
		    	
		    	mPressLogin = 0;
		    	
	    		Common.setLogin(LoginActivity.this, user.getEmail(), 
					    			user.getPassword(), 
					    			user.getAccess_token(),
					    			user.getPen_name());
	    		
//		    	Toast.makeText(this, getString(R.string.msg_login_successful), Toast.LENGTH_LONG).show();
				setResult(RESULT_OK);
				finish();
		    	
		    }
			
		}
	};
	
	private ResetPasswordLisener mResetPassListerner = new ResetPasswordLisener() {
		
		@Override
		public void requestStarted() {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.login_reset_pass_fail)).show();
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.login_reset_pass_fail)).show();
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.login_reset_pass_fail)).show();
		}
		
		@Override
		public void requestCompleted(String result) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			Toast.makeText(LoginActivity.this, getString(R.string.login_reset_pass_success), Toast.LENGTH_LONG).show();
		}
	};
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {

		findViewById(R.id.lnLoading).setVisibility(View.GONE);
		// Login successful
	    if(null != event.getResult() && !event.getResult().isEmpty()
	    		&& event.getResult().equals(Constant.LOGIN_ERROR_DEF)){
	    	
	    		createDialogMessage(getString(R.string.msg_error_title), 
						getString(R.string.msg_error_login_fail)).show();
	    		
	    		mPressLogin = 0;
	    }
	    else if(null != event.getUser() && mPressLogin == 1){
	    	
	    	mPressLogin = 0;
	    	
    		User user = event.getUser();
    		Common.setLogin(this, user.getEmail(), 
				    			user.getPassword(), 
				    			user.getAccess_token(),
				    			user.getPen_name());
    		
//	    	Toast.makeText(this, getString(R.string.msg_login_successful), Toast.LENGTH_LONG).show();
			setResult(RESULT_OK);
			finish();
	    	
	    }
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnLogin:
			processLogin();
			break;
		case R.id.tvSignUp:
			
			Intent it = new Intent(LoginActivity.this, TermsActivity.class);
			it.putExtra(TAB_KEY, mTabIndex);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivityForResult(it, 200);
			overridePendingTransition(0,0);
			break;
		case R.id.tvLabelForgotPassword:
			showPopupImage(findViewById(R.id.top_layout));
			break;
		case R.id.btnResetPassClose:
			mPopupWindow.dismiss();
			break;
		case R.id.btnPopSubmit:
			processResetPassword();
			break;	

		default:
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == 200 && resultCode == RESULT_OK){
			setResult(RESULT_OK);
			finish();
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		try {
			setResult(RESULT_CANCELED);
			finish();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void processLogin() {
		
		if(! isNetworkAvailable()){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_no_network)).show();
			return;
		}
		String username = ((TextView) findViewById(R.id.edtEmail)).getText().toString().trim();
		String password = ((TextView) findViewById(R.id.edtPass)).getText().toString().trim();
		// Validate input empty
		if(username.isEmpty()){
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_fill_email_empty))
				.show();
			((TextView) findViewById(R.id.edtEmail)).requestFocus();
			return;
		}
		
		// Validate input empty
		if(password.isEmpty()){
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_fill_password_empty))
				.show();
			((TextView) findViewById(R.id.edtPass)).requestFocus();
			return;
		}
		
		if(!checkEmailIsValid(username)){
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_email_invalid))
				.show();
			((TextView) findViewById(R.id.edtEmail)).requestFocus();
			return;
		}
		
		mPressLogin = 1;
		
//		Api.postLogin(this, username, password);
		new LoginApi().login(this, username, password, mLoginListener);
		findViewById(R.id.lnLoading).setVisibility(View.VISIBLE);

		
	}
	
	private void processResetPassword() {
		
		if(! isNetworkAvailable()){
			createDialogMessage(getString(R.string.msg_error_title), 
					getString(R.string.msg_error_no_network)).show();
			return;
		}
		
		if(edtPopupEmail.getText().toString().trim().isEmpty()){
			
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_fill_email_empty))
					.show();
			edtPopupEmail.requestFocus();
			
			return;
			
		}
		
		if(!checkEmailIsValid(edtPopupEmail.getText().toString().trim())){
			createDialogMessage(getString(R.string.msg_error_title), 
								getString(R.string.msg_error_email_invalid))
				.show();
			edtPopupEmail.requestFocus();
			return;
		}
		
		mPopupWindow.dismiss();
				
		new ResetPasswordApi().resetPassword(this, edtPopupEmail.getText().toString().trim(), mResetPassListerner);
		findViewById(R.id.lnLoading).setVisibility(View.VISIBLE);

		
	}
	

}
