package com.epsilon.textinthecity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.epsilon.textinthecity.common.Constant;

public class CreditsActivity extends BaseActivity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_credits);
		

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_credits), true);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SETTING);
			
		setTypeFaceControls();
		
		setupWidthCenter();
		
//		((TextView) findViewById(R.id.tvCredits1)).setText(Html.fromHtml(getString(R.string.credits_text)));
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	
	private void setTypeFaceControls() {
		
		((TextView) findViewById(R.id.tvCredits1)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvCredits2)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvCredits3)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvCredits4)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvCredits5)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvCredits11)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvCredits22)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvCredits33)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvCredits44)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvCredits55)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		
	}
	
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}
	
}
