package com.epsilon.textinthecity;

import java.util.List;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.widget.RelativeLayout;

import com.costum.android.widget.LoadMoreListView;
import com.epsilon.textinthecity.adapter.ListEventsAdapter;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.object.Event;

public class EventsActivity extends BaseActivity {
	
	private LoadMoreListView lvEvents;
	private ListEventsAdapter mAdapter;
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_events);
		

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_events), true);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SETTING);
					
		setupWidthCenter();
		
		lvEvents = (LoadMoreListView) findViewById(R.id.listEvents);
		
		List<Event> listEvents = (List<Event>)
				Common.getObjFromInternalFile(this, Constant.FILE_CACHE_LIST_ALL_EVENTS);
		if(null != listEvents && listEvents.size() > 0){
			mAdapter = new ListEventsAdapter(this, listEvents);
			lvEvents.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();
		}
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(null != mAdapter){
			mAdapter.notifyDataSetChanged();
		}
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}
	
}
