package com.epsilon.textinthecity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.epsilon.textinthecity.PoemApplication.TrackerName;
import com.epsilon.textinthecity.common.Common;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.Constant.SortField;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class PoemsActivity extends BaseFlagmentActivity implements OnClickListener {
	
	private PopupWindow mPopupWindow;
	private View mViewPopup;
	private TextView tvSortAuthor;
	private TextView tvSortYear;
	private TextView tvSortFavourite;
	private TextView tvSortTitle;
	private TextView tvSortLanguage;
	private TextView tvSortLocation;
	private TextView tvSortLanguageEng;
	private TextView tvSortLanguageChina;
	private TextView tvSortLanguageMalayu;
	private TextView tvSortLanguageTamil;
	private TextView tvSortZoneCentral;
	private TextView tvSortZoneEast;
	private TextView tvSortZoneIslands;
	private TextView tvSortZoneNorth;
	private TextView tvSortZoneNorthEast;
	private TextView tvSortZoneWest;
	
	private TextView tvFeatured;
	private TextView tvPublic;
	private LinearLayout lnSort;
	
	private ViewPager mPager;
	
	private SortField mCurrentItemSort = SortField.none;
	private int mFilter = 0;
	
	private PoemFeaturedFragment featuredFlagment = null;
	private PoemPublicFragment publicFlagment = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_poems);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_poems), false);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_POEMS);
		
		Tracker t = PoemApplication.getInstance().getTracker(
                TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
        	.setCategory(getString(R.string.google_analytics_event_category_tab_open))
        	.setAction(getString(R.string.google_analytics_event_action_open))
        	.setLabel(getString(R.string.google_analytics_event_open_tab_poem)).build());
		
		initControls();
        setupWidthCenter();
        
        initPopup();
        
        setupPager();
	}
	
	private void initControls(){
		tvFeatured = (TextView) findViewById(R.id.tvFeatured);
		tvPublic = (TextView) findViewById(R.id.tvPublic);
		lnSort = (LinearLayout) findViewById(R.id.lnSort);
		
		tvFeatured.setOnClickListener(this);
		tvPublic.setOnClickListener(this);
		lnSort.setOnClickListener(this);
		
		((Button) findViewById(R.id.btnEdit)).setVisibility(View.VISIBLE);
		((Button) findViewById(R.id.btnEdit)).setBackgroundResource(R.drawable.icon_search);
		((Button) findViewById(R.id.btnEdit)).setOnClickListener(this);
		
		tvFeatured.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		tvPublic.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvSortBy)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		
		tvFeatured.setTextColor(getResources().getColor(R.color.tab_white));

        mPager = (ViewPager)findViewById(R.id.pager);
       
	}
	
	/**
	 * Init view for popup
	 */
	@SuppressLint("InflateParams")
	private void initPopup() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 mViewPopup = inflater.inflate(R.layout.popup_sort, null, false);
		 
		 tvSortAuthor = (TextView) mViewPopup.findViewById(R.id.tvSortAuthor);
		 tvSortYear = (TextView) mViewPopup.findViewById(R.id.tvSortYear);
		 tvSortFavourite = (TextView) mViewPopup.findViewById(R.id.tvSortFavourite);
		 tvSortTitle = (TextView) mViewPopup.findViewById(R.id.tvSortTitle);
		 tvSortLanguage = (TextView) mViewPopup.findViewById(R.id.tvSortLanguage);
		 tvSortLocation = (TextView) mViewPopup.findViewById(R.id.tvSortLocation);
		 tvSortLanguageEng = (TextView) mViewPopup.findViewById(R.id.tvSortLanguageEng);
		 tvSortLanguageChina = (TextView) mViewPopup.findViewById(R.id.tvSortLanguageChina);
		 tvSortLanguageMalayu = (TextView) mViewPopup.findViewById(R.id.tvSortLanguageMalayu);
		 tvSortLanguageTamil = (TextView) mViewPopup.findViewById(R.id.tvSortLanguageTamil);
		 tvSortZoneCentral = (TextView) mViewPopup.findViewById(R.id.tvSortZoneCentral);
		 tvSortZoneEast = (TextView) mViewPopup.findViewById(R.id.tvSortZoneEast);
		 tvSortZoneIslands = (TextView) mViewPopup.findViewById(R.id.tvSortZoneIslands);
		 tvSortZoneNorth = (TextView) mViewPopup.findViewById(R.id.tvSortZoneNorth);
		 tvSortZoneNorthEast = (TextView) mViewPopup.findViewById(R.id.tvSortZoneNorthEast);
		 tvSortZoneWest = (TextView) mViewPopup.findViewById(R.id.tvSortZoneWest);
		 
		 
		 tvSortAuthor.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortYear.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortFavourite.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortTitle.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortLanguage.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortLocation.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortLanguageEng.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortLanguageChina.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortLanguageMalayu.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortLanguageTamil.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortZoneCentral.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortZoneEast.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortZoneIslands.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortZoneNorth.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortZoneNorthEast.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 tvSortZoneWest.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		 
		 tvSortAuthor.setOnClickListener(new EventClickSortItem());
		 tvSortYear.setOnClickListener(new EventClickSortItem());
		 tvSortFavourite.setOnClickListener(new EventClickSortItem());
		 tvSortTitle.setOnClickListener(new EventClickSortItem());
		 tvSortLanguage.setOnClickListener(new EventClickSortItem());
		 tvSortLocation.setOnClickListener(new EventClickSortItem());
		 tvSortLanguageEng.setOnClickListener(new EventClickSortItem());
		 tvSortLanguageChina.setOnClickListener(new EventClickSortItem());
		 tvSortLanguageMalayu.setOnClickListener(new EventClickSortItem());
		 tvSortLanguageTamil.setOnClickListener(new EventClickSortItem());
		 tvSortZoneCentral.setOnClickListener(new EventClickSortItem());
		 tvSortZoneWest.setOnClickListener(new EventClickSortItem());
		 tvSortZoneEast.setOnClickListener(new EventClickSortItem());
		 tvSortZoneIslands.setOnClickListener(new EventClickSortItem());
		 tvSortZoneNorth.setOnClickListener(new EventClickSortItem());
		 tvSortZoneNorthEast.setOnClickListener(new EventClickSortItem());
		 
		 if(!Common.checkLogin(this)){
			 tvSortFavourite.setVisibility(View.GONE);
			 mViewPopup.findViewById(R.id.vLine3).setVisibility(View.GONE);
		 }
		 
		 tvSortYear.setVisibility(View.GONE);
		 mViewPopup.findViewById(R.id.vLine2).setVisibility(View.GONE);
	}
	
	private void setupPager() {
		FragmentPagerAdapter adapter = new GoogleMusicAdapter(getSupportFragmentManager());
		mPager.setAdapter(adapter);
	        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int position) {
					tvFeatured.setTextColor(position==0 ? 
								getResources().getColor(R.color.tab_white) : 
								getResources().getColor(R.color.black_congrat));
					
					tvPublic.setTextColor(position==1 ? 
							getResources().getColor(R.color.tab_white) : 
							getResources().getColor(R.color.black_congrat));
					
				}
				
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}
				
				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
			});
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        mPager.setPadding(paddingCenterLayout, 
							0, 
							paddingCenterLayout, 
							0);
         
	}
	
	/**
	 * Show popup
	 * @param view
	 */
	public void showPopup(View view) {
		mPopupWindow = new PopupWindow(this);
	    mPopupWindow.setTouchable(true);
	    mPopupWindow.setFocusable(true);
	    mPopupWindow.setOutsideTouchable(true);
	    mPopupWindow.setTouchInterceptor(new OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	mPopupWindow.dismiss();
	                return true;
	            }
	            return false;
	        }
	    });
	    mPopupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
	    mPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
	    mPopupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_popup));
	    mPopupWindow.setOutsideTouchable(false);
	    mPopupWindow.setContentView(mViewPopup);
	    
	    mPopupWindow.showAsDropDown(view);
	}
	
	class GoogleMusicAdapter extends FragmentPagerAdapter {
		
        public GoogleMusicAdapter(FragmentManager fm) {
            super(fm);
            if(null == featuredFlagment){
    			featuredFlagment = PoemFeaturedFragment.newInstance();
    		}
            if(null == publicFlagment){
    			publicFlagment = PoemPublicFragment.newInstance();
    		}
        }

        @Override
        public Fragment getItem(int position) {
        	if(position == 0){
        		
        		return featuredFlagment;
        	}
        	else {
        		
        		return publicFlagment;
        	}
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        @Override
        public int getCount() {
          return 2;
        }
    }
	
	private class EventClickSortItem implements OnClickListener{

		@Override
		public void onClick(View v) {
			SortField sortField = SortField.none;
			int filter = 0;
			tvSortAuthor.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortYear.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortFavourite.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortTitle.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortLanguage.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortLocation.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortLanguageEng.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortLanguageChina.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortLanguageMalayu.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortLanguageTamil.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortZoneCentral.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortZoneEast.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortZoneIslands.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortZoneNorth.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortZoneNorthEast.setTextColor(getResources().getColor(R.color.tab_white));
			tvSortZoneWest.setTextColor(getResources().getColor(R.color.tab_white));
			
			if(v.getId() == tvSortAuthor.getId()){
				sortField = SortField.author;
				filter = 0;
				tvSortAuthor.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortYear.getId()){
				sortField = SortField.created_at;
				filter = 0;
				tvSortYear.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortFavourite.getId()){
				sortField = SortField.favourite;
				filter = 0;
				tvSortFavourite.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortTitle.getId()){
				sortField = SortField.title;
				filter = 0;
				tvSortTitle.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortLanguage.getId()){
				sortField = SortField.language;
				filter = 0;
				tvSortLanguage.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortLocation.getId()){
				sortField = SortField.location;
				filter = 0;
				tvSortLocation.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortLanguageEng.getId()){
				sortField = SortField.language;
				filter = Constant.LANGUAGE_ID_ENG;
				tvSortLanguageEng.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortLanguageChina.getId()){
				sortField = SortField.language;
				filter = Constant.LANGUAGE_ID_CHINA;
				tvSortLanguageChina.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortLanguageMalayu.getId()){
				sortField = SortField.language;
				filter = Constant.LANGUAGE_ID_MALAY;
				tvSortLanguageMalayu.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortLanguageTamil.getId()){
				sortField = SortField.language;
				filter = Constant.LANGUAGE_ID_TAMIL;
				tvSortLanguageTamil.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortZoneCentral.getId()){
//				sortField = SortField.location;
				sortField = SortField.title;
				filter = Constant.ZONE_ID_CENTRAL;
				tvSortZoneCentral.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortZoneEast.getId()){
//				sortField = SortField.location;
				sortField = SortField.title;
				filter = Constant.ZONE_ID_EAST;
				tvSortZoneEast.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortZoneIslands.getId()){
//				sortField = SortField.location;
				sortField = SortField.title;
				filter = Constant.ZONE_ID_ISLANDS;
				tvSortZoneIslands.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortZoneNorth.getId()){
//				sortField = SortField.location;
				sortField = SortField.title;
				filter = Constant.ZONE_ID_NORTH;
				tvSortZoneNorth.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortZoneNorthEast.getId()){
//				sortField = SortField.location;
				sortField = SortField.title;
				filter = Constant.ZONE_ID_NORTH_EAST;
				tvSortZoneNorthEast.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(v.getId() == tvSortZoneWest.getId()){
//				sortField = SortField.location;
				sortField = SortField.title;
				filter = Constant.ZONE_ID_WEST;
				tvSortZoneWest.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			
			if(mCurrentItemSort != sortField || mFilter != filter){
				mCurrentItemSort = sortField;
				mFilter = filter;
				if(mPager.getCurrentItem() == 0){
					featuredFlagment.setmCurrentItemSort(mCurrentItemSort);
					featuredFlagment.setmFilter(mFilter);
					featuredFlagment.sortPoems();
				}
				else {
					publicFlagment.setmCurrentItemSort(mCurrentItemSort);
					publicFlagment.setmFilter(mFilter);
					publicFlagment.sortPoems();
				}
			}
			
			mPopupWindow.dismiss();
		}
		
	}
	
	private void updatePopup() {
		tvSortAuthor.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortYear.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortFavourite.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortTitle.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortLanguage.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortLocation.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortLanguageEng.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortLanguageChina.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortLanguageMalayu.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortLanguageTamil.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortZoneCentral.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortZoneEast.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortZoneIslands.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortZoneNorth.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortZoneNorthEast.setTextColor(getResources().getColor(R.color.tab_white));
		tvSortZoneWest.setTextColor(getResources().getColor(R.color.tab_white));
		
		if(mCurrentItemSort == SortField.author){
			tvSortAuthor.setTextColor(getResources().getColor(R.color.black_congrat));
		}
		else if(mCurrentItemSort == SortField.created_at){
			tvSortYear.setTextColor(getResources().getColor(R.color.black_congrat));
		}
		else if(mCurrentItemSort == SortField.favourite){
			tvSortFavourite.setTextColor(getResources().getColor(R.color.black_congrat));
		}
		else if(mCurrentItemSort == SortField.language){
			if(mFilter == Constant.LANGUAGE_ID_ENG) {
				tvSortLanguageEng.setTextColor(getResources().getColor(R.color.black_congrat));
			} 
			else if(mFilter == Constant.LANGUAGE_ID_CHINA) {
				tvSortLanguageChina.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.LANGUAGE_ID_MALAY) {
				tvSortLanguageMalayu.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.LANGUAGE_ID_TAMIL) {
				tvSortLanguageTamil.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else{
				tvSortLanguage.setTextColor(getResources().getColor(R.color.black_congrat));
			}
		}
		else if(mCurrentItemSort == SortField.title){
			if(mFilter == Constant.ZONE_ID_CENTRAL) {
				tvSortZoneCentral.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_EAST) {
				tvSortZoneEast.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_ISLANDS) {
				tvSortZoneIslands.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_NORTH) {
				tvSortZoneNorth.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_NORTH_EAST) {
				tvSortZoneNorthEast.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_WEST) {
				tvSortZoneWest.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else {
				tvSortTitle.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			
		}
		else if(mCurrentItemSort == SortField.location){
			if(mFilter == Constant.ZONE_ID_CENTRAL) {
				tvSortZoneCentral.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_EAST) {
				tvSortZoneEast.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_ISLANDS) {
				tvSortZoneIslands.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_NORTH) {
				tvSortZoneNorth.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_NORTH_EAST) {
				tvSortZoneNorthEast.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else if(mFilter == Constant.ZONE_ID_WEST) {
				tvSortZoneWest.setTextColor(getResources().getColor(R.color.black_congrat));
			}
			else {
				tvSortLocation.setTextColor(getResources().getColor(R.color.black_congrat));
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvFeatured:
			
			mPager.setCurrentItem(0, true);
			
			tvFeatured.setTextColor(getResources().getColor(R.color.tab_white));
			tvPublic.setTextColor(getResources().getColor(R.color.black_congrat));
			break;
			
		case R.id.tvPublic:
			
			mPager.setCurrentItem(1, true);
			
			tvFeatured.setTextColor(getResources().getColor(R.color.black_congrat));
			tvPublic.setTextColor(getResources().getColor(R.color.tab_white));
			break;
			
		case R.id.btnEdit:
			Intent it = new Intent(PoemsActivity.this, SearchActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(it);
			overridePendingTransition(0,0);
			
			break;
			
		case R.id.lnSort:
			if(mPager.getCurrentItem() == 0 && featuredFlagment.checkFinishLoading()){
				showPopup(findViewById(R.id.lnSort));
				mCurrentItemSort = featuredFlagment.getmCurrentItemSort();
				mFilter = featuredFlagment.getmFilter();
				updatePopup();
			}
			else if(mPager.getCurrentItem() == 1 && publicFlagment.checkFinishLoading()){
				showPopup(findViewById(R.id.lnSort));
				mCurrentItemSort = publicFlagment.getmCurrentItemSort();
				mFilter = publicFlagment.getmFilter();
				updatePopup();
			}
			break;
			

		default:
			break;
		}
	}

}
