package com.epsilon.textinthecity;

import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.android.volley.VolleyError;
import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.epsilon.textinthecity.adapter.ListPoemPublicAdapter;
import com.epsilon.textinthecity.api.PoemPublicApi;
import com.epsilon.textinthecity.common.CleanUpUtil;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.Constant.SortField;
import com.epsilon.textinthecity.inter_face.PoemPublicLisener;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.object.PoemPagging;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.squareup.otto.Subscribe;

public final class PoemPublicFragment extends Fragment {
    
    private LoadMoreListView lvPoems;
	private ImageView imgLoading;
	private LinearLayout lnLoading;
	private ListPoemPublicAdapter mAdapter;
	private List<Poem> mListPoems;
	
	private int mCurrentPage = 0;
	private int mLastPage = 0;
	
	private boolean isFirst = true;
	
	private SortField mCurrentItemSort = SortField.none;
	private int mFilter = 0;
	
	private View mView;
	

    public SortField getmCurrentItemSort() {
		return mCurrentItemSort;
	}


	public void setmCurrentItemSort(SortField mCurrentItemSort) {
		this.mCurrentItemSort = mCurrentItemSort;
	}
	
	public int getmFilter() {
		return mFilter;
	}


	public void setmFilter(int mFilter) {
		this.mFilter = mFilter;
	}
	
	public boolean checkFinishLoading() {
		if(null != lnLoading && lnLoading.getVisibility() == View.GONE){
			return true;
		}
		return false;
	}
	

    public static PoemPublicFragment newInstance() {
        PoemPublicFragment fragment = new PoemPublicFragment();

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      
//        PoemHelper.getInstance().register(this);
        
        isFirst = true;
        
        if(null == mListPoems){
        	loadListPoems();
        }
    }
    
    @Override
    public void onDestroy() {
//    	PoemHelper.getInstance().unregister(this);
    	super.onDestroy();
    	
    	try {
			CleanUpUtil.cleanupView(mView);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	PoemApplication.getInstance().cancelPendingRequests(this);
    }

    @SuppressLint("InflateParams")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
    	View view = inflater.inflate(R.layout.poem_draft_view, null);
        	
        	lvPoems = (LoadMoreListView) view.findViewById(R.id.listPoems);
        	lnLoading = (LinearLayout) view.findViewById(R.id.lnLoading);
        	imgLoading = (ImageView) view.findViewById(R.id.imgLoading);
    		imgLoading.post(new Runnable() {
    			    @Override
    			    public void run() {
    			        try {
    			        	AnimationDrawable frameAnimation =
    				            (AnimationDrawable) imgLoading.getBackground();
    				        frameAnimation.start();
    					} catch (Exception e) {
    						e.printStackTrace();
    					}
    			    }
    			}
    		);
    		
    		if(null != mListPoems){
    			lnLoading.setVisibility(View.GONE);
    			processShowListPoems(mListPoems);
    		}
    		else if(!isFirst){
    			lnLoading.setVisibility(View.GONE);
    		}
    		
    		isFirst = false;
    	
    		mView = view;
        return view;
    }
    
    /**
	 * Load list poems from server
	 */
	private void loadListPoems() {
		
//		new ListPoemsPublicAsyncTask(getActivity(), 
//								mCurrentPage, 
//								Constant.PER_PAGE_POEM_DEF, 
//								mCurrentItemSort,
//								mFilter).execute();
		new PoemPublicApi().getAllPoemsPublic(getActivity(), 
								mCurrentPage, 
								Constant.PER_PAGE_POEM_DEF, 
								mCurrentItemSort,
								mFilter, mPoemPublicListener,
								this);
	}
	
	private PoemPublicLisener mPoemPublicListener = new PoemPublicLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			lnLoading.setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			lnLoading.setVisibility(View.GONE);
		}
		
		@Override
		public void requestCompleted(PoemPagging poemPagging) {
			lnLoading.setVisibility(View.GONE);
			if(null != poemPagging){
				processShowListPoems(poemPagging);
			}
		}
	};
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {
			    
		// Result for get list poems draft error
		if(null != event.getResult() && event.getResult().equals(Constant.POEM_PUBLIC_ERROR_DEF)){
	    		   
			lnLoading.setVisibility(View.GONE);
		}
		else if(null != event.getListPoemsPublic()){
			lnLoading.setVisibility(View.GONE);
			processShowListPoems(event.getListPoemsPublic());
		}
	}
	
	/**
	 * Process show list poems ti list view
	 */
	private void processShowListPoems(PoemPagging listPoemsPagging) {
		
		mCurrentPage = listPoemsPagging.getCurrent_page();
		mLastPage = listPoemsPagging.getLast_page();
		
		List<Poem> listPoemsTemp = listPoemsPagging.getData();
		if(null != listPoemsTemp && listPoemsTemp.size() > 0){
	    	if(null == mListPoems){
	    		mListPoems = listPoemsTemp;
	    		mAdapter = new ListPoemPublicAdapter(getActivity(), mListPoems);
	    		lvPoems.setAdapter(mAdapter);
	    		
	    		lvPoems.setOnLoadMoreListener(new OnLoadMoreListener() {
	    			public void onLoadMore() {
	    				if(mCurrentPage < mLastPage){
//	    					new ListPoemsPublicAsyncTask(getActivity(), 
//	    											mCurrentPage+1, 
//	    											Constant.PER_PAGE_POEM_DEF, 
//	    											mCurrentItemSort,
//	    											mFilter).execute();
	    					new PoemPublicApi().getAllPoemsPublic(getActivity(), 
									mCurrentPage+1, 
									Constant.PER_PAGE_POEM_DEF, 
									mCurrentItemSort,
									mFilter, mPoemPublicListener,
									PoemPublicFragment.this);
	    				}
	    				else {
	    					// Call onLoadMoreComplete when the LoadMore task, has finished
	    					lvPoems.onLoadMoreComplete();
	    				}
	    			}
	    		});
	    	}
	    	else {
	    		for (int i = 0; i < listPoemsTemp.size(); i++){
	    			mListPoems.add(listPoemsTemp.get(i));
//					mAdapter.addItem(listPoemsTemp.get(i));
				}
	    		mAdapter.notifyDataSetChanged();

				// Call onLoadMoreComplete when the LoadMore task, has finished
				lvPoems.onLoadMoreComplete();

	    	}
	    }
	}
	
	/**
	 * Process show list poems ti list view
	 */
	private void processShowListPoems(List<Poem> listPoemsTemp) {
		
		
		mListPoems = listPoemsTemp;
		mAdapter = new ListPoemPublicAdapter(getActivity(), mListPoems);
		lvPoems.setAdapter(mAdapter);
		
		lvPoems.setOnLoadMoreListener(new OnLoadMoreListener() {
			public void onLoadMore() {
				if(mCurrentPage < mLastPage){
//					new ListPoemsPublicAsyncTask(getActivity(), 
//											mCurrentPage+1, 
//											Constant.PER_PAGE_POEM_DEF, 
//											mCurrentItemSort,
//											mFilter).execute();
					new PoemPublicApi().getAllPoemsPublic(getActivity(), 
							mCurrentPage+1, 
							Constant.PER_PAGE_POEM_DEF, 
							mCurrentItemSort,
							mFilter, mPoemPublicListener,
							PoemPublicFragment.this);
				}
				else {
					// Call onLoadMoreComplete when the LoadMore task, has finished
					lvPoems.onLoadMoreComplete();
				}
			}
		});
	}

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    
    public void sortPoems() {
    	mListPoems = null;
    	mCurrentPage = 0;
    	mLastPage = 0;
    	mAdapter = new ListPoemPublicAdapter(getActivity(), new ArrayList<Poem>());
    	
    	lnLoading.setVisibility(View.VISIBLE);
//    	new ListPoemsPublicAsyncTask(getActivity(), 
//    							mCurrentPage, 
//    							Constant.PER_PAGE_POEM_DEF, 
//    							mCurrentItemSort, mFilter).execute();
    	new PoemPublicApi().getAllPoemsPublic(getActivity(), 
				mCurrentPage, 
				Constant.PER_PAGE_POEM_DEF, 
				mCurrentItemSort,
				mFilter, mPoemPublicListener,
				this);
    	if(null != mAdapter){
    		lvPoems.setAdapter(mAdapter);
    		mAdapter.notifyDataSetChanged();
    	}
    }
}
