package com.epsilon.textinthecity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.epsilon.textinthecity.adapter.ListPoemSearchAdapter;
import com.epsilon.textinthecity.api.PoemSearchApi;
import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.inter_face.PoemSearchLisener;
import com.epsilon.textinthecity.object.Poem;
import com.epsilon.textinthecity.object.PoemPagging;
import com.epsilon.textinthecity.task.taskevent.AsyncTaskResultEvent;
import com.squareup.otto.Subscribe;

public class SearchActivity extends BaseActivity implements OnClickListener {
		
	private ImageView imgLoading;
	
	private EditText edtSearch;
	private Button btnClose;
	private LoadMoreListView lvPoemSearch;
	private ListPoemSearchAdapter mAdapter;
	private List<Poem> mListPoem;
	
	private int mCurrentPage = 0;
	private int mLastPage = 0;
	
	private String mSearchText = "";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_search), true);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_POEMS);
		
		initControls();
				
		setTypeFaceControls();
		
		setupWidthCenter();
		
		
//		PoemHelper.getInstance().register(this);
		
	}
	
	@Override
	protected void onDestroy() {
//		try {
//			PoemHelper.getInstance().unregister(this);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		try {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

	        if (imm != null) {
	            imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onBackPressed();
	}
	
	/**
	 * Init controls from xml file
	 */
	private void initControls(){
		
		edtSearch = (EditText) findViewById(R.id.edtSearch);
		btnClose = (Button) findViewById(R.id.btnClose);
		lvPoemSearch = (LoadMoreListView) findViewById(R.id.listPoemSearch);
		
		imgLoading = (ImageView)findViewById(R.id.imgLoading);
		imgLoading.post(new Runnable() {
			    @Override
			    public void run() {
			        try {
			        	AnimationDrawable frameAnimation =
				            (AnimationDrawable) imgLoading.getBackground();
				        frameAnimation.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
			    }
			}
		);
		
		btnClose.setOnClickListener(this);
		
		edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) { 
					
					if(!edtSearch.getText().toString().trim().isEmpty()){
						
						mSearchText = edtSearch.getText().toString().trim();
						
						mCurrentPage = 0;
						mLastPage = 0;
						mListPoem = null;
						
						mAdapter = new ListPoemSearchAdapter(SearchActivity.this, new ArrayList<Poem>());
						lvPoemSearch.setAdapter(mAdapter);
						if(null != mAdapter){
							mAdapter.notifyDataSetChanged();
						}
						
						
						if(isNetworkAvailable()){
							findViewById(R.id.lnLoading).setVisibility(View.VISIBLE);
							((TextView) findViewById(R.id.tvError)).setVisibility(View.GONE);
//							new ListPoemsSearchAsyncTask(SearchActivity.this, 
//									mCurrentPage, 
//									Constant.PER_PAGE_POEM_DEF, 
//									mSearchText).execute();
							new PoemSearchApi().getAllPoemsSearch(SearchActivity.this, 
									mCurrentPage, 
									Constant.PER_PAGE_POEM_DEF, 
									mSearchText, mPoemSearchListener);
						}
						else {
							createDialogMessage(getString(R.string.msg_error_title), 
									getString(R.string.msg_error_no_network)).show();
						}
						
						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
						return true;
					}
					
				}
				return false;
			}
		});
//		InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//		inputMethodManager.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);
		
	}
	
	private void setTypeFaceControls() {
		edtSearch.setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
		((TextView) findViewById(R.id.tvError)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.9f))/2;
        ((RelativeLayout) findViewById(R.id.center_layout)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
         
	}
		
	private PoemSearchLisener mPoemSearchListener = new PoemSearchLisener() {
		
		@Override
		public void requestStarted() {
		}
		
		@Override
		public void requestEndedWithError(VolleyError error) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			((TextView) findViewById(R.id.tvError)).setVisibility(View.VISIBLE);
		}
		
		@Override
		public void requestCompleteWithErrorParseJson(String errorMsg) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			((TextView) findViewById(R.id.tvError)).setVisibility(View.VISIBLE);
		}
		
		@Override
		public void requestCompleted(PoemPagging poemPagging) {
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			if(null != poemPagging){
				processShowListPoems(poemPagging);
			}
		}
	};
	
	@Subscribe 
	public void onAsyncTaskResult(AsyncTaskResultEvent event) {

		// Result for get list poems draft error
		if(null != event.getResult() && event.getResult().equals(Constant.POEM_SEARCH_ERROR_DEF)){
	    		   
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			((TextView) findViewById(R.id.tvError)).setVisibility(View.VISIBLE);
		}
		else if(null != event.getListPoemSearch()){
			findViewById(R.id.lnLoading).setVisibility(View.GONE);
			processShowListPoems(event.getListPoemSearch());
		}
	}
	
	/**
	 * Process show list poems ti list view
	 */
	private void processShowListPoems(PoemPagging listPoemsPagging) {
		
		mCurrentPage = listPoemsPagging.getCurrent_page();
		mLastPage = listPoemsPagging.getLast_page();
		
		List<Poem> listPoemsTemp = listPoemsPagging.getData();
		if(null != listPoemsTemp && listPoemsTemp.size() > 0){
	    	if(null == mListPoem){
	    		mListPoem = listPoemsTemp;
	    		mAdapter = new ListPoemSearchAdapter(this, mListPoem);
	    		lvPoemSearch.setAdapter(mAdapter);
	    		
	    		lvPoemSearch.setOnLoadMoreListener(new OnLoadMoreListener() {
	    			public void onLoadMore() {
	    				if(mCurrentPage < mLastPage){
//	    					new ListPoemsSearchAsyncTask(SearchActivity.this, 
//	    											mCurrentPage+1, 
//	    											Constant.PER_PAGE_POEM_DEF, 
//	    											mSearchText).execute();
	    					
	    					new PoemSearchApi().getAllPoemsSearch(SearchActivity.this, 
	    											mCurrentPage+1, 
	    											Constant.PER_PAGE_POEM_DEF, 
	    											mSearchText, mPoemSearchListener);
	    				}
	    				else {
	    					// Call onLoadMoreComplete when the LoadMore task, has finished
	    					lvPoemSearch.onLoadMoreComplete();
	    				}
	    			}
	    		});
	    	}
	    	else {
	    		for (int i = 0; i < listPoemsTemp.size(); i++){
	    			mListPoem.add(listPoemsTemp.get(i));
//					mAdapter.addItem(listPoemsTemp.get(i));
				}
	    		mAdapter.notifyDataSetChanged();

				// Call onLoadMoreComplete when the LoadMore task, has finished
				lvPoemSearch.onLoadMoreComplete();

	    	}
	    }
		else {
			((TextView) findViewById(R.id.tvError)).setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnClose:
			edtSearch.setText("");
			break;

		default:
			break;
		}
	}
	
}
