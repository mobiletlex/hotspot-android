package com.epsilon.textinthecity;

import com.epsilon.textinthecity.common.Constant;
import com.epsilon.textinthecity.common.SharePreference;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class SettingFontActivity extends BaseActivity implements OnClickListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_font);

		addTitleLayout(R.id.top_layout, getString(R.string.screen_title_setting), true);
		
		addTabMenuLayout(R.id.bottom_layout, Constant.TAB_MENU_INDEX_SETTING);
		
		setupWidthCenter();
		
		setTypeFaceControls();
		
		initData();
	}
	
	/**
	 * Set up width and height map image
	 */
	private void setupWidthCenter() {
		
		Point point = new Point(0, 0);
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        
        // Set padding for center layout
        int paddingCenterLayout = (point.x - (int)(point.x*0.95f));
        ((LinearLayout) findViewById(R.id.lnFont)).setPadding(paddingCenterLayout, 
        													0, 
        													paddingCenterLayout, 
        													0);
        
        ((LinearLayout) findViewById(R.id.lnContentPreview)).setPadding(paddingCenterLayout, 
				0, 
				paddingCenterLayout, 
				0);
        
        ((LinearLayout) findViewById(R.id.lnPlayPreview)).setPadding(paddingCenterLayout, 
				0, 
				paddingCenterLayout, 
				0);
         
	}
	
	/**
	 * Set typeFace for text
	 */
	private void setTypeFaceControls() {
		
		((TextView) findViewById(R.id.tvFont)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvPreview)).setTypeface(PoemApplication.getInstance().typeFaceFuturaBold);
		((TextView) findViewById(R.id.tvContentPreview)).setTypeface(PoemApplication.getInstance().typeFaceFuturaOblique);
	}
	
	/**
	 * Init event click controls
	 */
	private void initData() {
		int seekValue = SharePreference.getPreferenceByKeyInt(this, Constant.KEY_FONT_SETTING, 50);
				
		((SeekBar) findViewById(R.id.skbFont)).setProgress(seekValue);
		((TextView) findViewById(R.id.tvContentPreview)).setTextSize(Constant.SETTING_FONT_DEF + (seekValue-50)/5);
		
		((SeekBar) findViewById(R.id.skbFont)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				SharePreference.setPreferenceInt(SettingFontActivity.this, Constant.KEY_FONT_SETTING, progress);
				
//				((TextView) findViewById(R.id.tvContentPreview)).setTextSize((progress*Constant.SETTING_FONT_DEF)/50);
				((TextView) findViewById(R.id.tvContentPreview)).setTextSize(Constant.SETTING_FONT_DEF + (progress-50)/5);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		

		default:
			break;
		}
	}

}

